#ifndef SIMULATEUR_H
#define SIMULATEUR_H

class Simulateur;//Forward Declaration
#include "../Processus/processus.h"
#include "../Processus/ProcessusManager.h"
#include "../Ordonnanceur/processeur.h"
#include <vector>

class Simulateur
{
private:
    Simulateur();
    std::vector<Processeur*> mProcesseur;
    static int& mCurrentTime();
    static void incrementTime(){mCurrentTime()++;}
    static Simulateur** mInstance(){static Simulateur* _mInstance = 0;return &_mInstance;}

public:
    static Simulateur* getInstance();
    static int getCurrentTime(){return mCurrentTime();}
    Simulateur& addProcessor(Processeur* inProcesseur);
    void run();
};

#endif // SIMULATEUR_H
