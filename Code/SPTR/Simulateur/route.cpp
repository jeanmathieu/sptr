#include "route.h"

Route::Route(unsigned int ID,unsigned int xDebut,unsigned int yDebut,
      unsigned int xFin,unsigned int yFin, unsigned int vitesse)
{
    mID = ID;
    mXDebut = xDebut;
    mYDebut = yDebut;
    mXFin = xFin;
    mYFin = yFin;
    mVitesse = vitesse;
}

Route& Route::operator=(const Route &in){	
	if (this != &in)
	{
    	mID=in.mID;
    	mXDebut = in.mXDebut;
    	mYDebut = in.mYDebut;
    	mXFin = in.mXFin;
    	mYFin = in.mYFin;
    	mVitesse = in.mVitesse;
	}
	return *this;
}
