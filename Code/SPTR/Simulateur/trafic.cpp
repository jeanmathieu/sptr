#include "Trafic.h"

Trafic::Trafic(unsigned int id,unsigned int xdebut,unsigned int ydebut,
                 unsigned int xfin,unsigned int yfin,unsigned int vitesse,
                 unsigned int periode,unsigned int phase)
{
    mID = id;
    mXDebut = xdebut;
    mYDebut = ydebut;
    mXFin = xfin;
    mYFin = yfin;
    mVitesse = vitesse;
    mPeriode = periode;
    mPhase = phase;
}
