#ifndef BRIS_H
#define BRIS_H

class Bris
{
public:
    Bris(){}
    Bris(unsigned int periode,unsigned int phase);

    unsigned int getPeriode() const {return mPeriode;}
    unsigned int getPhase() const {return mPhase;}

private:
    unsigned int mPeriode;
    unsigned int mPhase;

};

#endif // BRIS_H
