#ifndef PARAMETRES_H
#define PARAMETRES_H

#include <vector>
#include <QtXml/QtXml>
#include <QtCore>
#include <QDebug>
#include <Simulateur/bris.h>
#include <Simulateur/conducteur.h>
#include <Simulateur/feucirculation.h>
#include <Simulateur/route.h>
#include <Simulateur/temperature.h>
#include <Simulateur/Trafic.h>

using namespace std;

class Parametres
{
public:


    void setDureeSimulation(unsigned int dureeSimulation);
    unsigned int getDureeSimulation() const;

    void setCommunication(unsigned int communication);
    unsigned int getCommunication() const;

    void setAutoReparation(unsigned int autoReparation);
    unsigned int getAutoReparation() const;

    void setCollision(unsigned int collision);
    unsigned int getCollision() const;

    void setEchelle(unsigned int echelle);
    unsigned int getEchelle() const;

    void setXDepart(unsigned int xDepart);
    unsigned int getXDepart() const;

    void setYDepart(unsigned int yDepart);
    unsigned int getYDepart() const;

    void setXArrivee(unsigned int xArrivee);
    unsigned int getXArrivee() const;

    void setYArrivee(unsigned int yArrivee);
    unsigned int getYArrivee() const;

    void setVitesse(unsigned int vitesse);
    unsigned int getVitesse() const;

    void setAcceleration(unsigned int acceleration);
    unsigned int getAcceleration() const;

    void setConsommation(unsigned int consommation);
    unsigned int getConsommation() const;

    void setFeuJaune(unsigned int feuJaune);
    unsigned int getFeuJaune() const;

    void setTemperatureMin(unsigned int temperatureMin);
    unsigned int getTemperatureMin() const;

    void setTemperatureMax(unsigned int temperatureMax);
    unsigned int getTemperatureMax() const;

    void setStrategie(QString strategie);
    const QString getStrategie() const;

    void setBris(Bris bris);
    const Bris& getBris() const;

    void setConducteur(Conducteur conducteur);
    const Conducteur& getConducteur() const;

    void setFeuxCirculations(vector<FeuCirculation> feuxCirculations);
    const vector<FeuCirculation>& getFeuxCirculations() const;

    void setRoutes(QVector<Route> routes);
    const QVector<Route>& getRoutes() const;

    void setTemperature(unsigned int temperature);
    unsigned int getTemperature() const;

    void setTrafics(vector<Trafic> Trafics);
    const vector<Trafic>& getTrafics() const;

    void printParameters();

    static Parametres* getInstance(){
        if (!paramInstance_) paramInstance_=new Parametres();
        return paramInstance_;
    }

    bool chargerFichierParametres(const QString &inCheminFichierParametres);

    bool isValid(){return mValid;}
private:
    Parametres();
    static Parametres* paramInstance_;
    unsigned int mDureeSimulation;
    unsigned int mCommunication;
    unsigned int mAutoReparation;
    unsigned int mCollision;
    unsigned int mEchelle;
    unsigned int mXDepart;
    unsigned int mYDepart;
    unsigned int mXArrivee;
    unsigned int mYArrivee;
    unsigned int mVitesse;
    unsigned int mAcceleration;
    unsigned int mConsommation;
    unsigned int mFeuJaune;
    unsigned int mTemperatureMin;
    unsigned int mTemperatureMax;
    QString mStrategie;
    Bris mBris;
    Conducteur mConducteur;
    //TODO: Utiliser QVector
    vector<FeuCirculation> mFeuxCirculations;
    QVector<Route> mRoutes;
    unsigned int mTemperature;
    vector<Trafic> mTrafics;
    bool mValid;

    bool parseParametres(QDomNodeList nodeList);
    bool parseRoutes(QDomNodeList nodeList);
    bool parseRoute(QDomNodeList nodeList, QVector<Route> *routes, unsigned int id);
    bool parseFeuxCirculations(QDomNodeList nodeList);
    bool parseFeu(QDomNodeList nodeList, vector<FeuCirculation> *lFeux, unsigned int id);
    bool parseTrafic(QDomNodeList nodeList);
    bool parseParcour(QDomNodeList nodeList, vector<Trafic> *lTrafics, unsigned int id);
    bool parseBris(QDomNodeList nodeList);
    bool parseConducteur(QDomNodeList nodeList);
};

#endif // PARAMETRES_H
