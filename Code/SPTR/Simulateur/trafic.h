#ifndef Trafic_H
#define Trafic_H

class Trafic
{
public:
    Trafic(unsigned int id, unsigned int xdebut, unsigned int ydebut,
            unsigned int xfin, unsigned int yfin, unsigned int vitesse,
            unsigned int periode, unsigned int phase);

    unsigned int getID() const {return mID;}
    unsigned int getXDebut() const {return mXDebut;}
    unsigned int getYDebut() const {return mYDebut;}
    unsigned int getXFin() const {return mXFin;}
    unsigned int getYFin() const {return mYFin;}
    unsigned int getVitesse() const {return mVitesse;}
    unsigned int getPeriode() const {return mPeriode;}
    unsigned int getPhase() const {return mPhase;}

private:
    unsigned int mID;
    unsigned int mXDebut;
    unsigned int mYDebut;
    unsigned int mXFin;
    unsigned int mYFin;
    unsigned int mVitesse;
    unsigned int mPeriode;
    unsigned int mPhase;

};

#endif // Trafic_H
