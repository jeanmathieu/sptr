#ifndef TEMPERATURE_H
#define TEMPERATURE_H

class Temperature
{
public:
    Temperature(unsigned int temperature);

    unsigned int getTemperature() const {return mTemperature;}

private:
    unsigned int mTemperature;

};

#endif // TEMPERATURE_H
