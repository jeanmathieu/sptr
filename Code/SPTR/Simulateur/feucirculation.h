#ifndef FEUCIRCULATION_H
#define FEUCIRCULATION_H

#include <QtCore>

class FeuCirculation
{
public:
    FeuCirculation(unsigned int id,unsigned int coordonneeX,unsigned int coordonneeY,
                   QString position,unsigned int duree);

    unsigned int getID() const {return mID;}
    unsigned int getCoordonneeX() const {return mCoordonneeX;}
    unsigned int getCoordonneeY() const {return mCoordonneeY;}
    const QString getPosition() const {return mPosition;}
    unsigned int getDuree() const {return mDuree;}

private:
    unsigned int mID;
    unsigned int mCoordonneeX;
    unsigned int mCoordonneeY;
    QString mPosition;
    unsigned int mDuree;

};

#endif // FEUCIRCULATION_H
