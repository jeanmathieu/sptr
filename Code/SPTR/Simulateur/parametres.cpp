#include "parametres.h"
#include <QFileInfo>

Parametres* Parametres::paramInstance_= 0;

Parametres::Parametres()
{
}

bool Parametres::chargerFichierParametres(const QString &inCheminFichierParametres){
    qDebug()<<"inCheminFichierParametres: "<< inCheminFichierParametres;

    QDomDocument lDomFile;
    QFile file(inCheminFichierParametres);
    if(!file.open(QIODevice::ReadOnly))
    {
        return false;
    }
    if(!lDomFile.setContent(&file))
    {
        file.close();
        return false;
    }
    file.close();

    QDomElement root = lDomFile.documentElement();
    QDomNode xmlNode = root.firstChild();

    bool parsingOK = false;
    bool parametreSet[] = {false,false,false,false,false,false,false};
    while(!xmlNode.isNull())
    {
        if(xmlNode.isElement())
        {
            QDomElement currentElement = xmlNode.toElement();
            if(!currentElement.isNull())
            {
                if(currentElement.tagName()=="Parametres")
                {
                    parsingOK = parseParametres(currentElement.childNodes());
                    if(!parsingOK) return false;
                    parametreSet[0] = true;
                }
                if(currentElement.tagName()=="Routes")
                {
                    parsingOK = parseRoutes(currentElement.childNodes());
                    if(!parsingOK) return false;
                    parametreSet[1] = true;
                }
                if(currentElement.tagName()=="Feux")
                {
                    parsingOK = parseFeuxCirculations(currentElement.childNodes());
                    if(!parsingOK) return false;
                    parametreSet[2] = true;
                }
                if(currentElement.tagName()=="Trafic")
                {
                    parsingOK = parseTrafic(currentElement.childNodes());
                    if(!parsingOK) return false;
                    parametreSet[3] = true;
                }
                if(currentElement.tagName()=="Temperature")
                {
                    uint value = currentElement.text().toUInt(&parsingOK);
                    Parametres::getInstance()->setTemperature(value);
                    if(!parsingOK) return false;
                    parametreSet[4] = true;
                }
                if(currentElement.tagName()=="Bris")
                {
                    parsingOK = parseBris(currentElement.childNodes());
                    if(!parsingOK) return false;
                    parametreSet[5] = true;
                }
                if(currentElement.tagName()=="Conducteur")
                {
                    parsingOK = parseConducteur(currentElement.childNodes());
                    if(!parsingOK) return false;
                    parametreSet[6] = true;
                }
            }
        }
        xmlNode = xmlNode.nextSibling();
    }

    for(int i=0;i<7;i++)
        if(parametreSet[i] == false) // Un �l�ment a �t� manquant
            return false;

    return true;
}


void Parametres::setDureeSimulation(unsigned int dureeSimulation)
{
    mDureeSimulation = dureeSimulation;
}

unsigned int Parametres::getDureeSimulation() const
{
    return mDureeSimulation;
}

void Parametres::setCommunication(unsigned int communication)
{
    mCommunication = communication;
}

unsigned int Parametres::getCommunication() const
{
    return mCommunication;
}

void Parametres::setAutoReparation(unsigned int autoReparation)
{
    mAutoReparation = autoReparation;
}

unsigned int Parametres::getAutoReparation() const
{
    return mAutoReparation;
}

void Parametres::setCollision(unsigned int collision)
{
    mCollision = collision;
}

unsigned int Parametres::getCollision() const
{
    return mCollision;
}

void Parametres::setEchelle(unsigned int echelle)
{
    mEchelle = echelle;
}

unsigned int Parametres::getEchelle() const
{
    return mEchelle;
}

void Parametres::setXDepart(unsigned int xDepart)
{
    mXDepart = xDepart;
}

unsigned int Parametres::getXDepart() const
{
    return mXDepart;
}

void Parametres::setYDepart(unsigned int yDepart)
{
    mYDepart = yDepart;
}

unsigned int Parametres::getYDepart() const
{
    return mYDepart;
}

void Parametres::setXArrivee(unsigned int xArrivee)
{
    mXArrivee = xArrivee;
}

unsigned int Parametres::getXArrivee() const
{
    return mXArrivee;
}

void Parametres::setYArrivee(unsigned int yArrivee)
{
    mYArrivee = yArrivee;
}

unsigned int Parametres::getYArrivee() const
{
    return mYArrivee;
}

void Parametres::setVitesse(unsigned int vitesse)
{
    mVitesse = vitesse;
}

unsigned int Parametres::getVitesse() const
{
    return mVitesse;
}

void Parametres::setAcceleration(unsigned int acceleration)
{
    mAcceleration = acceleration;
}

unsigned int Parametres::getAcceleration() const
{
    return mAcceleration;
}

void Parametres::setConsommation(unsigned int consommation)
{
    mConsommation = consommation;
}

unsigned int Parametres::getConsommation() const
{
    return mConsommation;
}

void Parametres::setFeuJaune(unsigned int feuJaune)
{
    mFeuJaune = feuJaune;
}

unsigned int Parametres::getFeuJaune() const
{
    return mFeuJaune;
}

void Parametres::setTemperatureMin(unsigned int temperatureMin)
{
    mTemperatureMin = temperatureMin;
}

unsigned int Parametres::getTemperatureMin() const
{
    return mTemperatureMin;
}

void Parametres::setTemperatureMax(unsigned int temperatureMax)
{
    mTemperatureMax = temperatureMax;
}

unsigned int Parametres::getTemperatureMax() const
{
    return mTemperatureMax;
}

void Parametres::setStrategie(QString strategie)
{
    mStrategie = strategie;
}

const QString Parametres::getStrategie() const
{
    return mStrategie;
}

void Parametres::setBris(Bris bris)
{
    mBris = bris;
}

const Bris& Parametres::getBris() const
{
    return mBris;
}

void Parametres::setConducteur(Conducteur conducteur)
{
    mConducteur = conducteur;
}

const Conducteur& Parametres::getConducteur() const
{
    return mConducteur;
}

void Parametres::setFeuxCirculations(vector<FeuCirculation> feuxCirculations)
{
    mFeuxCirculations = feuxCirculations;
}

const vector<FeuCirculation>& Parametres::getFeuxCirculations() const
{
    return mFeuxCirculations;
}

void Parametres::setRoutes(QVector<Route> routes)
{
    mRoutes = routes;
}

const QVector<Route> &Parametres::getRoutes() const
{
    return mRoutes;
}

void Parametres::setTemperature(unsigned int temperature)
{
    mTemperature = temperature;
}

unsigned int Parametres::getTemperature() const
{
    return mTemperature;
}

void Parametres::setTrafics(vector<Trafic> Trafics)
{
    mTrafics = Trafics;
}

const vector<Trafic>& Parametres::getTrafics() const
{
    return mTrafics;
}

bool Parametres::parseParametres(QDomNodeList nodeList)
{
    bool isOK = false;

    bool parametreSet[16];
    for(int i = 0; i<16; i++)
        parametreSet[i] = false;

    for(int i = 0; i<nodeList.count(); i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            uint value;
            if(itemElem.tagName()=="Simulation")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setDureeSimulation(value);
                parametreSet[0] = true;
            }
            else if(itemElem.tagName()=="Communication")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setCommunication(value);
                parametreSet[1] = true;
            }
            else if(itemElem.tagName()=="AutoReparation")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setAutoReparation(value);
                parametreSet[2] = true;
            }
            else if(itemElem.tagName()=="Collision")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setCollision(value);
                parametreSet[3] = true;
            }
            else if(itemElem.tagName()=="Echelle")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setEchelle(value);
                parametreSet[4] = true;
            }
            else if(itemElem.tagName()=="XDepart")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setXDepart(value);
                parametreSet[5] = true;
            }
            else if(itemElem.tagName()=="YDepart")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setYDepart(value);
                parametreSet[6] = true;
            }
            else if(itemElem.tagName()=="XArrivee")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setXArrivee(value);
                parametreSet[7] = true;
            }
            else if(itemElem.tagName()=="YArrivee")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setYArrivee(value);
                parametreSet[8] = true;
            }
            else if(itemElem.tagName()=="Vitesse")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setVitesse(value);
                parametreSet[9] = true;
            }
            else if(itemElem.tagName()=="Acceleration")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setAcceleration(value);
                parametreSet[10] = true;
            }
            else if(itemElem.tagName()=="Consommation")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setConsommation(value);
                parametreSet[11] = true;
            }
            else if(itemElem.tagName()=="FeuJaune")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setFeuJaune(value);
                parametreSet[12] = true;
            }
            else if(itemElem.tagName()=="TemperatureMin")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setTemperatureMin(value);
                parametreSet[13] = true;
            }
            else if(itemElem.tagName()=="TemperatureMax")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                Parametres::getInstance()->setTemperatureMax(value);
                parametreSet[14] = true;
            }
            else if(itemElem.tagName()=="Strategie")
            {
                Parametres::getInstance()->setStrategie(itemElem.text());
                parametreSet[15] = true;
            }
            else
            {
                qDebug() << "ERROR in Parse Parametres" << endl;
                return false;
            }
        }
    }

    for(int i=0;i<16;i++)
        if(parametreSet[i] == false) // Un �l�ment a �t� manquant
            return false;

    return true;
}

bool Parametres::parseRoutes(QDomNodeList nodeList)
{
    if(nodeList.count()==0) return false;
    bool parseRouteOK = false;
    QVector<Route> lRoutes;
    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="Route")
            {
                bool isOK;
                uint value = itemElem.attribute("numero").toUInt(&isOK);
                if(!isOK) return false;
                parseRouteOK = parseRoute(itemElem.childNodes(),&lRoutes, value);
                if(!parseRouteOK) return false;
            }
        }
    }
    Parametres::getInstance()->setRoutes(lRoutes);
    return true;
}

bool Parametres::parseRoute(QDomNodeList nodeList, QVector<Route> *routes, unsigned int ID)
{
    int lXDebut = -1;
    int lYDebut = -1;
    int lXFin = -1;
    int lYFin = -1;
    int lVitesse = -1;
    uint value;
    bool isOK = false;

    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="XDebut")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lXDebut = value;
            }
            else if(itemElem.tagName()=="YDebut")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lYDebut = value;
            }
            else if(itemElem.tagName()=="XFin")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lXFin = value;
            }
            else if(itemElem.tagName()=="YFin")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lYFin = value;
            }
            else if(itemElem.tagName()=="Vitesse")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lVitesse = value;
            }
            else
            {
                qDebug() << "Wrong route element" << endl;
                return false;
            }
        }
    }

    if(lXDebut>=0 && lYDebut>=0 && lXFin>=0 && lYFin>=0 && lVitesse>=0)
    {
        Route lRoute(ID,lXDebut,lYDebut,lXFin,lYFin,lVitesse);
        routes->push_back(lRoute);
    }
    else return false; // Param�tre manquant ou invalide

    return true;
}

bool Parametres::parseFeuxCirculations(QDomNodeList nodeList)
{
    if(nodeList.count()==0) return false;
    bool parseFeuOK;
    vector<FeuCirculation> lFeux;
    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="Feu")
            {
                bool isOK;
                uint value = itemElem.attribute("numero").toUInt(&isOK);
                if(!isOK) return false;
                parseFeuOK = parseFeu(itemElem.childNodes(),&lFeux, value);
                if(!parseFeuOK) return false;
            }
        }
    }
    Parametres::getInstance()->setFeuxCirculations(lFeux);
    return true;
}

bool Parametres::parseFeu(QDomNodeList nodeList, vector<FeuCirculation> *lFeux, unsigned int ID)
{
     int lCoordonneeX = -1;
     int lCoordonneeY = -1;
     QString lPosition = "";
     int lDuree = -1;
     bool isOK;
     uint value;

     for(int i = 0; i<nodeList.count();i++)
     {
         QDomNode itemNode = nodeList.at(i);

         if(itemNode.isElement())
         {
             QDomElement itemElem = itemNode.toElement();
             if(itemElem.tagName()=="CoordonneeX")
             {
                 value = itemElem.text().toUInt(&isOK);
                 if(!isOK) return false;
                 lCoordonneeX = value;
             }
             else if(itemElem.tagName()=="CoordonneeY")
             {
                 value = itemElem.text().toUInt(&isOK);
                 if(!isOK) return false;
                 lCoordonneeY = value;
             }
             else if(itemElem.tagName()=="Position")
             {
                 lPosition = itemElem.text();
                 if(lPosition!="N" && lPosition!="S" && lPosition!="O" && lPosition!="E") return false;
             }
             else if(itemElem.tagName()=="Duree")
             {
                 value = itemElem.text().toUInt(&isOK);
                 if(!isOK) return false;
                 lDuree = value;
             }
             else
             {
                 qDebug() << "Wrong FeuxCirculations element" << endl;
                 return false;
             }
         }
     }
     if(lCoordonneeX>=0 && lCoordonneeY>=0 && lPosition!="" && lDuree>=0)
     {
         FeuCirculation lFeu(ID,lCoordonneeX,lCoordonneeY,lPosition,lDuree);
         lFeux->push_back(lFeu);
     }
     else return false;

     return true;
}

bool Parametres::parseTrafic(QDomNodeList nodeList)
{
    if(nodeList.count()==0) return false;
    bool parseParcourOK;
    vector<Trafic> lTrafics;
    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="Parcours")
            {
                bool isOK;
                uint value = itemElem.attribute("numero").toUInt(&isOK);
                if(!isOK) return false;
                parseParcourOK = parseParcour(itemElem.childNodes(),&lTrafics, value);
                if(!parseParcourOK) return false;
            }
        }
    }
    Parametres::getInstance()->setTrafics(lTrafics);
    return true;
}

bool Parametres::parseParcour(QDomNodeList nodeList, vector<Trafic> *lTrafics, unsigned int ID)
{
    int lXDebut = -1;
    int lYDebut = -1;
    int lXFin = -1;
    int lYFin = -1;
    int lVitesse = -1;
    int lPeriode = -1;
    int lPhase = -1;
    bool isOK;
    uint value;

    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="XDebut")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lXDebut = value;
            }
            else if(itemElem.tagName()=="YDebut")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lYDebut = value;
            }
            else if(itemElem.tagName()=="XFin")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lXFin = value;
            }
            else if(itemElem.tagName()=="YFin")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lYFin = value;
            }
            else if(itemElem.tagName()=="Vitesse")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lVitesse = value;
            }
            else if(itemElem.tagName()=="Periode")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lPeriode = value;
            }
            else if(itemElem.tagName()=="Phase")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lPhase = value;
            }
            else
            {
                qDebug() << "Wrong Trafic element" << endl;
                return false;
            }
        }
    }
    if(lXDebut>=0 && lYDebut>=0 && lXFin>=0 && lYFin>=0 && lVitesse>=0 && lPeriode>=0 && lPhase>=0)
    {
        Trafic lTrafic(ID,lXDebut,lYDebut,lXFin,lYFin,lVitesse,lPeriode,lPhase);
        lTrafics->push_back(lTrafic);
    }
    else return false;

    return true;
}

bool Parametres::parseBris(QDomNodeList nodeList)
{
    int lPeriode = -1;
    int lPhase = -1;
    bool isOK;
    uint value;

    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="Periode")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lPeriode = value;
            }
            else if(itemElem.tagName()=="Phase")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lPhase = value;
            }
            else
            {
                qDebug() << "Wrong bris element" << endl;
            }
        }
    }
    if(lPeriode>=0 && lPhase>=0)
    {
        Parametres::getInstance()->setBris(Bris(lPeriode,lPhase));
    }
    else return false;

    return true;
}

bool Parametres::parseConducteur(QDomNodeList nodeList)
{
    int lPeriode = -1;
    int lPhase = -1;
    bool isOK;
    uint value;

    for(int i = 0; i<nodeList.count();i++)
    {
        QDomNode itemNode = nodeList.at(i);

        if(itemNode.isElement())
        {
            QDomElement itemElem = itemNode.toElement();
            if(itemElem.tagName()=="Periode")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lPeriode = value;
            }
            else if(itemElem.tagName()=="Phase")
            {
                value = itemElem.text().toUInt(&isOK);
                if(!isOK) return false;
                lPhase = value;
            }
            else
            {
                qDebug() << "Wrong conducteur element" << endl;
                return false;
            }
        }
    }
    if(lPeriode>=0 && lPhase>=0)
    {
        Parametres::getInstance()->setConducteur(Conducteur(lPeriode,lPhase));
    }
    else return false;

    return true;
}

void Parametres::printParameters()
{
    qDebug() << "\nParametres: "
                << "\n     Simulation: " << mDureeSimulation
                << "\n     Communication: " << mCommunication
                << "\n     mAutoReparation: " << mAutoReparation
                << "\n     mCollision: " << mCollision
                << "\n     mEchelle: " << mEchelle
                << "\n     mXDepart: " << mXDepart
                << "\n     mYDepart: " << mYDepart
                << "\n     mXArrivee: " << mXArrivee
                << "\n     mYArrivee: " << mYArrivee
                << "\n     mVitesse: " << mVitesse
                << "\n     mAcceleration: " << mAcceleration
                << "\n     mConsommation: " << mConsommation
                << "\n     mFeuJaune: " << mFeuJaune
                << "\n     mTemperatureMin: " << mTemperatureMin
                << "\n     mTemperatureMax: " << mTemperatureMax
                << "\n     mStrategie: " << mStrategie << endl;

    qDebug() << "\nBris: "
             << "\n     Periode: " << mBris.getPeriode()
             << "\n     Phase: " << mBris.getPhase() << endl;

    qDebug() << "\nConducteurs: "
             << "\n     Periode: " << mConducteur.getPeriode()
             << "\n     Phase: " << mConducteur.getPhase() << endl;

    qDebug() << "\nFeuxCirculation: ";
    for(unsigned int i=0; i<mFeuxCirculations.size(); i++)
    {
        qDebug() << "\n     Feu no: " << mFeuxCirculations[i].getID()
                 << "\n     CoordonneeX: " << mFeuxCirculations[i].getCoordonneeX()
                 << "\n     CoordonneeY: " << mFeuxCirculations[i].getCoordonneeY()
                 << "\n     Position: " << mFeuxCirculations[i].getPosition()
                 << "\n     Duree: " << mFeuxCirculations[i].getDuree() << endl;
    }

    qDebug() << "\nRoutes: ";
    for(unsigned int i=0; i<mRoutes.size(); i++)
    {
        qDebug() << "\n     Route no: " << mRoutes[i].getID()
                 << "\n     XDebut: " << mRoutes[i].getXDebut()
                 << "\n     YDebut: " << mRoutes[i].getYDebut()
                 << "\n     XFin: " << mRoutes[i].getXFin()
                 << "\n     YFin: " << mRoutes[i].getYFin()
                 << "\n     Vitesse: " << mRoutes[i].getVitesse() << endl;
    }

    qDebug() << "\nTemperature: " << mTemperature;

    qDebug() << "\nTrafics";
    for(unsigned int i=0; i<mTrafics.size(); i++)
    {
        qDebug() << "\n     Trafic no: " << mTrafics[i].getID()
                 << "\n     XDebut: " << mTrafics[i].getXDebut()
                 << "\n     YDebut: " << mTrafics[i].getYDebut()
                 << "\n     XFin: " << mTrafics[i].getXFin()
                 << "\n     YFin: " << mTrafics[i].getYFin()
                 << "\n     Vitesse: " << mTrafics[i].getVitesse()
                 << "\n     Periode: " << mTrafics[i].getPeriode()
                 << "\n     Phase: " << mTrafics[i].getPhase() << endl;

    }

}
