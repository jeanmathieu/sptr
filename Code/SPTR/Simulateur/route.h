#ifndef ROUTE_H
#define ROUTE_H

class Route
{
public:
    Route(unsigned int ID,unsigned int xDebut,unsigned int yDebut,
          unsigned int xFin,unsigned int yFin, unsigned int vitesse);
    Route(){}
    unsigned int getID() const {return mID;}
    unsigned int getXDebut() const {return mXDebut;}
    unsigned int getYDebut() const {return mYDebut;}
    unsigned int getXFin() const {return mXFin;}
    unsigned int getYFin() const {return mYFin;}
    unsigned int getVitesse() const {return mVitesse;}
    Route& operator=(const Route &in);
private:
    unsigned int mID;
    unsigned int mXDebut;
    unsigned int mYDebut;
    unsigned int mXFin;
    unsigned int mYFin;
    unsigned int mVitesse;
};

#endif // ROUTE_H
