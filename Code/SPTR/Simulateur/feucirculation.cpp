#include "feucirculation.h"

FeuCirculation::FeuCirculation(unsigned int id,unsigned int coordonneeX,unsigned int coordonneeY,
                               QString position,unsigned int duree)
{
    mID = id;
    mCoordonneeX = coordonneeX;
    mCoordonneeY = coordonneeY;
    mPosition = position;
    mDuree = duree;
}
