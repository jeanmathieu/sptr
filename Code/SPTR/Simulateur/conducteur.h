#ifndef CONDUCTEUR_H
#define CONDUCTEUR_H

class Conducteur
{
public:
    Conducteur(){}
    Conducteur(unsigned int periode,unsigned int phase);

    unsigned int getPeriode() const {return mPeriode;}
    unsigned int getPhase() const {return mPhase;}

private:
    unsigned int mPeriode;
    unsigned int mPhase;

};

#endif // CONDUCTEUR_H
