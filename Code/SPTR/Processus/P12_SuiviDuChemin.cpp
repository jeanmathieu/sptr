#include "P12_SuiviDuChemin.h"

P12_SuiviDuChemin* P12_SuiviDuChemin::mInstance=0;

P12_SuiviDuChemin::P12_SuiviDuChemin(void)
{
	mExecutionOrder.push_back("C03");
	mExecutionOrder.push_back("R03");
	mExecutionOrder.push_back("C12");
	mExecutionOrder.push_back("C09");
	mExecutionOrder.push_back("R01");
	mExecutionOrder.push_back("R04");
	mExecutionOrder.push_back("C11");
	mExecutionOrder.push_back("R06");
	mExecutionOrder.push_back("C10");
}


P12_SuiviDuChemin::~P12_SuiviDuChemin(void)
{
}

void P12_SuiviDuChemin::activateRegulator(vector<Point> path, Point currentPosition, string currentDirection)
{
	bool cheminBosse = false;
	string direction = " ";
	if(path[0].x - currentPosition.x > 0)
		direction = "est";
	if(path[0].x - currentPosition.x < 0)
		direction = "ouest";
	if(path[0].y - currentPosition.y > 0)
		direction = "nord";
	if(path[0].y - currentPosition.y < 0)
		direction = "sud";
	//dépendamment du chemin et de la position
	if(abs(currentPosition.x - path[0].x) > 1 || abs(currentPosition.y - path[0].y) > 1)
	{
		//activer le régulateur de vitesse
        if(currentDirection.compare(direction) == 0){}
			//activer le régulateur de direction
	}
    if(cheminBosse){}//activer le régulateur de suspension
}

P12_SuiviDuChemin* P12_SuiviDuChemin::getInstance()
{
    if (mInstance==0) mInstance = new P12_SuiviDuChemin();
    return mInstance;
}
