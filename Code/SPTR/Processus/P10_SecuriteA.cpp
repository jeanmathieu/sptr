#include "P10_SecuriteA.h"

P10_SecuriteA* P10_SecuriteA::mInstance=0;

P10_SecuriteA::P10_SecuriteA(void)
{
	mExecutionOrder.push_back("R01");
	mExecutionOrder.push_back("R02");
	mExecutionOrder.push_back("R01");
	mExecutionOrder.push_back("C11");
	mExecutionOrder.push_back("R04");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("C07");
}


P10_SecuriteA::~P10_SecuriteA(void)
{
}

P10_SecuriteA* P10_SecuriteA::getInstance()
{
    if (mInstance==0) mInstance = new P10_SecuriteA();
    return mInstance;
}
