#include "P01_AutoVerification.h"

P01_AutoVerification* P01_AutoVerification::mInstance=0;

P01_AutoVerification::P01_AutoVerification(void)
{
    mBris = false;
    mAutoReparation = 0;
	mExecutionOrder.push_back("R05");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("C01");
	mExecutionOrder.push_back("R08");
	mExecutionOrder.push_back("E");
}


P01_AutoVerification::~P01_AutoVerification(void)
{
}

P01_AutoVerification* P01_AutoVerification::getInstance()
{
    if (mInstance==0) mInstance = new P01_AutoVerification();
    return mInstance;
}
