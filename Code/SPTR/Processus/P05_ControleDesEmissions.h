#ifndef _P05_ControleDesEmissions_H
#define _P05_ControleDesEmissions_H

#include "processus.h"

class P05_ControleDesEmissions :
	public Processus
{
public:
    P05_ControleDesEmissions(void);//TODO <- private
	~P05_ControleDesEmissions(void);
	bool newPath(int consommation);//Est-ce que le param�tre a �t� atteint
    static P05_ControleDesEmissions* getInstance();

private:
    static P05_ControleDesEmissions* mInstance;
	int mConsommationUA;
	list<string> mExecutionOrder;
};

#endif

