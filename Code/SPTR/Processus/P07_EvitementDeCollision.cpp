#include "P07_EvitementDeCollision.h"

P07_EvitementDeCollision* P07_EvitementDeCollision::mInstance=0;

P07_EvitementDeCollision::P07_EvitementDeCollision(void)
{
	mExecutionOrder.push_back("R03");
	mExecutionOrder.push_back("R09");
	mExecutionOrder.push_back("R07");
	mExecutionOrder.push_back("R05");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("C06");
	mExecutionOrder.push_back("C02");
}


P07_EvitementDeCollision::~P07_EvitementDeCollision(void)
{
}

P07_EvitementDeCollision* P07_EvitementDeCollision::getInstance()
{
    if (mInstance==0) mInstance = new P07_EvitementDeCollision();
    return mInstance;
}
