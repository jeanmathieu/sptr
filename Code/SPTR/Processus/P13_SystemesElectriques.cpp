#include "P13_SystemesElectriques.h"

P13_SystemesElectriques* P13_SystemesElectriques::mInstance=0;

P13_SystemesElectriques::P13_SystemesElectriques(void)
{
	mExecutionOrder.push_back("C07");
	mExecutionOrder.push_back("R05");
	mExecutionOrder.push_back("R11");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("R04");
}


P13_SystemesElectriques::~P13_SystemesElectriques(void)
{
}

P13_SystemesElectriques* P13_SystemesElectriques::getInstance()
{
    if (mInstance==0) mInstance = new P13_SystemesElectriques();
    return mInstance;
}
