#ifndef _P02_CalculDeChemin_H
#define _P02_CalculDeChemin_H


#include "processus.h"
class P02_CalculDeChemin :
	public Processus
{
public:
    P02_CalculDeChemin(void);//TODO <- private
	~P02_CalculDeChemin(void);
	vector<int> getPath(Point destination,Point depart);//pourrait �tre [distance,angle,distance,angle...] ou l'angle serait les virages � faire apr�s la distance
    static P02_CalculDeChemin* getInstance();

private:
    static P02_CalculDeChemin* mInstance;
	bool mADestination;
	list<string> mExecutionOrder;
};

#endif

