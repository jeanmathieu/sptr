#ifndef _P07_EvitementDeCollision_H
#define _P07_EvitementDeCollision_H

#include "processus.h"
class P07_EvitementDeCollision :
	public Processus
{
public:
	P07_EvitementDeCollision(void);
	~P07_EvitementDeCollision(void);
	int maxSpeed(/*param�tre de l'environnement et du radars*/);
	void carStopped(bool collision);//set speed to 0 pour mCollision u.t.
    static P07_EvitementDeCollision* getInstance();

private:
    static P07_EvitementDeCollision* mInstance;
	int mCollision;
	list<string> mExecutionOrder;
};

#endif

