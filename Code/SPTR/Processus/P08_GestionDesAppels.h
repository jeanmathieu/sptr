#ifndef _P08_GestionDesAppels_H
#define _P08_GestionDesAppels_H


#include "processus.h"
class P08_GestionDesAppels :
	public Processus
{
public:
	P08_GestionDesAppels(void);
	~P08_GestionDesAppels(void);
	void makeACall(bool askedCall);//fonction pour ex�cuter l'appel
    static P08_GestionDesAppels* getInstance();

private:
    static P08_GestionDesAppels* mInstance;
	list<string> mExecutionOrder;
};

#endif
