#include "P05_ControleDesEmissions.h"

P05_ControleDesEmissions* P05_ControleDesEmissions::mInstance=0;

P05_ControleDesEmissions::P05_ControleDesEmissions(void)
{
	mExecutionOrder.push_back("C04");
	mExecutionOrder.push_back("R06");
	mExecutionOrder.push_back("C05");
}


P05_ControleDesEmissions::~P05_ControleDesEmissions(void)
{
}

bool P05_ControleDesEmissions::newPath(int consommation)
{
	if(consommation >= mConsommationUA)
		return true;
	else
		return false;
}

P05_ControleDesEmissions* P05_ControleDesEmissions::getInstance()
{
    if (mInstance==0) mInstance = new P05_ControleDesEmissions();
    return mInstance;
}
