#ifndef _P11_SecuriteB_H
#define _P11_SecuriteB_H


#include "processus.h"
class P11_SecuriteB :
	public Processus
{
public:
	P11_SecuriteB(void);
	~P11_SecuriteB(void);
	void activateAirBag(bool collision);
	void activateABS(bool abs);
    static P11_SecuriteB* getInstance();

private:
    static P11_SecuriteB* mInstance;
	list<string> mExecutionOrder;
};

#endif

