#ifndef _P01_AutoVerification_H
#define _P01_AutoVerification_H

class P01_AutoVerification;
#include "processus.h"
//#include "R05_Environnement"
//#include "R08_Odometre"

class P01_AutoVerification :
	public Processus
{
public:
    P01_AutoVerification(void);//TODO <- private
	~P01_AutoVerification(void);
	void DropSpeed(bool broken, int time);
    static P01_AutoVerification* getInstance();

private:
    static P01_AutoVerification* mInstance;
    bool mBris;
	int mAutoReparation;
    list<QString> mExecutionOrder;
};

#endif

