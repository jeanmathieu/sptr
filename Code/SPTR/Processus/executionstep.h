
#ifndef __ExecutionStep_h__
#define __ExecutionStep_h__
#include <string>

class ExecutionStep
{
public:
    enum type{execution,ressource,communication};
    void setType(type inType);
    type getType();
    virtual std::string toString(){return "";}

protected:
    type mType;

};

#endif
