#include "P03_ChangementDeVitesse.h"

P03_ChangementDeVitesse* P03_ChangementDeVitesse::mInstance=0;

P03_ChangementDeVitesse::P03_ChangementDeVitesse(void)
{
	mExecutionOrder.push_back("R03");
	mExecutionOrder.push_back("C06");
	mExecutionOrder.push_back("C01");
	mExecutionOrder.push_back("C09");
	mExecutionOrder.push_back("R10");
	mExecutionOrder.push_back("C04");
}


P03_ChangementDeVitesse::~P03_ChangementDeVitesse(void)
{
}

P03_ChangementDeVitesse* P03_ChangementDeVitesse::getInstance()
{
    if (mInstance==0) mInstance = new P03_ChangementDeVitesse();
    return mInstance;
}
