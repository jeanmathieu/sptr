#include "P11_SecuriteB.h"

P11_SecuriteB* P11_SecuriteB::mInstance=0;

P11_SecuriteB::P11_SecuriteB(void)
{
	mExecutionOrder.push_back("C12");
	mExecutionOrder.push_back("R11");
	mExecutionOrder.push_back("R09");
	mExecutionOrder.push_back("R03");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("C08");
}


P11_SecuriteB::~P11_SecuriteB(void)
{
}

P11_SecuriteB* P11_SecuriteB::getInstance()
{
    if (mInstance==0) mInstance = new P11_SecuriteB();
    return mInstance;
}
