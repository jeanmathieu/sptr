#include <exception>
using namespace std;

#ifndef __ProcessusManager_h__
#define __ProcessusManager_h__

#include "./processus.h"
//#include "../Ressources/ressource.h"
#include <map>
#include "P01_AutoVerification.h"
#include "P02_CalculDeChemin.h"
#include "P03_ChangementDeVitesse.h"
#include "P04_Climat.h"
#include "P05_ControleDesEmissions.h"
#include "P06_EnvoiDeBilan.h"
#include "P07_EvitementDeCollision.h"
#include "P08_GestionDesAppels.h"
#include "P09_Messagerie.h"
#include "P10_SecuriteA.h"
#include "P11_SecuriteB.h"
#include "P12_SuiviDuChemin.h"
#include "P13_SystemesElectriques.h"

class ProcessusManager
{
private:
    static std::map<int, Processus*>& mProcessus()//static std::map<int, Processus*> mProcessus;
    {
        static std::map<int, Processus*> _mProcessus;
        return _mProcessus;
    }

public:
    static void initProcesses();
    static void initWakeUpCommunications(Processus* inProcessus); //TODO
    static Processus* getProcess(int inID);
    static std::map<int, Processus::state> getProcessesStates();
    //static std::map<int, int> getProcessesResources();//Les ressources ne sont pas encore définies
    static bool isReady(int inID);

};


#endif
