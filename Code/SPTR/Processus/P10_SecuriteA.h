#ifndef _P10_SecuriteA_H
#define _P10_SecuriteA_H


#include "processus.h"
class P10_SecuriteA :
	public Processus
{
public:
	P10_SecuriteA(void);
	~P10_SecuriteA(void);
	void driverAwake(/*image de la cam�ra du visage*/);
	void printOnWindscreen(/*image des cam�ras infra-rouges*/);
    static P10_SecuriteA* getInstance();

private:
    static P10_SecuriteA* mInstance;
	list<string> mExecutionOrder;
};

#endif

