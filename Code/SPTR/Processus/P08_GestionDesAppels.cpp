#include "P08_GestionDesAppels.h"

P08_GestionDesAppels* P08_GestionDesAppels::mInstance=0;

P08_GestionDesAppels::P08_GestionDesAppels(void)
{
	mExecutionOrder.push_back("R03");
	mExecutionOrder.push_back("C08");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("R12");
}


P08_GestionDesAppels::~P08_GestionDesAppels(void)
{
}

P08_GestionDesAppels* P08_GestionDesAppels::getInstance()
{
    if (mInstance==0) mInstance = new P08_GestionDesAppels();
    return mInstance;
}
