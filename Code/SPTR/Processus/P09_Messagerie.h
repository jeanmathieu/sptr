#ifndef _P09_Messagerie_H
#define _P09_Messagerie_H

#include "processus.h"
class P09_Messagerie :
	public Processus
{
public:
	P09_Messagerie(void);
	~P09_Messagerie(void);
	void printMessage(bool messageAcquired, string message);//j'ai mis un bool au cas o� c'�tait un message en r�p�tition
    static P09_Messagerie* getInstance();

private:
    static P09_Messagerie* mInstance;
	list<string> mExecutionOrder;
};

#endif
