#include "P04_Climat.h"

P04_Climat* P04_Climat::mInstance=0;

P04_Climat::P04_Climat(void)
{
    mTemperatureMax = 25;
    mTemperatureMin = 15;
	mExecutionOrder.push_back("R05");
	mExecutionOrder.push_back("E");
}


P04_Climat::~P04_Climat(void)
{
}

string P04_Climat::regulateTemperature(int outdoorTemperature)
{
	string regulate;
	if(outdoorTemperature > mTemperatureMax)
		regulate = "Climatisation";
	else if(outdoorTemperature < mTemperatureMin)
		regulate = "Chauffage";
	else
		regulate = "rien";
	return regulate;
}

P04_Climat* P04_Climat::getInstance()
{
    if (mInstance==0) mInstance = new P04_Climat();
    return mInstance;
}

