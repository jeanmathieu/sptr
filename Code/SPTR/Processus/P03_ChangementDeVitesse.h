#ifndef _P03_ChangementDeVitesse_H
#define _P03_ChangementDeVitesse_H

#include "processus.h"
class P03_ChangementDeVitesse :
    public Processus
{
public:
    P03_ChangementDeVitesse(void);//TODO <- private
    ~P03_ChangementDeVitesse(void);
    int changeSpeed(int currentSpeed, int futureSpeed);
    int acceleration(bool accelerationMax); //savoir si on minimise l'accélération ou non
    static P03_ChangementDeVitesse* getInstance();

private:
    static P03_ChangementDeVitesse* mInstance;
    list<string> mExecutionOrder;
};

#endif
