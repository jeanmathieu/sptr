#include "P09_Messagerie.h"

P09_Messagerie* P09_Messagerie::mInstance=0;

P09_Messagerie::P09_Messagerie(void)
{
	mExecutionOrder.push_back("R12");
	mExecutionOrder.push_back("R04");
}


P09_Messagerie::~P09_Messagerie(void)
{
}

P09_Messagerie* P09_Messagerie::getInstance()
{
    if (mInstance==0) mInstance = new P09_Messagerie();
    return mInstance;
}
