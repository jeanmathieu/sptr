#ifndef _P13_SystemesElectriques_H
#define _P13_SystemesElectriques_H

#include "processus.h"
class P13_SystemesElectriques :
	public Processus
{
public:
	P13_SystemesElectriques(void);
	~P13_SystemesElectriques(void);
	void keepDriverAwake(void);//stimuler le conducteur(je sais pas quoi mettre comme param�tre
    static P13_SystemesElectriques* getInstance();

private:
    static P13_SystemesElectriques* mInstance;
	list<string> mExecutionOrder;
};

#endif
