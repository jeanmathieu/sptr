#ifndef _P12_SuiviDuChemin_H
#define _P12_SuiviDuChemin_H

#include "processus.h"
class P12_SuiviDuChemin :
	public Processus
{
public:
	P12_SuiviDuChemin(void);
	~P12_SuiviDuChemin(void);
	void activateRegulator(vector<Point> path, Point currentPosition, string currentDirection);//fonction pour activer les régulateurs
    static P12_SuiviDuChemin* getInstance();

private:
    static P12_SuiviDuChemin* mInstance;
	list<string> mExecutionOrder;
};
#endif

