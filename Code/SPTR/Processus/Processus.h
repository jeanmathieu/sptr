#ifndef _Processus_H
#define _Processus_H

#include <vector>
#include <stdlib.h>
#include <list>
#include <stack>
#include <QtCore>
#include "../Processus/executionstep.h"

using namespace std;

typedef struct {int x, y;} Point;

class Processus
{
protected:
    int mID;
    int mEtatProc;
    int mType;
    ExecutionStep* mCurrentAction;
    std::stack<ExecutionStep*> mExecutionSteps;
    int mStartTime;
    int mMaxRunTime;
    int mEndConstraint;
    int mPriority;
    int mPeriode;//Processus periodique
public:
	Processus(void);
	~Processus(void);
     enum ID{P01=1, P02, P03, P04, P05, P06, P07, P08, P09, P10, P11, P12, P13};
     enum state{running,ready,suspended,asleep};
     enum type{periodic,sporadic};
     void run();

    //Processus periodique
    int getPeriod();
    void setPeriod(int inPeriode);

    int getId();
    void setId(ID inID);
    void setState(state inState);
    int getState();
    type getType();
    void setType(type inType);
    int getRunTime();//return mExecutionSteps.size();
    int getEndConstraint();
    void setEndConstraint(int inEndConstraint);
    int getPriority();
    void setPriority(int inPriority);
    ExecutionStep* getCurrentAction();
    void setCurrentAction(ExecutionStep* inCurrentAction);
    int getStartTime();
    void setStartTime(int inStartTime);
    int getMaxRunTime();
    void setMaxRunTime(int inMaxRunTime);

private:
	int mAccelerationMax;
	int mVitesseMax;
};

#endif

