#include "P02_CalculDeChemin.h"

P02_CalculDeChemin* P02_CalculDeChemin::mInstance=0;

P02_CalculDeChemin::P02_CalculDeChemin(void)
{
	mExecutionOrder.push_back("R02");
	mExecutionOrder.push_back("R07");
	mExecutionOrder.push_back("R05");
	mExecutionOrder.push_back("E");
	mExecutionOrder.push_back("C02");
	mExecutionOrder.push_back("C03");
}


P02_CalculDeChemin::~P02_CalculDeChemin(void)
{
}

P02_CalculDeChemin* P02_CalculDeChemin::getInstance()
{
    if (mInstance==0) mInstance = new P02_CalculDeChemin();
    return mInstance;
}
