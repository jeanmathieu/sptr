#ifndef _P06_EnvoiDeBilan_H
#define _P06_EnvoiDeBilan_H


#include "processus.h"
class P06_EnvoiDeBilan :
	public Processus
{
public:
	P06_EnvoiDeBilan(void);
	~P06_EnvoiDeBilan(void);
	void sendAssessment(int currentTime);
    static P06_EnvoiDeBilan* getInstance();

private:
    static P06_EnvoiDeBilan* mInstance;
	int mTimeOfAssessment;
	list<string> mExecutionOrder;
};

#endif
