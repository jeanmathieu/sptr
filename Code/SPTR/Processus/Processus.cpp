#include "Processus.h"

Processus::Processus(void)
{
}

Processus::~Processus(void)
{
}

int Processus::getPeriod()
{
    return mPeriode;
}

void Processus::setPeriod(int inPeriode)
{
    mPeriode = inPeriode;
}

int Processus::getId()
{
    return mID;
}

void Processus::setId(ID inID)
{
    mID = inID;
}

void Processus::setState(state inState)
{
    mEtatProc = inState;
}

int Processus::getState()
{
    return mEtatProc;
}

Processus::type Processus::getType()
{
    return (type)mType;
}

void Processus::setType(type inType)
{
    mType = inType;
}

int Processus::getRunTime()
{
    return mExecutionSteps.size();
}

int Processus::getEndConstraint()
{
    return mEndConstraint;
}

void Processus::setEndConstraint(int inEndConstraint)
{
    mEndConstraint = inEndConstraint;
}

int Processus::getPriority()
{
    return mPriority;
}

void Processus::setPriority(int inPriority)
{
    mPriority = inPriority;
}

ExecutionStep* Processus::getCurrentAction()
{
    return mCurrentAction;
}

void Processus::setCurrentAction(ExecutionStep *inCurrentAction)
{
    mCurrentAction = inCurrentAction;
}

int Processus::getStartTime()
{
    return mStartTime;
}

void Processus::setStartTime(int inStartTime)
{
    mStartTime = inStartTime;
}

int Processus::getMaxRunTime()
{
    return mMaxRunTime;
}

void Processus::setMaxRunTime(int inMaxRunTime)
{
    mMaxRunTime = inMaxRunTime;
}

void Processus::run()//TODO
{

}
