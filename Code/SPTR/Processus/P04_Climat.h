#ifndef _P04_Climat_H
#define _P04_Climat_H


#include "processus.h"
class P04_Climat :
	public Processus
{
public:
    P04_Climat(void);//TODO <- private
	~P04_Climat(void);
	string regulateTemperature(int outdoorTemperature);//valeur de retour? string pour "chauffage" ou "climatisation"
    static P04_Climat* getInstance();

private:
    static P04_Climat* mInstance;
	int mTemperatureMax;
	int mTemperatureMin;
	list<string> mExecutionOrder;
};

#endif
