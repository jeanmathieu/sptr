#include "ressource.h"

Ressource::Ressource() : mEtat(Ressource::FREE), mBusyTimeLeft(0),mLeaseTime(0), mCurrentUser(0), mID(0)
{
    setType(ExecutionStep::ressource);
}

void Ressource::free()
{
    mCurrentUser=0;
    setState(Ressource::FREE);
}

void Ressource::reserve(int inCurrentUser)
{
    mCurrentUser = inCurrentUser;
    setBusyTimeLeft(mLeaseTime);
    setState(Ressource::BUSY);
}

std::string Ressource::toString()
{
    std::stringstream lStringstream;
    std::string lString;

    lStringstream << mID;
    lStringstream >> lString;

    return lString;
}

int Ressource::useResource()
{
    mBusyTimeLeft--;
    return mBusyTimeLeft;
}

int Ressource::getId()
{
    return mID;
}

std::string Ressource::getName()
{
    return mName;
}

void Ressource::setUsers(std::list<int> inUsers)
{
    mUsers = inUsers;
}

std::list<int> Ressource::getUsers()
{
    return mUsers;
}

void Ressource::setState(State inState)
{
    mEtat = inState;
}

int Ressource::getState()
{
    return mEtat;
}

int Ressource::getCurrentUser()
{
    return mCurrentUser;
}

void Ressource::setLeaseTime(int inLeaseTime)
{
    mLeaseTime = inLeaseTime;
}

int Ressource::getLeaseTime()
{
    return mLeaseTime;
}

void Ressource::setBusyTimeLeft(int inBusyTimeLeft)
{
    mBusyTimeLeft = inBusyTimeLeft;
}

int Ressource::getBusyTimeLeft()
{
    return mBusyTimeLeft;
}
