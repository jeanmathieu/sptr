#-------------------------------------------------
#
# Project created by QtCreator 2013-03-03T13:05:25
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Test
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += \
    main_test.cpp \
    Tests/OrdonnenceurTEST.cpp \
    Ordonnanceur/ordonnenceur.cpp \
    Processus/ProcessusManager.cpp \
    Processus/Processus.cpp \
    Processus/P13_SystemesElectriques.cpp \
    Processus/P12_SuiviDuChemin.cpp \
    Processus/P11_SecuriteB.cpp \
    Processus/P10_SecuriteA.cpp \
    Processus/P09_Messagerie.cpp \
    Processus/P08_GestionDesAppels.cpp \
    Processus/P07_EvitementDeCollision.cpp \
    Processus/P06_EnvoiDeBilan.cpp \
    Processus/P05_ControleDesEmissions.cpp \
    Processus/P04_Climat.cpp \
    Processus/P03_ChangementDeVitesse.cpp \
    Processus/P02_CalculDeChemin.cpp \
    Processus/P01_AutoVerification.cpp \
    Tests/logitem.cpp \
    Tests/logitems.cpp \
    Ressources/ressource.cpp \
    Processus/executionstep.cpp \
    Ordonnanceur/communication.cpp \
    Simulateur/simulateur.cpp \
    Ordonnanceur/processeur.cpp

HEADERS += \
    Tests/OrdonnenceurTEST.h \
    Ordonnanceur/ordonnenceur.h \
    Processus/ProcessusManager.h \
    Processus/Processus.h \
    Processus/P13_SystemesElectriques.h \
    Processus/P12_SuiviDuChemin.h \
    Processus/P11_SecuriteB.h \
    Processus/P10_SecuriteA.h \
    Processus/P09_Messagerie.h \
    Processus/P08_GestionDesAppels.h \
    Processus/P07_EvitementDeCollision.h \
    Processus/P06_EnvoiDeBilan.h \
    Processus/P05_ControleDesEmissions.h \
    Processus/P04_Climat.h \
    Processus/P03_ChangementDeVitesse.h \
    Processus/P02_CalculDeChemin.h \
    Processus/P01_AutoVerification.h \
    Tests/logitem.h \
    Tests/logitems.h \
    Ressources/ressource.h \
    Processus/executionstep.h \
    Ordonnanceur/communication.h \
    Simulateur/simulateur.h \
    Ordonnanceur/processeur.h
