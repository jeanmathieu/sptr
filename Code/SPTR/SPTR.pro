QT += core
QT += xml
QT += widgets
qtHaveModule(printsupport): QT += printsupport
qtHaveModule(opengl): QT += opengl

HEADERS += \
    Simulateur/route.h \
    Simulateur/temperature.h \
    Simulateur/bris.h \
    Simulateur/conducteur.h \
    Simulateur/parametres.h \
    Simulateur/feucirculation.h \
    Simulateur/trafic.h \
    Processus/Processus.h \
    Processus/P13_SystemesElectriques.h \
    Processus/P12_SuiviDuChemin.h \
    Processus/P11_SecuriteB.h \
    Processus/P10_SecuriteA.h \
    Processus/P09_Messagerie.h \
    Processus/P08_GestionDesAppels.h \
    Processus/P07_EvitementDeCollision.h \
    Processus/P06_EnvoiDeBilan.h \
    Processus/P05_ControleDesEmissions.h \
    Processus/P04_Climat.h \
    Processus/P03_ChangementDeVitesse.h \
    Processus/P02_CalculDeChemin.h \
    Processus/P01_AutoVerification.h \
    Interface/mainwindow.h \
    Interface/loadfichierparametresform.h \
    Interface/map.h \
    Interface/mapitem.h \
    Interface/groute.h \
    Interface/view.h \
    Ordonnanceur/processeur.h \
    Ordonnanceur/ordonnenceur.h \
    Processus/ProcessusManager.h \
    Simulateur/simulateur.h \
    Ressources/ressource.h \
    Processus/executionstep.h \
    Ordonnanceur/communication.h

SOURCES += \
    Simulateur/route.cpp \
    Simulateur/temperature.cpp \
    Simulateur/bris.cpp \
    Simulateur/conducteur.cpp \
    Simulateur/parametres.cpp \
    Simulateur/feucirculation.cpp \
    Simulateur/trafic.cpp \
    Processus/P01_AutoVerification.cpp \
    Processus/Processus.cpp \
    Processus/P13_SystemesElectriques.cpp \
    Processus/P12_SuiviDuChemin.cpp \
    Processus/P11_SecuriteB.cpp \
    Processus/P10_SecuriteA.cpp \
    Processus/P09_Messagerie.cpp \
    Processus/P08_GestionDesAppels.cpp \
    Processus/P07_EvitementDeCollision.cpp \
    Processus/P06_EnvoiDeBilan.cpp \
    Processus/P05_ControleDesEmissions.cpp \
    Processus/P04_Climat.cpp \
    Processus/P03_ChangementDeVitesse.cpp \
    Processus/P02_CalculDeChemin.cpp \
    Interface/mainwindow.cpp \
    Interface/loadfichierparametresform.cpp \
    main.cpp \
    Interface/map.cpp \
    Interface/mapitem.cpp \
    Interface/groute.cpp \
    Interface/view.cpp \
    Ordonnanceur/processeur.cpp \
    Ordonnanceur/ordonnenceur.cpp \
    Processus/ProcessusManager.cpp \
    Simulateur/simulateur.cpp \
    Ressources/ressource.cpp \
    Processus/executionstep.cpp \
    Ordonnanceur/communication.cpp

OTHER_FILES += \
    sptr-scenario.xml \
    Interface/SPTR5ConduiteSimulation.pro

FORMS += \
    Interface/mainwindow.ui \
    Interface/loadfichierparametresform.ui

RESOURCES += \
    Ressources.qrc
