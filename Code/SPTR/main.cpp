#include "Interface/mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    if(!w.isParametreLoaded())
        return -1000;

    //Parametres::getInstance()->printParameters();

    w.show();
    return a.exec();
}


//#define MAIN_TESTPARSER
//#ifdef MAIN_TESTPARSER

//#include <QtCore>
//#include <QtDebug>
//#include <Simulateur/xmlparser.h>
//#include <Simulateur/parametres.h>


//int main(int argc, char *argv[])
//{
//    QCoreApplication a(argc,argv);
//    XmlParser xmlParser;
//    bool test = xmlParser.readXml("../SPTR/sptr-scenario.xml");


//    Parametres::getInstance()->printParameters();

//    qDebug() << "Test value : " << test << endl;
//    qDebug() << "end" << endl;
//    return a.exec();
//}

//#endif
