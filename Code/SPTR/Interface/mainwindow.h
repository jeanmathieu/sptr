#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "Interface/loadfichierparametresform.h"
#include "Simulateur/parametres.h"
#include "Interface/map.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool LoadFichierParametres();

    bool isParametreLoaded(){return mParametreLoaded;}
private:

    void setAllDockWidgetToDefaultPosition();

    bool mParametreLoaded;
    Ui::MainWindow *ui;
    Map* mMapScene;
};

#endif // MAINWINDOW_H
