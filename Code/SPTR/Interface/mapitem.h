#ifndef MAPITEM_H
#define MAPITEM_H

#include <QGraphicsObject>

class MapItem : public QGraphicsObject
{
public:
    MapItem();
//    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
//    virtual QRectF boundingRect()const=0;


    const QPointF& getPosition()const {return mPosition;}
    void setPosition(const QPointF& inPosition){mPosition=inPosition;}
//    int getWidth()const{return mWidth;}
//    void setWidth(int inWidth){mWidth=inWidth;}
//    int getHeigth()const{return mHeight;}
//    void setHeigth(int inHeigth){mHeight=inHeigth;}
protected:
    QPointF mPosition;
    //unsigned int mWidth;
    //unsigned int mHeight;
    unsigned int mScaleFactor;
};

#endif // MAPITEM_H
