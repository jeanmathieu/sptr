#ifndef MAP_H
#define MAP_H

#include <QGraphicsScene>
#include <Interface/groute.h>
class Map : public QGraphicsScene
{
    Q_OBJECT
public:
    explicit Map(QObject *parent = 0);
    ~Map();
signals:
    
public slots:
private:
    QVector<GRoute*> mGRoutes;
};

#endif // MAP_H
