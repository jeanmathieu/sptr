#include "Interface/loadfichierparametresform.h"
#include "ui_loadfichierparametresform.h"
#include <QFileDialog>
#include "Simulateur/parametres.h"
#include "QMessageBox"

LoadFichierParametresForm::LoadFichierParametresForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadFichierParametresForm)
{
    ui->setupUi(this);
    initConnect();
    ui->cheminFichierParametreEdit->setText("C:/Users/Loic Tanon/Documents/Genie Informatique/GIF3003sysParallelesEtTpsReels/ProjetHiver2013/sptr-scenario.xml");
}

LoadFichierParametresForm::~LoadFichierParametresForm()
{
    delete ui;
}

void LoadFichierParametresForm::initConnect(){
    connect(ui->parcourirBtn,SIGNAL(clicked()),this,SLOT(onParcourirBtnClicked()));
    connect(ui->ChargerBtn,SIGNAL(clicked()),this,SLOT(onChargerBtnClicked()));
    connect(ui->annulerBtn,SIGNAL(clicked()),this,SLOT(onAnnulerBtnClicked()));
}

void LoadFichierParametresForm::onParcourirBtnClicked(){
   QString lPath=QFileDialog::getOpenFileName(this,tr("Choisir le fichier de parametrage"),"",tr("Fichier xml (*.xml)"));
   ui->cheminFichierParametreEdit->setText(lPath);
}

void LoadFichierParametresForm::onChargerBtnClicked(){
    Parametres* param_= Parametres::getInstance();
    if(param_->chargerFichierParametres(ui->cheminFichierParametreEdit->text()))
        this->accept();
    else{
        QMessageBox::critical(this,tr("Erreur"),tr("Le fichier choisi n'est pas valide"));
    }
}

void LoadFichierParametresForm::onAnnulerBtnClicked(){
    qDebug()<<"Annuler";
    this->reject();
}
