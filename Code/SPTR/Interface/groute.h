#ifndef GROUTE_H
#define GROUTE_H
#include <Interface/mapitem.h>
#include <Simulateur/route.h>

class GRoute : public MapItem
{
public:
    GRoute();
    GRoute(Route inRouteInfo);
    void setRouteInfo(Route inRoute){mRouteInfo=inRoute;}
    const Route& getRouteInfo()const{return mRouteInfo;}

    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual QRectF boundingRect()const;

    int getLength()const{return mLength;}
    void setLength(int inLength){mLength=inLength;}
private:
    Route mRouteInfo;
    unsigned int mLength;

};

#endif // GROUTE_H
