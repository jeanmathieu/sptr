#include "map.h"
#include <Simulateur/parametres.h>

Map::Map(QObject *parent) :
    QGraphicsScene(parent)
{
    foreach(Route route,Parametres::getInstance()->getRoutes()){
        GRoute* lGRoute= new GRoute(route);
        mGRoutes.push_back(lGRoute);
    }

    foreach(GRoute* gRoute, mGRoutes){
        qDebug()<<"route ID: "<<gRoute->getRouteInfo().getID();
        gRoute->setPos((gRoute->getRouteInfo().getXDebut()+ gRoute->getRouteInfo().getXFin())/2,
                       (gRoute->getRouteInfo().getYDebut()+ gRoute->getRouteInfo().getYFin())/2);
        addItem(gRoute);
    }
}
Map::~Map(){
    qDeleteAll(mGRoutes);
}
