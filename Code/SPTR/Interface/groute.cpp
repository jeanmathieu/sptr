#include "groute.h"
#include "QPainter"
GRoute::GRoute(Route inRouteInfo)
{
    setRouteInfo(inRouteInfo);
    setLength(inRouteInfo.getXDebut() - inRouteInfo.getXFin());
}
GRoute::GRoute(){

}

void GRoute::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget){

    painter->drawLine(mapToItem(this,mRouteInfo.getXDebut(),mRouteInfo.getYDebut()),
                      mapToItem(this,mRouteInfo.getXFin(),mRouteInfo.getYFin()));



    painter->drawPoint(mapToItem(this,pos().x(),pos().y()));
}

QRectF GRoute::boundingRect() const{
    return QRectF(-100,-100,100,100);
}
