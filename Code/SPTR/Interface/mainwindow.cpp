#include "Interface/mainwindow.h"
#include "ui_mainwindow.h"
#include "Interface/view.h"
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setAllDockWidgetToDefaultPosition();
    mParametreLoaded=LoadFichierParametres();
    if(mParametreLoaded)
        mMapScene=new Map();
    View *view = new View("Carte");
    view->view()->setScene(mMapScene);
    ui->centralWidget->layout()->addWidget(view);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete mMapScene;
}

bool MainWindow::LoadFichierParametres(){
    LoadFichierParametresForm LoadFichierParametresDialg(this);
    int lIsLoaded = LoadFichierParametresDialg.exec();
    return (lIsLoaded==QDialog::Accepted)?true:false;
}

void MainWindow::setAllDockWidgetToDefaultPosition(){
    addDockWidget(Qt::BottomDockWidgetArea,ui->ordonnancementDockWidget,Qt::Vertical);
    tabifyDockWidget(ui->parametresDockWidget,ui->simulationDockWidget);
    tabifyDockWidget(ui->simulationDockWidget,ui->statistiqueDockWidget);
    ui->parametresDockWidget->raise();
}
