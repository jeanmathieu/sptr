#-------------------------------------------------
#
# Project created by QtCreator 2013-02-17T23:24:06
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SPTR5ConduiteSimulation
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    parametres.cpp \
    loadfichierparametresform.cpp

HEADERS  += mainwindow.h \
    parametres.h \
    loadfichierparametresform.h

FORMS    += mainwindow.ui \
    loadfichierparametresform.ui
