#include "ordonnenceur.h"
#include "../Processus/ProcessusManager.h"
#include "./communication.h"
#include "../Simulateur/simulateur.h" //getCurrentTime()
#include <algorithm> //max

Ordonnenceur::Ordonnenceur() : mSchedulingStategy(RoundRobin)
{
}

Ordonnenceur::Ordonnenceur(std::list<Processus::ID> inIDs, schedulingStrategy inStrategy)
{
    mSchedulingStategy = inStrategy;

    std::list<Processus::ID>::iterator lIterator;
    std::list<Processus::ID> lIDs;
    for(lIterator=inIDs.begin();lIterator!=inIDs.end();lIterator++){if (!listContains(lIDs,*lIterator)){lIDs.push_back(*lIterator);}}//On evite l'insertion en double

    for(lIterator=lIDs.begin();lIterator!=lIDs.end();lIterator++)
    {
        int lID = *lIterator;
        mProcessus.push_back(ProcessusManager::getProcess(lID));
    }
}

Ordonnenceur& Ordonnenceur::addProcess(Processus *inProcessus)
{
    mProcessus.push_back(inProcessus);
    return *this;
}

Ordonnenceur& Ordonnenceur::setSchedulingStrategy(schedulingStrategy inStrategy)
{
    mSchedulingStategy = inStrategy;
    return *this;
}

bool Ordonnenceur::listContains(std::list<Processus *> &inList, Processus *inProcessus)
{
    std::list<Processus *>::iterator lListIterator = inList.begin();
    bool lContains = false;

    while(lListIterator != inList.end())
    {
        if (*lListIterator == inProcessus) return true;
        lListIterator++;
    }

    return lContains;
}

bool Ordonnenceur::listContains(std::list<Processus::ID> &inList, Processus::ID inID)
{
    std::list<Processus::ID>::iterator lListIterator = inList.begin();
    bool lContains = false;

    while(lListIterator != inList.end())
    {
        if (*lListIterator == inID) return true;
        lListIterator++;
    }

    return lContains;
}

void Ordonnenceur::updateStates()
{
    //TODO pour l'instant onplace tout les processus a ready
    int lSize = mProcessus.size();
    for(int i=0;i<lSize;i++)
    {
        mProcessus[i]->setState(Processus::ready);
    }
}

void Ordonnenceur::staticSchedule()
{
    switch(mSchedulingStategy)
    {
        case RoundRobin:
        {
         for (int unsigned i=0;i<mProcessus.size();i++)
            {
                Processus* lProcessus = mProcessus[i];

                switch (lProcessus->getState())
                {
                    case Processus::ready:
                    {
                    if (!(listContains(mReady,lProcessus)))
                        {
                            mReady.push_back(lProcessus);
                            mSuspended.remove(lProcessus);
                            mAsleep.remove(lProcessus);
                        }
                    }break;

                    case Processus::suspended:
                    {
                        if (!(listContains(mSuspended,lProcessus)))
                        {
                            mSuspended.push_back(lProcessus);
                            mReady.remove(lProcessus);
                            mAsleep.remove(lProcessus);
                        }
                    }break;

                    case Processus::asleep:
                    {
                        if (!(listContains(mAsleep,lProcessus)))
                        {
                            mAsleep.push_back(lProcessus);
                            mSuspended.remove(lProcessus);
                            mReady.remove(lProcessus);
                        }
                    }break;

                }
            }

        }break;

        case RateMonotonic:
        {
            mReady.clear();
            mSuspended.clear();
            mAsleep.clear();

            for (int unsigned i=0;i<mProcessus.size();i++)
            {
                Processus* lProcessus = mProcessus[i];
                if (lProcessus->getType() == Processus::periodic) lProcessus->setPriority(1000000 - lProcessus->getPeriod());
                else lProcessus->setPriority(1000000 - lProcessus->getMaxRunTime());

                switch(lProcessus->getState())
                {
                    case Processus::ready:
                        mReady.push_back(lProcessus);
                    break;
                    case Processus::suspended:
                        mSuspended.push_back(lProcessus);
                    break;
                    case Processus::asleep:
                        mAsleep.push_back(lProcessus);
                    break;
                }
            }

            //Colloection.sort(mReady) <- TODO


        }break;

        case RateMonotonicAdj:
        {
            mReady.clear();
            mSuspended.clear();
            mAsleep.clear();

            for (int unsigned i=0;i<mProcessus.size();i++)
            {
                Processus* lProcessus = mProcessus[i];
                if (lProcessus->getType() == Processus::periodic) lProcessus->setPriority(1000000 - lProcessus->getPeriod());
                else lProcessus->setPriority(1000000 - lProcessus->getMaxRunTime());

                switch(lProcessus->getState())
                {
                    case Processus::ready:
                        mReady.push_back(lProcessus);
                    break;
                    case Processus::suspended:
                    {
                        mSuspended.push_back(lProcessus);
                        ExecutionStep* lExecutionStep = lProcessus->getCurrentAction();
                            if (lExecutionStep->getType() == ExecutionStep::communication)
                            {
                              Communication* lCommunication = (Communication*)lExecutionStep;
                              if (lProcessus->getId() == lCommunication->getSender())
                              {
                                  Processus* lReceiver = ProcessusManager::getProcess(lCommunication->getReceiver());
                                  lReceiver->setPriority(max(lProcessus->getPriority(), lReceiver->getPriority()));
                              }
                              else if (lProcessus->getId()==lCommunication->getReceiver())
                              {
                                  Processus* lSender = ProcessusManager::getProcess(lCommunication->getSender());
                                  lSender->setPriority(max(lProcessus->getPriority(), lSender->getPriority()));
                              }
                            }
                            else if (lExecutionStep->getType()== ExecutionStep::ressource)
                            {
                                Ressource* lRessource = (Ressource*)lExecutionStep;
                                Processus* lCurrentUSer = ProcessusManager::getProcess(lRessource->getCurrentUser());
                                lCurrentUSer->setPriority(max(lCurrentUSer->getPriority(),lProcessus->getPriority()));
                            }
                    }
                    break;
                    case Processus::asleep:
                        mAsleep.push_back(lProcessus);
                    break;
                }

            }

        }break;
        //Colloection.sort(mReady) <- TODO

    }
}

void Ordonnenceur::dynamicSchedule()
{
    mReady.clear();
    mSuspended.clear();
    mAsleep.clear();

     switch(mSchedulingStategy)
     {
        case EarliestDeadline:
        {
             for (int unsigned i=0;i<mProcessus.size();i++)
             {
                 Processus* lProcessus = mProcessus[i];
                 lProcessus->setPriority(1000000 - (lProcessus->getEndConstraint() - Simulateur::getCurrentTime()));

                 switch(lProcessus->getState())
                 {
                     case Processus::ready:
                         mReady.push_back(lProcessus);
                     break;
                     case Processus::suspended:
                         mSuspended.push_back(lProcessus);
                     break;
                     case Processus::asleep:
                         mAsleep.push_back(lProcessus);
                     break;
                 }
             }

             //Collection.sort(mReady) <- TODO

        }break;

        case LeastSlack:
        {
             for (int unsigned i=0;i<mProcessus.size();i++)
             {
                 Processus* lProcessus = mProcessus[i];
                 lProcessus->setPriority(1000000 - (lProcessus->getEndConstraint() - Simulateur::getCurrentTime() - lProcessus->getRunTime()));

                 switch(lProcessus->getState())
                 {
                     case Processus::ready:
                         mReady.push_back(lProcessus);
                     break;
                     case Processus::suspended:
                         mSuspended.push_back(lProcessus);
                     break;
                     case Processus::asleep:
                         mAsleep.push_back(lProcessus);
                     break;
                 }

             }

              //Collection.sort(mReady) <- TODO

        }break;


     }


}

void Ordonnenceur::run()
{
    checkPeriodicity();
    updateStates();

    switch(mSchedulingStategy)
    {
        case RoundRobin:
        {
            staticSchedule();

            if (!mReady.size()==0)
            {
                std::list<Processus*>::iterator  lIterator = mReady.begin();

                Processus& lProcessus = **lIterator;

                lProcessus.setState(Processus::running);
                lProcessus.run();
                mReady.remove(*lIterator);
            }
        }break;

        case RateMonotonic:
        case RateMonotonicAdj:
        {
            staticSchedule();
            if (!mReady.size()==0)
            {
                std::list<Processus*>::iterator  lIterator = mReady.begin();
                Processus& lProcessus = **lIterator;
                lProcessus.setState(Processus::running);
                lProcessus.run();
            }
        }break;

        case EarliestDeadline:
        case LeastSlack:
        {
            dynamicSchedule();
            if (!mReady.size()==0)
            {
                std::list<Processus*>::iterator  lIterator = mReady.begin();
                Processus& lProcessus = **lIterator;
                lProcessus.setState(Processus::running);
                lProcessus.run();
            }
        }break;



    }

}



void Ordonnenceur::checkPeriodicity()
{
    for (int unsigned i=0;i<mProcessus.size();i++)
    {
        Processus* lProcessus = mProcessus[i];
        if (lProcessus->getType() == Processus::periodic)
        {
            if (Simulateur::getCurrentTime() % lProcessus->getPeriod() == 1)
            {
                if (lProcessus->getRunTime() != 0){}
                //else lProcessus->init(); //TODO <- A decider
            }
        }
    }
}
