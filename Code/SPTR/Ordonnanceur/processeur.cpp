#include "processeur.h"

Processeur::Processeur()
{
}

Processeur::Processeur(int inID,std::list<Processus::ID> inIDs, Ordonnenceur::schedulingStrategy inStrategy)
    : mOrdonnenceur(inIDs,inStrategy), mID(inID)
{
}

Processeur& Processeur::addProcess(Processus *inProcessus)
{
    mOrdonnenceur.addProcess(inProcessus);
    return *this;
}

Processeur& Processeur::setSchedulingStrategy(Ordonnenceur::schedulingStrategy inStrategy)
{
    mOrdonnenceur.setSchedulingStrategy(inStrategy);
    return *this;
}

void Processeur::run()
{
    mOrdonnenceur.run();
}

Ordonnenceur& Processeur::getScheduler()
{
    return mOrdonnenceur;
}


