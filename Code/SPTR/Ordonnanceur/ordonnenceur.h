#ifndef ORDONNENCEUR_H
#define ORDONNENCEUR_H

class Ordonnenceur;//Forward Declaration
#include <list>
#include <vector>
#include "../Processus/processus.h"
#include "../Ressources/ressource.h"

class Ordonnenceur
{
private:
    friend class OrdonnenceurTEST;
    bool listContains(std::list<Processus*>& inList, Processus* inProcessus);//Helper function
    bool listContains(std::list<Processus::ID>& inList, Processus::ID inID);//Helper function
    int mSchedulingStategy; //default:RoundRobin
    std::vector<Processus*> mProcessus;

    std::list<Processus*> mReady;
    std::list<Processus*> mSuspended;
    std::list<Processus*> mAsleep;

    void checkPeriodicity();
    void updateStates();
    void staticSchedule();
    void dynamicSchedule();

public:
    enum schedulingStrategy{RoundRobin,RateMonotonicAdj,RateMonotonic,EarliestDeadline,LeastSlack};
    Ordonnenceur();
    Ordonnenceur(std::list<Processus::ID> inIDs, schedulingStrategy inStrategy);
    Ordonnenceur& addProcess(Processus* inProcessus);
    Ordonnenceur& setSchedulingStrategy(schedulingStrategy inStrategy);

    void run();
};

#endif // ORDONNENCEUR_H
