#include "./OrdonnenceurTEST.h"

void OrdonnenceurTEST::doTest()
{
    constructeur_PID();

    std::string lTests;
    mItems.toString(lTests);
    cout << lTests;
}


void OrdonnenceurTEST::constructeur_PID()
{
    bool lPassed = true;
    std::string lName="Constructeur de l'ordonnenceur par des ID de processus, aucun doublons";

    std::list<Processus::ID> lProcessusIDs;
    lProcessusIDs.push_back(Processus::P01);
    lProcessusIDs.push_back(Processus::P02);
    lProcessusIDs.push_back(Processus::P03);
    lProcessusIDs.push_back(Processus::P01);//Ne devrait pas etre insere en double

    Ordonnenceur lOrdonnenceur(lProcessusIDs,Ordonnenceur::RoundRobin);
    int lSize = lOrdonnenceur.mProcessus.size();
    if (lSize != 3) lPassed = 0;

    mItems.add(lPassed,lName);
}

