#include "logitem.h"

LogItem::LogItem(bool inSucces, std::string inDescription): mDescription(inDescription), mSuccess(inSucces)
{
}

std::string LogItem::toString()
{
    std::string lOk;
    if (mSuccess) lOk = "Success: ";
    else          lOk = "Failure: ";

    return lOk + mDescription;
}
