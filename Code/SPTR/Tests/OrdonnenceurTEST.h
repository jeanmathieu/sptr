#ifndef _OrdonnenceurTEST_h_
#define _OrdonnenceurTEST_h_

#include "../Ordonnanceur/ordonnenceur.h"
#include "logitems.h"
#include <string>
#include "iostream"

class OrdonnenceurTEST
{
private:
    LogItems mItems;
    void constructeur_PID();
public:
    void doTest();

};

#endif
