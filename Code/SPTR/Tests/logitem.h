#ifndef LOGITEM_H
#define LOGITEM_H

#include <string>

class LogItem
{
private:
    bool mSuccess;
    std::string mDescription;

public:
    LogItem(bool inSucces, std::string inDescription);
    std::string toString();
};

#endif // LOGITEM_H
