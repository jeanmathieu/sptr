#include "logitems.h"

LogItems::LogItems()
{
}

void LogItems::add(bool inSucces, std::string inDescription)
{
    mItems.push_back(LogItem(inSucces,inDescription));
}

void LogItems::toString(std::string &outString)
{
    int lSize = mItems.size();

    for(int i=0;i<lSize;i++)
    {
        outString += mItems[i].toString() + "\n";
    }

}
