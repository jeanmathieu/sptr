#ifndef LOGITEMS_H
#define LOGITEMS_H

#include "./logitem.h"
#include <vector>

class LogItems
{
private:
    std::vector<LogItem> mItems;

public:
    LogItems();
    void add(bool inSucces, std::string inDescription);
    void toString(std::string& outString);


};

#endif // LOGITEMS_H
