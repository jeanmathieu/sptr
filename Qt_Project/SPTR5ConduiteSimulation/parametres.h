//By Dgtan*****************************************//
//*************************************************//
//*************************************************//
//*** Classe contenant tous les paramètres de la simulation
//*c-à-d ceux du fichier de specification (Xml)
//*C'est un singleton
//*************************************************//
//*************************************************//
#ifndef PARAMETRES_H
#define PARAMETRES_H
#include <QtCore>
#include <QString>
class Parametres
{
public:
    static Parametres* getInstance(){
        if (!paramInstance_) paramInstance_=new Parametres();
        return paramInstance_;
    }
    bool chargerFichierParametres(const QString &inCheminFichierParametres);
private:
    Parametres();
    static Parametres* paramInstance_;
};

#endif // PARAMETRES_H
