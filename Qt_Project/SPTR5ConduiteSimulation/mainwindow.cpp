#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),param_(0),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setAllDockWidgetToDefaultPosition();

}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::LoadFichierParametres(){
    LoadFichierParametresForm LoadFichierParametresDialg(this);
    int isLoaded = LoadFichierParametresDialg.exec();
    return (isLoaded==QDialog::Accepted)?true:false;
}

void MainWindow::setAllDockWidgetToDefaultPosition(){
    addDockWidget(Qt::BottomDockWidgetArea,ui->ordonnancementDockWidget,Qt::Vertical);
    tabifyDockWidget(ui->parametresDockWidget,ui->simulationDockWidget);
    tabifyDockWidget(ui->simulationDockWidget,ui->statistiqueDockWidget);
    ui->parametresDockWidget->raise();
}
