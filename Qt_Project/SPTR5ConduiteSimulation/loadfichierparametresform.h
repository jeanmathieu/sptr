#ifndef LOADFICHIERPARAMETRESFORM_H
#define LOADFICHIERPARAMETRESFORM_H

#include <QDialog>

namespace Ui {
class LoadFichierParametresForm;
}

class LoadFichierParametresForm : public QDialog
{
    Q_OBJECT
    
public:
    explicit LoadFichierParametresForm(QWidget *parent = 0);
    ~LoadFichierParametresForm();
private slots:
    void onParcourirBtnClicked();
    void onChargerBtnClicked();
    void onAnnulerBtnClicked();
private:
    void initConnect();
    Ui::LoadFichierParametresForm *ui;
};

#endif // LOADFICHIERPARAMETRESFORM_H
