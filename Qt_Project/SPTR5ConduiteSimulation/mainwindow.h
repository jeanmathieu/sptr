#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "loadfichierparametresform.h"
#include "parametres.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool LoadFichierParametres();


private:

    void setAllDockWidgetToDefaultPosition();

    Ui::MainWindow *ui;
    Parametres* param_;

};

#endif // MAINWINDOW_H
