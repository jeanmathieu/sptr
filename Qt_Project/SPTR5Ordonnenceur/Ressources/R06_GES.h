#ifndef __R06_GES_h__
#define __R06_GES_h__

#include "ressource.h"

class R06_GES: public Ressource
{
private:
    friend class RessourceManager;
    R06_GES()
    {
        mID = Ressource::R07;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R06_GES();}
        return *mInstance();
    }

};


#endif

