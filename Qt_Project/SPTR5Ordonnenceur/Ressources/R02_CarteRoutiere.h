#ifndef __R02_CarteRoutiere_h__
#define __R02_CarteRoutiere_h__

#include "ressource.h"

class R02_CarteRoutiere: public Ressource
{
private:
    friend class RessourceManager;
    R02_CarteRoutiere()
    {
        mID = Ressource::R02;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R02_CarteRoutiere();}
        return *mInstance();
    }

};


#endif
