#ifndef __R05_Environnement_h__
#define __R05_Environnement_h__

#include "ressource.h"

class R05_Environnement: public Ressource
{
private:
    friend class RessourceManager;
    R05_Environnement()
    {
        mID = Ressource::R05;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R05_Environnement();}
        return *mInstance();
    }

};


#endif
