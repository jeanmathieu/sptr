#ifndef __R03_CompteurVitesse_h__
#define __R03_CompteurVitesse_h__

#include "ressource.h"

class R03_CompteurVitesse: public Ressource
{
private:
    friend class RessourceManager;
    R03_CompteurVitesse()
    {
        mID = Ressource::R03;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R03_CompteurVitesse();}
        return *mInstance();
    }

};


#endif
