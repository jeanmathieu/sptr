#ifndef __RessourceManager_h__
#define __RessourceManager_h__

class RessourceManager;//Forward Declaration
#include "ressource.h"
#include "../Processus/processus.h"
#include <list>
#include <map>

#include "R01_CarteGraphique.h"
#include "R02_CarteRoutiere.h"
#include "R03_CompteurVitesse.h"
#include "R04_ModuleAffichage.h"
#include "R05_Environnement.h"
#include "R06_GES.h"
#include "R07_GPS.h"
#include "R08_Odometre.h"
#include "R09_Radar.h"
#include "R10_RegulateurVitesse.h"
#include "R11_StationMeteo.h"
#include "R12_Telecom.h"


class RessourceManager
{
protected:
    //static std::map<Ressource::ID, Ressource*> mRessources;
    static std::map<Ressource::ID, Ressource*>& mRessources()
    {
        static std::map<Ressource::ID, Ressource*> _mRessources;
        return _mRessources;
    }

public:
    static void initResources()
    {
        mRessources()[Ressource::R01] = R01_CarteGraphique::getInstance();
        mRessources()[Ressource::R02] = R02_CarteRoutiere::getInstance();
        mRessources()[Ressource::R03] = R03_CompteurVitesse::getInstance();
        mRessources()[Ressource::R04] = R04_ModuleAffichage::getInstance();
        mRessources()[Ressource::R05] = R05_Environnement::getInstance();
        mRessources()[Ressource::R06] = R06_GES::getInstance();
        mRessources()[Ressource::R07] = R07_GPS::getInstance();
        mRessources()[Ressource::R08] = R08_Odometre::getInstance();
        mRessources()[Ressource::R09] = R09_Radar::getInstance();
        mRessources()[Ressource::R10] = R10_RegulateurVitesse::getInstance();
        mRessources()[Ressource::R11] = R11_StationMeteo::getInstance();
        mRessources()[Ressource::R12] = R12_Telecom::getInstance();
        mRessources()[Ressource::R12] = R12_Telecom::getInstance();
    }

    static Ressource* getResource(Ressource::ID inID)
    {
        return mRessources()[inID];
    }

    static Ressource* getResource(Ressource::ID inID, int inLeaseTime)
    {
        Ressource* lRessource = mRessources()[inID];
        lRessource->setLeaseTime(inLeaseTime);
        return lRessource;
    }

    static std::list<Ressource::State> getResourcesStates()
    {
        std::list<Ressource::State> lStates;
        std::map<Ressource::ID, Ressource*>::const_iterator lIterator;
        for(lIterator =  mRessources().begin();lIterator!=mRessources().end();lIterator++)
        {
            lStates.push_back((Ressource::State) (*lIterator).second->getState());
        }

        return lStates;
    }

    static bool isBusy(Ressource::ID inID)
    {
        Ressource* lRessource = mRessources()[inID];
        return (lRessource->getState() == Ressource::BUSY);
    }


    static std::list<int> getResourceUsers(Ressource::ID inID)
    {
        Ressource* lRessource = mRessources()[inID];
        return lRessource->getUsers();
    }

    static int getResourceCurrentUser(Ressource::ID inID)
    {
        Ressource* lRessource = mRessources()[inID];
        return lRessource->getCurrentUser();
    }

    static int useResource(Ressource::ID inID)
    {
        Ressource* lRessource = mRessources()[inID];
        return lRessource->useResource();
    }

    static void freeResource(Ressource::ID inID)
    {
        Ressource* lRessource = mRessources()[inID];
        lRessource->free();
    }

    static void clearFreeResources()
    {
        std::map<Ressource::ID, Ressource*>::const_iterator lIterator;
        for(lIterator =  mRessources().begin();lIterator!=mRessources().end();lIterator++)
        {
            Ressource* lRessource = (*lIterator).second;
            if (lRessource->getBusyTimeLeft()==0) freeResource((Ressource::ID)lRessource->getId());
        }
    }


    static void reserveResource(Ressource::ID inRessourceID, int inProcessusID) {
        Ressource* lRessource = getResource(inRessourceID);
        lRessource->reserve(inProcessusID);
    }


 /*
    static Ressource* getResource(Ressource::ID inID);
    static Ressource* getResource(Ressource::ID inID, int inLeaseTime);
    static std::list<Ressource::State> getResourcesStates();
    static bool isBusy(Ressource::ID inID);
    static std::list<int> getResourceUsers(Ressource::ID inID);
    static int getResourceCurrentUser(Ressource::ID inID);
    static int useResource(Ressource::ID inID);
    static void freeResource(Ressource::ID inID);
    static void clearFreeResources();
    static void reserveResource(Ressource::ID inRessourceID, int inProcessusID);
    */
};

#endif
