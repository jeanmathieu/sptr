#ifndef __R04_ModuleAffichage_h__
#define __R04_ModuleAffichage_h__

#include "ressource.h"

class R04_ModuleAffichage: public Ressource
{
private:
    friend class RessourceManager;
    R04_ModuleAffichage()
    {
        mID = Ressource::R04;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R04_ModuleAffichage();}
        return *mInstance();
    }

};


#endif
