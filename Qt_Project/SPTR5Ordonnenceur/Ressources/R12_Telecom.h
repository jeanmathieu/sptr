#ifndef __R12_Telecom_h__
#define __R12_Telecom_h__

#include "ressource.h"

class R12_Telecom: public Ressource
{
private:
    friend class RessourceManager;
    R12_Telecom()
    {
        mID = Ressource::R12;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R12_Telecom();}
        return *mInstance();
    }

};


#endif
