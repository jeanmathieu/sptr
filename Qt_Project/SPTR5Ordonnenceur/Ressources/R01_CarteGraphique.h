#ifndef __R01_CarteGraphique_h__
#define __R01_CarteGraphique_h__

#include "ressource.h"

class R01_CarteGraphique: public Ressource
{
private:
    friend class RessourceManager;
    R01_CarteGraphique()
    {
        mID = Ressource::R01;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R01_CarteGraphique();}
        return *mInstance();
    }

};

#endif
