#ifndef __R08_Odometre_h__
#define __R08_Odometre_h__

#include "ressource.h"

class R08_Odometre: public Ressource
{
private:
    friend class RessourceManager;
    R08_Odometre()
    {
        mID = Ressource::R08;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R08_Odometre();}
        return *mInstance();
    }

};


#endif
