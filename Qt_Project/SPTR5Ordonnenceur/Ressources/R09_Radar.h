#ifndef __R09_Radar_h__
#define __R09_Radar_h__

#include "ressource.h"

class R09_Radar: public Ressource
{
private:
    friend class RessourceManager;
    R09_Radar()
    {
        mID = Ressource::R09;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R09_Radar();}
        return *mInstance();
    }

};


#endif
