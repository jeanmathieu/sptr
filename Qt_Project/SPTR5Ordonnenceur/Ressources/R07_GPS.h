#ifndef __R07_GPS_h__
#define __R07_GPS_h__

#include "ressource.h"

class R07_GPS: public Ressource
{
private:
    friend class RessourceManager;
    R07_GPS()
    {
        mID = Ressource::R07;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R07_GPS();}
        return *mInstance();
    }

};


#endif
