#include "ressourcemanager.h"


/*
Ressource* RessourceManager::getResource(Ressource::ID inID)
{
    return mRessources[inID];
}

Ressource* RessourceManager::getResource(Ressource::ID inID, int inLeaseTime)
{
    Ressource* lRessource = mRessources[inID];
    lRessource->setLeaseTime(inLeaseTime);
    return lRessource;
}

std::list<Ressource::State> RessourceManager::getResourcesStates()
{
    std::list<Ressource::State> lStates;
    std::map<Ressource::ID, Ressource*>::const_iterator lIterator;
    for(lIterator =  mRessources.begin();lIterator!=mRessources.end();lIterator++)
    {
        lStates.push_back((Ressource::State) (*lIterator).second->getState());
    }

    return lStates;
}

bool RessourceManager::isBusy(Ressource::ID inID)
{
    Ressource* lRessource = mRessources[inID];
    return (lRessource->getState() == Ressource::BUSY);
}

std::list<Processus::ID> RessourceManager::getResourceUsers(Ressource::ID inID)
{
    Ressource* lRessource = mRessources[inID];
    return lRessource->getUsers();
}

Processus::ID RessourceManager::getResourceCurrentUser(Ressource::ID inID)
{
    Ressource* lRessource = mRessources[inID];
    return (Processus::ID) lRessource->getCurrentUser();
}

int RessourceManager::useResource(Ressource::ID inID)
{
    Ressource* lRessource = mRessources[inID];
    return lRessource->useResource();
}

void RessourceManager::freeResource(Ressource::ID inID)
{
    Ressource* lRessource = mRessources[inID];
    lRessource->free();
}

void RessourceManager::clearFreeResources()
{
    std::map<Ressource::ID, Ressource*>::const_iterator lIterator;
    for(lIterator =  mRessources.begin();lIterator!=mRessources.end();lIterator++)
    {
        Ressource* lRessource = (*lIterator).second;
        if (lRessource->getBusyTimeLeft()==0) freeResource((Ressource::ID)lRessource->getId());
    }
}

void RessourceManager::reserveResource(Ressource::ID inRessourceID, Processus::ID inProcessusID) {
    Ressource* lRessource = getResource(inRessourceID);
    lRessource->reserve(inProcessusID);
}
*/
