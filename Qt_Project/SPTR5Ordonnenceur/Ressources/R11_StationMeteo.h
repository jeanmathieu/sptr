#ifndef __R11_StationMeteo_h__
#define __R11_StationMeteo_h__

#include "ressource.h"

class R11_StationMeteo: public Ressource
{
private:
    friend class RessourceManager;
    R11_StationMeteo()
    {
        mID = Ressource::R11;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R11_StationMeteo();}
        return *mInstance();
    }

};


#endif
