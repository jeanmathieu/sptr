#ifndef __R10_RegulateurVitesse_h__
#define __R10_RegulateurVitesse_h__

#include "ressource.h"

class R10_RegulateurVitesse: public Ressource
{
private:
    friend class RessourceManager;
    R10_RegulateurVitesse()
    {
        mID = Ressource::R10;
    }

    static Ressource** mInstance()
    {
        static Ressource* _mInstance = 0;
        return &_mInstance;
    }

protected:
    static Ressource* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new R10_RegulateurVitesse();}
        return *mInstance();
    }

};


#endif
