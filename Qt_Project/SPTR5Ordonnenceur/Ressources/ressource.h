#ifndef RESSOURCE_H
#define RESSOURCE_H

class Ressource;//Forward Declaration
//#include "../Processus/processus.h"
#include "../Processus/executionstep.h"
#include <string>
#include <list>
#include <sstream>
#include <iomanip>

class Ressource : public ExecutionStep
{
protected:
    friend class RessourceManager;
    int mID;
    std::string mName;
    std::list<int> mUsers;//Processus::ID
    int mEtat;
    int mCurrentUser;//Processus::ID
    int mLeaseTime;
    int mBusyTimeLeft;
    void free();
    void reserve(int inCurrentUser);//Processus::ID

public:
    Ressource();
    enum ID{R01=1, R02, R03, R04, R05, R06, R07, R08, R09, R10, R11, R12};
    enum State{BUSY=1, FREE};
    std::string toString();
    int useResource();
    int getId();//Ressource::ID
    std::string getName();
    void setUsers(std::list<int> inUsers);
    std::list<int> getUsers();
    void setState(State inState);
    int getState();//Ressource::State
    int getCurrentUser();//Processus::ID
    void setLeaseTime(int inLeaseTime);
    int getLeaseTime();
    void setBusyTimeLeft(int inBusyTimeLeft);
    int getBusyTimeLeft();


};

#endif // RESSOURCE_H
