#include "simulateur.h"

Simulateur::Simulateur()
{
    RessourceManager::initResources();
}

Simulateur* Simulateur::getInstance()
{
    if (*mInstance() == 0){ *mInstance() = new Simulateur();}
    return *mInstance();
}


int& Simulateur::mCurrentTime()//equivalent(mais impossible) a:    static int mCurrentTime=1;
   {
       static int _mCurrentTime=1;
       return _mCurrentTime;
   }


Simulateur& Simulateur::addProcessor(Processeur *inProcesseur)
{
    mProcesseur.push_back(inProcesseur);
    return *this;
}

void Simulateur::run()
{
    int lSize = mProcesseur.size();
    for (int i=0; i<lSize;i++)
    {
        mProcesseur[i]->run();
    }

    incrementTime();
}
