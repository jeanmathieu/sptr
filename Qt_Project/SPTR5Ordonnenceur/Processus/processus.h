#ifndef PROCESSUS_H
#define PROCESSUS_H

//class Processus;//Forward Declaration
#include "executionstep.h"
#include "../Ordonnanceur/communicationobserver.h"
#include <stack>
#include <string>
#include <list>

class Processus : public CommunicationObserver
{
protected:
    friend class Ordonnenceur;
    friend class ProcessusManager;
    int mID;
    int mEtatProc;
    int mType;
    //int T;
    int c;
    int d;
    std::string mName;
    std::list<int> mUsedRessources;
    ExecutionStep* mCurrentAction;
    std::stack<ExecutionStep*> mExecutionSteps;
    std::list<CommunicationObserver*> mCommunicationListeners;
    std::list<int> mCommunicationReceived;
    std::list<int> mWakeUpCommunications;
    int mStartTime;
    int mMaxRunTime;
    int mEndConstraint;
    int mPriority;

public:
    enum ID{P01=1, P02, P03, P04, P05, P06, P07, P08, P09, P10, P11, P12, P13};
    enum state{running,ready,suspended,asleep};
    enum type{periodic,sporadic};
    Processus();
    Processus(ID inID);
    void run();
    void init();
    ExecutionStep* peekNextAction();
    void fireCommunicate(Communication* inCommunication);
    virtual void communicate(Communication* inCommunication);
    virtual std::string toString();
    void addCommunicationListener(CommunicationObserver* inCommunicationObserver);
    void setName(std::string inName);
    std::string getName();
    int getId(){return mID;}
    void setId(ID inID){mID = inID;}
    int getType(){return mType;}
    void setType(type inType){mType = inType;}
    void setState(state inState);
    int getState(){return mEtatProc;}
    std::list<int> getUsedRessources(){return mUsedRessources;}
    void setUsedRessources(std::list<int> inUsedRessources) {mUsedRessources = inUsedRessources;}
    std::stack<ExecutionStep*> getSequence(){return mExecutionSteps;}
    int getRunTime() {return mExecutionSteps.size();	}
    int getEndConstraint() {return mEndConstraint;}
    void setEndConstraint(int inEndConstraint) {mEndConstraint = inEndConstraint;}
    int getPriority() {return mPriority;}
    void setPriority(int inPriority) {mPriority = inPriority;}
    std::list<int> getCommunicationReceived() {return mCommunicationReceived;}
    void setCommunicationReceived(std::list<int> inCommunicationReceived){mCommunicationReceived = inCommunicationReceived;}
    void setSequence(std::stack<ExecutionStep*> inSequence) {mExecutionSteps = inSequence;}
    ExecutionStep* getCurrentAction() {	return mCurrentAction;}
    std::list<int> getWakeUpCommunications() {return mWakeUpCommunications;}
    void setWakeUpCommunications(std::list<int> inWakeUpCommunications) {mWakeUpCommunications = inWakeUpCommunications;}
    void setCurrentAction(ExecutionStep* inCurrentAction) {mCurrentAction = inCurrentAction;}
    //public int compareTo(Object obj)
    int getStartTime() {return mStartTime;}
    void setStartTime(int inStartTime) {mStartTime = inStartTime;}
    int getMaxRunTime() {return mMaxRunTime;}
    void setMaxRunTime(int inMaxRunTime) {mMaxRunTime = inMaxRunTime;}

};


#endif // PROCESSUS_H
