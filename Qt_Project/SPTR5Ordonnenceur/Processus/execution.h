#ifndef __Execution_h__
#define __Execution_h__

class Execution;//Forward Declaration
#include "executionstep.h"
#include <string>
#include <sstream>
#include <iomanip>

class Execution: public ExecutionStep
{
private:
    int mBusyTimeLeft;

public:
    Execution(int inC);
    int execute();
    std::string toString();
    void setBusyTimeLeft(int inBusyTimeLeft);
    int getBusyTimeLeft();
};

#endif
