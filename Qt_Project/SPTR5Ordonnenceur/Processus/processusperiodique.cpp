#include "processusperiodique.h"

ProcessusPeriodique::ProcessusPeriodique()
{
    mType = periodic;
}

int ProcessusPeriodique::getPeriod()
{
    return mPeriode;
}

void ProcessusPeriodique::setPeriod(int inPeriode)
{
    mPeriode = inPeriode;
}

void init()
{
    //Processus::init();
}
