#include "P01_AutoVerification.h"
#include "../Simulateur/simulateur.h"

P01_AutoVerification::P01_AutoVerification(void)
{
    mBris = false;
    mAutoReparation = 0;
    setName("P01_AutoVerification");
    setPeriod(25);
    setMaxRunTime(getPeriod());
    //Simulateur::getInstance()->addThickListener(this)  //TODO
}

void P01_AutoVerification::init()
{
    //ProcessusPeriodique::init(); //TODO

    std::stack<ExecutionStep*> lExecutionSteps; // R05 E C01 R08 E
    lExecutionSteps.push(new Execution(1));
    lExecutionSteps.push(RessourceManager::getResource(Ressource::R08, 1));
    lExecutionSteps.push(new Communication(Communication::C01, 1));
    lExecutionSteps.push(new Execution(1));
    lExecutionSteps.push(RessourceManager::getResource(Ressource::R05, 1));
    setSequence(lExecutionSteps);
}

P01_AutoVerification::~P01_AutoVerification(void)
{
}
