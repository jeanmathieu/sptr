#include <exception>
using namespace std;

#ifndef __ProcessusManager_h__
#define __ProcessusManager_h__

//class ProcessusManager;//Forward Declaration
#include "./processus.h"
#include "../Ressources/ressource.h"
#include <map>
#include "P01_AutoVerification.h"
#include "P02_CalculDeChemin.h"
#include "P03_ChangementDeVitesse.h"
#include "P04_Climat.h"
#include "P05_ControleDesEmissions.h"
#include "P06_EnvoiDeBilan.h"
#include "P07_EvitementDeCollision.h"
#include "P08_GestionDesAppels.h"
#include "P09_Messagerie.h"
#include "P10_SecuriteA.h"
#include "P11_SecuriteB.h"
#include "P12_SuiviDuChemin.h"
#include "P13_SystemesElectriques.h"

class ProcessusManager
{
private:
    static std::map<int, Processus*>& mProcessus()//static std::map<int, Processus*> mProcessus;
    {
        static std::map<int, Processus*> _mProcessus;
        return _mProcessus;
    }

public:
    static void initProcesses()
    {
        mProcessus()[Processus::P01] = P01_AutoVerification::getInstance();
        mProcessus()[Processus::P02] = P02_CalculDeChemin::getInstance();
        mProcessus()[Processus::P03] = P03_ChangementDeVitesse::getInstance();
        mProcessus()[Processus::P04] = P04_Climat::getInstance();
        mProcessus()[Processus::P05] = P05_ControleDesEmissions::getInstance();
        mProcessus()[Processus::P06] = P06_EnvoiDeBilan::getInstance();
        mProcessus()[Processus::P07] = P07_EvitementDeCollision::getInstance();
        mProcessus()[Processus::P08] = P08_GestionDesAppels::getInstance();
        mProcessus()[Processus::P09] = P09_Messagerie::getInstance();
        mProcessus()[Processus::P10] = P10_SecuriteA::getInstance();
        mProcessus()[Processus::P11] = P11_SecuriteB::getInstance();
        mProcessus()[Processus::P12] = P12_SuiviDuChemin::getInstance();
        mProcessus()[Processus::P13] = P13_SystemesElectriques::getInstance();

        std::map<int, Processus*>::const_iterator lIterator;
        for(lIterator =  mProcessus().begin();lIterator!=mProcessus().end();lIterator++)
        {
            initWakeUpCommunications( (*lIterator).second );
        }
    }

    static void initWakeUpCommunications(Processus* inProcessus) //TODO
    {
        /*
        std::list<int> lMessageSenders = SimulatorConstants.MESSAGE_SENDER_PER_PROCESSUS.get(p.getId());
        if (messageSenders != null)
        {
            for(ID sender : messageSenders)
            {
                ProcessManager.getProcess(sender).addCommunicationListener(p);
                System.out.print(p.getId() + " is listening to " + sender + "\n");
            }
        }
        */
    }

    static Processus* getProcess(int inID)
    {
        return mProcessus()[inID];
    }

    static std::map<int, Processus::state> getProcessesStates()
    {
        std::map<int, Processus::state> lStates;
        std::map<int, Processus*>::const_iterator lIterator;
        for(lIterator =  mProcessus().begin();lIterator!=mProcessus().end();lIterator++)
        {
            lStates[(*lIterator).first] = (Processus::state)(*lIterator).second->getState();
        }

        return lStates;


    }

    static std::map<int, int> getProcessesResources()
    {
        std::map<int, int> lRessources;
        std::map<int, Processus*>::const_iterator lIterator;
        for(lIterator =  mProcessus().begin();lIterator!=mProcessus().end();lIterator++)
        {
            int lID = 0;
            ExecutionStep* lExecutionStep =  (*lIterator).second->getCurrentAction();
            if (lExecutionStep->getType()==ExecutionStep::ressource) lID = ((Ressource*)lExecutionStep)->getId();

            lRessources[(*lIterator).first] = lID;
        }

        return lRessources;
    }

    static bool isReady(int inID)
    {
        if (inID>13) return Processus::asleep;
        return getProcess(inID)->getState();
    }
};


#endif
