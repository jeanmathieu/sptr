#ifndef _P01_AutoVerification_h__
#define _P01_AutoVerification_h__

//#include "processus.h"
#include "processusperiodique.h"
//#include "R05_Environnement"
//#include "R08_Odometre"
#include "executionstep.h"
#include "execution.h"
#include "../Ressources/ressourcemanager.h"
#include "../Ressources/ressource.h"
#include "../Ordonnanceur/communication.h"


class P01_AutoVerification : public ProcessusPeriodique
{
public:
    P01_AutoVerification(void);
	~P01_AutoVerification(void);
	void DropSpeed(bool broken, int time);

private:
    bool mBris;
	int mAutoReparation;
    //list<QString> mExecutionOrder;

    void init();

    static Processus** mInstance()
    {
        static Processus* _mInstance = 0;
        return &_mInstance;
    }

public:
    static Processus* getInstance()
    {
        if (*mInstance() == 0){ *mInstance() = new P01_AutoVerification();}
        return *mInstance();
    }

};

#endif

