#include "processus.h"
#include "../Simulateur/simulateur.h"
#include "../Ressources/ressourcemanager.h"
#include "../Ressources/ressource.h"
#include "../Ordonnanceur/communication.h"
//#include <QtCore>
#include "../Ordonnanceur/path.h"
#include "../Ordonnanceur/PathManager.h"
#include "execution.h"
#include <algorithm>
using namespace std;


Processus::Processus() : mEtatProc(ready)
{
}

Processus::Processus(ID inID) : mEtatProc(ready)
{
    mID = inID;
}

void Processus::run()
{
    if (!mExecutionSteps.size()){mCurrentAction =0;return;}

    mCurrentAction = mExecutionSteps.top();

    if (mCurrentAction->getType() == ExecutionStep::communication)
    {
        if ( ((Communication*)mCurrentAction)->getSender() == this->getId() )
        {
            Path::ID lPathID = (Path::ID)((Communication*)mCurrentAction)->getDirectPath();
            if (PathManager::isFree(lPathID)) PathManager::takePath(lPathID,(Communication*)mCurrentAction,((Communication*)mCurrentAction)->getDimension());
            if (PathManager::usePath(lPathID) < 1){mExecutionSteps.pop();}
        }
        else if( ((Communication*) mCurrentAction)->getReceiver() == this->getId()) std::string lError="";
    }

    if (mCurrentAction->getType() == ExecutionStep::execution)
    {
        if( ((Execution*) mCurrentAction)->execute() < 1)
        {
            mExecutionSteps.pop();
        }
    }
    else if (mCurrentAction->getType() == ExecutionStep::ressource)
    {
        Ressource::ID lRessourceId = (Ressource::ID)((Ressource*) mCurrentAction)->getId();
        if (RessourceManager::isBusy(lRessourceId)) this->setState(suspended);
        else
        {
            RessourceManager::reserveResource(lRessourceId,(Processus::ID)this->mID);
            if(RessourceManager::useResource(lRessourceId)<1)
            {
                mExecutionSteps.pop();
            }
        }
    }
}

void Processus::init()
{
    setStartTime(Simulateur::getInstance()->getCurrentTime());
    setEndConstraint(mStartTime+mMaxRunTime);
}

ExecutionStep* Processus::peekNextAction()
{
    return mExecutionSteps.top();
}

void Processus::fireCommunicate(Communication* inCommunication)
{
    std::list<CommunicationObserver*>::iterator lIterator;

    //mCommunicationListeners
    for (lIterator = mCommunicationListeners.begin();lIterator!=mCommunicationListeners.end();lIterator++)
    {
        (*lIterator)->communicate(inCommunication);
    }
}

void Processus::communicate(Communication* inCommunication)
{
    if (inCommunication->getReceiver() == mID) {
        //System.out.println(this.id + " received " + inCommunication.getId() + " from " + inCommunication.getSender());
        mCommunicationReceived.push_back(inCommunication->getId());

    }
    if (mWakeUpCommunications.size() != 0)
    {
        //is inCommunication.getId() in mWakeUpCommunications
        std::list<int>::iterator lIterator = std::find(mWakeUpCommunications.begin(), mWakeUpCommunications.end(), inCommunication->getId());

        if(lIterator !=mWakeUpCommunications.end())
        {
            //System.out.println(this.id + " received [wake-up communicaiton] " + inCommunication.getId() + " from " + inCommunication.getSender());
            this->init();
        }
    }
}

std::string Processus::toString()
{
    std::string lString = "";
    if(mCurrentAction != 0) lString = getCurrentAction()->toString();
    return lString;
}

void Processus::addCommunicationListener(CommunicationObserver* inCommunicationObserver)
{
    mCommunicationListeners.push_back(inCommunicationObserver);
}

void Processus::setName(std::string inName)
{
    mName = inName;
}

std::string Processus::getName()
{
    return mName;
}




void Processus::setState(Processus::state inState)
{
    mEtatProc = inState;
}
