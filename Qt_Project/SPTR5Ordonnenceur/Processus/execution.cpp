#include "execution.h"

Execution::Execution(int inC)
{
    setBusyTimeLeft(inC);
    setType(ExecutionStep::execution);
}

int Execution::execute()
{
    mBusyTimeLeft--;
    return mBusyTimeLeft;
}

std::string Execution::toString()
{
    return "Execution";
}

void Execution::setBusyTimeLeft(int inBusyTimeLeft)
{
    mBusyTimeLeft = inBusyTimeLeft;
}

int Execution::getBusyTimeLeft()
{
    return mBusyTimeLeft;
}

