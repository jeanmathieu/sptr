#ifndef PROCESSUSPERIODIQUE_H
#define PROCESSUSPERIODIQUE_H

//class ProcessusPeriodique;//Forward Declaration
#include "processus.h"

class ProcessusPeriodique : public Processus
{
private:
    int mPeriode;

public:
    ProcessusPeriodique();
    int getPeriod();
    void setPeriod(int inPeriode);
    void init();
};

#endif // PROCESSUSPERIODIQUE_H
