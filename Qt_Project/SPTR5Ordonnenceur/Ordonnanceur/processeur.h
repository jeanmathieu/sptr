#ifndef PROCESSEUR_H
#define PROCESSEUR_H

class Processeur;//Forward declaration
#include "../Processus/processus.h"
#include "ordonnenceur.h"

class Processeur
{
protected:
    int mID;
    Ordonnenceur mOrdonnenceur;

public:
    Processeur();
    Processeur& addProcess(Processus* inProcessus);
    Processeur& setSchedulingStrategy(Ordonnenceur::schedulingStrategy inStrategy);
    Ordonnenceur& getScheduler();
    void run();
};

#endif // PROCESSEUR_H
