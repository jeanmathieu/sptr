#ifndef __PathManager_h__
#define __PathManager_h__

class PathManager;//Forward declaration
#include "Path.h"
#include "Communication.h"
#include <map>

class PathManager
{
private: 
    static std::map<Path::ID, Path*>& mPaths()
    {
        static std::map<Path::ID, Path*> _mPaths;
        return _mPaths;
    }

    static Path& mPath1()
    {
        static Path _mPath1 =  Path(Path::PATH1,Path::FREE);
        return _mPath1;
    }

    static Path& mPath2()
    {
        static Path _mPath2 =  Path(Path::PATH2,Path::FREE);
        return _mPath2;
    }
    static Path& mPath3()
    {
        static Path _mPath3 =  Path(Path::PATH3,Path::FREE);
        return _mPath3;
    }

    static Path& mLocal()
    {
        static Path _mLocal =  Path(Path::LOCAL,Path::FREE);
        return _mLocal;
    }

		
public:
	static void initPaths() 
	{
        mPaths()[Path::PATH1] = &mPath1();
        mPaths()[Path::PATH2] = &mPath2();
        mPaths()[Path::PATH3] = &mPath3();
        mPaths()[Path::LOCAL] = &mLocal();
	}

    static Path* getPath(Path::ID inID){return mPaths()[inID];}
    static bool isFree(Path::ID inID){return mPaths()[inID]->getState() == Path::FREE;}
    static Communication* getCurrentCommunication(Path::ID inID){ return getPath(inID)->getCurrentCommunication();}

    static void takePath(Path::ID inPathID, Communication* inCommunication, int inLeaseTime)
	{
        Path* lPath = getPath(inPathID);
        lPath->setCurrentCommunication(inCommunication);
        lPath->setState(Path::BUSY);
        lPath->setBusyTimeLeft(inLeaseTime);
	}

    static int usePath(Path::ID inID)
	{
        return getPath(inID)->usePath();
	}

    static void freePath(Path::ID inID)
	{
        getPath(inID)->free();
	}

	static void clearFreePaths() 
	{
       std::map<Path::ID, Path*>::iterator lIterator;
       for(lIterator = mPaths().begin();lIterator!=mPaths().end();lIterator++)
       {
           int lID = (*lIterator).first;
           if (getPath((Path::ID)lID)->getBusyTimeLeft() == 0)
           {
               freePath((Path::ID)lID);
           }
       }

	}

};
#endif
