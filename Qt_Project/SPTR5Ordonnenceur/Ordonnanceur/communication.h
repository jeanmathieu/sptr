#include <exception>
using namespace std;

#ifndef __Communication_h__
#define __Communication_h__

class Communication;//Forward Declaration
//#include "path.h"
#include "../Processus/processus.h"
#include "../Processus/executionstep.h"
#include <string>

class Communication: public ExecutionStep
{	
private:
    friend class Path;
    int mID;//Communication::ID
    int mNature;//Nature
    int mSender;
    int mReceiver;
	int mDimension;
    int mDirectPath; //Path::ID

public: 
    enum ID{C01=1, C02, C03, C04, C05, C06, C07, C08, C09, C10, C11, C12};
	enum Nature{BLOCKING, NON_BLOCKING};
    Communication(Communication::ID inID, int inCommTime);
	std::string toString();
    void setId(ID inId);
    ID getId();
	void setNature(Nature inNature);
    int getNature();//Nature
    void setSender(int inSender);
    int getSender();
    void setReceiver(int inReceiver);
    int getReceiver();
	void setDimension(int aDimension);
	int getDimension();
    int getDirectPath();//Path::ID
};

#endif
