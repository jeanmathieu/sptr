#include "processeur.h"

Processeur::Processeur()
{
}

Processeur& Processeur::addProcess(Processus *inProcessus)
{
    mOrdonnenceur.addProcess(inProcessus);
    return *this;
}

Processeur& Processeur::setSchedulingStrategy(Ordonnenceur::schedulingStrategy inStrategy)
{
    mOrdonnenceur.setSchedulingStrategy(inStrategy);
    return *this;
}

void Processeur::run()
{
    mOrdonnenceur.run();
}

Ordonnenceur& Processeur::getScheduler()
{
    return mOrdonnenceur;
}


