#include <exception>
using namespace std;

#include "Path.h"
#include "Communication.h"
#include "../Processus/ProcessusManager.h"

Path::Path(ID inID, State inState)
{
    mID = inID;
    mState = inState;
}

int Path::usePath() {
    mBusyTimeLeft--;
    return mBusyTimeLeft;
}

void Path::free() {
    if (mCurrentCommunication != 0) {
        ProcessusManager::getProcess(mCurrentCommunication->getSender())->fireCommunicate(mCurrentCommunication);
        setCurrentCommunication(0);
        setState(FREE);
    }
}

Path::ID Path::getId() {
    return mID;
}

void Path::setState(State inState) {
    mState = inState;
}

Path::State Path::getState() {
    return mState;
}

void Path::setCurrentCommunication(Communication* inCurrentCommunication) {
    mCurrentCommunication = inCurrentCommunication;
}

Communication* Path::getCurrentCommunication() {
    return mCurrentCommunication;
}

void Path::setBusyTimeLeft(int inBusyTimeLeft) {
    mBusyTimeLeft = inBusyTimeLeft;
}

int Path::getBusyTimeLeft() {
    return mBusyTimeLeft;
}

