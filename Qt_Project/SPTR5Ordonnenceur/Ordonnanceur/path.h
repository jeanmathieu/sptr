#include <exception>
using namespace std;

#ifndef __Path_h__
#define __Path_h__

#include "communication.h"

class Path
{
public:
    enum ID	{PATH1, PATH2, PATH3, LOCAL};
    enum State{BUSY, FREE};

private: 
    friend class Communication;
    friend class PathManager;
	ID mID;
	State mState;
    Communication* mCurrentCommunication;
	int mBusyTimeLeft;

public: 
	Path(ID inID, State inState);
	int usePath();
	void free();
    ID getId();
	void setState(State inState);
	State getState();
	void setCurrentCommunication(Communication* inCurrentCommunication);
	Communication* getCurrentCommunication();
	void setBusyTimeLeft(int inBusyTimeLeft);
	int getBusyTimeLeft();
};

#endif
