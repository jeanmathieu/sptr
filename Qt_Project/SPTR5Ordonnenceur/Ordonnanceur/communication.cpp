#include <exception>
using namespace std;

#include "communication.h"
#include "path.h"
#include "../Processus/processus.h"
#include "../Processus/executionstep.h"
#include <sstream>
#include <iomanip>
#include <string>

//TODO <- qu'est-ce qui initialise mDirectPath?
Communication::Communication(Communication::ID inID, int inCommTime)
{
    setType(ExecutionStep::communication);
    switch (inID)
    {
    case C01:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P01);
        setReceiver(Processus::P03);
        setDimension(inCommTime);
        break;
    case C02:
        setId(inID);
        setNature(BLOCKING);
        setSender(Processus::P02);
        setReceiver(Processus::P07);
        setDimension(inCommTime);
        break;
    case C03:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P02);
        setReceiver(Processus::P12);
        setDimension(inCommTime);
        break;
    case C04:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P03);
        setReceiver(Processus::P05);	// Sporadique
        setDimension(inCommTime);
        break;
    case C05:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P05);
        setReceiver(Processus::P02);
        setDimension(inCommTime);
        break;
    case C06:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P07);
        setReceiver(Processus::P03);	// Sporadique
        setDimension(inCommTime);
        break;
    case C07:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P10);
        setReceiver(Processus::P13);	// Sporadique
        setDimension(inCommTime);
        break;
    case C08:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P11);
        setReceiver(Processus::P08);	// Sporadique
        setDimension(inCommTime);
        break;
    case C09:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P12);
        setReceiver(Processus::P03);	// Sporadique
        setDimension(inCommTime);
        break;
    case C10:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P12);
        setReceiver(Processus::P06);	// Sporadique
        setDimension(inCommTime);
        break;
    case C11:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P12);
        setReceiver(Processus::P10);
        setDimension(inCommTime);
        break;
    case C12:
        setId(inID);
        setNature(NON_BLOCKING);
        setSender(Processus::P12);
        setReceiver(Processus::P11);	// Sporadique
        setDimension(inCommTime);
        break;
    default:
        //invalide
        break;
    }
}

std::string Communication::toString()
{
    std::stringstream lStringstream;
    std::string lString;

    lStringstream << mID;
    lStringstream >> lString;

    return lString;
}

void Communication::setId(Communication::ID inID)
{
    mID = inID;
}

Communication::ID Communication::getId() {
    return (Communication::ID)mID;
}

void Communication::setNature(Nature inNature) {
    mNature = inNature;
}

int Communication::getNature()
{
    return mNature;
}

void Communication::setSender(int inSender)
{
    mSender = inSender;
}

int Communication::getSender()
{
    return mSender;
}

void Communication::setReceiver(int inReceiver)
{
    mReceiver = inReceiver;
}

int Communication::getReceiver()
{
    return mReceiver;
}

void Communication::setDimension(int inDimension)
{
    mDimension = inDimension;
}

int Communication::getDimension()
{
    return mDimension;
}

int Communication::getDirectPath()
{
    return mDirectPath;
}

