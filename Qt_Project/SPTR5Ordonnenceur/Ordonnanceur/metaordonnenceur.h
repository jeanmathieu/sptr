#ifndef METAORDONNENCEUR_H
#define METAORDONNENCEUR_H

#include <vector>
#include "../Processus/processus.h"
#include "../Ressources/ressource.h"
#include "communication.h"

class MetaOrdonnenceur
{
private:
    std::vector<Processus*> mProcessus;
    std::vector<Ressource*> mRessources;
    std::vector<Communication*> mCommunications;

public:
    MetaOrdonnenceur();
    virtual ~MetaOrdonnenceur();
};

#endif // METAORDONNENCEUR_H
