#include "ordonnenceur.h"
#include "../Processus/ProcessusManager.h"

Ordonnenceur::Ordonnenceur() : mSchedulingStategy(RoundRobin)
{
    mRunning = 0;
}

Ordonnenceur::Ordonnenceur(std::list<Processus::ID> inIDs, schedulingStrategy inStrategy)
{
    mSchedulingStategy = inStrategy;

    std::list<Processus::ID>::iterator lIterator;
    for(lIterator=inIDs.begin();lIterator!=inIDs.end();lIterator++)
    {
        int lID = *lIterator;
        mProcessus.push_back(ProcessusManager::getProcess(lID));
    }
}

Ordonnenceur& Ordonnenceur::addProcess(Processus *inProcessus)
{
    mProcessus.push_back(inProcessus);
    return *this;
}

Ordonnenceur& Ordonnenceur::setSchedulingStrategy(schedulingStrategy inStrategy)
{
    mSchedulingStategy = inStrategy;
    return *this;
}

bool Ordonnenceur::listContains(std::list<Processus *> &inList, Processus *inProcessus)
{
    std::list<Processus *>::iterator lListIterator = inList.begin();
    bool lContains = false;

    while(lListIterator != inList.end())
    {
        if (*lListIterator == inProcessus) return true;
        lListIterator++;
    }

    return lContains;
}

void Ordonnenceur::updateStates()
{
    //TODO pour l'instant onplace tout les processus a ready
    int lSize = mProcessus.size();
    for(int i=0;i<lSize;i++)
    {
        mProcessus[i]->setState(Processus::ready);
    }
}

void Ordonnenceur::staticSchedule()
{
    switch(mSchedulingStategy)
    {
        case RoundRobin:
        {
         for (int unsigned i=0;i<mProcessus.size();i++)
            {
                Processus* lProcessus = mProcessus[i];

                switch (lProcessus->getState())
                {
                    case Processus::ready:
                    {
                    if (!(listContains(mReady,lProcessus)))
                        {
                            mReady.push_back(lProcessus);
                            mSuspended.remove(lProcessus);
                            mAsleep.remove(lProcessus);
                        }
                    }break;

                    case Processus::suspended:
                    {
                        if (!(listContains(mSuspended,lProcessus)))
                        {
                            mSuspended.push_back(lProcessus);
                            mReady.remove(lProcessus);
                            mAsleep.remove(lProcessus);
                        }
                    }break;

                    case Processus::asleep:
                    {
                        if (!(listContains(mAsleep,lProcessus)))
                        {
                            mAsleep.push_back(lProcessus);
                            mSuspended.remove(lProcessus);
                            mReady.remove(lProcessus);
                        }
                    }break;

                }
            }

        }break;

    }
}

void Ordonnenceur::run() //TODO
{
    //checkPeriodicity();
    updateStates();

    switch(mSchedulingStategy)
    {
        case RoundRobin:
        {
            staticSchedule();

            if (!mReady.size()==0)
            {
                std::list<Processus*>::iterator  lIterator = mReady.begin();

                Processus& lProcessus = **lIterator;

                lProcessus.setState(Processus::running);
                lProcessus.run();
                mReady.remove(*lIterator);
            }
        }break;

    }

    if (mRunning) mRunning->run();
    //mRunning = 0;
}


