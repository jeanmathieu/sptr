#ifndef __CommunicationObserver_h__
#define __CommunicationObserver_h__

//class CommunicationObserver;//Forward Declaration
#include "../Processus/processus.h"
#include "Communication.h"

class CommunicationObserver
{
	public:
        virtual void communicate(Communication* inCommunication) = 0;
};


#endif
