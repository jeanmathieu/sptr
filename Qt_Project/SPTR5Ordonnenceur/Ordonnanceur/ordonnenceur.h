#ifndef ORDONNENCEUR_H
#define ORDONNENCEUR_H

class Ordonnenceur;//Forward Declaration
#include <list>
#include <vector>
#include "../Processus/processus.h"

class Ordonnenceur
{
private:
    bool listContains(std::list<Processus*>& inList, Processus* inProcessus);//Helper function
    int mSchedulingStategy; //default:RoundRobin
    std::vector<Processus*> mProcessus;

    std::list<Processus*> mReady;
    std::list<Processus*> mSuspended;
    std::list<Processus*> mAsleep;
    Processus* mRunning;


    void checkPeriodicity();
    void updateStates();
    void staticSchedule();
    void dynamicSchedule();

public:
    enum schedulingStrategy{RoundRobin,RateMonotonicAdj,RateMonotonic,EarliestDeadline,LeastSlack};
    Ordonnenceur();
    Ordonnenceur(std::list<Processus::ID> inIDs, schedulingStrategy inStrategy);
    Ordonnenceur& addProcess(Processus* inProcessus);
    Ordonnenceur& setSchedulingStrategy(schedulingStrategy inStrategy);

    void run();
};

#endif // ORDONNENCEUR_H
