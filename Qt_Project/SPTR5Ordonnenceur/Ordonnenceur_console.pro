#-------------------------------------------------
#
# Project created by QtCreator 2013-02-24T08:20:23
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Ordonnenceur_console
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    Ordonnanceur/processeur.cpp \
    Ordonnanceur/ordonnenceur.cpp \
    Ordonnanceur/metaordonnenceur.cpp \
    Ordonnanceur/communication.cpp \
    Processus/processussporadique.cpp \
    Processus/processusperiodique.cpp \
    Processus/processus.cpp \
    Ressources/ressource.cpp \
    Simulateur/simulateur.cpp \
    Processus/ProcessusManager.cpp \
    Processus/executionstep.cpp \
    Processus/P01_AutoVerification.cpp \
    Ressources/R12_Telecom.cpp \
    Ressources/R11_StationMeteo.cpp \
    Ressources/R10_RegulateurVitesse.cpp \
    Ressources/R09_Radar.cpp \
    Ressources/R08_Odometre.cpp \
    Ressources/R07_GPS.cpp \
    Ressources/R06_GES.cpp \
    Ressources/R05_Environnement.cpp \
    Ressources/R04_ModuleAffichage.cpp \
    Ressources/R03_CompteurVitesse.cpp \
    Ressources/R02_CarteRoutiere.cpp \
    Ressources/R01_CarteGraphique.cpp \
    Ressources/ressourcemanager.cpp \
    Ordonnanceur/pathManager.cpp \
    Ordonnanceur/path.cpp \
    Ordonnanceur/communicationobserver.cpp \
    Processus/execution.cpp \
    Processus/P13_SystemesElectriques.cpp \
    Processus/P12_SuiviDuChemin.cpp \
    Processus/P11_SecuriteB.cpp \
    Processus/P10_SecuriteA.cpp \
    Processus/P09_Messagerie.cpp \
    Processus/P08_GestionDesAppels.cpp \
    Processus/P07_EvitementDeCollision.cpp \
    Processus/P06_EnvoiDeBilan.cpp \
    Processus/P05_ControleDesEmissions.cpp \
    Processus/P04_Climat.cpp \
    Processus/P03_ChangementDeVitesse.cpp \
    Processus/P02_CalculDeChemin.cpp

HEADERS += \
    Ordonnanceur/processeur.h \
    Ordonnanceur/ordonnenceur.h \
    Ordonnanceur/metaordonnenceur.h \
    Ordonnanceur/communication.h \
    Processus/processussporadique.h \
    Processus/processusperiodique.h \
    Processus/processus.h \
    Ressources/ressource.h \
    Simulateur/simulateur.h \
    Processus/ProcessusManager.h \
    Processus/executionstep.h \
    Processus/P01_AutoVerification.h \
    Ressources/resourcemanager.h \
    Ressources/R12_Telecom.h \
    Ressources/R11_StationMeteo.h \
    Ressources/R10_RegulateurVitesse.h \
    Ressources/R09_Radar.h \
    Ressources/R08_Odometre.h \
    Ressources/R07_GPS.h \
    Ressources/R06_GES.h \
    Ressources/R05_Environnement.h \
    Ressources/R04_ModuleAffichage.h \
    Ressources/R03_CompteurVitesse.h \
    Ressources/R02_CarteRoutiere.h \
    Ressources/R01_CarteGraphique.h \
    Ressources/ressourcemanager.h \
    Ordonnanceur/PathManager.h \
    Ordonnanceur/path.h \
    Ordonnanceur/communicationobserver.h \
    Processus/execution.h \
    Processus/P12_SuiviDuChemin.h \
    Processus/P11_SecuriteB.h \
    Processus/P10_SecuriteA.h \
    Processus/P09_Messagerie.h \
    Processus/P08_GestionDesAppels.h \
    Processus/P07_EvitementDeCollision.h \
    Processus/P06_EnvoiDeBilan.h \
    Processus/P05_ControleDesEmissions.h \
    Processus/P04_Climat.h \
    Processus/P03_ChangementDeVitesse.h \
    Processus/P02_CalculDeChemin.h \
    Processus/P13_SystemesElectriques.h
