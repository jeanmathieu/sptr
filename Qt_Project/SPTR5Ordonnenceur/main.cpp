#include <QCoreApplication>
#include "iostream"
#include <sstream>
#include <iomanip>

#include "./Ordonnanceur/processeur.h"
#include "./Processus/processusperiodique.h"
#include "./Ordonnanceur/ordonnenceur.h"
#include "./Simulateur/simulateur.h"

#include "./Processus/P01_AutoVerification.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Simulateur* lSimulateur = Simulateur::getInstance();
    Processeur lProcesseur;
    P01_AutoVerification lP01;
    lProcesseur.addProcess(&lP01);
    lSimulateur->addProcessor(&lProcesseur);

    for (unsigned int i=0;i<6;i++)
    {
        lSimulateur->run();
    }

    return a.exec();
}
