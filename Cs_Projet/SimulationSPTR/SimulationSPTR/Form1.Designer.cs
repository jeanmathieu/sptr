﻿partial class Form1
{
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <mXML name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</mXML>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Carte = new System.Windows.Forms.Panel();
            this.pictureCarte = new System.Windows.Forms.PictureBox();
            this.processOrderGrid = new System.Windows.Forms.DataGridView();
            this.ut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.panel5 = new System.Windows.Forms.Panel();
            this.PositionLabel = new System.Windows.Forms.Label();
            this.Fctlabel = new System.Windows.Forms.Label();
            this.FeuJaunesign = new System.Windows.Forms.PictureBox();
            this.FeuRougesign = new System.Windows.Forms.PictureBox();
            this.FeuVertsign = new System.Windows.Forms.PictureBox();
            this.CheckEngSign = new System.Windows.Forms.PictureBox();
            this.ABSsign = new System.Windows.Forms.PictureBox();
            this.UTlabel = new System.Windows.Forms.Label();
            this.Intersectionlabel = new System.Windows.Forms.Label();
            this.Publabel = new System.Windows.Forms.Label();
            this.Meteolabel = new System.Windows.Forms.Label();
            this.Dirlabel = new System.Windows.Forms.Label();
            this.Templabel = new System.Windows.Forms.Label();
            this.Odolabel = new System.Windows.Forms.Label();
            this.SpeedLabel = new System.Windows.Forms.Label();
            this.tabInterfaceControl = new System.Windows.Forms.TabControl();
            this.Paramètres = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.vitesseMaxAffiche = new System.Windows.Forms.TextBox();
            this.accelerationMaxAffiche = new System.Windows.Forms.TextBox();
            this.vitesseMaxLabel = new System.Windows.Forms.Label();
            this.lblArrivalX = new System.Windows.Forms.Label();
            this.accelerationMaxLabel = new System.Windows.Forms.Label();
            this.txtArrivalX = new System.Windows.Forms.TextBox();
            this.lblArrivalY = new System.Windows.Forms.Label();
            this.txtArrivalY = new System.Windows.Forms.TextBox();
            this.lblDepartX = new System.Windows.Forms.Label();
            this.txtDepartX = new System.Windows.Forms.TextBox();
            this.lblDepartY = new System.Windows.Forms.Label();
            this.txtDepartY = new System.Windows.Forms.TextBox();
            this.consoAffiche = new System.Windows.Forms.TextBox();
            this.consoLabel = new System.Windows.Forms.Label();
            this.facteurEchelleAffiche = new System.Windows.Forms.TextBox();
            this.dureeReparationAffiche = new System.Windows.Forms.TextBox();
            this.facteurEchelleLabel = new System.Windows.Forms.Label();
            this.dureeReparationLabel = new System.Windows.Forms.Label();
            this.dureeSimulAffiche = new System.Windows.Forms.TextBox();
            this.dureeSimulLabel = new System.Windows.Forms.Label();
            this.dureeCollisionAffiche = new System.Windows.Forms.TextBox();
            this.dureeCollisionLabel = new System.Windows.Forms.Label();
            this.Statistiques = new System.Windows.Forms.TabPage();
            this.txtNbrNetworkLinks = new System.Windows.Forms.TextBox();
            this.txtNbrProcessor = new System.Windows.Forms.TextBox();
            this.txtTotalTimeBloc = new System.Windows.Forms.TextBox();
            this.txtMaxTimebloc = new System.Windows.Forms.TextBox();
            this.txtStdDevChargeProcessor = new System.Windows.Forms.TextBox();
            this.txtMeanChargeProcessor = new System.Windows.Forms.TextBox();
            this.txtDmi = new System.Windows.Forms.TextBox();
            this.txtDpa = new System.Windows.Forms.TextBox();
            this.txtTotalDeadline = new System.Windows.Forms.TextBox();
            this.txtRespectedDeadline = new System.Windows.Forms.TextBox();
            this.txtGES = new System.Windows.Forms.TextBox();
            this.txtGESTotal = new System.Windows.Forms.TextBox();
            this.txtPin = new System.Windows.Forms.TextBox();
            this.lblNbrNetworkLinks = new System.Windows.Forms.Label();
            this.lblNbrProcessor = new System.Windows.Forms.Label();
            this.lblTotalTimebloc = new System.Windows.Forms.Label();
            this.lblMaxTimeBloc = new System.Windows.Forms.Label();
            this.lblStdDevChargeProcessor = new System.Windows.Forms.Label();
            this.lblMeanChargeProcessor = new System.Windows.Forms.Label();
            this.lblDmi = new System.Windows.Forms.Label();
            this.lblDpa = new System.Windows.Forms.Label();
            this.lblTotalDeadline = new System.Windows.Forms.Label();
            this.lblRespectedDeadline = new System.Windows.Forms.Label();
            this.lblGES = new System.Windows.Forms.Label();
            this.lblGESTotal = new System.Windows.Forms.Label();
            this.lblPin = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Stats2 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtStatTotal = new System.Windows.Forms.TextBox();
            this.txtabsPin = new System.Windows.Forms.TextBox();
            this.txtOptimalOper = new System.Windows.Forms.TextBox();
            this.txtEffParcours = new System.Windows.Forms.TextBox();
            this.txtRespContrtmps = new System.Windows.Forms.TextBox();
            this.txtMinimArchi = new System.Windows.Forms.TextBox();
            this.lblStatTotal = new System.Windows.Forms.Label();
            this.lblabsPin = new System.Windows.Forms.Label();
            this.lblOptimalOper = new System.Windows.Forms.Label();
            this.lblEffParcours = new System.Windows.Forms.Label();
            this.lblRespContrtmps = new System.Windows.Forms.Label();
            this.lblMinimArchi = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDelayBetweenUT = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.simulProgress = new System.Windows.Forms.ProgressBar();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cbOrderType = new System.Windows.Forms.ToolStripComboBox();
            this.btnSimAll = new System.Windows.Forms.ToolStripButton();
            this.btnPause = new System.Windows.Forms.ToolStripButton();
            this.btnSim1 = new System.Windows.Forms.ToolStripButton();
            this.btnResetSim = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.aideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Carte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCarte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.processOrderGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FeuJaunesign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuRougesign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuVertsign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEngSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ABSsign)).BeginInit();
            this.tabInterfaceControl.SuspendLayout();
            this.Paramètres.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.Statistiques.SuspendLayout();
            this.Stats2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Carte
            // 
            this.Carte.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.Carte.Controls.Add(this.pictureCarte);
            this.Carte.Location = new System.Drawing.Point(3, 3);
            this.Carte.Name = "Carte";
            this.Carte.Size = new System.Drawing.Size(825, 579);
            this.Carte.TabIndex = 0;
            // 
            // pictureCarte
            // 
            this.pictureCarte.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.pictureCarte.BackgroundImage = global::SimulationSPTR.Properties.Resources.grass;
            this.pictureCarte.Location = new System.Drawing.Point(-3, 0);
            this.pictureCarte.MinimumSize = new System.Drawing.Size(380, 380);
            this.pictureCarte.Name = "pictureCarte";
            this.pictureCarte.Size = new System.Drawing.Size(825, 646);
            this.pictureCarte.TabIndex = 1;
            this.pictureCarte.TabStop = false;
            this.pictureCarte.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureCarte_Paint);
            this.pictureCarte.Resize += new System.EventHandler(this.pictureCarte_Resize);
            // 
            // processOrderGrid
            // 
            this.processOrderGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.processOrderGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ut,
            this.process1,
            this.process2,
            this.process3,
            this.process4,
            this.process5,
            this.process6,
            this.process7,
            this.process8,
            this.process9,
            this.process10,
            this.process11,
            this.process12,
            this.process13});
            this.processOrderGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processOrderGrid.Location = new System.Drawing.Point(0, 0);
            this.processOrderGrid.Name = "processOrderGrid";
            this.processOrderGrid.Size = new System.Drawing.Size(356, 82);
            this.processOrderGrid.TabIndex = 0;
            this.processOrderGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.processOrderGrid_RowsAdded);
            // 
            // ut
            // 
            this.ut.HeaderText = "U.T";
            this.ut.Name = "ut";
            // 
            // process1
            // 
            this.process1.HeaderText = "P1";
            this.process1.Name = "process1";
            // 
            // process2
            // 
            this.process2.HeaderText = "P2";
            this.process2.Name = "process2";
            // 
            // process3
            // 
            this.process3.HeaderText = "P3";
            this.process3.Name = "process3";
            // 
            // process4
            // 
            this.process4.HeaderText = "P4";
            this.process4.Name = "process4";
            // 
            // process5
            // 
            this.process5.HeaderText = "P5";
            this.process5.Name = "process5";
            // 
            // process6
            // 
            this.process6.HeaderText = "P6";
            this.process6.Name = "process6";
            // 
            // process7
            // 
            this.process7.HeaderText = "P7";
            this.process7.Name = "process7";
            // 
            // process8
            // 
            this.process8.HeaderText = "P8";
            this.process8.Name = "process8";
            // 
            // process9
            // 
            this.process9.HeaderText = "P9";
            this.process9.Name = "process9";
            // 
            // process10
            // 
            this.process10.HeaderText = "P10";
            this.process10.Name = "process10";
            // 
            // process11
            // 
            this.process11.HeaderText = "P11";
            this.process11.Name = "process11";
            // 
            // process12
            // 
            this.process12.HeaderText = "P12";
            this.process12.Name = "process12";
            // 
            // process13
            // 
            this.process13.HeaderText = "P13";
            this.process13.Name = "process13";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(837, 630);
            this.splitContainer1.SplitterDistance = 356;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.Carte);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.processOrderGrid);
            this.splitContainer2.Size = new System.Drawing.Size(356, 630);
            this.splitContainer2.SplitterDistance = 544;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.panel5);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tabInterfaceControl);
            this.splitContainer3.Size = new System.Drawing.Size(477, 630);
            this.splitContainer3.SplitterDistance = 426;
            this.splitContainer3.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.BackgroundImage = global::SimulationSPTR.Properties.Resources.dash1;
            this.panel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel5.Controls.Add(this.PositionLabel);
            this.panel5.Controls.Add(this.Fctlabel);
            this.panel5.Controls.Add(this.FeuJaunesign);
            this.panel5.Controls.Add(this.FeuRougesign);
            this.panel5.Controls.Add(this.FeuVertsign);
            this.panel5.Controls.Add(this.CheckEngSign);
            this.panel5.Controls.Add(this.ABSsign);
            this.panel5.Controls.Add(this.UTlabel);
            this.panel5.Controls.Add(this.Intersectionlabel);
            this.panel5.Controls.Add(this.Publabel);
            this.panel5.Controls.Add(this.Meteolabel);
            this.panel5.Controls.Add(this.Dirlabel);
            this.panel5.Controls.Add(this.Templabel);
            this.panel5.Controls.Add(this.Odolabel);
            this.panel5.Controls.Add(this.SpeedLabel);
            this.panel5.Location = new System.Drawing.Point(4, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(470, 373);
            this.panel5.TabIndex = 0;
            // 
            // PositionLabel
            // 
            this.PositionLabel.BackColor = System.Drawing.Color.Transparent;
            this.PositionLabel.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.PositionLabel.Location = new System.Drawing.Point(137, 225);
            this.PositionLabel.Name = "PositionLabel";
            this.PositionLabel.Size = new System.Drawing.Size(193, 23);
            this.PositionLabel.TabIndex = 2;
            this.PositionLabel.Text = "label5";
            this.PositionLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Fctlabel
            // 
            this.Fctlabel.BackColor = System.Drawing.Color.Transparent;
            this.Fctlabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fctlabel.ForeColor = System.Drawing.Color.Navy;
            this.Fctlabel.Location = new System.Drawing.Point(137, 300);
            this.Fctlabel.Name = "Fctlabel";
            this.Fctlabel.Size = new System.Drawing.Size(196, 24);
            this.Fctlabel.TabIndex = 28;
            this.Fctlabel.Text = "CHARGEMENT EN COURS";
            this.Fctlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // FeuJaunesign
            // 
            this.FeuJaunesign.BackColor = System.Drawing.Color.Transparent;
            this.FeuJaunesign.BackgroundImage = global::SimulationSPTR.Properties.Resources.feuJaune;
            this.FeuJaunesign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.FeuJaunesign.Location = new System.Drawing.Point(415, 256);
            this.FeuJaunesign.Name = "FeuJaunesign";
            this.FeuJaunesign.Size = new System.Drawing.Size(49, 43);
            this.FeuJaunesign.TabIndex = 27;
            this.FeuJaunesign.TabStop = false;
            this.FeuJaunesign.Visible = false;
            // 
            // FeuRougesign
            // 
            this.FeuRougesign.BackColor = System.Drawing.Color.Transparent;
            this.FeuRougesign.BackgroundImage = global::SimulationSPTR.Properties.Resources.feuRouge;
            this.FeuRougesign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.FeuRougesign.Location = new System.Drawing.Point(415, 256);
            this.FeuRougesign.Name = "FeuRougesign";
            this.FeuRougesign.Size = new System.Drawing.Size(49, 43);
            this.FeuRougesign.TabIndex = 26;
            this.FeuRougesign.TabStop = false;
            this.FeuRougesign.Visible = false;
            // 
            // FeuVertsign
            // 
            this.FeuVertsign.BackColor = System.Drawing.Color.Transparent;
            this.FeuVertsign.BackgroundImage = global::SimulationSPTR.Properties.Resources.feuVert;
            this.FeuVertsign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.FeuVertsign.Location = new System.Drawing.Point(416, 257);
            this.FeuVertsign.Name = "FeuVertsign";
            this.FeuVertsign.Size = new System.Drawing.Size(49, 43);
            this.FeuVertsign.TabIndex = 25;
            this.FeuVertsign.TabStop = false;
            this.FeuVertsign.Visible = false;
            // 
            // CheckEngSign
            // 
            this.CheckEngSign.BackColor = System.Drawing.Color.Transparent;
            this.CheckEngSign.BackgroundImage = global::SimulationSPTR.Properties.Resources.CheckEng;
            this.CheckEngSign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CheckEngSign.Location = new System.Drawing.Point(383, 207);
            this.CheckEngSign.Name = "CheckEngSign";
            this.CheckEngSign.Size = new System.Drawing.Size(33, 24);
            this.CheckEngSign.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CheckEngSign.TabIndex = 24;
            this.CheckEngSign.TabStop = false;
            this.CheckEngSign.Visible = false;
            // 
            // ABSsign
            // 
            this.ABSsign.BackColor = System.Drawing.Color.Transparent;
            this.ABSsign.BackgroundImage = global::SimulationSPTR.Properties.Resources.ABS;
            this.ABSsign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ABSsign.Location = new System.Drawing.Point(383, 160);
            this.ABSsign.Name = "ABSsign";
            this.ABSsign.Size = new System.Drawing.Size(33, 27);
            this.ABSsign.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ABSsign.TabIndex = 23;
            this.ABSsign.TabStop = false;
            this.ABSsign.Visible = false;
            // 
            // UTlabel
            // 
            this.UTlabel.BackColor = System.Drawing.Color.Transparent;
            this.UTlabel.ForeColor = System.Drawing.Color.LightGray;
            this.UTlabel.Location = new System.Drawing.Point(362, 305);
            this.UTlabel.Name = "UTlabel";
            this.UTlabel.Size = new System.Drawing.Size(58, 11);
            this.UTlabel.TabIndex = 22;
            this.UTlabel.Text = "0000";
            this.UTlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Intersectionlabel
            // 
            this.Intersectionlabel.BackColor = System.Drawing.Color.Transparent;
            this.Intersectionlabel.Location = new System.Drawing.Point(359, 271);
            this.Intersectionlabel.Name = "Intersectionlabel";
            this.Intersectionlabel.Size = new System.Drawing.Size(61, 24);
            this.Intersectionlabel.TabIndex = 21;
            this.Intersectionlabel.Text = "LOADING";
            this.Intersectionlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Publabel
            // 
            this.Publabel.BackColor = System.Drawing.Color.Transparent;
            this.Publabel.ForeColor = System.Drawing.Color.Navy;
            this.Publabel.Location = new System.Drawing.Point(85, 341);
            this.Publabel.Name = "Publabel";
            this.Publabel.Size = new System.Drawing.Size(305, 18);
            this.Publabel.TabIndex = 20;
            this.Publabel.Text = "CHARGEMENT PUBLICITE";
            this.Publabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Meteolabel
            // 
            this.Meteolabel.BackColor = System.Drawing.Color.Transparent;
            this.Meteolabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Meteolabel.ForeColor = System.Drawing.Color.Navy;
            this.Meteolabel.Location = new System.Drawing.Point(137, 260);
            this.Meteolabel.Name = "Meteolabel";
            this.Meteolabel.Size = new System.Drawing.Size(196, 24);
            this.Meteolabel.TabIndex = 19;
            this.Meteolabel.Text = "CHARGEMENT METEO";
            this.Meteolabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Dirlabel
            // 
            this.Dirlabel.BackColor = System.Drawing.Color.Transparent;
            this.Dirlabel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dirlabel.ForeColor = System.Drawing.Color.DarkRed;
            this.Dirlabel.Location = new System.Drawing.Point(60, 215);
            this.Dirlabel.Name = "Dirlabel";
            this.Dirlabel.Size = new System.Drawing.Size(59, 29);
            this.Dirlabel.TabIndex = 18;
            this.Dirlabel.Text = "OUEST";
            this.Dirlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Templabel
            // 
            this.Templabel.BackColor = System.Drawing.Color.Transparent;
            this.Templabel.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Templabel.ForeColor = System.Drawing.Color.DarkRed;
            this.Templabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Templabel.Location = new System.Drawing.Point(61, 153);
            this.Templabel.Name = "Templabel";
            this.Templabel.Size = new System.Drawing.Size(43, 34);
            this.Templabel.TabIndex = 17;
            this.Templabel.Text = "00";
            this.Templabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Odolabel
            // 
            this.Odolabel.BackColor = System.Drawing.Color.Transparent;
            this.Odolabel.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Odolabel.ForeColor = System.Drawing.Color.Navy;
            this.Odolabel.Location = new System.Drawing.Point(151, 188);
            this.Odolabel.Name = "Odolabel";
            this.Odolabel.Size = new System.Drawing.Size(123, 45);
            this.Odolabel.TabIndex = 16;
            this.Odolabel.Text = "0";
            this.Odolabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SpeedLabel
            // 
            this.SpeedLabel.BackColor = System.Drawing.Color.Transparent;
            this.SpeedLabel.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpeedLabel.ForeColor = System.Drawing.Color.Navy;
            this.SpeedLabel.Location = new System.Drawing.Point(154, 146);
            this.SpeedLabel.Name = "SpeedLabel";
            this.SpeedLabel.Size = new System.Drawing.Size(123, 45);
            this.SpeedLabel.TabIndex = 15;
            this.SpeedLabel.Text = "0";
            this.SpeedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabInterfaceControl
            // 
            this.tabInterfaceControl.Controls.Add(this.Paramètres);
            this.tabInterfaceControl.Controls.Add(this.Statistiques);
            this.tabInterfaceControl.Controls.Add(this.Stats2);
            this.tabInterfaceControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabInterfaceControl.Location = new System.Drawing.Point(0, 0);
            this.tabInterfaceControl.Name = "tabInterfaceControl";
            this.tabInterfaceControl.SelectedIndex = 0;
            this.tabInterfaceControl.Size = new System.Drawing.Size(477, 200);
            this.tabInterfaceControl.TabIndex = 0;
            // 
            // Paramètres
            // 
            this.Paramètres.Controls.Add(this.panel4);
            this.Paramètres.Location = new System.Drawing.Point(4, 22);
            this.Paramètres.Name = "Paramètres";
            this.Paramètres.Size = new System.Drawing.Size(469, 174);
            this.Paramètres.TabIndex = 4;
            this.Paramètres.Text = "Paramètres";
            this.Paramètres.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.AutoScroll = true;
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.numericUpDown1);
            this.panel4.Controls.Add(this.vitesseMaxAffiche);
            this.panel4.Controls.Add(this.accelerationMaxAffiche);
            this.panel4.Controls.Add(this.vitesseMaxLabel);
            this.panel4.Controls.Add(this.lblArrivalX);
            this.panel4.Controls.Add(this.accelerationMaxLabel);
            this.panel4.Controls.Add(this.txtArrivalX);
            this.panel4.Controls.Add(this.lblArrivalY);
            this.panel4.Controls.Add(this.txtArrivalY);
            this.panel4.Controls.Add(this.lblDepartX);
            this.panel4.Controls.Add(this.txtDepartX);
            this.panel4.Controls.Add(this.lblDepartY);
            this.panel4.Controls.Add(this.txtDepartY);
            this.panel4.Controls.Add(this.consoAffiche);
            this.panel4.Controls.Add(this.consoLabel);
            this.panel4.Controls.Add(this.facteurEchelleAffiche);
            this.panel4.Controls.Add(this.dureeReparationAffiche);
            this.panel4.Controls.Add(this.facteurEchelleLabel);
            this.panel4.Controls.Add(this.dureeReparationLabel);
            this.panel4.Controls.Add(this.dureeSimulAffiche);
            this.panel4.Controls.Add(this.dureeSimulLabel);
            this.panel4.Controls.Add(this.dureeCollisionAffiche);
            this.panel4.Controls.Add(this.dureeCollisionLabel);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(469, 174);
            this.panel4.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 110);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 34;
            this.label6.Text = "Taille des voitures:";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown1.Location = new System.Drawing.Point(162, 106);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(138, 20);
            this.numericUpDown1.TabIndex = 33;
            this.numericUpDown1.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // vitesseMaxAffiche
            // 
            this.vitesseMaxAffiche.Location = new System.Drawing.Point(161, 288);
            this.vitesseMaxAffiche.Name = "vitesseMaxAffiche";
            this.vitesseMaxAffiche.Size = new System.Drawing.Size(139, 20);
            this.vitesseMaxAffiche.TabIndex = 20;
            // 
            // accelerationMaxAffiche
            // 
            this.accelerationMaxAffiche.Location = new System.Drawing.Point(161, 314);
            this.accelerationMaxAffiche.Name = "accelerationMaxAffiche";
            this.accelerationMaxAffiche.Size = new System.Drawing.Size(139, 20);
            this.accelerationMaxAffiche.TabIndex = 19;
            // 
            // vitesseMaxLabel
            // 
            this.vitesseMaxLabel.AutoSize = true;
            this.vitesseMaxLabel.Location = new System.Drawing.Point(7, 291);
            this.vitesseMaxLabel.Name = "vitesseMaxLabel";
            this.vitesseMaxLabel.Size = new System.Drawing.Size(96, 13);
            this.vitesseMaxLabel.TabIndex = 18;
            this.vitesseMaxLabel.Text = "Vitesse maximale : ";
            // 
            // lblArrivalX
            // 
            this.lblArrivalX.AutoSize = true;
            this.lblArrivalX.Location = new System.Drawing.Point(5, 239);
            this.lblArrivalX.Name = "lblArrivalX";
            this.lblArrivalX.Size = new System.Drawing.Size(95, 13);
            this.lblArrivalX.TabIndex = 30;
            this.lblArrivalX.Text = "Position arrivée X :";
            // 
            // accelerationMaxLabel
            // 
            this.accelerationMaxLabel.AutoSize = true;
            this.accelerationMaxLabel.Location = new System.Drawing.Point(5, 317);
            this.accelerationMaxLabel.Name = "accelerationMaxLabel";
            this.accelerationMaxLabel.Size = new System.Drawing.Size(118, 13);
            this.accelerationMaxLabel.TabIndex = 17;
            this.accelerationMaxLabel.Text = "Accélération maximale :";
            // 
            // txtArrivalX
            // 
            this.txtArrivalX.Location = new System.Drawing.Point(161, 236);
            this.txtArrivalX.Name = "txtArrivalX";
            this.txtArrivalX.Size = new System.Drawing.Size(139, 20);
            this.txtArrivalX.TabIndex = 29;
            // 
            // lblArrivalY
            // 
            this.lblArrivalY.AutoSize = true;
            this.lblArrivalY.Location = new System.Drawing.Point(5, 265);
            this.lblArrivalY.Name = "lblArrivalY";
            this.lblArrivalY.Size = new System.Drawing.Size(95, 13);
            this.lblArrivalY.TabIndex = 28;
            this.lblArrivalY.Text = "Position arrivée Y :";
            // 
            // txtArrivalY
            // 
            this.txtArrivalY.Location = new System.Drawing.Point(161, 262);
            this.txtArrivalY.Name = "txtArrivalY";
            this.txtArrivalY.Size = new System.Drawing.Size(139, 20);
            this.txtArrivalY.TabIndex = 27;
            // 
            // lblDepartX
            // 
            this.lblDepartX.AutoSize = true;
            this.lblDepartX.Location = new System.Drawing.Point(5, 187);
            this.lblDepartX.Name = "lblDepartX";
            this.lblDepartX.Size = new System.Drawing.Size(93, 13);
            this.lblDepartX.TabIndex = 26;
            this.lblDepartX.Text = "Position départ X :";
            // 
            // txtDepartX
            // 
            this.txtDepartX.Location = new System.Drawing.Point(161, 184);
            this.txtDepartX.Name = "txtDepartX";
            this.txtDepartX.Size = new System.Drawing.Size(139, 20);
            this.txtDepartX.TabIndex = 25;
            // 
            // lblDepartY
            // 
            this.lblDepartY.AutoSize = true;
            this.lblDepartY.Location = new System.Drawing.Point(5, 213);
            this.lblDepartY.Name = "lblDepartY";
            this.lblDepartY.Size = new System.Drawing.Size(93, 13);
            this.lblDepartY.TabIndex = 22;
            this.lblDepartY.Text = "Position départ Y :";
            // 
            // txtDepartY
            // 
            this.txtDepartY.Location = new System.Drawing.Point(161, 210);
            this.txtDepartY.Name = "txtDepartY";
            this.txtDepartY.Size = new System.Drawing.Size(139, 20);
            this.txtDepartY.TabIndex = 21;
            // 
            // consoAffiche
            // 
            this.consoAffiche.Location = new System.Drawing.Point(161, 158);
            this.consoAffiche.Name = "consoAffiche";
            this.consoAffiche.Size = new System.Drawing.Size(139, 20);
            this.consoAffiche.TabIndex = 15;
            // 
            // consoLabel
            // 
            this.consoLabel.AutoSize = true;
            this.consoLabel.Location = new System.Drawing.Point(5, 161);
            this.consoLabel.Name = "consoLabel";
            this.consoLabel.Size = new System.Drawing.Size(82, 13);
            this.consoLabel.TabIndex = 14;
            this.consoLabel.Text = "Consommation :";
            // 
            // facteurEchelleAffiche
            // 
            this.facteurEchelleAffiche.Location = new System.Drawing.Point(161, 132);
            this.facteurEchelleAffiche.Name = "facteurEchelleAffiche";
            this.facteurEchelleAffiche.Size = new System.Drawing.Size(139, 20);
            this.facteurEchelleAffiche.TabIndex = 16;
            // 
            // dureeReparationAffiche
            // 
            this.dureeReparationAffiche.Location = new System.Drawing.Point(161, 53);
            this.dureeReparationAffiche.Name = "dureeReparationAffiche";
            this.dureeReparationAffiche.Size = new System.Drawing.Size(139, 20);
            this.dureeReparationAffiche.TabIndex = 3;
            // 
            // facteurEchelleLabel
            // 
            this.facteurEchelleLabel.AutoSize = true;
            this.facteurEchelleLabel.Location = new System.Drawing.Point(5, 135);
            this.facteurEchelleLabel.Name = "facteurEchelleLabel";
            this.facteurEchelleLabel.Size = new System.Drawing.Size(94, 13);
            this.facteurEchelleLabel.TabIndex = 12;
            this.facteurEchelleLabel.Text = "Facteur d\'échelle :";
            // 
            // dureeReparationLabel
            // 
            this.dureeReparationLabel.AutoSize = true;
            this.dureeReparationLabel.Location = new System.Drawing.Point(5, 56);
            this.dureeReparationLabel.Name = "dureeReparationLabel";
            this.dureeReparationLabel.Size = new System.Drawing.Size(138, 13);
            this.dureeReparationLabel.TabIndex = 2;
            this.dureeReparationLabel.Text = "Durée Auto-reparation  u.t. :";
            // 
            // dureeSimulAffiche
            // 
            this.dureeSimulAffiche.Location = new System.Drawing.Point(161, 26);
            this.dureeSimulAffiche.Name = "dureeSimulAffiche";
            this.dureeSimulAffiche.Size = new System.Drawing.Size(139, 20);
            this.dureeSimulAffiche.TabIndex = 1;
            // 
            // dureeSimulLabel
            // 
            this.dureeSimulLabel.AutoSize = true;
            this.dureeSimulLabel.Location = new System.Drawing.Point(5, 29);
            this.dureeSimulLabel.Name = "dureeSimulLabel";
            this.dureeSimulLabel.Size = new System.Drawing.Size(150, 13);
            this.dureeSimulLabel.TabIndex = 0;
            this.dureeSimulLabel.Text = "Durée de la simulation en u.t. :";
            // 
            // dureeCollisionAffiche
            // 
            this.dureeCollisionAffiche.Location = new System.Drawing.Point(161, 79);
            this.dureeCollisionAffiche.Name = "dureeCollisionAffiche";
            this.dureeCollisionAffiche.Size = new System.Drawing.Size(139, 20);
            this.dureeCollisionAffiche.TabIndex = 11;
            // 
            // dureeCollisionLabel
            // 
            this.dureeCollisionLabel.AutoSize = true;
            this.dureeCollisionLabel.Location = new System.Drawing.Point(5, 86);
            this.dureeCollisionLabel.Name = "dureeCollisionLabel";
            this.dureeCollisionLabel.Size = new System.Drawing.Size(115, 13);
            this.dureeCollisionLabel.TabIndex = 9;
            this.dureeCollisionLabel.Text = "Durée collision en u.t. :";
            // 
            // Statistiques
            // 
            this.Statistiques.AutoScroll = true;
            this.Statistiques.Controls.Add(this.txtNbrNetworkLinks);
            this.Statistiques.Controls.Add(this.txtNbrProcessor);
            this.Statistiques.Controls.Add(this.txtTotalTimeBloc);
            this.Statistiques.Controls.Add(this.txtMaxTimebloc);
            this.Statistiques.Controls.Add(this.txtStdDevChargeProcessor);
            this.Statistiques.Controls.Add(this.txtMeanChargeProcessor);
            this.Statistiques.Controls.Add(this.txtDmi);
            this.Statistiques.Controls.Add(this.txtDpa);
            this.Statistiques.Controls.Add(this.txtTotalDeadline);
            this.Statistiques.Controls.Add(this.txtRespectedDeadline);
            this.Statistiques.Controls.Add(this.txtGES);
            this.Statistiques.Controls.Add(this.txtGESTotal);
            this.Statistiques.Controls.Add(this.txtPin);
            this.Statistiques.Controls.Add(this.lblNbrNetworkLinks);
            this.Statistiques.Controls.Add(this.lblNbrProcessor);
            this.Statistiques.Controls.Add(this.lblTotalTimebloc);
            this.Statistiques.Controls.Add(this.lblMaxTimeBloc);
            this.Statistiques.Controls.Add(this.lblStdDevChargeProcessor);
            this.Statistiques.Controls.Add(this.lblMeanChargeProcessor);
            this.Statistiques.Controls.Add(this.lblDmi);
            this.Statistiques.Controls.Add(this.lblDpa);
            this.Statistiques.Controls.Add(this.lblTotalDeadline);
            this.Statistiques.Controls.Add(this.lblRespectedDeadline);
            this.Statistiques.Controls.Add(this.lblGES);
            this.Statistiques.Controls.Add(this.lblGESTotal);
            this.Statistiques.Controls.Add(this.lblPin);
            this.Statistiques.Controls.Add(this.label2);
            this.Statistiques.Controls.Add(this.label1);
            this.Statistiques.Location = new System.Drawing.Point(4, 22);
            this.Statistiques.Name = "Statistiques";
            this.Statistiques.Padding = new System.Windows.Forms.Padding(3);
            this.Statistiques.Size = new System.Drawing.Size(469, 174);
            this.Statistiques.TabIndex = 1;
            this.Statistiques.Text = "Statistiques";
            this.Statistiques.UseVisualStyleBackColor = true;
            // 
            // txtNbrNetworkLinks
            // 
            this.txtNbrNetworkLinks.Location = new System.Drawing.Point(280, 344);
            this.txtNbrNetworkLinks.Name = "txtNbrNetworkLinks";
            this.txtNbrNetworkLinks.ReadOnly = true;
            this.txtNbrNetworkLinks.Size = new System.Drawing.Size(139, 20);
            this.txtNbrNetworkLinks.TabIndex = 55;
            this.txtNbrNetworkLinks.TextChanged += new System.EventHandler(this.txtNbrNetworkLinks_TextChanged);
            // 
            // txtNbrProcessor
            // 
            this.txtNbrProcessor.Location = new System.Drawing.Point(280, 318);
            this.txtNbrProcessor.Name = "txtNbrProcessor";
            this.txtNbrProcessor.ReadOnly = true;
            this.txtNbrProcessor.Size = new System.Drawing.Size(139, 20);
            this.txtNbrProcessor.TabIndex = 53;
            this.txtNbrProcessor.TextChanged += new System.EventHandler(this.txtNbrProcessor_TextChanged);
            // 
            // txtTotalTimeBloc
            // 
            this.txtTotalTimeBloc.Location = new System.Drawing.Point(280, 240);
            this.txtTotalTimeBloc.Name = "txtTotalTimeBloc";
            this.txtTotalTimeBloc.ReadOnly = true;
            this.txtTotalTimeBloc.Size = new System.Drawing.Size(139, 20);
            this.txtTotalTimeBloc.TabIndex = 49;
            this.txtTotalTimeBloc.TextChanged += new System.EventHandler(this.txtTotalTimeBloc_TextChanged);
            // 
            // txtMaxTimebloc
            // 
            this.txtMaxTimebloc.Location = new System.Drawing.Point(280, 214);
            this.txtMaxTimebloc.Name = "txtMaxTimebloc";
            this.txtMaxTimebloc.ReadOnly = true;
            this.txtMaxTimebloc.Size = new System.Drawing.Size(139, 20);
            this.txtMaxTimebloc.TabIndex = 47;
            this.txtMaxTimebloc.TextChanged += new System.EventHandler(this.txtMaxTimebloc_TextChanged);
            // 
            // txtStdDevChargeProcessor
            // 
            this.txtStdDevChargeProcessor.Location = new System.Drawing.Point(280, 188);
            this.txtStdDevChargeProcessor.Name = "txtStdDevChargeProcessor";
            this.txtStdDevChargeProcessor.ReadOnly = true;
            this.txtStdDevChargeProcessor.Size = new System.Drawing.Size(139, 20);
            this.txtStdDevChargeProcessor.TabIndex = 45;
            this.txtStdDevChargeProcessor.TextChanged += new System.EventHandler(this.txtStdDevChargeProcessor_TextChanged);
            // 
            // txtMeanChargeProcessor
            // 
            this.txtMeanChargeProcessor.Location = new System.Drawing.Point(280, 162);
            this.txtMeanChargeProcessor.Name = "txtMeanChargeProcessor";
            this.txtMeanChargeProcessor.ReadOnly = true;
            this.txtMeanChargeProcessor.Size = new System.Drawing.Size(139, 20);
            this.txtMeanChargeProcessor.TabIndex = 43;
            this.txtMeanChargeProcessor.TextChanged += new System.EventHandler(this.txtMeanChargeProcessor_TextChanged);
            // 
            // txtDmi
            // 
            this.txtDmi.Location = new System.Drawing.Point(280, 136);
            this.txtDmi.Name = "txtDmi";
            this.txtDmi.ReadOnly = true;
            this.txtDmi.Size = new System.Drawing.Size(139, 20);
            this.txtDmi.TabIndex = 41;
            this.txtDmi.TextChanged += new System.EventHandler(this.txtDmi_TextChanged);
            // 
            // txtDpa
            // 
            this.txtDpa.Location = new System.Drawing.Point(280, 110);
            this.txtDpa.Name = "txtDpa";
            this.txtDpa.ReadOnly = true;
            this.txtDpa.Size = new System.Drawing.Size(139, 20);
            this.txtDpa.TabIndex = 41;
            this.txtDpa.TextChanged += new System.EventHandler(this.txtDpa_TextChanged);
            // 
            // txtTotalDeadline
            // 
            this.txtTotalDeadline.Location = new System.Drawing.Point(280, 84);
            this.txtTotalDeadline.Name = "txtTotalDeadline";
            this.txtTotalDeadline.ReadOnly = true;
            this.txtTotalDeadline.Size = new System.Drawing.Size(139, 20);
            this.txtTotalDeadline.TabIndex = 41;
            this.txtTotalDeadline.TextChanged += new System.EventHandler(this.txtTotalDeadline_TextChanged);
            // 
            // txtRespectedDeadline
            // 
            this.txtRespectedDeadline.Location = new System.Drawing.Point(280, 58);
            this.txtRespectedDeadline.Name = "txtRespectedDeadline";
            this.txtRespectedDeadline.ReadOnly = true;
            this.txtRespectedDeadline.Size = new System.Drawing.Size(139, 20);
            this.txtRespectedDeadline.TabIndex = 39;
            this.txtRespectedDeadline.TextChanged += new System.EventHandler(this.txtRespectedDeadline_TextChanged);
            // 
            // txtGES
            // 
            this.txtGES.Location = new System.Drawing.Point(280, 292);
            this.txtGES.Name = "txtGES";
            this.txtGES.ReadOnly = true;
            this.txtGES.Size = new System.Drawing.Size(139, 20);
            this.txtGES.TabIndex = 37;
            this.txtGES.TextChanged += new System.EventHandler(this.txtGES_TextChanged);
            // 
            // txtGESTotal
            // 
            this.txtGESTotal.Location = new System.Drawing.Point(280, 266);
            this.txtGESTotal.Name = "txtGESTotal";
            this.txtGESTotal.ReadOnly = true;
            this.txtGESTotal.Size = new System.Drawing.Size(139, 20);
            this.txtGESTotal.TabIndex = 37;
            this.txtGESTotal.TextChanged += new System.EventHandler(this.txtGESTotal_TextChanged);
            // 
            // txtPin
            // 
            this.txtPin.Location = new System.Drawing.Point(280, 32);
            this.txtPin.Name = "txtPin";
            this.txtPin.ReadOnly = true;
            this.txtPin.Size = new System.Drawing.Size(139, 20);
            this.txtPin.TabIndex = 35;
            this.txtPin.TextChanged += new System.EventHandler(this.txtPin_TextChanged);
            // 
            // lblNbrNetworkLinks
            // 
            this.lblNbrNetworkLinks.AutoSize = true;
            this.lblNbrNetworkLinks.Location = new System.Drawing.Point(10, 347);
            this.lblNbrNetworkLinks.Name = "lblNbrNetworkLinks";
            this.lblNbrNetworkLinks.Size = new System.Drawing.Size(183, 13);
            this.lblNbrNetworkLinks.TabIndex = 56;
            this.lblNbrNetworkLinks.Text = "Nombre de liens dans le réseau (NLi):";
            // 
            // lblNbrProcessor
            // 
            this.lblNbrProcessor.AutoSize = true;
            this.lblNbrProcessor.Location = new System.Drawing.Point(10, 321);
            this.lblNbrProcessor.Name = "lblNbrProcessor";
            this.lblNbrProcessor.Size = new System.Drawing.Size(149, 13);
            this.lblNbrProcessor.TabIndex = 54;
            this.lblNbrProcessor.Text = "Nombre de processeurs (NPr):";
            // 
            // lblTotalTimebloc
            // 
            this.lblTotalTimebloc.AutoSize = true;
            this.lblTotalTimebloc.Location = new System.Drawing.Point(10, 243);
            this.lblTotalTimebloc.Name = "lblTotalTimebloc";
            this.lblTotalTimebloc.Size = new System.Drawing.Size(164, 13);
            this.lblTotalTimebloc.TabIndex = 50;
            this.lblTotalTimebloc.Text = "Temps de blocage maximal (TBt):";
            this.lblTotalTimebloc.Click += new System.EventHandler(this.lblTotalTimebloc_Click);
            // 
            // lblMaxTimeBloc
            // 
            this.lblMaxTimeBloc.AutoSize = true;
            this.lblMaxTimeBloc.Location = new System.Drawing.Point(10, 217);
            this.lblMaxTimeBloc.Name = "lblMaxTimeBloc";
            this.lblMaxTimeBloc.Size = new System.Drawing.Size(190, 13);
            this.lblMaxTimeBloc.TabIndex = 48;
            this.lblMaxTimeBloc.Text = "Maximum du temps de blocage (TBm): ";
            this.lblMaxTimeBloc.Click += new System.EventHandler(this.lblMaxTimeBloc_Click);
            // 
            // lblStdDevChargeProcessor
            // 
            this.lblStdDevChargeProcessor.AutoSize = true;
            this.lblStdDevChargeProcessor.Location = new System.Drawing.Point(10, 191);
            this.lblStdDevChargeProcessor.Name = "lblStdDevChargeProcessor";
            this.lblStdDevChargeProcessor.Size = new System.Drawing.Size(257, 13);
            this.lblStdDevChargeProcessor.TabIndex = 46;
            this.lblStdDevChargeProcessor.Text = "Écart Type Chargement Temporel Processeurs (Tle): ";
            this.lblStdDevChargeProcessor.Click += new System.EventHandler(this.lblStdDevChargeProcessor_Click);
            // 
            // lblMeanChargeProcessor
            // 
            this.lblMeanChargeProcessor.AutoSize = true;
            this.lblMeanChargeProcessor.Location = new System.Drawing.Point(10, 165);
            this.lblMeanChargeProcessor.Name = "lblMeanChargeProcessor";
            this.lblMeanChargeProcessor.Size = new System.Drawing.Size(255, 13);
            this.lblMeanChargeProcessor.TabIndex = 44;
            this.lblMeanChargeProcessor.Text = "Moyenne Chargement Temporel Processeurs (TLm): ";
            this.lblMeanChargeProcessor.Click += new System.EventHandler(this.lblMeanChargeProcessor_Click);
            // 
            // lblDmi
            // 
            this.lblDmi.AutoSize = true;
            this.lblDmi.Location = new System.Drawing.Point(10, 139);
            this.lblDmi.Name = "lblDmi";
            this.lblDmi.Size = new System.Drawing.Size(169, 13);
            this.lblDmi.TabIndex = 42;
            this.lblDmi.Text = "durée minimale du parcours (Dmi): ";
            this.lblDmi.Click += new System.EventHandler(this.lblDmi_Click);
            // 
            // lblDpa
            // 
            this.lblDpa.AutoSize = true;
            this.lblDpa.Location = new System.Drawing.Point(10, 113);
            this.lblDpa.Name = "lblDpa";
            this.lblDpa.Size = new System.Drawing.Size(169, 13);
            this.lblDpa.TabIndex = 42;
            this.lblDpa.Text = "Durée du parcours effectué (Dpa):";
            this.lblDpa.Click += new System.EventHandler(this.lblDpa_Click);
            // 
            // lblTotalDeadline
            // 
            this.lblTotalDeadline.AutoSize = true;
            this.lblTotalDeadline.Location = new System.Drawing.Point(10, 87);
            this.lblTotalDeadline.Name = "lblTotalDeadline";
            this.lblTotalDeadline.Size = new System.Drawing.Size(161, 13);
            this.lblTotalDeadline.TabIndex = 42;
            this.lblTotalDeadline.Text = "Nombre total d\'échéance (Nec): ";
            this.lblTotalDeadline.Click += new System.EventHandler(this.lblTotalDeadline_Click);
            // 
            // lblRespectedDeadline
            // 
            this.lblRespectedDeadline.AutoSize = true;
            this.lblRespectedDeadline.Location = new System.Drawing.Point(10, 61);
            this.lblRespectedDeadline.Name = "lblRespectedDeadline";
            this.lblRespectedDeadline.Size = new System.Drawing.Size(131, 13);
            this.lblRespectedDeadline.TabIndex = 40;
            this.lblRespectedDeadline.Text = "Échéance respecté (Ere): ";
            this.lblRespectedDeadline.Click += new System.EventHandler(this.lblRespectedDeadline_Click);
            // 
            // lblGES
            // 
            this.lblGES.AutoSize = true;
            this.lblGES.Location = new System.Drawing.Point(10, 295);
            this.lblGES.Name = "lblGES";
            this.lblGES.Size = new System.Drawing.Size(133, 13);
            this.lblGES.TabIndex = 38;
            this.lblGES.Text = "GES Minimum émis (Tem): ";
            this.lblGES.Click += new System.EventHandler(this.lblGES_Click);
            // 
            // lblGESTotal
            // 
            this.lblGESTotal.AutoSize = true;
            this.lblGESTotal.Location = new System.Drawing.Point(10, 269);
            this.lblGESTotal.Name = "lblGESTotal";
            this.lblGESTotal.Size = new System.Drawing.Size(118, 13);
            this.lblGESTotal.TabIndex = 38;
            this.lblGESTotal.Text = "GES Total émis (Mem): ";
            this.lblGESTotal.Click += new System.EventHandler(this.lblGESTotal_Click);
            // 
            // lblPin
            // 
            this.lblPin.AutoSize = true;
            this.lblPin.Location = new System.Drawing.Point(10, 35);
            this.lblPin.Name = "lblPin";
            this.lblPin.Size = new System.Drawing.Size(118, 13);
            this.lblPin.TabIndex = 36;
            this.lblPin.Text = "Point d\'inaptitude (Pin): ";
            this.lblPin.Click += new System.EventHandler(this.lblPin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(277, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Unités :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Critères ";
            // 
            // Stats2
            // 
            this.Stats2.AutoScroll = true;
            this.Stats2.Controls.Add(this.panel1);
            this.Stats2.Location = new System.Drawing.Point(4, 22);
            this.Stats2.Name = "Stats2";
            this.Stats2.Size = new System.Drawing.Size(469, 174);
            this.Stats2.TabIndex = 3;
            this.Stats2.Text = "Statistiques 2";
            this.Stats2.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtStatTotal);
            this.panel1.Controls.Add(this.txtabsPin);
            this.panel1.Controls.Add(this.txtOptimalOper);
            this.panel1.Controls.Add(this.txtEffParcours);
            this.panel1.Controls.Add(this.txtRespContrtmps);
            this.panel1.Controls.Add(this.txtMinimArchi);
            this.panel1.Controls.Add(this.lblStatTotal);
            this.panel1.Controls.Add(this.lblabsPin);
            this.panel1.Controls.Add(this.lblOptimalOper);
            this.panel1.Controls.Add(this.lblEffParcours);
            this.panel1.Controls.Add(this.lblRespContrtmps);
            this.panel1.Controls.Add(this.lblMinimArchi);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(469, 174);
            this.panel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(277, 6);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 16);
            this.label3.TabIndex = 72;
            this.label3.Text = "Unités :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(66, 16);
            this.label5.TabIndex = 71;
            this.label5.Text = "Critères ";
            // 
            // txtStatTotal
            // 
            this.txtStatTotal.Location = new System.Drawing.Point(280, 159);
            this.txtStatTotal.Name = "txtStatTotal";
            this.txtStatTotal.ReadOnly = true;
            this.txtStatTotal.Size = new System.Drawing.Size(139, 20);
            this.txtStatTotal.TabIndex = 69;
            // 
            // txtabsPin
            // 
            this.txtabsPin.Location = new System.Drawing.Point(280, 29);
            this.txtabsPin.Name = "txtabsPin";
            this.txtabsPin.ReadOnly = true;
            this.txtabsPin.Size = new System.Drawing.Size(139, 20);
            this.txtabsPin.TabIndex = 59;
            // 
            // txtOptimalOper
            // 
            this.txtOptimalOper.Location = new System.Drawing.Point(280, 107);
            this.txtOptimalOper.Name = "txtOptimalOper";
            this.txtOptimalOper.ReadOnly = true;
            this.txtOptimalOper.Size = new System.Drawing.Size(139, 20);
            this.txtOptimalOper.TabIndex = 60;
            // 
            // txtEffParcours
            // 
            this.txtEffParcours.Location = new System.Drawing.Point(280, 81);
            this.txtEffParcours.Name = "txtEffParcours";
            this.txtEffParcours.ReadOnly = true;
            this.txtEffParcours.Size = new System.Drawing.Size(139, 20);
            this.txtEffParcours.TabIndex = 61;
            // 
            // txtRespContrtmps
            // 
            this.txtRespContrtmps.Location = new System.Drawing.Point(280, 55);
            this.txtRespContrtmps.Name = "txtRespContrtmps";
            this.txtRespContrtmps.ReadOnly = true;
            this.txtRespContrtmps.Size = new System.Drawing.Size(139, 20);
            this.txtRespContrtmps.TabIndex = 62;
            // 
            // txtMinimArchi
            // 
            this.txtMinimArchi.Location = new System.Drawing.Point(280, 133);
            this.txtMinimArchi.Name = "txtMinimArchi";
            this.txtMinimArchi.ReadOnly = true;
            this.txtMinimArchi.Size = new System.Drawing.Size(139, 20);
            this.txtMinimArchi.TabIndex = 63;
            // 
            // lblStatTotal
            // 
            this.lblStatTotal.AutoSize = true;
            this.lblStatTotal.Location = new System.Drawing.Point(10, 162);
            this.lblStatTotal.Name = "lblStatTotal";
            this.lblStatTotal.Size = new System.Drawing.Size(37, 13);
            this.lblStatTotal.TabIndex = 70;
            this.lblStatTotal.Text = "Total: ";
            // 
            // lblabsPin
            // 
            this.lblabsPin.AutoSize = true;
            this.lblabsPin.Location = new System.Drawing.Point(10, 32);
            this.lblabsPin.Name = "lblabsPin";
            this.lblabsPin.Size = new System.Drawing.Size(158, 13);
            this.lblabsPin.TabIndex = 64;
            this.lblabsPin.Text = "Absence de points d\'inaptitude: ";
            // 
            // lblOptimalOper
            // 
            this.lblOptimalOper.AutoSize = true;
            this.lblOptimalOper.Location = new System.Drawing.Point(10, 110);
            this.lblOptimalOper.Name = "lblOptimalOper";
            this.lblOptimalOper.Size = new System.Drawing.Size(101, 13);
            this.lblOptimalOper.TabIndex = 65;
            this.lblOptimalOper.Text = "Opération optimale: ";
            // 
            // lblEffParcours
            // 
            this.lblEffParcours.AutoSize = true;
            this.lblEffParcours.Location = new System.Drawing.Point(10, 84);
            this.lblEffParcours.Name = "lblEffParcours";
            this.lblEffParcours.Size = new System.Drawing.Size(116, 13);
            this.lblEffParcours.TabIndex = 66;
            this.lblEffParcours.Text = "Efficacite du parcours: ";
            // 
            // lblRespContrtmps
            // 
            this.lblRespContrtmps.AutoSize = true;
            this.lblRespContrtmps.Location = new System.Drawing.Point(10, 58);
            this.lblRespContrtmps.Name = "lblRespContrtmps";
            this.lblRespContrtmps.Size = new System.Drawing.Size(174, 13);
            this.lblRespContrtmps.TabIndex = 67;
            this.lblRespContrtmps.Text = "Respect des contraintes de temps: ";
            // 
            // lblMinimArchi
            // 
            this.lblMinimArchi.AutoSize = true;
            this.lblMinimArchi.Location = new System.Drawing.Point(10, 136);
            this.lblMinimArchi.Name = "lblMinimArchi";
            this.lblMinimArchi.Size = new System.Drawing.Size(130, 13);
            this.lblMinimArchi.TabIndex = 68;
            this.lblMinimArchi.Text = "Minimalisme architectural: ";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtDelayBetweenUT);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.simulProgress);
            this.panel2.Location = new System.Drawing.Point(0, 190);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(438, 86);
            this.panel2.TabIndex = 1;
            // 
            // txtDelayBetweenUT
            // 
            this.txtDelayBetweenUT.Location = new System.Drawing.Point(219, 57);
            this.txtDelayBetweenUT.Name = "txtDelayBetweenUT";
            this.txtDelayBetweenUT.Size = new System.Drawing.Size(139, 20);
            this.txtDelayBetweenUT.TabIndex = 33;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(38, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Délais en milliseconde entre les ut  :";
            // 
            // simulProgress
            // 
            this.simulProgress.ForeColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.simulProgress.Location = new System.Drawing.Point(12, 3);
            this.simulProgress.Name = "simulProgress";
            this.simulProgress.Size = new System.Drawing.Size(417, 43);
            this.simulProgress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.simulProgress.TabIndex = 1;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabel1,
            this.cbOrderType,
            this.btnSimAll,
            this.btnPause,
            this.btnSim1,
            this.btnResetSim});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(837, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(104, 22);
            this.toolStripLabel1.Text = "Ordonnancement:";
            // 
            // cbOrderType
            // 
            this.cbOrderType.DropDownWidth = 139;
            this.cbOrderType.Name = "cbOrderType";
            this.cbOrderType.Size = new System.Drawing.Size(121, 25);
            // 
            // btnSimAll
            // 
            this.btnSimAll.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnSimAll.Image = global::SimulationSPTR.Properties.Resources.play;
            this.btnSimAll.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSimAll.Name = "btnSimAll";
            this.btnSimAll.Size = new System.Drawing.Size(23, 22);
            this.btnSimAll.Text = "Simuler";
            this.btnSimAll.Click += new System.EventHandler(this.btnSimAll_Click);
            // 
            // btnPause
            // 
            this.btnPause.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPause.Image = global::SimulationSPTR.Properties.Resources.pause;
            this.btnPause.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(23, 22);
            this.btnPause.Text = "Pause";
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnSim1
            // 
            this.btnSim1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnSim1.Image = ((System.Drawing.Image)(resources.GetObject("btnSim1.Image")));
            this.btnSim1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnSim1.Name = "btnSim1";
            this.btnSim1.Size = new System.Drawing.Size(25, 22);
            this.btnSim1.Text = "+1";
            this.btnSim1.Click += new System.EventHandler(this.simulerButton_Click);
            // 
            // btnResetSim
            // 
            this.btnResetSim.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnResetSim.Image = global::SimulationSPTR.Properties.Resources.stop;
            this.btnResetSim.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnResetSim.Name = "btnResetSim";
            this.btnResetSim.Size = new System.Drawing.Size(23, 22);
            this.btnResetSim.Text = "Recommencer";
            this.btnResetSim.Click += new System.EventHandler(this.btnResetSim_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 661);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(837, 22);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aideToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(148, 26);
            this.contextMenuStrip1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.contextMenuStrip1_MouseClick);
            // 
            // aideToolStripMenuItem
            // 
            this.aideToolStripMenuItem.Name = "aideToolStripMenuItem";
            this.aideToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.aideToolStripMenuItem.Text = "À propos de...";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(837, 683);
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(700, 200);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SimulationSPTR";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.Carte.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureCarte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.processOrderGrid)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FeuJaunesign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuRougesign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuVertsign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEngSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ABSsign)).EndInit();
            this.tabInterfaceControl.ResumeLayout(false);
            this.Paramètres.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.Statistiques.ResumeLayout(false);
            this.Statistiques.PerformLayout();
            this.Stats2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView processOrderGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ut;
        private System.Windows.Forms.DataGridViewTextBoxColumn process1;
        private System.Windows.Forms.DataGridViewTextBoxColumn process2;
        private System.Windows.Forms.DataGridViewTextBoxColumn process3;
        private System.Windows.Forms.DataGridViewTextBoxColumn process4;
        private System.Windows.Forms.DataGridViewTextBoxColumn process5;
        private System.Windows.Forms.DataGridViewTextBoxColumn process6;
        private System.Windows.Forms.DataGridViewTextBoxColumn process7;
        private System.Windows.Forms.DataGridViewTextBoxColumn process8;
        private System.Windows.Forms.DataGridViewTextBoxColumn process9;
        private System.Windows.Forms.DataGridViewTextBoxColumn process10;
        private System.Windows.Forms.DataGridViewTextBoxColumn process11;
        private System.Windows.Forms.DataGridViewTextBoxColumn process12;
        private System.Windows.Forms.DataGridViewTextBoxColumn process13;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Panel Carte;
        private System.Windows.Forms.PictureBox pictureCarte;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cbOrderType;
        private System.Windows.Forms.ToolStripButton btnSimAll;
        private System.Windows.Forms.ToolStripButton btnPause;
        private System.Windows.Forms.ToolStripButton btnSim1;
        private System.Windows.Forms.ToolStripButton btnResetSim;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label Fctlabel;
        private System.Windows.Forms.PictureBox FeuJaunesign;
        private System.Windows.Forms.PictureBox FeuRougesign;
        private System.Windows.Forms.PictureBox FeuVertsign;
        private System.Windows.Forms.PictureBox CheckEngSign;
        private System.Windows.Forms.PictureBox ABSsign;
        private System.Windows.Forms.Label UTlabel;
        private System.Windows.Forms.Label Intersectionlabel;
        private System.Windows.Forms.Label Publabel;
        private System.Windows.Forms.Label Meteolabel;
        private System.Windows.Forms.Label Dirlabel;
        private System.Windows.Forms.Label Templabel;
        private System.Windows.Forms.Label Odolabel;
        private System.Windows.Forms.Label SpeedLabel;
        private System.Windows.Forms.Label PositionLabel;
        private System.Windows.Forms.TabControl tabInterfaceControl;
        private System.Windows.Forms.TabPage Paramètres;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox vitesseMaxAffiche;
        private System.Windows.Forms.TextBox accelerationMaxAffiche;
        private System.Windows.Forms.Label vitesseMaxLabel;
        private System.Windows.Forms.Label lblArrivalX;
        private System.Windows.Forms.Label accelerationMaxLabel;
        private System.Windows.Forms.TextBox txtArrivalX;
        private System.Windows.Forms.Label lblArrivalY;
        private System.Windows.Forms.TextBox txtArrivalY;
        private System.Windows.Forms.Label lblDepartX;
        private System.Windows.Forms.TextBox txtDepartX;
        private System.Windows.Forms.Label lblDepartY;
        private System.Windows.Forms.TextBox txtDepartY;
        private System.Windows.Forms.TextBox consoAffiche;
        private System.Windows.Forms.Label consoLabel;
        private System.Windows.Forms.TextBox facteurEchelleAffiche;
        private System.Windows.Forms.TextBox dureeReparationAffiche;
        private System.Windows.Forms.Label facteurEchelleLabel;
        private System.Windows.Forms.Label dureeReparationLabel;
        private System.Windows.Forms.TextBox dureeSimulAffiche;
        private System.Windows.Forms.Label dureeSimulLabel;
        private System.Windows.Forms.TextBox dureeCollisionAffiche;
        private System.Windows.Forms.Label dureeCollisionLabel;
        private System.Windows.Forms.TabPage Statistiques;
        private System.Windows.Forms.TextBox txtNbrNetworkLinks;
        private System.Windows.Forms.TextBox txtNbrProcessor;
        private System.Windows.Forms.TextBox txtTotalTimeBloc;
        private System.Windows.Forms.TextBox txtMaxTimebloc;
        private System.Windows.Forms.TextBox txtStdDevChargeProcessor;
        private System.Windows.Forms.TextBox txtMeanChargeProcessor;
        private System.Windows.Forms.TextBox txtDmi;
        private System.Windows.Forms.TextBox txtDpa;
        private System.Windows.Forms.TextBox txtTotalDeadline;
        private System.Windows.Forms.TextBox txtRespectedDeadline;
        private System.Windows.Forms.TextBox txtGES;
        private System.Windows.Forms.TextBox txtGESTotal;
        private System.Windows.Forms.TextBox txtPin;
        private System.Windows.Forms.Label lblNbrNetworkLinks;
        private System.Windows.Forms.Label lblNbrProcessor;
        private System.Windows.Forms.Label lblTotalTimebloc;
        private System.Windows.Forms.Label lblMaxTimeBloc;
        private System.Windows.Forms.Label lblStdDevChargeProcessor;
        private System.Windows.Forms.Label lblMeanChargeProcessor;
        private System.Windows.Forms.Label lblDmi;
        private System.Windows.Forms.Label lblDpa;
        private System.Windows.Forms.Label lblTotalDeadline;
        private System.Windows.Forms.Label lblRespectedDeadline;
        private System.Windows.Forms.Label lblGES;
        private System.Windows.Forms.Label lblGESTotal;
        private System.Windows.Forms.Label lblPin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Stats2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtStatTotal;
        private System.Windows.Forms.TextBox txtabsPin;
        private System.Windows.Forms.TextBox txtOptimalOper;
        private System.Windows.Forms.TextBox txtEffParcours;
        private System.Windows.Forms.TextBox txtRespContrtmps;
        private System.Windows.Forms.TextBox txtMinimArchi;
        private System.Windows.Forms.Label lblStatTotal;
        private System.Windows.Forms.Label lblabsPin;
        private System.Windows.Forms.Label lblOptimalOper;
        private System.Windows.Forms.Label lblEffParcours;
        private System.Windows.Forms.Label lblRespContrtmps;
        private System.Windows.Forms.Label lblMinimArchi;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtDelayBetweenUT;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ProgressBar simulProgress;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem aideToolStripMenuItem;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
}

