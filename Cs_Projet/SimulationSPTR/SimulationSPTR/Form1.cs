using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

 public partial class Form1 : Form
 {
   
    private System.Windows.Forms.Timer timer1;

    Simulateur mSim;
    SimulationXML param = SimulationXML.Instance;
    string fileName;
    private Map mCarte;
    bool mSimThreadCancel;
    string mRandObjLock = "NotNull";
    bool mSimRunning;
    string mSimRunningLock = "NotNull";
    double mDmi;

    public Form1()
    {
      InitializeComponent();

      if (param.setXMLFilename() == false)
      {
          Environment.Exit(1);
      }


      fileName = param.mFilename; 
      param.readParametres(fileName);
      param.readRoute(fileName);
      param.readFeu(fileName);
      param.readTrafic(fileName);
      mCarte = new Map();
      mSimThreadCancel = false;
      simulProgress.Minimum = 0;
      simulProgress.Maximum = 1;
      mSimRunning = false;
      mSim = new Simulateur();

      R02CarteRoutiere.Instance.init();
      mCarte.clearRoad();
      
      foreach (var value in R02CarteRoutiere.Instance.calculChemin.mRoutes)
      {
        Road route = new Road(new Point(value.Debut.X / SimulationXML.Instance.Echelle, value.Debut.Y / SimulationXML.Instance.Echelle), new Point(value.Fin.X / SimulationXML.Instance.Echelle, value.Fin.Y / SimulationXML.Instance.Echelle));
        mCarte.addRoute(route);
      }

      
      foreach (SimulationXML.FeuStructure i in param.Feu)
      {
        Feu feux = new Feu(new Point(i.CoordonneeX, i.CoordonneeY), Color.Red, i.Position);
        mCarte.addFeu(feux);
      }

      mCarte.MoveAuto(new Point(param.XDepart, param.YDepart), R07GPS.directionEnum.UNDEFINED);

      components = new System.ComponentModel.Container();
      timer1 = new System.Windows.Forms.Timer(components);
      timer1.Tick += new System.EventHandler(timer1_Tick);
      timer1.Interval = 10;
      timer1.Start();
      chercherXml_Click(new Object(),new EventArgs());

    }

    private void timer1_Tick(object sender, System.EventArgs e)
    {
      UpdateInterface();
    }
    
    private void UpdateInterface()
    {
      
        SpeedLabel.Text = R03CompteurDeVitesse.Instance.vitesse.ToString();
        Odolabel.Text = R08Odometre.Instance.Odometre.ToString();
        Templabel.Text = R05Environnement.Instance.temperatureExterieur.ToString();
        Meteolabel.Text = R11StationMeteo.Instance.CurrentMeteo.ToString();
        Fctlabel.Text = R04ModuleAffichage.Instance.DerniereCommande.ToString();
        Publabel.Text = R04ModuleAffichage.Instance.MessagePublicCommerciaux.ToString();
        UTlabel.Text = mSim.CurrentUT.ToString();

        if (R07GPS.Instance.CurrentDirection == R07GPS.directionEnum.UNDEFINED)
            Dirlabel.Text = "LOST";
        else
            Dirlabel.Text = R07GPS.Instance.CurrentDirection.ToString();

        if (R05Environnement.Instance.EtatVehicule == R05Environnement.EtatVehiculeEnum.BRISE)
            CheckEngSign.Visible = true;
        else
            CheckEngSign.Visible = false;

        if (R03CompteurDeVitesse.Instance.frienABSEnAction != 0)
            ABSsign.Visible = true;
        else
            ABSsign.Visible = false;




      txtPin.Text = R04ModuleAffichage.Instance.PointInaptitude.ToString();
      double Temp = 0, Temp2 = 0;
      mSim.ProcessorsInSim.ForEach(x=> Temp += x.Ordonnenceur.NbPeriodeRespectee);
      txtRespectedDeadline.Text = Temp.ToString();
        double Temp1 = Temp;
      mSim.ProcessorsInSim.ForEach(x => Temp += x.Ordonnenceur.NbPeriodesNonRespectees);
      txtTotalDeadline.Text = Temp.ToString();
        double Temp3 = Temp;
      Temp = 0;
      if (mSim.ProcessorsInSim.Count > 0)
      {
        mSim.ProcessorsInSim.Where(x => x.Ordonnenceur.NbProcesseurUtilises + x.Ordonnenceur.NbProcesseurNonUtilises > 0).ToList().ForEach(x => Temp += (double)x.Ordonnenceur.NbProcesseurUtilises / (double)(x.Ordonnenceur.NbProcesseurUtilises + x.Ordonnenceur.NbProcesseurNonUtilises));
        Temp /= mSim.ProcessorsInSim.Count;
        
        mSim.ProcessorsInSim.Where(x => x.Ordonnenceur.NbProcesseurUtilises + x.Ordonnenceur.NbProcesseurNonUtilises > 0).ToList().ForEach(x => Temp2 += Math.Pow(((double)x.Ordonnenceur.NbProcesseurUtilises / (double)(x.Ordonnenceur.NbProcesseurUtilises + x.Ordonnenceur.NbProcesseurNonUtilises) - Temp), 2));
        Temp2 /= mSim.ProcessorsInSim.Count;
        Temp2 = Math.Pow(Temp2, 0.5);
      }
      txtMeanChargeProcessor.Text = Temp.ToString();
      double MeanCharge = Temp;
      txtStdDevChargeProcessor.Text = Temp2.ToString();
      double StdDevCharge = Temp2;
      Temp = 0;
      mSim.ProcessorsInSim.ForEach(x=> x.Ordonnenceur.NbProcessusBloquesMaximum.ToList().ForEach(y => Temp = Math.Max(y.Value, Temp)));
      double TotalBloc = Temp;
      txtMaxTimebloc.Text = Temp.ToString();
      double MaxTimebloc = Temp;
      Temp = 0;
      mSim.ProcessorsInSim.ForEach(x=> x.Ordonnenceur.NbProcessusBloquesMaximum.ToList().ForEach(y => Temp += y.Value));
      txtTotalTimeBloc.Text = Temp.ToString();
      double TotalTimeBloc = Temp;  
      txtGESTotal.Text = R06GES.Instance.GESTotalEmis.ToString();
      txtGES.Text = R06GES.Instance.getMinimumGESParcours().ToString();
      txtNbrProcessor.Text = mSim.ProcessorsInSim.Count.ToString();
      txtNbrNetworkLinks.Text = mSim.Network.getNbLien.ToString();
      double Pin = 20, RespCtrTmps = 20, EffParcour = 20, OptimalOper = 20, MiniArchi = 20;
      Pin = 0.2*(100.0 - R04ModuleAffichage.Instance.PointInaptitude);
      txtabsPin.Text = Pin.ToString();
      RespCtrTmps = 0.2*100*Temp1/Temp3;
      txtRespContrtmps.Text = RespCtrTmps.ToString();

      txtDpa.Text = mSim.CurrentUT.ToString();

      txtDmi.Text = mDmi.ToString();

      if (mSim.CurrentUT > 0 && (mDmi / (double)mSim.CurrentUT <= 1.0))
      { EffParcour = 0.2 * 100.0 * mDmi / (double)mSim.CurrentUT; }
      txtEffParcours.Text = EffParcour.ToString();
      OptimalOper = 0.2 * 25.0 * (MeanCharge + (1.0 - StdDevCharge) + (TotalTimeBloc - MaxTimebloc) / TotalTimeBloc + (double)R06GES.Instance.getMinimumGESParcours() / (double)R06GES.Instance.GESTotalEmis);
      if (TotalTimeBloc == 0 || R06GES.Instance.GESTotalEmis == 0) { txtOptimalOper.Text = "20"; }
      else { txtOptimalOper.Text = OptimalOper.ToString(); }
      MiniArchi = 0.2 * 50.0 * ((13.0 - (double)mSim.ProcessorsInSim.Count) / 12.0 + (((double)mSim.ProcessorsInSim.Count * ((double)mSim.ProcessorsInSim.Count - 1.0) / 2.0 - (double)mSim.Network.getNbLien) / ((double)mSim.ProcessorsInSim.Count * ((double)mSim.ProcessorsInSim.Count - 1.0) / 2.0 - ((double)mSim.ProcessorsInSim.Count - 1.0))));
      txtMinimArchi.Text = MiniArchi.ToString();
      txtStatTotal.Text = (Pin + RespCtrTmps + EffParcour + OptimalOper + MiniArchi).ToString();
      
      PositionLabel.Text = R07GPS.Instance.PositionVoiture.ToString();

      if (R07GPS.Instance.CurrentDirection != R07GPS.directionEnum.UNDEFINED)
      {
          Point prochainIntersection = R02CarteRoutiere.Instance.calculChemin.NextNode(R07GPS.Instance.PositionVoiture, R07GPS.Instance.CurrentDirection);
        FeuIntersection FeuIntersection = R05Environnement.Instance.listFeuIntersection.Find(x => x.FeuPosition == prochainIntersection);
        
        string text;
        if (FeuIntersection == null)
        {
            FeuVertsign.Visible = true;
            FeuJaunesign.Visible = false;
            FeuRougesign.Visible = false;
            Intersectionlabel.Text = "END";
        }
              
        else if (FeuIntersection.directionVertJaune == R07GPS.Instance.CurrentDirection)
        {
            text = FeuIntersection.EtatFeuVertJaune.ToString();
            if (text == "VERT")
            {
                FeuVertsign.Visible = true;
                FeuJaunesign.Visible = false;
                FeuRougesign.Visible = false;
                Intersectionlabel.Text = '(' + ((FeuIntersection.FeuPosition.X) / 256).ToString() + ',' + ((FeuIntersection.FeuPosition.Y) / 256).ToString() + ") " + (R07GPS.Instance.CurrentDirection.ToString()).Substring(0, 1);
            }
            else
            {
                FeuVertsign.Visible = false;
                FeuJaunesign.Visible = true;
                FeuRougesign.Visible = false;
                Intersectionlabel.Text = '(' + ((FeuIntersection.FeuPosition.X) / 256).ToString() + ',' + ((FeuIntersection.FeuPosition.Y) / 256).ToString() + ") " + (R07GPS.Instance.CurrentDirection.ToString()).Substring(0, 1);
            }
        }
        else
        {
            Intersectionlabel.Text = "RIEN";
            FeuVertsign.Visible = false;
            FeuJaunesign.Visible = false;
            FeuRougesign.Visible =true;
            Intersectionlabel.Text = '('+((FeuIntersection.FeuPosition.X) / 256).ToString() + ',' + ((FeuIntersection.FeuPosition.Y) / 256).ToString() + ") " + (R07GPS.Instance.CurrentDirection.ToString()).Substring(0, 1);

        }       
        
      }



      

      mCarte.MoveAuto(R07GPS.Instance.PositionVoiture, R07GPS.Instance.CurrentDirection);

      if (R05Environnement.Instance.traficAuto != null)
        mCarte.MoveTrafic(R05Environnement.Instance.traficAuto.etatTraficRoutier);

      R07GPS.directionEnum Direction = R07GPS.Instance.CurrentDirection;
      if (SimulationXML.Instance.TempsSimulation != 0)
      {
        simulProgress.Maximum = SimulationXML.Instance.TempsSimulation;
        simulProgress.Value = mSim.CurrentUT;
      }

      Odolabel.Text = R08Odometre.Instance.Odometre.ToString();

      mCarte.updateFeuColor();
      processOrderGrid.ResumeLayout(true);
      processOrderGrid.SuspendLayout();

      Carte.Refresh();
    }
    
    private void chercherXml_Click(object sender, EventArgs e)
    {
      
      SimulationXML.Instance.readTousLesParametres();
      
      
      

      
      

      
      dureeSimulAffiche.Text = SimulationXML.Instance.TempsSimulation.ToString();
      dureeReparationAffiche.Text = SimulationXML.Instance.AutoReparation.ToString();
      dureeCollisionAffiche.Text = SimulationXML.Instance.Collision.ToString();
      cbOrderType.SelectedItem = SimulationXML.Instance.OrderType;
      facteurEchelleAffiche.Text = SimulationXML.Instance.Echelle.ToString();
      consoAffiche.Text = SimulationXML.Instance.Consommation.ToString();
      txtDepartX.Text = ((SimulationXML.Instance.XDepart)/SimulationXML.Instance.Echelle).ToString();
      txtDepartY.Text = ((SimulationXML.Instance.YDepart)/SimulationXML.Instance.Echelle).ToString();
      txtArrivalX.Text = ((SimulationXML.Instance.XArrivee) / SimulationXML.Instance.Echelle).ToString();
      txtArrivalY.Text = ((SimulationXML.Instance.YArrivee) / SimulationXML.Instance.Echelle).ToString();
      vitesseMaxAffiche.Text = SimulationXML.Instance.Vitesse.ToString();
      accelerationMaxAffiche.Text = SimulationXML.Instance.Acceleration.ToString();
    }
    
    private void Form1_Load(object sender, EventArgs e)
    {
      
      foreach (Ordonnanceur.Ordonnancement aOT in Enum.GetValues(typeof(Ordonnanceur.Ordonnancement)))
      {
        if (aOT != Ordonnanceur.Ordonnancement.Undefined) { cbOrderType.Items.Add(aOT); }
      }
      cbOrderType.SelectedItem = cbOrderType.Items[0];

      
      splitContainer1.SplitterDistance = splitContainer1.Width - panel5.Width;
      splitContainer3.SplitterDistance = panel5.Height;

      processOrderGrid.Columns[0].Width = 50;
    }
    
    private void simulerButton_Click(object sender, EventArgs e)
    {
      InitSim();
      mSim.ExecuteUT();
      UpdateProcessusList(mSim.CurrentUT - 1);
    }
    
    private void btnSimAll_Click(object sender, EventArgs e)
    {
      InitSim();
      System.Threading.ThreadPool.QueueUserWorkItem((new WaitCallback(SimulationCallWrapper)), null);
    }

     private void SimulationCallWrapper(object pNull)
    {
      int DelayBetweenUT = 10;
      bool aKillSignal = false;
      bool aReachedEnd = (SimulationXML.Instance.XArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.X && (SimulationXML.Instance.XArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.X &&
        (SimulationXML.Instance.YArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.Y && (SimulationXML.Instance.YArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.Y;
      int.TryParse(txtDelayBetweenUT.Text, out DelayBetweenUT);
      lock (mSimRunningLock) { mSimRunning = true; }
      
      while (mSim.CurrentUT < SimulationXML.Instance.TempsSimulation && !aKillSignal && !aReachedEnd)
      {
        mSim.ExecuteUT();
        lock (mRandObjLock) { aKillSignal = mSimThreadCancel; }
        UpdateProcessusList(mSim.CurrentUT - 1);
        aReachedEnd = (SimulationXML.Instance.XArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.X && (SimulationXML.Instance.XArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.X &&
          (SimulationXML.Instance.YArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.Y && (SimulationXML.Instance.YArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.Y;
        Thread.Sleep(DelayBetweenUT);
      }
      lock (mSimRunningLock) { mSimRunning = false; }
      if (mSim.GetProcessusByUT().Count != SimulationXML.Instance.TempsSimulation) { Console.WriteLine("Simulation did not run as many ut as expected"); }

      
      if (aKillSignal) { lock (mRandObjLock) { mSimThreadCancel = false; } }
    }
    
    private void btnResetSim_Click(object sender, EventArgs e)
    {
      KillSimulation();
      
      InitSim(true);
    }

    private void KillSimulation()
    {
      bool aRet = true;
      bool aSimRunning = false;
      lock (mSimRunningLock) { aSimRunning = mSimRunning; }
      if (aSimRunning)
      {
        lock (mRandObjLock) { mSimThreadCancel = true; }
        
        while (aRet)
        { 
          lock (mRandObjLock) { aRet = mSimThreadCancel; }
          Thread.Sleep(1000);
        }
      }
    }

    private void UpdateProcessusList(int pCurUt)
    {
      
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(() => { UpdateProcessusList(pCurUt); }));
      }
      else
      {
        processOrderGrid.Rows.Add(mSim.getProcessusState(pCurUt));
        Color[] lColor = mSim.getProcessusColor(pCurUt);
        int lCount = lColor.Length;
        int lIndex = processOrderGrid.Rows.Count - 2;

        if (lIndex < 0) return; 
        for (int i = 0; i < lCount; i++)
        {
            processOrderGrid.Rows[lIndex].Cells[i].Style.BackColor = lColor[i];  
        }

      }
    }

    private void InitSim(bool Reset = false)
    {
      if (mSim.CurrentUT == 0 || Reset)
      {
        ResourceManager.init();

        Point depart = new Point(SimulationXML.Instance.XDepart, SimulationXML.Instance.YDepart);
        List<Point> listChemin = new List<Point>();
        R02CarteRoutiere.Instance.calculChemin.calculNouveauChemin(listChemin, depart, R07GPS.directionEnum.UNDEFINED);
        mDmi = R02CarteRoutiere.Instance.calculChemin.calculLongueurCheminEnUT(listChemin, depart);

        int.TryParse(dureeSimulAffiche.Text, out SimulationXML.Instance.TempsSimulation);
        int.TryParse(dureeReparationAffiche.Text, out SimulationXML.Instance.AutoReparation);
        int.TryParse(dureeCollisionAffiche.Text, out SimulationXML.Instance.Collision);
        SimulationXML.Instance.OrderType = (Ordonnanceur.Ordonnancement)cbOrderType.SelectedItem;
        int.TryParse(facteurEchelleAffiche.Text, out SimulationXML.Instance.Echelle);
        int.TryParse(consoAffiche.Text, out SimulationXML.Instance.Consommation);
        int Tempdepartx;
        int.TryParse(txtDepartX.Text, out Tempdepartx);
        Tempdepartx *= SimulationXML.Instance.Echelle;
        SimulationXML.Instance.XDepart = Tempdepartx;
        //int.TryParse(txtDepartX.Text, out SimulationXML.Instance.XDepart);
        int Tempdeparty;
        int.TryParse(txtDepartY.Text, out Tempdeparty);
        Tempdeparty *= SimulationXML.Instance.Echelle;
        SimulationXML.Instance.YDepart = Tempdeparty;
        //int.TryParse(txtDepartY.Text, out SimulationXML.Instance.YDepart);

        int TempArrivex;
        int.TryParse(txtArrivalX.Text, out TempArrivex);
        TempArrivex *= SimulationXML.Instance.Echelle;
        SimulationXML.Instance.XArrivee = TempArrivex;
        //int.TryParse(txtArrivalX.Text, out SimulationXML.Instance.XArrivee);

        int TempArrivey;
        int.TryParse(txtArrivalY.Text, out TempArrivey);
        TempArrivey *= SimulationXML.Instance.Echelle;
        SimulationXML.Instance.YArrivee = TempArrivey;
        //int.TryParse(txtArrivalY.Text, out SimulationXML.Instance.YArrivee);
        int.TryParse(vitesseMaxAffiche.Text, out SimulationXML.Instance.Vitesse);
        int.TryParse(accelerationMaxAffiche.Text, out SimulationXML.Instance.Acceleration);
        processOrderGrid.Rows.Clear();
        Ordonnanceur.InitRessourcePartagees();

        mSim = new Simulateur();
      }
    }

    private void Form1_FormClosing(object sender, FormClosingEventArgs e)
    {
      KillSimulation();
    }

    private void pictureCarte_Paint(object sender, PaintEventArgs e)
    {
      mCarte.DrawMap(e.Graphics);
    }

    private void btnPause_Click(object sender, EventArgs e)
    {
      KillSimulation();
    }
         
    private void pictureCarte_Resize(object sender, EventArgs e)
    {
      if (Size.Width > Size.Height) mCarte.ChangePixelRatio((int)(Size.Height / 42));
      else mCarte.ChangePixelRatio((int)(Size.Width / 42));
      Carte.Refresh();
    }
         
    private void Form1_Resize(object sender, EventArgs e)
    {
      Carte.Size = Size;
      pictureCarte.Size = Size;
    }

    private void processOrderGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
    {
       processOrderGrid.FirstDisplayedScrollingRowIndex = processOrderGrid.RowCount -1;
    }

    private void txtNbrNetworkLinks_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtNbrProcessor_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtStdDevChargeProcessor_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtMaxTimebloc_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtTotalTimeBloc_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtGESTotal_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtGES_TextChanged(object sender, EventArgs e)
    {

    }

    private void lblStdDevChargeProcessor_Click(object sender, EventArgs e)
    {

    }

    private void lblMaxTimeBloc_Click(object sender, EventArgs e)
    {

    }

    private void lblTotalTimebloc_Click(object sender, EventArgs e)
    {

    }

    private void lblPin_Click(object sender, EventArgs e)
    {

    }

    private void txtPin_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtRespectedDeadline_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtTotalDeadline_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtDpa_TextChanged(object sender, EventArgs e)
    {

    }

    private void lblDpa_Click(object sender, EventArgs e)
    {

    }

    private void lblTotalDeadline_Click(object sender, EventArgs e)
    {

    }

    private void lblRespectedDeadline_Click(object sender, EventArgs e)
    {

    }

    private void lblDmi_Click(object sender, EventArgs e)
    {

    }

    private void lblMeanChargeProcessor_Click(object sender, EventArgs e)
    {

    }

    private void lblGESTotal_Click(object sender, EventArgs e)
    {

    }

    private void txtDmi_TextChanged(object sender, EventArgs e)
    {

    }

    private void txtMeanChargeProcessor_TextChanged(object sender, EventArgs e)
    {

    }

    private void lblGES_Click(object sender, EventArgs e)
    {

    }

    private void contextMenuStrip1_MouseClick(object sender, MouseEventArgs e)
    {
        MessageBox.Show("Projet réalisé dans le cadre du cours \" Systèmes Parralèles et Temps Réel\" du Département de Génie Électrique et Informatique à la session d'hiver 2013" + Environment.NewLine + Environment.NewLine + "Les membres du projet sont:"
            + Environment.NewLine + "- Loïc Tanon"
            + Environment.NewLine + "- Steven Demers"
            + Environment.NewLine + "- Jean-Mathieu Bégin"
            + Environment.NewLine + "- Nicolas Morissette"
            + Environment.NewLine + "- Maxime Goussen"
            + Environment.NewLine + "- Jonathan Grenier", "À propos",
        MessageBoxButtons.OK, MessageBoxIcon.None);
        
    }

    private void numericUpDown1_ValueChanged(object sender, EventArgs e)
    {
        SimulationXML.Instance.mTailleVoiture = Convert.ToInt32(numericUpDown1.Value);
    }
   
}
