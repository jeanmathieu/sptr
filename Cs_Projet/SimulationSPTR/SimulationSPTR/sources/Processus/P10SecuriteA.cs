using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


class P10SecuriteA : Processus
  {
    public enum sequenceExec : int { R01A = 0, R02, R01B, R04, E, END };
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R01, Ressource.RID.R02, Ressource.RID.R01, Ressource.RID.R04, Ressource.RID.INVALID, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P12 };
    
    public P10SecuriteA()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 10;
      contrainteFin = periode;
      ProcessusID = Message.ID.P10;
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }
    
    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if (etapeExecution == 3)
      {
        message.Add(attMsgSrcId[0]);
      }

      return message;
    }
    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }
    
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R01A;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P10 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R01A:
          etapeExecution++;
          break;
        case sequenceExec.R02:
          etapeExecution++;
          break;
        case sequenceExec.R01B:
          etapeExecution++;
          break;
        case sequenceExec.R04:
          if (inMsg.Count != 1)
            throw new Exception("P10 n'a pas re�u de message");

          etapeExecution++;
          break;
        case sequenceExec.E:
          lMsgAEnvoyer.Add(new Message(false, Message.ID.P10, Message.ID.P13, 1));
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}

