using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Drawing;

class P12SuiviDuChemin : Processus
  {
    public enum SequenceID { R03 = 0, R01, R04, R06, END };
    int longueur_du_chemin_calculer = 0;
    List<Point> listpointducheminasuivre = null;
    Ressource.RID[] resourceBySeq = new Ressource.RID[] { Ressource.RID.R03, Ressource.RID.R01, Ressource.RID.R04, Ressource.RID.R06, Ressource.RID.INVALID };
    Message.ID[] waitMessageSrcId = new Message.ID[] { Message.ID.P02 };
   
    public P12SuiviDuChemin()
    {
      etapeExecution = (int)SequenceID.END;
      periode = 30;
      contrainteFin = periode;
      ProcessusID = Message.ID.P12;
    }
    
    public override Ressource.RID getRess()
    {
      return resourceBySeq[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if (etapeExecution == (int)SequenceID.R03)
      {
        message.Add(waitMessageSrcId[0]);
      }

      return message;
    }
    
    public override int getTempsRestant()
    {
      return (int)SequenceID.END - etapeExecution;
    }
    
    public override void reinitEtat()
    {
      etapeExecution = (int)SequenceID.R03;
    }
   
    public override Processus.etat getEtat()
    {
      if ((SequenceID)etapeExecution == SequenceID.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P12 " + etapeExecution);
      List<Message> sendMsg = new List<Message>();

      switch ((SequenceID)etapeExecution)
      {
        case SequenceID.R03:
          etapeExecution++;
          int vitesse = R03CompteurDeVitesse.Instance.vitesse;

          if (inMsg.Count == 0 || inMsg[0].SourceID != Message.ID.P02)
            throw new Exception("Le message n'a pas été recu");

          
          longueur_du_chemin_calculer = inMsg[0].mMsgPrincipal;
          int vitesseDeP02 = inMsg[0].mVitesse;
          int distanceIntersection = inMsg[0].mDistance;
          FeuIntersection.EtatFeu couleurDuFeu = (FeuIntersection.EtatFeu)inMsg[0].mCouleur;
          listpointducheminasuivre = inMsg[0].mCheminASuivre;

          Message msgP11 = new Message(false, Message.ID.P12, Message.ID.P11, 1);
          Message msgP03 = new Message(false, Message.ID.P12, Message.ID.P03, 1);

          
          if (vitesse > 0)
          {
            int nbUTAvantIntersection = distanceIntersection / vitesse;

            
            if (nbUTAvantIntersection < (1 * periode) && couleurDuFeu != FeuIntersection.EtatFeu.VERT)
            {
              msgP03.mMsgPrincipal = 0; 
              msgP11.mMsgPrincipal = 1; 
            }
            else if (nbUTAvantIntersection < (1 * periode))
            {
              msgP03.mMsgPrincipal = 1; 
              msgP11.mMsgPrincipal = 0; 
            }
            else 
            {
              msgP03.mMsgPrincipal = vitesseDeP02; 
              msgP11.mMsgPrincipal = 0; 
            }
          }
          else 
          {
            if (distanceIntersection < (1 * periode) && couleurDuFeu != FeuIntersection.EtatFeu.VERT)
            {
              msgP03.mMsgPrincipal = 0; 
              msgP11.mMsgPrincipal = 1; 
            }
            else
            {
              msgP03.mMsgPrincipal = 1; 
              msgP11.mMsgPrincipal = 0; 
            }
          }

          sendMsg.Add(msgP11);
          sendMsg.Add(msgP03);
          break;
        case SequenceID.R01:
          etapeExecution++;
          break;
        case SequenceID.R04:
          sendMsg.Add(new Message(false, Message.ID.P12, Message.ID.P10, 1 + (int)Math.Floor((double)longueur_du_chemin_calculer / (mXML.Echelle * 8.0))));
          R04ModuleAffichage.Instance.listpointducheminasuivre = listpointducheminasuivre;
          etapeExecution++;
          break;
        case SequenceID.R06:
          sendMsg.Add(new Message(false, Message.ID.P12, Message.ID.P06, 1));
          etapeExecution++;
          break;
        case SequenceID.END:
          break;
      }

      return sendMsg;
    }
}
