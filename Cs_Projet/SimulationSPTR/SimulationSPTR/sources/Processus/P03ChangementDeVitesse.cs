using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


class P03ChangementDeVitesse : Processus
  {
    int acceleration = 1;
    int posAcceleration = 0;

    private int suiviCheminVitesse = -99;
    private int autoVerifVitesse = -99;
    private int vitesseCollision = -99;

    int vitesseActuelle = 0;
    int nouvelleVitesse;
    int accelerationParUT;
    int diffVitesse;
    int derniereVitesse = 0;

    public enum sequenceExec : int { R03 = 0, R10, END };

    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R03, Ressource.RID.R10, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P07, Message.ID.P01, Message.ID.P12 };
    
    public P03ChangementDeVitesse()
    {
      DeclencheurProcessus.Add(Message.ID.P07);
      DeclencheurProcessus.Add(Message.ID.P12);
      etapeExecution = (int)sequenceExec.END;
      periode = 0;
      contrainteFin = 10;
      ProcessusID = Message.ID.P03;
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }
    
    public override List<Message.ID> getMsg()
    {
      List<Message.ID> msg = new List<Message.ID>();

      if (etapeExecution == (int)sequenceExec.R10 && posAcceleration == 0)
      {
        msg.Add(attMsgSrcId[0]);
        msg.Add(attMsgSrcId[1]);
        msg.Add(attMsgSrcId[2]);
      }

      return msg;
    }

    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution + (acceleration - posAcceleration) - 1; ;
    }
    
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R03;
      acceleration = 1;
      posAcceleration = 0;
    }
    
    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P03 " + etapeExecution + "," + posAcceleration);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R03:
          R03CompteurDeVitesse.Instance.vitesse = vitesseActuelle;
          etapeExecution++;
          break;
        case sequenceExec.R10:
          if (posAcceleration == 0)
          {
            if (inMsg.Count != 3)
              throw new Exception("P03 n'a pas recu trois messages");

            for (int i = 0; i < inMsg.Count; i++)
            {
              switch (inMsg[i].SourceID)
              {
                case Message.ID.P12:
                  suiviCheminVitesse = inMsg[i].mMsgPrincipal;
                  break;
                case Message.ID.P01:
                  autoVerifVitesse = inMsg[i].mMsgPrincipal;
                  break;
                case Message.ID.P07:
                  vitesseCollision = inMsg[i].mMsgPrincipal;
                  break;
              }
            }
            
            if (vitesseCollision != -99)
            {
              nouvelleVitesse = Math.Min(vitesseCollision, suiviCheminVitesse);
            }
            else if (autoVerifVitesse != -99)
            {
              nouvelleVitesse = autoVerifVitesse;
            }
            else if (suiviCheminVitesse != -99)
            {
              nouvelleVitesse = suiviCheminVitesse;
            }
            else
            {
              nouvelleVitesse = 0;
            }

            if (nouvelleVitesse > SimulationXML.Instance.Vitesse)
              nouvelleVitesse = SimulationXML.Instance.Vitesse;

            
            diffVitesse = nouvelleVitesse - vitesseActuelle;
            if (diffVitesse != 0)
            {
              acceleration = 1 + Math.Max(1, (int)Math.Ceiling(Math.Abs(diffVitesse) / (double)mXML.Acceleration));
              accelerationParUT = diffVitesse / (acceleration - 1);
            }
            else
            {
              acceleration = 1;
              accelerationParUT = 0;
            }

            derniereVitesse = nouvelleVitesse;
          }
          
          if (posAcceleration >= acceleration - 1)
          {
            vitesseActuelle = nouvelleVitesse;
            R10RegulateurVitesse.Instance.vitesse = vitesseActuelle;

            Message msg = new Message(false, Message.ID.P03, Message.ID.P05, 1);
            msg.mMsgPrincipal = Math.Abs(diffVitesse);
            lMsgAEnvoyer.Add(msg);

            etapeExecution++;
          }
          else
          {
            posAcceleration++;
            vitesseActuelle = vitesseActuelle + accelerationParUT;
            R10RegulateurVitesse.Instance.vitesse = vitesseActuelle;
          }

          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
