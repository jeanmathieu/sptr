using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Runtime.CompilerServices;

class P01AutoVerification : Processus
  {   
    public enum sequenceExec { R05 = 0, E1, R08, E2, END };
    R05Environnement.EtatVehiculeEnum mEtatVehicule;
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R05, Ressource.RID.INVALID, Ressource.RID.R08, Ressource.RID.INVALID, Ressource.RID.INVALID };

    public P01AutoVerification()
    {
      periode = 30;
      contrainteFin = periode;
      etapeExecution = (int)sequenceExec.END;
      ProcessusID = Message.ID.P01;
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }
    
    public override List<Message.ID> getMsg()
    {
      List<Message.ID> msg = new List<Message.ID>();
      return msg;
    }
    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }

    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R05;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }

    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P01 ({0})", etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R05:
          mEtatVehicule = R05Environnement.Instance.EtatVehicule;
          etapeExecution++;
          break;
        case sequenceExec.E1:
          etapeExecution++;
          Message msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);

          if (mEtatVehicule == R05Environnement.EtatVehiculeEnum.BRISE)
          {
            msg.mMsgPrincipal = 1; 
            lMsgAEnvoyer.Add(msg);
          }
          else
          {
            msg.mMsgPrincipal = -99; 
            lMsgAEnvoyer.Add(msg);
          }
          break;
        case sequenceExec.R08:
          etapeExecution++;
          break;
        case sequenceExec.E2:
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
