using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

class P04Climat : Processus
  {
    public enum sequenceExec : int { R05 = 0, E, END };

    int temperature = 1;
    int temperaturePosition = 0;
    double temperatureExterieur;

    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R05, Ressource.RID.INVALID, Ressource.RID.INVALID };
    public P04Climat()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 30;
      contrainteFin = periode;
      ProcessusID = Message.ID.P04;
    }

    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    
    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();
      return message;
    }

    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution + (temperature - temperaturePosition) - 1;
    }

    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R05;
      temperature = 1;
      temperaturePosition = 0;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END) return etat.END;
      else return etat.EXEC;
    }

    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P04 " + etapeExecution + "," + temperaturePosition);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R05:
          temperatureExterieur = R05Environnement.Instance.temperatureExterieur;
          etapeExecution++;

          temperaturePosition = 0;
          break;
        case sequenceExec.E:
          if (temperatureExterieur < 15 || temperatureExterieur > 25)
          {
            if (temperaturePosition == 0)
            {
              temperature = 1 + (int)Math.Min(Math.Abs(15.0 - temperatureExterieur), Math.Abs(25.0 - temperatureExterieur)) / 5;
            }
            if (temperaturePosition >= temperature - 1)
            {
              etapeExecution++;
            }
            else
            {
              temperaturePosition++;
            }
          }
          else
            etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
