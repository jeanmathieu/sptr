using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Drawing;

class P02CalculDuChemin : Processus
  {
    int nbRoute = 4;
    int routeCalculer = 3;
    int longueurCheminCalculer = 0;
    int nouvelleVitesse = 0;
    CalculChemin calculChemin = null;
    List<Point> pointCheminASuivre = new List<Point>();
    int distanceProchaineIntersection;
    Point PositionActuelle = new Point(0, 0);
    R07GPS.directionEnum DirectionActuelle = R07GPS.directionEnum.NORD;
    FeuIntersection.EtatFeu etatProchainFeu;

    public enum sequenceExec { R02 = 0, R07, R05, E, END };

    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R02, Ressource.RID.R07, Ressource.RID.R05, Ressource.RID.INVALID, Ressource.RID.INVALID };

    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.INVALID, Message.ID.INVALID, Message.ID.INVALID, Message.ID.INVALID };

    
    public P02CalculDuChemin()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 30;
      contrainteFin = periode;
      ProcessusID = Message.ID.P02;
    }

    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if (attMsgSrcId[etapeExecution] != Message.ID.INVALID)
        message.Add(attMsgSrcId[etapeExecution]);

      return message;
    }

    
    public override int getTempsRestant()
    {
      return ((int)sequenceExec.END - etapeExecution) + (nbRoute - routeCalculer) - 1;
    }

    
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R02;
      nbRoute = 4;
      routeCalculer = 0;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }

    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P02 " + etapeExecution + "," + routeCalculer);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R02:
          calculChemin = R02CarteRoutiere.Instance.calculChemin;
          etapeExecution++;
          break;
        case sequenceExec.R07:
          
          PositionActuelle = R07GPS.Instance.PositionVoiture;
          DirectionActuelle = R07GPS.Instance.CurrentDirection;

            calculChemin.calculNouveauChemin(pointCheminASuivre, PositionActuelle, DirectionActuelle);
            longueurCheminCalculer = calculChemin.calculLongueurChemin(pointCheminASuivre, PositionActuelle);
            if (DirectionActuelle == R07GPS.directionEnum.UNDEFINED)
              R07GPS.Instance.CurrentDirection = calculChemin.directionDepart(pointCheminASuivre, PositionActuelle);

          
          nouvelleVitesse = calculChemin.getVitesse(PositionActuelle);

          if (DirectionActuelle != R07GPS.directionEnum.UNDEFINED)
            distanceProchaineIntersection = calculChemin.distanceNextNode(PositionActuelle, DirectionActuelle);
          else
            distanceProchaineIntersection = 10000;

          etapeExecution++;
          break;
        case sequenceExec.R05:
          etapeExecution++;

          if (DirectionActuelle != R07GPS.directionEnum.UNDEFINED)
          {
            try
            {
                Point lIntersection = calculChemin.NextNode(PositionActuelle, DirectionActuelle);
              FeuIntersection FeuIntersection = R05Environnement.Instance.listFeuIntersection.Find(x => x.FeuPosition == lIntersection);
              if (FeuIntersection == null) etatProchainFeu = FeuIntersection.EtatFeuVertJaune; 
              else if (FeuIntersection.directionVertJaune == DirectionActuelle)
                etatProchainFeu = FeuIntersection.EtatFeuVertJaune;
              else
                etatProchainFeu = FeuIntersection.EtatFeu.ROUGE;
            }
            catch (Exception)
            {
             
            }
          }

          routeCalculer = 0;
          break;
        case sequenceExec.E:
          if (routeCalculer == 0)
          {
            nbRoute = 4 + (int)Math.Floor((double)longueurCheminCalculer / (mXML.Echelle * 8.0));
          }
          if (routeCalculer >= nbRoute - 1)
          {
            lMsgAEnvoyer.Add(new Message(true, Message.ID.P02, Message.ID.P07, 1));

            Message lMsg = new Message(false, Message.ID.P02, Message.ID.P12, nbRoute - 3);
            lMsg.mCheminASuivre = pointCheminASuivre;
            lMsg.mMsgPrincipal = longueurCheminCalculer;
            lMsg.mVitesse = nouvelleVitesse;
            lMsg.mDistance = distanceProchaineIntersection;
            lMsg.mCouleur = (int)etatProchainFeu;
            lMsgAEnvoyer.Add(lMsg);

            etapeExecution++;
          }
          else
            routeCalculer++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}

