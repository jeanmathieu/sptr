using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

class P09Messagerie : Processus
  {
    public enum sequenceExec : int { R12 = 0, R04, END };
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R12, Ressource.RID.R04, Ressource.RID.R04 };
    string newMsg;

    public P09Messagerie()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 30;
      contrainteFin = periode;
      ProcessusID = Message.ID.P09;
    }

    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();
      return message;
    }

    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }

    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R12;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }

    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P09 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R12:
          newMsg = R12Telecommunication.Instance.Message;
          etapeExecution++;
          break;
        case sequenceExec.R04:
          R04ModuleAffichage.Instance.MessagePublicCommerciaux = newMsg;
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
