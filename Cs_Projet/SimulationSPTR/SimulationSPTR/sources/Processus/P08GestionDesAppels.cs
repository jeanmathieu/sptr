using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


class P08GestionDesAppels : Processus
  {
    public enum sequenceExec : int { R03 = 0, E, R12, END };

    int nbDureeAppel = 1;
    int dureeAppelPos = 0;
    double vitesseActuelle = 0;
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R03, Ressource.RID.INVALID, Ressource.RID.R12, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P11 };
    
    public P08GestionDesAppels()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 0;
      contrainteFin = 100;
      ProcessusID = Message.ID.P08;
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if ((etapeExecution == (int)sequenceExec.E) && (dureeAppelPos == 0))
      {
        message.Add(attMsgSrcId[0]);
      }

      return message;
    }

    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution + (nbDureeAppel - dureeAppelPos) - 1;
    }

    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R03;
      nbDureeAppel = 1;
      dureeAppelPos = 0;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }

    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P08 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R03:
          etapeExecution++;
          vitesseActuelle = R03CompteurDeVitesse.Instance.vitesse;
          dureeAppelPos = 0;

          break;
        case sequenceExec.E:
          if (dureeAppelPos == 0)
          {
            if (inMsg.Count != 1)
              throw new Exception("Un message devrait �tre re�u");

            nbDureeAppel = (int)(1 + 60 - (7 * vitesseActuelle));
          }
          if (dureeAppelPos < nbDureeAppel - 1)
          {
            dureeAppelPos++;
          }
          else
          {
            etapeExecution++;
          }
          break;
        case sequenceExec.R12:
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}

