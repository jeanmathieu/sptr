using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


class P13SystemesElectriques : Processus
  {
    public enum sequenceExec : int { R05 = 0, R11, E, R04, END };
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R05, Ressource.RID.R11, Ressource.RID.INVALID, Ressource.RID.R04, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P10 };
    string nouvelleMeteo;
    int commandePos = 0;
    private string[] commande = new string[] {"Vibration siège","Air climatisé","Ouverture des fenêtres","Volume radio = 70%","Couper radio"};
    
    public P13SystemesElectriques()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 0;
      contrainteFin = 7;
      ProcessusID = Message.ID.P13;
      DeclencheurProcessus.Add(Message.ID.P10);
    }

    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if (etapeExecution == 0)
      {
        message.Add(attMsgSrcId[0]);
      }

      return message;
    }

    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }

    
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R05;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }

    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P13 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R05:
              if (inMsg.Count == 0 && DeclencheurProcessus.Count > 0)
                  throw new Exception("P13 n'a pas reçu de message"); 
          etapeExecution++;
          break;
        case sequenceExec.R11:
          nouvelleMeteo = R11StationMeteo.Instance.CurrentMeteo;
          etapeExecution++;
          break;
        case sequenceExec.E:
          etapeExecution++;
          break;
        case sequenceExec.R04:
          R04ModuleAffichage.Instance.Meteo = nouvelleMeteo;
          R04ModuleAffichage.Instance.DerniereCommande = commande[commandePos];

          commandePos++;
          if (commandePos >= commande.Count())
          {
            commandePos = 0;
          }
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
