using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class MetaOrdonnanceur
{
    static public void GereConflits(List<Processeur> inProcesseurs)
    {
      List<Ordonnanceur> lOrdonnenceurs = new List<Ordonnanceur>();
      inProcesseurs.Where(x => x.Ordonnenceur.ProcessusCourant != null).ToList().ForEach(x => lOrdonnenceurs.Add(x.Ordonnenceur));
      if (lOrdonnenceurs.Count > 0) { GereConflits(lOrdonnenceurs); }
    }

    static private void GereConflits(List<Ordonnanceur> inOrdonnenceurs)
    {
      List<EtatRessource> lRessourcesLibres = inOrdonnenceurs.First().EtatRessourceParagees.Where(x => x.verrouillee == false).ToList();
      List<EtatProcessus> lProcessusPrets = new List<EtatProcessus>();
      List<EtatProcessus> lProcessusAExecuter = new List<EtatProcessus>();
      
      inOrdonnenceurs.Where(x => x.MessageAttendusPourDebloquer(x.ProcessusCourant).Count == 0).ToList().ForEach(x => lProcessusPrets.Add(x.ProcessusCourant));
      
      lProcessusPrets = lProcessusPrets.Where(x => x.Processus.getEtat() == Processus.etat.EXEC).ToList();
      
      foreach (EtatProcessus aPS in lProcessusPrets)
      {
        Ordonnanceur lOrdonnenceur = inOrdonnenceurs.Single(x => x.ProcessusCourant.Processus.ProcessusID == aPS.Processus.ProcessusID);
        if (lOrdonnenceur.TamponSortie.Count(x => x.SourceID == aPS.Processus.ProcessusID && x.DelaiTransmission > 0) == 0) { lProcessusAExecuter.Add(aPS); }
      }
      
      foreach (EtatRessource lEtatRessource in lRessourcesLibres)
      {
        
        List<EtatProcessus> lProcessusEnAttente = lProcessusAExecuter.Where(x => x.Processus.getRess() == lEtatRessource.getID).ToList();
        EtatProcessus lEtatProcessus = lProcessusEnAttente.OrderBy(x => x.Priority).FirstOrDefault();
        if (lEtatProcessus != null)
        { 
          lEtatRessource.verrouiller(lEtatProcessus.Processus);
        }
        if (lProcessusEnAttente.Count > 0 && !lEtatRessource.verrouillee) { throw new Exception("La ressource n'a pas ete verrouillé au bon moment"); }
      }
      foreach (EtatRessource lEtatRessource in lRessourcesLibres.Where(x => x.verrouillee))
      {
        EtatRessource lEtatRessourceVerrouille = inOrdonnenceurs.First().EtatRessourceParagees.Single(x => x.getID == lEtatRessource.getID);
        if (lEtatRessourceVerrouille.verrouProcessus != lEtatRessource.verrouProcessus)
        {
          throw new Exception("La ressource n'a pas reussi a se mettre a jour");
        }
      }
    }
}
