using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Processeur
  {
    private Ordonnanceur mOrdonnenceur;
    private List<Processus> mProcessus;
    private static int mCount = 0;
    private int mID;

   
    public Processeur()
    {
        mProcessus = new List<Processus>();
        mOrdonnenceur = null;
        mID = mCount + 1;
        mCount = mID;
    }

    public void AddProcessus(Processus inProcessus)
    {
      mProcessus.Add(inProcessus);
    }
    
    public List<Tuple<int, Message.ID>> GetProcessus()
    {
      List<Tuple<int, Message.ID>> Ret = new List<Tuple<int, Message.ID>>();
      mProcessus.ForEach(x => Ret.Add(new Tuple<int, Message.ID>(mID, x.ProcessusID)));
      return Ret;
    }

    public Ordonnanceur Ordonnenceur
    {
      get { return mOrdonnenceur; }
      set { mOrdonnenceur = value; }
    }
    public int ID
    {
        get { return mID; }
    }  
    public List<Processus> Processus
    {
      get { return mProcessus; }
      set { mProcessus = value; }
    }

    
}