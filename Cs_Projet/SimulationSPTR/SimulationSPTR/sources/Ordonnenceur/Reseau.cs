using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class Reseau
  {
    private class Lien
    {
      public bool estBloque;
      public Message.ID verrouProcessus;

      public Lien(bool inBloque, Message.ID inProcessusID)
      {
          estBloque = inBloque;
          verrouProcessus = inProcessusID;
      }
      public Lien()
      {
        estBloque = false;
        verrouProcessus = Message.ID.INVALID;
      }
      
    };

    private List<Processeur> mProcesseurs; 
    private Dictionary<Tuple<int, int>, Lien> mLiens;
    private static List<Tuple<Message.ID, Message.ID>> mLiensEntreProcessus;
    
    public Reseau()
    {
      PreparerLiensEntreProcessus();
    }
    
    public Reseau(List<Processeur> inProcesseurs)
    {
      PreparerLiensEntreProcessus();
      mProcesseurs = inProcesseurs;
      CreerLiensEntreProcessus(inProcesseurs);
    }

    public void EnvoieMessage(List<Message> ioMessageCourant, List<Message> ioTampon, int inNow)
    {
        Message lMessage = null;
        List<Message> lProcessusLocaux = ioMessageCourant.Where(x => getProcesseur(x.DestinationID) == getProcesseur(x.SourceID)).ToList();

        ioTampon.Where(x => x.Source == null).ToList().ForEach(x => x.Source = getProcesseur(x.SourceID));
        ioTampon.Where(x => x.Destination == null).ToList().ForEach(x => x.Destination = getProcesseur(x.DestinationID));


        for (int i = lProcessusLocaux.Count - 1; i >= 0; --i)
        {
            lMessage = lProcessusLocaux[i];
            lMessage.DelaiTransmission = 0;
            if (lMessage.Blocking)
            {
                getProcesseur(lMessage.SourceID).Ordonnenceur.EtatsProcessus.Single(x => x.Processus.ProcessusID == lMessage.SourceID).ProcessusState = EtatProcessus.state.Pr�t;
            }
            getProcesseur(lMessage.DestinationID).Ordonnenceur.ReceptionMessage(lMessage, inNow);
            int lCountTMP = ioTampon.Count;
            ioMessageCourant.Remove(lMessage);
            ioTampon.Remove(lMessage);
            if (lCountTMP - 1 != ioTampon.Count) { throw new Exception("Un  message ne s'est pas retir� du tampon"); }
        }

        for (int i = ioMessageCourant.Count - 1; i >= 0; --i)
        {
            lMessage = ioMessageCourant[i];
            Tuple<int, int> lTuple = new Tuple<int, int>(getProcesseur(lMessage.SourceID).ID, getProcesseur(lMessage.DestinationID).ID);

            if (lMessage.DelaiTransmission == 0)
            {
                if (lMessage.Blocking)
                {
                    getProcesseur(lMessage.SourceID).Ordonnenceur.EtatsProcessus.Single(x => x.Processus.ProcessusID == lMessage.SourceID).ProcessusState = EtatProcessus.state.Pr�t;
                }
                if (mLiens[lTuple].verrouProcessus != lMessage.SourceID) { throw new Exception("Le processus n'a pas verrouill� le lien qu'il utilise"); }

                getProcesseur(lMessage.DestinationID).Ordonnenceur.ReceptionMessage(lMessage, inNow);
                int lCountTMP = ioTampon.Count;
                ioTampon.Remove(lMessage);
                if (lCountTMP - 1 != ioTampon.Count) { throw new Exception("Un  message ne s'est pas retir� du tampn"); }
                mLiens[lTuple] = new Lien();

                foreach (EtatProcessus aPS in lMessage.Source.Ordonnenceur.EtatsProcessus.Where(x => x.ProcessusState == EtatProcessus.state.Bloqu�SurLien))
                {
                    foreach (Message aM in ioTampon.Where(x => x.SourceID == aPS.Processus.ProcessusID))
                    {
                        if (getProcesseur(aM.DestinationID).ID == lTuple.Item2) { aPS.ProcessusState = EtatProcessus.state.Pr�t; }
                    }
                }
                break;
            }
            else
            {
                if (mLiens[lTuple].estBloque == false)
                {
                    mLiens[lTuple] = new Lien(true, lMessage.SourceID);
                    --lMessage.DelaiTransmission;
                    break;
                }
                else if (mLiens[lTuple].estBloque == true && mLiens[lTuple].verrouProcessus == lMessage.SourceID)
                {
                    --lMessage.DelaiTransmission;
                    break;
                }
                else
                {
                    EtatProcessus lEtatProcessus = lMessage.Source.Ordonnenceur.EtatsProcessus.SingleOrDefault(x => x.Processus.ProcessusID == lMessage.SourceID);
                    if (lEtatProcessus != null) { lEtatProcessus.ProcessusState = EtatProcessus.state.Bloqu�SurLien; }
                }
            }

        }

    }


    private Processeur getProcesseur(Message.ID inProcessusID)
    {
        Processeur aRet = null;
        aRet = mProcesseurs.SingleOrDefault(x => x.Processus.SingleOrDefault(y => y.ProcessusID == inProcessusID) != null);
        return aRet;
    }


    private Processeur getProcesseur(Processus inProcessus)
    {
        Processeur lProcesseur = null;
        lProcesseur = mProcesseurs.SingleOrDefault(x => x.Processus.Contains(inProcessus));
        return lProcesseur;
    }


    public int getNbLien
    {
        get
        {
            int lCount = 0;
            List<Tuple<int, int>> lKeys = mLiens.Keys.ToList();
            List<Tuple<int, int>> lAEffacer = new List<Tuple<int, int>>();
            for (int i = lKeys.Count - 1; i >= 0; --i)
            {
                if (lAEffacer.Count(x => (x.Item1 == lKeys[i].Item1 && x.Item2 == lKeys[i].Item2) || (x.Item2 == lKeys[i].Item1 && x.Item1 == lKeys[i].Item2)) == 0)
                {
                    ++lCount;
                    lAEffacer.Add(lKeys[i]);
                    lKeys.Remove(lKeys[i]);
                }
            }
            return lCount;
        }
    }

    private void CreerLiensEntreProcessus(List<Processeur> pLP)
    {
        foreach (Tuple<Message.ID, Message.ID> aTup in mLiensEntreProcessus)
        {
            Processeur P1 = getProcesseur(aTup.Item1);
            Processeur P2 = getProcesseur(aTup.Item2);
            if (P1 != null && P2 != null)
            {
                Tuple<int, int> LinkKey = new Tuple<int, int>(P1.ID, P2.ID);
                if (P1.ID != P2.ID && !mLiens.Keys.Contains(LinkKey)) { mLiens.Add(LinkKey, new Lien()); }
            }
        }
    }
    


    private void PreparerLiensEntreProcessus()
    {
      if (mLiensEntreProcessus == null)
      {
        mLiensEntreProcessus = new List<Tuple<Message.ID, Message.ID>>();
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P01, Message.ID.P03));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P02, Message.ID.P12));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P02, Message.ID.P07));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P03, Message.ID.P05));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P05, Message.ID.P06));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P07, Message.ID.P03));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P10, Message.ID.P13));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P11, Message.ID.P08));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P03));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P06));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P10));
        mLiensEntreProcessus.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P11));
      }
      mProcesseurs = new List<Processeur>();
      mLiens = new Dictionary<Tuple<int, int>, Lien>();
    }


    
    
    
}
