using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Trafic
{

    public List<leTrafic> traficRoutier = new List<leTrafic>();
    public List<etatTrafic> etatTraficRoutier = new List<etatTrafic>();
    public List<KeyValuePair<int, int>> etatTraficTempsRestant = new List<KeyValuePair<int, int>>();
    public List<KeyValuePair<int, int>> etatTraficPhase = new List<KeyValuePair<int, int>>();
    private int nextUID;

    public struct etatTrafic
    {
        public int Numero;
        public Point position;
        public int Vitesse;
        public int tempsQuiReste;
        public R07GPS.directionEnum direction;
        public int UID;

        public etatTrafic(etatTrafic inEtat)
        {
            Numero = inEtat.Numero;
            position = inEtat.position;
            Vitesse = inEtat.Vitesse;
            tempsQuiReste = inEtat.tempsQuiReste;
            direction = R07GPS.directionEnum.UNDEFINED;
            UID = 0;
        }
    }

    public struct leTrafic
    {
        public int Numero;
        public Point Debut;
        public Point Fin;
        public int Vitesse;
        public int Periode;
        public int Phase;
        public leTrafic(leTrafic inTrafic)
        {
            Numero = inTrafic.Numero;
            Debut = inTrafic.Debut;
            Fin = inTrafic.Fin;
            Vitesse = inTrafic.Vitesse;
            Periode = inTrafic.Periode;
            Phase = inTrafic.Phase;
        }
    }
    public void chargerTraficXML()
    {

        int nombreVoitureTrafic = SimulationXML.Instance.Trafic.Count();
        leTrafic lTempTrafic = new leTrafic();

        for (int i = 0; i < nombreVoitureTrafic; i++)
        {
            lTempTrafic.Numero = SimulationXML.Instance.Trafic.ElementAt(i).Numero;
            lTempTrafic.Debut.X = SimulationXML.Instance.Trafic.ElementAt(i).XDebut;
            lTempTrafic.Debut.Y = SimulationXML.Instance.Trafic.ElementAt(i).YDebut;
            lTempTrafic.Fin.X = SimulationXML.Instance.Trafic.ElementAt(i).XFin;
            lTempTrafic.Fin.Y = SimulationXML.Instance.Trafic.ElementAt(i).YFin;
            lTempTrafic.Vitesse = SimulationXML.Instance.Trafic.ElementAt(i).Vitesse;
            lTempTrafic.Periode = SimulationXML.Instance.Trafic.ElementAt(i).Periode;
            lTempTrafic.Phase = SimulationXML.Instance.Trafic.ElementAt(i).Phase;
            traficRoutier.Add(lTempTrafic);
            etatTraficPhase.Add(new KeyValuePair<int, int>(traficRoutier.ElementAt(i).Numero, traficRoutier.ElementAt(i).Phase));
        }

    }

    public List<etatTrafic> UpdatePeriodeTrafic()
    {
        List<etatTrafic> lEtatTraficRoutiere = new List<etatTrafic>();

        List<KeyValuePair<int, int>> tempEtatTraficTempsRestant = new List<KeyValuePair<int, int>>();
        List<KeyValuePair<int, int>> tempEtatTraficTempsRestant01 = new List<KeyValuePair<int, int>>();
        if (etatTraficTempsRestant.Count != 0)
        {
            var tabPeriode = etatTraficTempsRestant.FindAll(x => x.Value == 0);
            if (tabPeriode.Count != 0)
            {
                List<KeyValuePair<int, int>> tempEtatTraficNouvellePeriode = new List<KeyValuePair<int, int>>();
                for (int i = 0; i < tabPeriode.Count; i++)
                {

                    int indNumeroTrafic = tabPeriode.ElementAt(i).Key;
                    int tempPeriode = traficRoutier.Find(x => x.Numero == indNumeroTrafic).Periode;

                    etatTrafic lTempEtatTrafic = new etatTrafic();
                    lTempEtatTrafic.Numero = indNumeroTrafic;
                    lTempEtatTrafic.position = traficRoutier.Find(x => x.Numero == indNumeroTrafic).Debut;
                    lTempEtatTrafic.Vitesse = traficRoutier.Find(x => x.Numero == indNumeroTrafic).Vitesse;
                    lTempEtatTrafic.tempsQuiReste = tempPeriode;
                    lTempEtatTrafic.UID = nextUID;
                    nextUID++;
                    lEtatTraficRoutiere.Add(lTempEtatTrafic);

                    tempEtatTraficNouvellePeriode.Add(new KeyValuePair<int, int>(indNumeroTrafic, tempPeriode));
                }

                for (int i = 0; i < etatTraficTempsRestant.Count; i++)
                {
                    int ltempDejaEcrite = 0;
                    for (int j = 0; j < tempEtatTraficNouvellePeriode.Count; j++)
                    {
                        if (etatTraficTempsRestant[i].Key == tempEtatTraficNouvellePeriode[j].Key)
                        {
                            tempEtatTraficTempsRestant01.Add(tempEtatTraficNouvellePeriode[j]);
                            ltempDejaEcrite = 1;
                            break;
                        }
                    }
                    if (ltempDejaEcrite == 0) tempEtatTraficTempsRestant01.Add(etatTraficTempsRestant[i]);

                }
                etatTraficTempsRestant = tempEtatTraficTempsRestant01;

            }

            for (int i = 0; i < etatTraficTempsRestant.Count; i++)
            {
                tempEtatTraficTempsRestant.Add(new KeyValuePair<int, int>(etatTraficTempsRestant[i].Key, etatTraficTempsRestant[i].Value - 1));
            }
            etatTraficTempsRestant = tempEtatTraficTempsRestant;
        }

        return lEtatTraficRoutiere;
    }

    public List<etatTrafic> UpdatePhaseTrafic()
    {
        List<KeyValuePair<int, int>> tempEtatTraficPhase = new List<KeyValuePair<int, int>>();
        List<KeyValuePair<int, int>> tempEtatTraficPhase01 = new List<KeyValuePair<int, int>>();

        if (etatTraficPhase.Count != 0)
        {
            for (int i = 0; i < etatTraficPhase.Count; i++)
            {
                tempEtatTraficPhase.Add(new KeyValuePair<int, int>(etatTraficPhase[i].Key, etatTraficPhase[i].Value - 1));
            }
            etatTraficPhase = tempEtatTraficPhase;

            var tabPhase = etatTraficPhase.FindAll(x => x.Value == 0);
            if (tabPhase.Count != 0)
            {
                for (int i = 0; i < tabPhase.Count; i++)
                {
                    var lElementTrafic = traficRoutier.Find(x => x.Numero == tabPhase[i].Key);

                    etatTrafic lTempEtatTrafic = new etatTrafic();

                    lTempEtatTrafic.Numero = lElementTrafic.Numero;
                    lTempEtatTrafic.position = lElementTrafic.Debut;
                    lTempEtatTrafic.tempsQuiReste = lElementTrafic.Periode;
                    lTempEtatTrafic.Vitesse = lElementTrafic.Vitesse;
                    lTempEtatTrafic.UID = nextUID;
                    nextUID++;

                    etatTraficRoutier.Add(lTempEtatTrafic);

                    etatTraficTempsRestant.Add(new KeyValuePair<int, int>(lTempEtatTrafic.Numero, lTempEtatTrafic.tempsQuiReste));

                }

                for (int i = 0; i < etatTraficPhase.Count; i++)
                {
                    if (etatTraficPhase[i].Value != 0)
                        tempEtatTraficPhase01.Add(etatTraficPhase[i]);
                }
                etatTraficPhase = tempEtatTraficPhase01;

            }

        }

        return UpdatePeriodeTrafic();

    }

    public void UpdateTrafic()
    {
        List<etatTrafic> lEtatTraficRoutiere = UpdatePhaseTrafic();

        for (int i = 0; i < etatTraficRoutier.Count; i++)
        {
            Boolean circuleHorizontale = false;
            Boolean circulePositive = false;
            etatTrafic lTempEtatTrafic = new etatTrafic();
            int flagTrafic = 0;

            var varTempTrafic = traficRoutier.Find(x => x.Numero == etatTraficRoutier.ElementAt(i).Numero);

            if (varTempTrafic.Debut.Y == varTempTrafic.Fin.Y)
                circuleHorizontale = true;
            if (varTempTrafic.Debut.X < varTempTrafic.Fin.X || varTempTrafic.Debut.Y < varTempTrafic.Fin.Y)
                circulePositive = true;

            if (circuleHorizontale)
            {
                if (circulePositive)
                {
                    lTempEtatTrafic.position.X = etatTraficRoutier.ElementAt(i).position.X + etatTraficRoutier.ElementAt(i).Vitesse;
                    lTempEtatTrafic.position.Y = etatTraficRoutier.ElementAt(i).position.Y;
                    lTempEtatTrafic.direction = R07GPS.directionEnum.EST;

                    if (lTempEtatTrafic.position.X <= traficRoutier.Find(x => x.Numero == varTempTrafic.Numero).Fin.X)
                        flagTrafic = 1;
                }
                else
                {
                    lTempEtatTrafic.position.X = etatTraficRoutier.ElementAt(i).position.X - etatTraficRoutier.ElementAt(i).Vitesse;
                    lTempEtatTrafic.position.Y = etatTraficRoutier.ElementAt(i).position.Y;
                    lTempEtatTrafic.direction = R07GPS.directionEnum.OUEST;

                    if (lTempEtatTrafic.position.X >= traficRoutier.Find(x => x.Numero == varTempTrafic.Numero).Fin.X)
                        flagTrafic = 1;
                }
            }
            else
            {
                if (circulePositive)
                {
                    lTempEtatTrafic.position.Y = etatTraficRoutier.ElementAt(i).position.Y + etatTraficRoutier.ElementAt(i).Vitesse;
                    lTempEtatTrafic.position.X = etatTraficRoutier.ElementAt(i).position.X;
                    lTempEtatTrafic.direction = R07GPS.directionEnum.SUD;

                    if (lTempEtatTrafic.position.Y <= traficRoutier.Find(x => x.Numero == varTempTrafic.Numero).Fin.Y)
                        flagTrafic = 1;
                }
                else
                {
                    lTempEtatTrafic.position.Y = etatTraficRoutier.ElementAt(i).position.Y - etatTraficRoutier.ElementAt(i).Vitesse;
                    lTempEtatTrafic.position.X = etatTraficRoutier.ElementAt(i).position.X;
                    lTempEtatTrafic.direction = R07GPS.directionEnum.NORD;

                    if (lTempEtatTrafic.position.Y >= traficRoutier.Find(x => x.Numero == varTempTrafic.Numero).Fin.Y)
                        flagTrafic = 1;
                }
            }

            if (flagTrafic == 1)
            {
                lTempEtatTrafic.Numero = varTempTrafic.Numero;
                lTempEtatTrafic.Vitesse = varTempTrafic.Vitesse;
                lTempEtatTrafic.tempsQuiReste = varTempTrafic.Periode;
                lTempEtatTrafic.UID = etatTraficRoutier.ElementAt(i).UID;

                lEtatTraficRoutiere.Add(lTempEtatTrafic);
            }
        }

        etatTraficRoutier = lEtatTraficRoutiere;
    }


}
