using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Map : ElementGraphique
{
    public Map()
    {
        mRoads = new List<Road>();
        mGrid = new Grille();
        mFeux = new List<Feu>();
        mAuto = new Voiture(new Point(1, 1), Color.Blue);
        mTrafic = new List<Voiture>();
    }


    public void addRoute(Road inRoad)
    {
        mRoads.Add(inRoad);
    }

    public void addFeu(Feu inFeu)
    {
        mFeux.Add(inFeu);
    }

    public void addCar(Voiture inAuto)
    {
        inAuto.ratioPixel = ratioPixel;
        mTrafic.Add(inAuto);
    }

    public void updateFeuColor()
    {
        IEnumerable<Feu> query;

        foreach (FeuIntersection intersection in R05Environnement.Instance.listFeuIntersection)
        {
            query = mFeux.Where(x => x.mPosIntersection == intersection.FeuPosition);
            foreach (Feu currFeu in query)
            {
                if (intersection.directionVertJaune == currFeu.OrientationGPS)
                {
                    if (intersection.EtatFeuVertJaune == FeuIntersection.EtatFeu.JAUNE)
                        currFeu.ChangeColor(Color.Yellow);
                    else
                        currFeu.ChangeColor(Color.Green);
                }
                else
                {
                    currFeu.ChangeColor(Color.Red);
                }
            }
        }
    }

    public void MoveTrafic(List<Trafic.etatTrafic> inEtatTrafic)
    {
        mTrafic.Clear();
        foreach (Trafic.etatTrafic currTrafic in inEtatTrafic)
        {
            Voiture auto;

            auto = new Voiture(currTrafic.position, Color.PowderBlue);

            auto.setDirection(currTrafic.direction);
            addCar(auto);
            ChangePixelRatio(ratioPixel);
        }
    }

    public void MoveAuto(Point inPosition, R07GPS.directionEnum inDirection)
    {
        mAuto.MoveCar(inPosition);
        mAuto.setDirection(inDirection);
    }


    public void DrawMap(Graphics Graph)
    {
        foreach (Road road in mRoads)
        {
            road.DrawRoad(Graph);
        }

        mGrid.DrawGrille(Graph);

        foreach (Feu i in mFeux)
        {
            i.DrawFeu(Graph);
        }
        foreach (Voiture i in mTrafic)
        {
            i.DrawTraffic(Graph);
        }
        mAuto.DrawAuto(Graph);
    }
    public void ChangePixelRatio(int inRatio)
    {
        ratioPixel = inRatio;
        foreach (Road Route in mRoads)
        {
            Route.ratioPixel = inRatio;
        }
        foreach (Feu feu in mFeux)
        {
            feu.ratioPixel = inRatio;
        }
        mGrid.ratioPixel = inRatio;
        mAuto.ratioPixel = inRatio;
        foreach (Voiture car in mTrafic)
        {
            car.ratioPixel = inRatio;
        }
    }

    public void clearRoad()
    {
        mRoads.Clear();
    }


    private List<Road> mRoads;
    private Grille mGrid;
    private List<Feu> mFeux;
    private Voiture mAuto;
    private List<Voiture> mTrafic;
}
