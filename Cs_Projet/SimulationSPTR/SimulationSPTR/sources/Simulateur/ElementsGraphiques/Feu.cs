using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Feu : ElementGraphique
{   
    public static Image mColorG = SimulationSPTR.Properties.Resources.LigthG;
    public static Image mColorY = SimulationSPTR.Properties.Resources.LigthY;
    public static Image mColorR = SimulationSPTR.Properties.Resources.LigthR;

   
    
    public void ChangeColor(Color inColor)
    {
      mColor = inColor;
    }
    
    public void DrawFeu(Graphics Graph)
    {
        mSize = new Size(1 * ratioPixel - 1, 1 * ratioPixel / 2 - 1);
      //Point lCoordonnee = new Point((mPosition.X + SidesSpace) * PixelRatio + PixelRatio / 4, (mPosition.Y + TopSpace) * PixelRatio + PixelRatio / 4);
      Point lCoordonnee = new Point((mPos.X + LeftRight) * ratioPixel + ratioPixel / 15, (mPos.Y + Top) * ratioPixel + ratioPixel / 4);

      //Graph.FillEllipse(new SolidBrush(mColor), new Rectangle(lCoordonnee, mSize));
      if (mColor == Color.Green) Graph.DrawImage(mColorG, lCoordonnee.X, lCoordonnee.Y, mSize.Width, mSize.Height);
      else if (mColor == Color.Yellow) Graph.DrawImage(mColorY, lCoordonnee.X,lCoordonnee.Y, mSize.Width , mSize.Height);
      else Graph.DrawImage(mColorR, lCoordonnee.X, lCoordonnee.Y, mSize.Width, mSize.Height);
    }

     public R07GPS.directionEnum OrientationGPS
    {
      get { return mOrientationGPS; }
      set { mOrientationGPS = value; }
    }
 
    public Feu(Point inPos, Color inColor, String inOrientation)
    {

      mPosIntersection = inPos;
      mOrientation = inOrientation;
      
      //mSize = new Size(1 * PixelRatio / 2, 1 * PixelRatio / 2);
      mSize = new Size(1 * (ratioPixel+1), 1 * (ratioPixel+1) / 2);
      switch (mOrientation)
      {
        case "N":
          mPos.X = (inPos.X / SimulationXML.Instance.Echelle) - 2;
          mPos.Y = (inPos.Y / SimulationXML.Instance.Echelle) - 2;
          
          mOrientationGPS = R07GPS.directionEnum.SUD;
          break;
        case "S":
          mPos.X = inPos.X / SimulationXML.Instance.Echelle;
          mPos.Y = (inPos.Y / SimulationXML.Instance.Echelle);
          mOrientationGPS = R07GPS.directionEnum.NORD;
          break;
        case "E":
          mPos.X = inPos.X / SimulationXML.Instance.Echelle;
          mPos.Y = (inPos.Y / SimulationXML.Instance.Echelle) - 2;
          mOrientationGPS = R07GPS.directionEnum.OUEST;
          break;
        case "O":
          mPos.X = (inPos.X / SimulationXML.Instance.Echelle) - 2;
          mPos.Y = (inPos.Y / SimulationXML.Instance.Echelle);
          mOrientationGPS = R07GPS.directionEnum.EST;
          break;
      }
      mColor = inColor;

      if (mPos.X <= 0)
      {
        mPos.X = 0;
      }
      else if (mPos.X > 32)
      {
        mPos.X = 32;
      }

      if (mPos.Y <= 0)
      {
        mPos.Y = 0;
      }
      else if (mPos.Y > 32)
      {
        mPos.Y = 32;
      }
    }
    
    private Color mColor;
    private Point mPos;
    private String mOrientation;
    private Size mSize;
    public Point mPosIntersection;
    private R07GPS.directionEnum mOrientationGPS;

}
