using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Grille : ElementGraphique
  {
    public Grille()
    {
    }

    public void DrawGrille(Graphics inGraph)
    {
      for (int i = 0; i < 33; i++)
      {
          string number = (i+1).ToString();
          Point StringEmplacement = new Point((i + LeftRight) * ratioPixel, Top * ratioPixel);
          Point StringSize = new Point((i + LeftRight) * ratioPixel, (32 + Top) * ratioPixel);
          Size rectanglesize = new Size(StringSize);
          RectangleF StringZone = new RectangleF(StringEmplacement,rectanglesize);
        inGraph.DrawLine(new Pen(Color.Black), (i + LeftRight) * ratioPixel, Top * ratioPixel, (i + LeftRight) * ratioPixel, (32 + Top) * ratioPixel);
        inGraph.DrawLine(new Pen(Color.Black), LeftRight * ratioPixel, (i + Top) * ratioPixel, (32 + LeftRight) * ratioPixel, (i + Top) * ratioPixel);
       if(i!= 32) inGraph.DrawString(number, font, myBrush, (i + LeftRight) * ratioPixel, Top * ratioPixel-15);
       if (i != 32) inGraph.DrawString(number, font, myBrush, LeftRight * ratioPixel-17, (i + Top) * ratioPixel);
        
      }
    }
    
    Font font = new Font("Arial", 9.0f);
    Brush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
}
