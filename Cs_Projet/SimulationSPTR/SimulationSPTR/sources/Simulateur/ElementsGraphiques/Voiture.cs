using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Reflection;

public class Voiture : ElementGraphique
{
    public static Image mImageCarUp = SimulationSPTR.Properties.Resources.CarU;
    public static Image mImageCarDown = SimulationSPTR.Properties.Resources.CarD;
    public static Image mImageCarLeft = SimulationSPTR.Properties.Resources.CarL;
    public static Image mImageCarRight = SimulationSPTR.Properties.Resources.CarR;

    public static Image mImageTrafficUp = SimulationSPTR.Properties.Resources.TrafficU;
    public static Image mImageTrafficDown = SimulationSPTR.Properties.Resources.TrafficD;
    public static Image mImageTrafficLeft = SimulationSPTR.Properties.Resources.trafficL;
    public static Image mImageTrafficRight = SimulationSPTR.Properties.Resources.TrafficR;

    public Voiture(Point inPos, Color inColor)
    {
        mColor = inColor;
        mSize = new Size(1 * ratioPixel / 2, 1 * ratioPixel / 2);
        mPosition.X = inPos.X - 1;
        mPosition.Y = inPos.Y - 1;

    }


    public void MoveCar(Point inPoint)
    {
        mPosition.X = inPoint.X;
        mPosition.Y = inPoint.Y;
    }
    public void DrawTraffic(Graphics Graph)
    {
        if (mPosition.X > 0 && mPosition.Y > 0)
        {
            setDirection(mDirection);
            int lEchelle;
            if (SimulationXML.Instance.Echelle > 0) lEchelle = SimulationXML.Instance.Echelle;
            else lEchelle = 1;
            //mSize = new Size(1 * ratioPixel / 2, 1 * ratioPixel / 4);
            mSize = new Size(1 * ratioPixel * (SimulationXML.Instance.mTailleVoiture / 100) / 2, (SimulationXML.Instance.mTailleVoiture / 100) * ratioPixel / 4);
            //mSize = new Size(PixelRatio, PixelRatio);

            Point lCoordonnee = new Point((mPosition.X * ratioPixel / lEchelle) + ((LeftRight - 1) * ratioPixel) + mOffsetX, (mPosition.Y * ratioPixel / lEchelle) + ((Top - 1) * ratioPixel) + mOffsetY);

            //Graph.FillEllipse(new SolidBrush(mColor), new Rectangle(lCoordonnee, mSize));
            if (mDirection == R07GPS.directionEnum.EST) Graph.DrawImage(mImageTrafficRight, lCoordonnee.X, lCoordonnee.Y, mSize.Width, mSize.Height);
            else if (mDirection == R07GPS.directionEnum.OUEST) Graph.DrawImage(mImageTrafficLeft, lCoordonnee.X, lCoordonnee.Y, mSize.Width, mSize.Height);
            else if (mDirection == R07GPS.directionEnum.NORD) Graph.DrawImage(mImageTrafficUp, lCoordonnee.X, lCoordonnee.Y, mSize.Height, mSize.Width);
            else Graph.DrawImage(mImageTrafficDown, lCoordonnee.X, lCoordonnee.Y, mSize.Height, mSize.Width);
        }
    }
    public void setDirection(R07GPS.directionEnum inDirection)
    {
        mDirection = inDirection;
        switch (inDirection)
        {
            case R07GPS.directionEnum.EST:
                mOffsetY = ratioPixel / 2;
                mOffsetX = ratioPixel / 4;
                break;
            case R07GPS.directionEnum.NORD:
                mOffsetY = ratioPixel / 4;
                mOffsetX = ratioPixel / 2;
                break;
            case R07GPS.directionEnum.OUEST:
                mOffsetY = 0;
                mOffsetX = ratioPixel / 4;
                break;
            case R07GPS.directionEnum.SUD:
                mOffsetY = ratioPixel / 4;
                mOffsetX = 0;
                break;
            case R07GPS.directionEnum.UNDEFINED:
                mOffsetY = ratioPixel / 4;
                mOffsetX = ratioPixel / 4;
                break;
        }
    }

    public void DrawAuto(Graphics Graph)
    {
        if (mPosition.X > 0 && mPosition.Y > 0)
        {
            setDirection(mDirection);
            int lEchelle;
            if (SimulationXML.Instance.Echelle > 0) lEchelle = SimulationXML.Instance.Echelle;
            else lEchelle = 1;

            mSize = new Size(1 * ratioPixel * (SimulationXML.Instance.mTailleVoiture / 100) / 2, (SimulationXML.Instance.mTailleVoiture / 100) * ratioPixel / 4);
            //mSize = new Size(PixelRatio, PixelRatio);

            Point lCoordonnee = new Point((mPosition.X * ratioPixel / lEchelle) + ((LeftRight - 1) * ratioPixel) + mOffsetX, (mPosition.Y * ratioPixel / lEchelle) + ((Top - 1) * ratioPixel) + mOffsetY);

            //Graph.FillEllipse(new SolidBrush(mColor), new Rectangle(lCoordonnee, mSize));
            if (mDirection == R07GPS.directionEnum.EST) Graph.DrawImage(mImageCarRight, lCoordonnee.X, lCoordonnee.Y, mSize.Width, mSize.Height);
            else if (mDirection == R07GPS.directionEnum.OUEST) Graph.DrawImage(mImageCarLeft, lCoordonnee.X, lCoordonnee.Y, mSize.Width, mSize.Height);
            else if (mDirection == R07GPS.directionEnum.NORD) Graph.DrawImage(mImageCarUp, lCoordonnee.X, lCoordonnee.Y, mSize.Height, mSize.Width);
            else Graph.DrawImage(mImageCarDown, lCoordonnee.X, lCoordonnee.Y, mSize.Height, mSize.Width);
        }
    }


    private Point mPosition;
    private Color mColor;
    private Size mSize;
    int mOffsetY = 0;
    int mOffsetX = 0;
    R07GPS.directionEnum mDirection = R07GPS.directionEnum.EST;

}
