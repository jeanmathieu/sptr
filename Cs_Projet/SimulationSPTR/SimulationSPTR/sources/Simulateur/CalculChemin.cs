using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo("SimulationSPTRtests.CalculCheminTest")]

class CalculChemin
{
    public CalculChemin()
    {
    }
    public R07GPS.directionEnum directionDepart(List<Point> listPointCheminASuivre, Point posVoiture)
    {
        if (listPointCheminASuivre.Count > 0)
        {
            if (posVoiture == listPointCheminASuivre[0])
            {
                if (listPointCheminASuivre.Count() > 0)
                    return calculDirectionGPS(posVoiture, listPointCheminASuivre[1]);
                else
                    return R07GPS.directionEnum.UNDEFINED;
            }
            else
                return calculDirectionGPS(posVoiture, listPointCheminASuivre[0]);
        }
        else
            throw new Exception("Pas de chemin a suivre");
    }

    public bool isPosNoeud(Point inPosVoiture)
    {
        List<Point> courrant = mNoeudRoutes.FindAll(x => x == inPosVoiture);
        return courrant.Count() == 1;
    }

    public int distanceNextNode(Point positionVoiture, R07GPS.directionEnum direction)
    {
        int distance = 0;
        Point prochainNoeud;

        prochainNoeud = NextNode(positionVoiture, direction);

        distance = (int)calculDistance(positionVoiture, prochainNoeud);

        return distance;
    }


    public int calculLongueurChemin(List<Point> listpointducheminasuivre, Point positionVoiture)
    {

        double longueurChemin = 0;

        Point lPointArrivee = new Point(SimulationXML.Instance.XArrivee, SimulationXML.Instance.YArrivee);
        int estUnSommetFin = 0;
        if (lPointArrivee == listpointducheminasuivre[listpointducheminasuivre.Count - 1]) estUnSommetFin = 1;

        int estUnSommetDebut = 1;
        int lIndiceDuTableau = listpointducheminasuivre.IndexOf(positionVoiture);

        if (lIndiceDuTableau == -1)
        {
            estUnSommetDebut = 0;
            lIndiceDuTableau = -2;

            for (int i = 0; i < listpointducheminasuivre.Count - 1; i++)
            {
                if (positionVoiture.X == listpointducheminasuivre[i].X && positionVoiture.X == listpointducheminasuivre[i + 1].X)
                {
                    if ((positionVoiture.Y > listpointducheminasuivre[i].Y && positionVoiture.Y < listpointducheminasuivre[i + 1].Y) || (positionVoiture.Y < listpointducheminasuivre[i].Y && positionVoiture.Y > listpointducheminasuivre[i + 1].Y))
                    {
                        lIndiceDuTableau = i + 1;
                        break;
                    }
                }
                if (positionVoiture.Y == listpointducheminasuivre[i].Y && positionVoiture.Y == listpointducheminasuivre[i + 1].Y)
                {
                    if ((positionVoiture.X > listpointducheminasuivre[i].X && positionVoiture.X < listpointducheminasuivre[i + 1].X) || (positionVoiture.X < listpointducheminasuivre[i].X && positionVoiture.X > listpointducheminasuivre[i + 1].X))
                    {
                        lIndiceDuTableau = i + 1;
                        break;
                    }
                }

            }
        }

        if (lIndiceDuTableau == -2)
        {
            lIndiceDuTableau = 0;
        }


        if (estUnSommetDebut == 0)
        {
            longueurChemin += calculDistance(positionVoiture, listpointducheminasuivre[lIndiceDuTableau]);
        }


        if (estUnSommetFin == 1)
        {
            longueurChemin += calculDistance(listpointducheminasuivre[listpointducheminasuivre.Count - 1], lPointArrivee);
        }

        for (int i = lIndiceDuTableau; i < listpointducheminasuivre.Count - 2; i++)
        {
            longueurChemin += calculDistance(listpointducheminasuivre[i], listpointducheminasuivre[i + 1]);
        }

        return (int)longueurChemin;
    }



    public R07GPS.directionEnum direction(List<Point> listpointducheminasuivre, Point positionVoiture, R07GPS.directionEnum direction)
    {
        R07GPS.directionEnum nouvelleDirection;
        Point Pcourant;
        int courrant = listpointducheminasuivre.IndexOf(positionVoiture);

        if (courrant == -1)
            return direction;

        if (courrant + 1 < listpointducheminasuivre.Count)
            Pcourant = listpointducheminasuivre[courrant + 1];
        else
            return R07GPS.directionEnum.UNDEFINED;

        if (positionVoiture.X < Pcourant.X)
            nouvelleDirection = R07GPS.directionEnum.EST;
        else if (positionVoiture.X > Pcourant.X)
            nouvelleDirection = R07GPS.directionEnum.OUEST;
        else if (positionVoiture.Y > Pcourant.Y)
            nouvelleDirection = R07GPS.directionEnum.NORD;
        else if (positionVoiture.Y < Pcourant.Y)
            nouvelleDirection = R07GPS.directionEnum.SUD;
        else
            nouvelleDirection = R07GPS.directionEnum.UNDEFINED;

        return nouvelleDirection;
    }

    public Point NextNode(Point positionVoiture, R07GPS.directionEnum direction)
    {
        Point prochainNoeud = new Point();

        List<Point> listPoint = new List<Point>();
        int min = int.MaxValue;

        switch (direction)
        {
            case R07GPS.directionEnum.EST:
                listPoint = mNoeudRoutes.FindAll(x => x.Y == positionVoiture.Y && x.X > positionVoiture.X);
                foreach (Point noeud in listPoint)
                {
                    if (min > noeud.X - positionVoiture.X)
                    {
                        prochainNoeud = noeud;
                        min = noeud.X - positionVoiture.X;
                    }
                }
                break;
            case R07GPS.directionEnum.OUEST:
                listPoint = mNoeudRoutes.FindAll(x => x.Y == positionVoiture.Y && x.X < positionVoiture.X);
                foreach (Point noeud in listPoint)
                {
                    if (min > positionVoiture.X - noeud.X)
                    {
                        prochainNoeud = noeud;
                        min = positionVoiture.X - noeud.X;
                    }
                }
                break;
            case R07GPS.directionEnum.NORD:
                listPoint = mNoeudRoutes.FindAll(x => x.Y < positionVoiture.Y && x.X == positionVoiture.X);
                foreach (Point noeud in listPoint)
                {
                    if (min > positionVoiture.Y - noeud.Y)
                    {
                        prochainNoeud = noeud;
                        min = positionVoiture.Y - noeud.Y;
                    }
                }
                break;
            case R07GPS.directionEnum.SUD:
                listPoint = mNoeudRoutes.FindAll(x => x.Y > positionVoiture.Y && x.X == positionVoiture.X);
                foreach (Point noeud in listPoint)
                {
                    if (min > noeud.Y - positionVoiture.Y)
                    {
                        prochainNoeud = noeud;
                        min = noeud.Y - positionVoiture.Y;
                    }
                }
                break;
            case R07GPS.directionEnum.UNDEFINED:
                prochainNoeud = positionVoiture;
                break;
        }

        if (listPoint.Count == 0)
            prochainNoeud = positionVoiture;

        return prochainNoeud;
    }

    public int getVitesse(Point positionVoiture)
    {
        int NumRouteCourant = positionPointRoute(positionVoiture);

        if (NumRouteCourant == -1) throw new Exception("La voiture n'est pas sur une route");

        RouteAvecPoints routeCourant = mRoutes.Find(x => x.Numero == NumRouteCourant);

        return routeCourant.Vitesse;
    }

    public int calculLongueurCheminEnUT(List<Point> listpointducheminasuivre, Point positionVoiture)
    {
        double longueurChemin = 0;

        Point lPointArrivee = new Point(SimulationXML.Instance.XArrivee, SimulationXML.Instance.YArrivee);
        int estUnSommetFin = 0;
        if (lPointArrivee == listpointducheminasuivre[listpointducheminasuivre.Count - 1]) estUnSommetFin = 1;

        int estUnSommetDebut = 1;
        int lIndiceDuTableau = listpointducheminasuivre.IndexOf(positionVoiture);

        if (lIndiceDuTableau == -1)
        {
            estUnSommetDebut = 0;
            lIndiceDuTableau = -2;

            for (int i = 0; i < listpointducheminasuivre.Count - 1; i++)
            {
                if (positionVoiture.X == listpointducheminasuivre[i].X && positionVoiture.X == listpointducheminasuivre[i + 1].X)
                {
                    if ((positionVoiture.Y > listpointducheminasuivre[i].Y && positionVoiture.Y < listpointducheminasuivre[i + 1].Y) || (positionVoiture.Y < listpointducheminasuivre[i].Y && positionVoiture.Y > listpointducheminasuivre[i + 1].Y))
                    {
                        lIndiceDuTableau = i + 1;
                        break;
                    }
                }
                if (positionVoiture.Y == listpointducheminasuivre[i].Y && positionVoiture.Y == listpointducheminasuivre[i + 1].Y)
                {
                    if ((positionVoiture.X > listpointducheminasuivre[i].X && positionVoiture.X < listpointducheminasuivre[i + 1].X) || (positionVoiture.X < listpointducheminasuivre[i].X && positionVoiture.X > listpointducheminasuivre[i + 1].X))
                    {
                        lIndiceDuTableau = i + 1;
                        break;
                    }
                }

            }
        }

        if (lIndiceDuTableau == -2)
        {
            lIndiceDuTableau = 0;
        }


        if (estUnSommetDebut == 0)
        {
            longueurChemin += calculDistance(positionVoiture, listpointducheminasuivre[lIndiceDuTableau]) / getVitesse(positionVoiture);
        }

        if (estUnSommetFin == 1)
        {
            longueurChemin += calculDistance(listpointducheminasuivre[listpointducheminasuivre.Count - 2], lPointArrivee) / getVitesse(lPointArrivee);
        }

        for (int i = lIndiceDuTableau; i < listpointducheminasuivre.Count - 2; i++)
        {
            RouteAvecPoints route = mRoutes.Find(x => (x.Debut == listpointducheminasuivre[i] && x.Fin == listpointducheminasuivre[i + 1]) || (x.Fin == listpointducheminasuivre[i] && x.Debut == listpointducheminasuivre[i + 1]));
            longueurChemin += calculDistance(listpointducheminasuivre[i], listpointducheminasuivre[i + 1]) / route.Vitesse;
        }

        return (int)longueurChemin;
    }


    public bool calculNouveauChemin(List<Point> nouvellelistpointducheminasuivre, Point positionVoiture, R07GPS.directionEnum direction)
    {
        int numNoeudDepartDebut, numNoeudDepartFin, numNoeudFinDebut, numNoeudFinFin;
        RouteAvecPoints routeCourant;
        Point pointArrive = new Point(SimulationXML.Instance.XArrivee, SimulationXML.Instance.YArrivee);

        if (direction == R07GPS.directionEnum.UNDEFINED)
            direction = R07GPS.directionEnum.OUEST;
        Point prochainNoeud = NextNode(positionVoiture, direction);

        if (nouvellelistpointducheminasuivre != null) { nouvellelistpointducheminasuivre.Clear(); }

        int numRouteDepart = positionPointRoute(positionVoiture);
        int numRouteFin = positionPointRoute(pointArrive);
        if (numRouteDepart != 0)
        {
            routeCourant = mRoutes.Find(x => x.Numero == numRouteDepart);
            numNoeudDepartDebut = mNoeudRoutes.IndexOf(routeCourant.Debut);
            numNoeudDepartFin = mNoeudRoutes.IndexOf(routeCourant.Fin);
        }
        else
        {
            numNoeudDepartDebut = mNoeudRoutes.IndexOf(positionVoiture);
            numNoeudDepartFin = mNoeudRoutes.IndexOf(positionVoiture);
        }

        if (numRouteFin != 0)
        {
            routeCourant = mRoutes.Find(x => x.Numero == numRouteFin);
            numNoeudFinDebut = mNoeudRoutes.IndexOf(routeCourant.Debut);
            numNoeudFinFin = mNoeudRoutes.IndexOf(routeCourant.Fin);
        }
        else
        {
            numNoeudFinDebut = mNoeudRoutes.IndexOf(pointArrive);
            numNoeudFinFin = mNoeudRoutes.IndexOf(pointArrive);
        }

        List<List<int>> listeCheminOptimal = calculCheminMinTable(numNoeudDepartDebut, numNoeudDepartFin, numNoeudFinDebut, numNoeudFinFin);
        List<int> nouvelleListNoeudASuivre = listeCheminOptimal[0];

        foreach (var value in nouvelleListNoeudASuivre)
        {
            nouvellelistpointducheminasuivre.Add(mNoeudRoutes.ElementAt(value));
        }

        Point arrive = new Point(SimulationXML.Instance.XArrivee, SimulationXML.Instance.YArrivee);
        nouvellelistpointducheminasuivre.Add(arrive);
        return true;
    }



    public List<RouteAvecPoints> mRoutes = new List<RouteAvecPoints>();
    public List<Point> mNoeudRoutes = new List<Point>();
    public List<List<int>> mChemin = new List<List<int>>();
    public List<List<int>> mCheminTab = new List<List<int>>();
    public Boolean[] mTaboo = new Boolean[100];
    public int[] mPath = new int[20];

    public struct RouteAvecPoints
    {
        public Point Debut;
        public Point Fin;
        public int Vitesse;
        public int Numero;
        public RouteAvecPoints(RouteAvecPoints inRoutePoints)
        {
            Debut = inRoutePoints.Debut;
            Fin = inRoutePoints.Fin;
            Vitesse = inRoutePoints.Vitesse;
            Numero = inRoutePoints.Numero;
        }
    }

    public struct Connection
    {
        public Point Sommet;
        public double Distance;
        public int Vitesse;
        public string Direction;
        public double Poids;
        public Connection(Connection inConnectiononnection)
        {
            Sommet = inConnectiononnection.Sommet;
            Distance = inConnectiononnection.Distance;
            Vitesse = inConnectiononnection.Vitesse;
            Direction = inConnectiononnection.Direction;
            Poids = inConnectiononnection.Poids;
        }
    }


    public double calculDistance(Point inPoint1, Point inPoint2)
    {
        return (Math.Sqrt(Math.Pow(inPoint1.X - inPoint2.X, 2) + Math.Pow(inPoint1.Y - inPoint2.Y, 2)));
    }

    public double calculPoids(double inDistance, int inVitesse)
    {
        return (inDistance / inVitesse);

    }

    public string calculDirection(Point inPoint1, Point inPoint2)
    {
        if (inPoint1.X == inPoint2.X)
        {
            if (inPoint2.Y > inPoint1.Y) return "S";
            else return "N";
        }

        else if (inPoint1.Y == inPoint2.Y)
        {
            if (inPoint2.X > inPoint1.X) return "E";
            else return "O";
        }
        else return "X";
    }

    public R07GPS.directionEnum calculDirectionGPS(Point inPoint1, Point inPoint2)
    {
        if (inPoint1.X == inPoint2.X)
        {
            if (inPoint2.Y > inPoint1.Y) return R07GPS.directionEnum.SUD;
            else return R07GPS.directionEnum.NORD;
        }

        else if (inPoint1.Y == inPoint2.Y)
        {
            if (inPoint2.X > inPoint1.X) return R07GPS.directionEnum.EST;
            else return R07GPS.directionEnum.OUEST;
        }
        else return R07GPS.directionEnum.UNDEFINED;
    }



    public int positionPointRoute(Point inPoint)
    {
        List<RouteAvecPoints> lRouteTmp = new List<RouteAvecPoints>();
        int lTmpRouteVoiture = -1;

        foreach (Point lPoint in mNoeudRoutes)
        {
            if (inPoint == lPoint)
            {
                lTmpRouteVoiture = 0;
                break;
            }
        }
        if (lTmpRouteVoiture != 0)
        {
            foreach (Point lPoint in mNoeudRoutes)
            {
                if (inPoint.X == lPoint.X)
                {
                    for (int i = 0; i < mRoutes.Count; i++)
                    {
                        if (inPoint.X == mRoutes[i].Debut.X && inPoint.X == mRoutes[i].Fin.X)
                        {
                            lRouteTmp.Add(mRoutes[i]);
                        }
                    }
                    for (int i = 0; i < lRouteTmp.Count; i++)
                    {
                        if ((inPoint.Y > lRouteTmp[i].Debut.Y && inPoint.Y < lRouteTmp[i].Fin.Y) || (inPoint.Y < lRouteTmp[i].Debut.Y && inPoint.Y > lRouteTmp[i].Fin.Y))
                        {
                            lTmpRouteVoiture = lRouteTmp[i].Numero;
                        }
                    }
                }

                if (inPoint.Y == lPoint.Y)
                {
                    for (int i = 0; i < mRoutes.Count; i++)
                    {
                        if (inPoint.Y == mRoutes[i].Debut.Y && inPoint.Y == mRoutes[i].Fin.Y)
                        {
                            lRouteTmp.Add(mRoutes[i]);
                        }
                    }
                    for (int i = 0; i < lRouteTmp.Count; i++)
                    {
                        if ((inPoint.X > lRouteTmp[i].Debut.X && inPoint.X < lRouteTmp[i].Fin.X) || (inPoint.X < lRouteTmp[i].Debut.X && inPoint.X > lRouteTmp[i].Fin.X))
                        {
                            lTmpRouteVoiture = lRouteTmp[i].Numero;
                        }
                    }
                }
            }
        }
        return (lTmpRouteVoiture);
    }

    public void supprimerCheminRedondants()
    {
        mRoutes.RemoveAll(x => x.Numero == 10 || x.Numero == 12 || x.Numero == 17 || x.Numero == 20 || x.Numero == 23);
    }

    public List<List<int>> cheminTabConstruire()
    {
        List<List<int>> tempCheminTab = new List<List<int>>();

        foreach (Point hPoint in mNoeudRoutes)
        {
            List<int> souChemin = new List<int>();
            foreach (Point vPoint in mNoeudRoutes)
            {
                int inConnectionExiste = 0;
                foreach (RouteAvecPoints lRoute in mRoutes)
                {
                    if (hPoint == lRoute.Debut && vPoint == lRoute.Fin || vPoint == lRoute.Debut && hPoint == lRoute.Fin)
                        inConnectionExiste = 1;
                }
                if (inConnectionExiste == 1)
                    souChemin.Add(1);
                else
                    souChemin.Add(0);
            }
            tempCheminTab.Add(souChemin);
        }
        return (tempCheminTab);
    }


    public void chargerRoutesXML()
    {

        int nombreRoute = SimulationXML.Instance.Route.Count();
        Point lPointDebut = new Point();
        Point lPointFin = new Point();
        RouteAvecPoints lTempRouteAvecPoints = new RouteAvecPoints();
        mRoutes.Clear();
        mNoeudRoutes.Clear();

        for (int i = 0; i < nombreRoute; i++)
        {
            lPointDebut.X = SimulationXML.Instance.Route.ElementAt(i).XDebut * SimulationXML.Instance.Echelle;
            lPointDebut.Y = SimulationXML.Instance.Route.ElementAt(i).YDebut * SimulationXML.Instance.Echelle;

            lPointFin.X = SimulationXML.Instance.Route.ElementAt(i).XFin * SimulationXML.Instance.Echelle;
            lPointFin.Y = SimulationXML.Instance.Route.ElementAt(i).YFin * SimulationXML.Instance.Echelle;


            lTempRouteAvecPoints.Debut = lPointDebut;
            lTempRouteAvecPoints.Fin = lPointFin;
            lTempRouteAvecPoints.Numero = SimulationXML.Instance.Route.ElementAt(i).Numero;
            lTempRouteAvecPoints.Vitesse = SimulationXML.Instance.Route.ElementAt(i).Vitesse;

            mRoutes.Add(lTempRouteAvecPoints);


            int lPtDebutExiste = 0;
            int lPtFinExiste = 0;

            foreach (Point k in mNoeudRoutes)
            {
                if (k == lPointDebut) lPtDebutExiste = 1;
                if (k == lPointFin) lPtFinExiste = 1;
            }

            if (lPtDebutExiste == 0) mNoeudRoutes.Add(lPointDebut);
            if (lPtFinExiste == 0) mNoeudRoutes.Add(lPointFin);
        }
        cheminReConstruire();
        cheminReConstruire();
        cheminReConstruire();


        supprimerCheminRedondants();
    }


    public void cheminReConstruire()
    {
        List<KeyValuePair<Point, RouteAvecPoints>> tempNoeud = noeudSurRoute();

        for (int i = 0; i < tempNoeud.Count; i++)
        {
            int routeExiste1 = 0;
            int routeExiste2 = 0;
            int numeroDernierRoute = mRoutes.Last().Numero;
            Point lPointDebut = new Point();
            Point lPointFin1 = new Point();
            Point lPointFin2 = new Point();
            RouteAvecPoints lTempRouteAvecPoints = new RouteAvecPoints();
            RouteAvecPoints lTempRouteAvecPoints1 = new RouteAvecPoints();
            RouteAvecPoints lTempRouteAvecPoints2 = new RouteAvecPoints();
            lPointDebut = tempNoeud[i].Key;
            lTempRouteAvecPoints = tempNoeud[i].Value;
            lPointFin1 = lTempRouteAvecPoints.Debut;
            lPointFin2 = lTempRouteAvecPoints.Fin;

            lTempRouteAvecPoints1.Debut = lPointDebut;
            lTempRouteAvecPoints1.Fin = lPointFin1;
            lTempRouteAvecPoints1.Numero = numeroDernierRoute + 1;
            lTempRouteAvecPoints1.Vitesse = lTempRouteAvecPoints.Vitesse;

            lTempRouteAvecPoints2.Debut = lPointDebut;
            lTempRouteAvecPoints2.Fin = lPointFin2;
            lTempRouteAvecPoints2.Numero = numeroDernierRoute + 2;
            lTempRouteAvecPoints2.Vitesse = lTempRouteAvecPoints.Vitesse;

            foreach (RouteAvecPoints lRoute in mRoutes)
            {
                if ((lRoute.Debut == lTempRouteAvecPoints1.Debut && lRoute.Fin == lTempRouteAvecPoints1.Fin) || (lRoute.Debut == lTempRouteAvecPoints1.Fin && lRoute.Fin == lTempRouteAvecPoints1.Debut))
                {
                    routeExiste1 = 1;
                    break;
                }
                else
                    routeExiste1 = 0;
            }
            foreach (RouteAvecPoints lRoute in mRoutes)
            {
                if ((lRoute.Debut == lTempRouteAvecPoints2.Debut && lRoute.Fin == lTempRouteAvecPoints2.Fin) || (lRoute.Debut == lTempRouteAvecPoints2.Fin && lRoute.Fin == lTempRouteAvecPoints2.Debut))
                {
                    routeExiste2 = 1;
                    break;
                }
                else
                    routeExiste2 = 0;
            }
            if (routeExiste1 == 0)
                mRoutes.Add(lTempRouteAvecPoints1);
            if (routeExiste2 == 0)
                mRoutes.Add(lTempRouteAvecPoints2);
        }
    }


    public List<KeyValuePair<Point, RouteAvecPoints>> noeudSurRoute()
    {
        List<KeyValuePair<Point, RouteAvecPoints>> sousNoeud = new List<KeyValuePair<Point, RouteAvecPoints>>();

        foreach (Point lPoint in mNoeudRoutes)
        {
            foreach (RouteAvecPoints lRoute in mRoutes)
            {
                if (lPoint == lRoute.Debut || lPoint == lRoute.Fin)
                    continue;
                else if (lPoint.X > lRoute.Debut.X && lPoint.X < lRoute.Fin.X && lPoint.Y == lRoute.Debut.Y && lPoint.Y == lRoute.Fin.Y)
                {
                    sousNoeud.Add(new KeyValuePair<Point, RouteAvecPoints>(lPoint, lRoute));
                }
                else if (lPoint.X < lRoute.Debut.X && lPoint.X > lRoute.Fin.X && lPoint.Y == lRoute.Debut.Y && lPoint.Y == lRoute.Fin.Y)
                {
                    sousNoeud.Add(new KeyValuePair<Point, RouteAvecPoints>(lPoint, lRoute));
                }
                else if (lPoint.Y > lRoute.Debut.Y && lPoint.Y < lRoute.Fin.Y && lPoint.X == lRoute.Debut.X && lPoint.X == lRoute.Fin.X)
                {
                    sousNoeud.Add(new KeyValuePair<Point, RouteAvecPoints>(lPoint, lRoute));
                }
                else if (lPoint.Y < lRoute.Debut.Y && lPoint.Y > lRoute.Fin.Y && lPoint.X == lRoute.Debut.X && lPoint.X == lRoute.Fin.X)
                {
                    sousNoeud.Add(new KeyValuePair<Point, RouteAvecPoints>(lPoint, lRoute));
                }
            }
        }
        return (sousNoeud);
    }


    public void exploreChemin(int position, int target, int depth, int dim)
    {
        mPath[depth] = position;
        if (position == target)
        {
            List<int> sublistChemin = new List<int>();
            for (int i = 0; i <= depth; i++)
            {
                sublistChemin.Add(mPath[i]);
            }
            mChemin.Add(sublistChemin);
            return;
        }
        mTaboo[position] = true;

        for (int j = 0; j < dim; j++)
        {
            if (mCheminTab[position][j] == 0 || mTaboo[j])
                continue;
            exploreChemin(j, target, depth + 1, dim);
        }
        mTaboo[position] = false;
    }


    public List<double> calculPoidsCheminsExplorer()
    {
        List<double> lTempPoidsChemins = new List<double>();
        double poidsChemin = 0;

        for (int i = 0; i < mChemin.Count; i++)
        {
            for (int j = 0; j < mChemin.ElementAt(i).Count - 1; j++)
            {
                foreach (var lRoute in mRoutes)
                {
                    if ((lRoute.Debut == mNoeudRoutes.ElementAt(mChemin.ElementAt(i).ElementAt(j))
                        && lRoute.Fin == mNoeudRoutes.ElementAt(mChemin.ElementAt(i).ElementAt(j + 1))
                            || (lRoute.Fin == mNoeudRoutes.ElementAt(mChemin.ElementAt(i).ElementAt(j))
                        && lRoute.Debut == mNoeudRoutes.ElementAt(mChemin.ElementAt(i).ElementAt(j + 1)))))
                    {
                        double dist = calculDistance(lRoute.Debut, lRoute.Fin);
                        poidsChemin += dist / lRoute.Vitesse;
                        break;
                    }
                }
            }
            lTempPoidsChemins.Add(poidsChemin);
            poidsChemin = 0;
        }
        return (lTempPoidsChemins);
    }


    public List<List<int>> calculCheminMinTable(int noeudStart1, int noeudEnd1, int noeudStart2, int noeudEnd2)
    {
        mCheminTab.Clear();
        mChemin.Clear();
        List<List<int>> cheminTable = cheminTabConstruire();
        mCheminTab = cheminTable;
        exploreChemin(noeudStart1, noeudStart2, 0, cheminTable.Count);
        exploreChemin(noeudStart1, noeudEnd2, 0, cheminTable.Count);
        exploreChemin(noeudEnd1, noeudStart2, 0, cheminTable.Count);
        exploreChemin(noeudEnd1, noeudEnd2, 0, cheminTable.Count);

        List<List<int>> tempCheminMinTable = new List<List<int>>();

        List<double> listesDesPoidsChemins = calculPoidsCheminsExplorer();
        double minPoids = listesDesPoidsChemins.Min();

        for (int i = 0; i < listesDesPoidsChemins.Count; i++)
        {
            if (listesDesPoidsChemins.ElementAt(i) == minPoids)
            {
                tempCheminMinTable.Add(mChemin.ElementAt(i));
            }
        }
        return (tempCheminMinTable);
    }

}
