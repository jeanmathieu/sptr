using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class R01CarteGraphique : Ressource
  {
    private R01CarteGraphique()
    {
      ResourceId = Ressource.RID.R01;
    }

    public override void init()
    {
    }

    private static R01CarteGraphique mInst = null;

    public static R01CarteGraphique Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R01CarteGraphique();

        return mInst;
      }
    }


    public override void executeEachUT()
    {

    }

}
