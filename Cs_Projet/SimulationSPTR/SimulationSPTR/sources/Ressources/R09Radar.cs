using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class R09Radar : Ressource
  {
    Trafic.etatTrafic mPlusProche;
    int mDistanceMin;
    int mUIDAutoAvant;
    private int mLastUID;
    private int mCollision;

    private R09Radar()
    {
      ResourceId = Ressource.RID.R09;
      mDistanceMin = int.MaxValue;
    }

    public override void init()
    {
      mCollision = 0;
    }

    private static R09Radar mInst = null;

    public static R09Radar Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R09Radar();

        return mInst;
      }
    }
    
    public void quelDistanceTrafic(out int distanceAutoAvant, out int vitesseAutoAvant, out int UIDAuto)
    {
      distanceAutoAvant = mDistanceMin;
      vitesseAutoAvant = mPlusProche.Vitesse;
      UIDAuto = mUIDAutoAvant;
    }
    
    public bool quelDistanceAuto(int UIDAuto, out int distanceAutoAvant, out int vitesseAutoAvant)
    {
      Trafic.etatTrafic auto = R05Environnement.Instance.traficAuto.etatTraficRoutier.Find(x => x.UID == UIDAuto);
      Point positionVoiture = R07GPS.Instance.PositionVoiture;
      R07GPS.directionEnum directionVoiture = R07GPS.Instance.CurrentDirection;

      if (auto.UID == 0)
      {
        distanceAutoAvant = int.MaxValue;
        vitesseAutoAvant = 0;
        return true;
      }

      positionVoiture = R07GPS.Instance.PositionVoiture;

      switch (directionVoiture)
      {
        case R07GPS.directionEnum.EST:
          distanceAutoAvant = auto.position.X - positionVoiture.X;
          break;
        case R07GPS.directionEnum.OUEST:
          distanceAutoAvant = positionVoiture.X - auto.position.X;
          break;
        case R07GPS.directionEnum.NORD:
          distanceAutoAvant = positionVoiture.Y - auto.position.Y;
          break;
        case R07GPS.directionEnum.SUD:
          distanceAutoAvant = auto.position.Y - positionVoiture.Y;
          break;
        default:
          distanceAutoAvant = int.MaxValue;
          break;
      }

      vitesseAutoAvant = auto.Vitesse;

      return false;
    }
    
    public bool isCollision()
    {
      return mCollision != 0;
    }

    public void detectionCollision()
    {
      int uid;
      int distance;
      int vitesse;

      if (mCollision == 0)
      {
        quelDistanceTrafic(out distance, out vitesse, out uid);

        if (uid != mLastUID && mLastUID != 0)
        {
          if (quelDistanceAuto(mLastUID, out distance, out vitesse) == false)
          {
            if (distance <= 0)
            {
              mCollision = 1;
              R04ModuleAffichage.Instance.PointInaptitude += 20;
            }
          }
        }
        mLastUID = uid;
      }
      else
      {
        if (mCollision >= SimulationXML.Instance.Collision)
          mCollision = 0;
        else
          mCollision++;
        mLastUID = 0;
      }
    }
    
    public override void executeEachUT()
    {
      Point positionVoiture;
      R07GPS.directionEnum directionVoiture;
      List<Trafic.etatTrafic> listEtatTrafic;
      List<Trafic.etatTrafic> listTraficAvant = new List<Trafic.etatTrafic>();

      positionVoiture = R07GPS.Instance.PositionVoiture;
      directionVoiture = R07GPS.Instance.CurrentDirection;

      listEtatTrafic = R05Environnement.Instance.traficAuto.etatTraficRoutier;

      switch (directionVoiture)
      {
        case R07GPS.directionEnum.EST:
          listTraficAvant = listEtatTrafic.FindAll(x => x.position.Y == positionVoiture.Y && x.position.X > positionVoiture.X && x.direction == directionVoiture);
          break;
        case R07GPS.directionEnum.OUEST:
          listTraficAvant = listEtatTrafic.FindAll(x => x.position.Y == positionVoiture.Y && x.position.X < positionVoiture.X && x.direction == directionVoiture);
          break;
        case R07GPS.directionEnum.NORD:
          listTraficAvant = listEtatTrafic.FindAll(x => x.position.Y < positionVoiture.Y && x.position.X == positionVoiture.X && x.direction == directionVoiture);
          break;
        case R07GPS.directionEnum.SUD:
          listTraficAvant = listEtatTrafic.FindAll(x => x.position.Y > positionVoiture.Y && x.position.X == positionVoiture.X && x.direction == directionVoiture);
          break;
        case R07GPS.directionEnum.UNDEFINED:
          break;
      }

      mDistanceMin = int.MaxValue;
      foreach (Trafic.etatTrafic autoAvant in listTraficAvant)
      {
        if ((Math.Abs(autoAvant.position.X - positionVoiture.X) + Math.Abs(autoAvant.position.Y - positionVoiture.Y)) < mDistanceMin)
        {
          mDistanceMin = Math.Abs(autoAvant.position.X - positionVoiture.X);
          mPlusProche = autoAvant;
          mUIDAutoAvant = autoAvant.UID;
        }
      }
      detectionCollision();
    }
}
