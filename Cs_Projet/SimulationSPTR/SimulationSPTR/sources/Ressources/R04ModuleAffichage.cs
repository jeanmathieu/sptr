using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class R04ModuleAffichage : Ressource
  {
    private R04ModuleAffichage()
    {
      ResourceId = Ressource.RID.R04;
    }

    public override void init()
    {
      listpointducheminasuivre.Clear();
      PointInaptitude = 0;
    }

    private static R04ModuleAffichage mInst = null;

    public static R04ModuleAffichage Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R04ModuleAffichage();

        return mInst;
      }
    }

    public override void executeEachUT()
    {
    }

    public List<Point> listpointducheminasuivre = new List<Point>();
    public int PointInaptitude = 0;
    public string MessagePublicCommerciaux = System.String.Empty;
    public string DerniereCommande = System.String.Empty;
    public string Meteo = System.String.Empty;
}
