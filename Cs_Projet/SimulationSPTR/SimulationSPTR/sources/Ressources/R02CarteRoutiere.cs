using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class R02CarteRoutiere : Ressource
  {
    Point mLastPositionVoiture = new Point(0, 0);
    R07GPS.directionEnum lastDirection = R07GPS.directionEnum.UNDEFINED;

    private R02CarteRoutiere()
    {
      ResourceId = Ressource.RID.R02;
    }

    private static R02CarteRoutiere mInst = null;

    public CalculChemin calculChemin = new CalculChemin();

    public static R02CarteRoutiere Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R02CarteRoutiere();

        return mInst;
      }
    }
    
    public override void init()
    {
      calculChemin.chargerRoutesXML();
    }
    
    public override void executeEachUT()
    {
      Point posVoiture;
      R07GPS.directionEnum currDirection = R07GPS.Instance.CurrentDirection;
      int distanceAFaire;
      
      if (R03CompteurDeVitesse.Instance.frienABSEnAction == 0)
        distanceAFaire = (int)R03CompteurDeVitesse.Instance.vitesse;
      else
        distanceAFaire = 0;

      calculPointInaptitudeVitesse(R07GPS.Instance.PositionVoiture);

      if (R04ModuleAffichage.Instance.listpointducheminasuivre != null)
      {
        
        while (distanceAFaire > 0)
        {
          posVoiture = R07GPS.Instance.PositionVoiture;
          calculPointInaptitudeRouge(posVoiture);

          if (calculChemin.isPosNoeud(posVoiture) == true)
          {
            currDirection = calculChemin.direction(R04ModuleAffichage.Instance.listpointducheminasuivre, posVoiture, currDirection);
            R07GPS.Instance.CurrentDirection = currDirection;
          }

          if (R07GPS.Instance.CurrentDirection != R07GPS.directionEnum.UNDEFINED)
          {
            int parcourir = Math.Min(distanceAFaire, calculChemin.distanceNextNode(posVoiture, currDirection));
            distanceAFaire = distanceAFaire - parcourir;
            R07GPS.Instance.AvanceLaVoiture(parcourir);
          }
          else
            distanceAFaire = 0;
        }
      }
    }

    
    void calculPointInaptitudeRouge(Point positionVoiture)
    {
      Point currPositionVoiture = R07GPS.Instance.PositionVoiture;
      
      if (mLastPositionVoiture != currPositionVoiture && lastDirection != R07GPS.directionEnum.UNDEFINED)
      {
        FeuIntersection currFeu;

        try
        {
          currFeu = R05Environnement.Instance.listFeuIntersection.Single(x => x.FeuPosition == R07GPS.Instance.PositionVoiture);

          if (currFeu.directionVertJaune != lastDirection)
          {
            R04ModuleAffichage.Instance.PointInaptitude += 10;
          }
        }
        catch (Exception)
        {
          
        }
      }
    }

    
    void calculPointInaptitudeVitesse(Point positionVoiture)
    {
      if (calculChemin.isPosNoeud(positionVoiture) == false)
      {
        int VitesseDeLaRoute = calculChemin.getVitesse(positionVoiture);

        if (R03CompteurDeVitesse.Instance.vitesse > VitesseDeLaRoute)
          R04ModuleAffichage.Instance.PointInaptitude++;
      }
    }
}
