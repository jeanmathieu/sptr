using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class R10RegulateurVitesse : Ressource
  {
    private R10RegulateurVitesse()
    {
      ResourceId = Ressource.RID.R10;
    }

    public override void init()
    {
      vitesse = 0;
    }

    private static R10RegulateurVitesse mInst = null;

    public static R10RegulateurVitesse Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R10RegulateurVitesse();

        return mInst;
      }
    }

    public override void executeEachUT()
    {

    }

    public int vitesse;
}
