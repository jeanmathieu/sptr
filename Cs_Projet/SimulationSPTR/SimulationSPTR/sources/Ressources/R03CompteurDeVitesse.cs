using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class R03CompteurDeVitesse : Ressource
  {
    private R03CompteurDeVitesse()
    {
      ResourceId = Ressource.RID.R03;
    }

    public override void init()
    {
      vitesse = 0;
    }

    private static R03CompteurDeVitesse mInst = null;

    public static R03CompteurDeVitesse Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R03CompteurDeVitesse();

        return mInst;
      }
    }

    public override void executeEachUT()
    {
      if (R09Radar.Instance.isCollision())
        vitesse = 0;
      else
        vitesse = R10RegulateurVitesse.Instance.vitesse;
    }

    public int vitesse; 
    public int frienABSEnAction;
}

