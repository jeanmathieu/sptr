using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class EtatRessource
  {
    private Ressource.RID ID;
    private bool mVerrouille;
    private Processus mVerrouProcessus;
    
    private void Init()
    {
      ID = 0;
      mVerrouille = false;
      mVerrouProcessus = null;
    }

    public EtatRessource()
    {
      Init();
    }
    
    public EtatRessource(Ressource.RID inRessourceID)
    {
      Init();
      ID = inRessourceID;
    }
          
    public void verrouiller(Processus inProcessus)
    {
      if (mVerrouille) throw new Exception("La ressource �tait d�j� verrouill� alors qu'on tentait de la verrouiller");

      mVerrouille = true;
      mVerrouProcessus = inProcessus;
    }

    public void deVerrouiller(Processus inProcessus)
    {
      if (mVerrouProcessus != inProcessus) throw new Exception("La ressource ne peut �tre d�verrouill� que par le processus qui la poss�de");

      mVerrouille = false;
      mVerrouProcessus = null;
    }

    public bool verrouillee
    {
      get { return mVerrouille; }
      set { mVerrouille = value; }
    }
    public Processus verrouProcessus
    {
      get { return mVerrouProcessus; }
      set { mVerrouProcessus = value; }
    }
    public Ressource.RID getID
    {
      get { return ID; }
      set { ID = value; }
    }
}
