using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class R12Telecommunication : Ressource
  {
    private R12Telecommunication()
    {
      ResourceId = Ressource.RID.R12;
    }

    int periodUT;
    int messageActif;

    private string[] MessageExterne = new string[] { 
      "Au ralenti sur un tron�on de la route 17", 
      "Gr�ve �tudiante en cours, redoublez de prudence", 
      "Une voiture intelligente fait 1 mort et 2 bless�s en Chine", 
      "Chocolat TOBLERONE en vente 10$/1kg chez MAXI"};

    public override void init()
    {
      periodUT = 0;
      messageActif = 0;
    }

    private static R12Telecommunication mInst = null;

    public static R12Telecommunication Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R12Telecommunication();

        return mInst;
      }
    }

    public string Message
    {
      get { return MessageExterne[messageActif]; }
    }
    
    public override void executeEachUT()
    {
      if (periodUT > 500)
      {
        messageActif++;
        if (messageActif >= 4)
          messageActif = 0;
        periodUT = 0;
      }

      periodUT++;
    }
}
