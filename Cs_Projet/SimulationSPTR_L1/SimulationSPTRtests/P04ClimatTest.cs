﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P04ClimatTest and is intended
    ///to contain all P04ClimatTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P04ClimatTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P04Climat Constructor
        ///</summary>
        [TestMethod()]
        public void P04ClimatConstructorTest()
        {
            P04Climat target = new P04Climat();
      Assert.IsTrue(target.periode == 30);
      Assert.IsTrue(target.contrainteFin == 30);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }


    /// <summary>
    ///A test for execute
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P04Climat target = new P04Climat(); // TODO: Initialize to an appropriate value
      List<Message> actual;
      List<Message> inMsg = null; // TODO: Initialize to an appropriate value
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 1+1);

      R05Environnement.Instance.temperatureExterieur = 20;

      // Envoi un message de dépense de ua, regarde si le compteur d'émission fonctionne
      Assert.IsTrue(target.getRess() == Ressource.RID.R05);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 0 + 1);

      Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
      Assert.IsTrue(target.getMsg().Count == 0);
      
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 0);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);

      R05Environnement.Instance.temperatureExterieur = 40;
      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 1 + 1);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 0 + 1);

      for(int i=0;i<4;i++)
      {
        Assert.IsTrue(target.getEtat() != Processus.etat.END);
        actual = target.run(inMsg);
        Assert.IsTrue(target.getTempsRestant() == 0 + (4-(i+1)));
      }
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(target.getTempsRestant() == 0);

      R05Environnement.Instance.temperatureExterieur = 0;
      target.reinitEtat();

      for (int i = 0; i < 5; i++)
      {
        Assert.IsTrue(target.getEtat() != Processus.etat.END);
        actual = target.run(inMsg);
      }
      Assert.IsTrue(target.getEtat() == Processus.etat.END);

      Assert.IsTrue((P04Climat.sequenceExec)target.etapeExecution == P04Climat.sequenceExec.END);
    }

  }
}
