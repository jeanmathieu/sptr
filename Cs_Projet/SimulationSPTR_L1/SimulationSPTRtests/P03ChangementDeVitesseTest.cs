﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P03ChangementDeVitesseTest and is intended
    ///to contain all P03ChangementDeVitesseTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P03ChangementDeVitesseTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P03ChangementDeVitesse Constructor
        ///</summary>
        [TestMethod()]
        public void P03ChangementDeVitesseConstructorTest()
        {
            P03ChangementDeVitesse target = new P03ChangementDeVitesse();

      Assert.IsTrue(target.periode == 0);
      Assert.IsTrue(target.contrainteFin == 10);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 2);
      Assert.IsTrue(target.DeclencheurProcessus[0] == Message.ID.P07);
      Assert.IsTrue(target.DeclencheurProcessus[1] == Message.ID.P12);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void P03_executeTest()
    {
      P03ChangementDeVitesse target = new P03ChangementDeVitesse();
      List<Message> inMsg = new List<Message>(); 
      List<Message> actual;
      Message msg;
      target.mXML.Acceleration = 2;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 1 + 1);

      SimulationXML.Instance.Vitesse = 20;

      Assert.IsTrue(target.getRess() == Ressource.RID.R03);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      //Ne genera pas de message
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0 + 1);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      Assert.IsTrue(R03CompteurDeVitesse.Instance.vitesse == 0);

      msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P12, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      // On test le processus quand tout les message sont a -99
      Assert.IsTrue(target.getRess() == Ressource.RID.R10);
      Assert.IsTrue(target.getMsg().Count == 3);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 3); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].Blocking == false);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P03);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P05);
      Assert.IsTrue(actual[0].DelaiTransmission == 1);
      Assert.IsTrue(target.getTempsRestant() == 0);

      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 1 + 1);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 0 + 1);
      msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P12, Message.ID.P03, 1);
      msg.mMsgPrincipal = 10;
      inMsg.Add(msg);
      
      // Should not been taking in account
      R03CompteurDeVitesse.Instance.vitesse = 20;
      // Should Exec 6 times 1+(10-1) / 2
      
      for(int i=0;i<6;i++)
      {
        Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
        actual = target.run(inMsg);
        Assert.IsTrue(target.getTempsRestant() == 0 + (6 -(i+1)));
        inMsg.Clear();
      }

      Assert.IsTrue(target.getTempsRestant() == 0);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(R10RegulateurVitesse.Instance.vitesse == 10);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].mMsgPrincipal == 10);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P03);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P05);

      // Send that the car is broken it shall slow down up to 1.0
      target.reinitEtat();
      actual = target.run(inMsg);
      msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);
      msg.mMsgPrincipal = 1;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P12, Message.ID.P03, 1);
      msg.mMsgPrincipal = 10;
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 3); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(R10RegulateurVitesse.Instance.vitesse == 1.0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].mMsgPrincipal == 9);

      // Send that the car is not broken anymore it shall go back to road limit
      target.reinitEtat();
      actual = target.run(inMsg);
      msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P12, Message.ID.P03, 1);
      msg.mMsgPrincipal = 10;
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 3); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(R10RegulateurVitesse.Instance.vitesse == 10.0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].mMsgPrincipal == 9);

      // Send speed of the car ahead
      target.reinitEtat();
      actual = target.run(inMsg);
      msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
      msg.mMsgPrincipal = 5;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P12, Message.ID.P03, 1);
      msg.mMsgPrincipal = 10;
      inMsg.Add(msg);

      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 3); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(R10RegulateurVitesse.Instance.vitesse == 5.0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].mMsgPrincipal == 5);

      // Send speed of the car ahead
      target.reinitEtat();
      actual = target.run(inMsg);
      msg = new Message(false, Message.ID.P01, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
      msg.mMsgPrincipal = -99;
      inMsg.Add(msg);
      msg = new Message(false, Message.ID.P12, Message.ID.P03, 1);
      msg.mMsgPrincipal = 10;
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 3); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(R10RegulateurVitesse.Instance.vitesse == 10.0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].mMsgPrincipal == 5);

      Assert.IsTrue((P03ChangementDeVitesse.sequenceExec)target.etapeExecution == P03ChangementDeVitesse.sequenceExec.END);
    }
  }
}
