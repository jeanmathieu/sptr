﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P13SystemesElectriquesTest and is intended
    ///to contain all P13SystemesElectriquesTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P13SystemesElectriquesTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P13SystemesElectriques Constructor
        ///</summary>
        [TestMethod()]
        public void P13SystemesElectriquesConstructorTest()
        {
            P13SystemesElectriques target = new P13SystemesElectriques();

      Assert.IsTrue(target.periode == 0);
      Assert.IsTrue(target.contrainteFin == 7);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 1);
      Assert.IsTrue(target.DeclencheurProcessus[0] == Message.ID.P10);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
        }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P13SystemesElectriques target = new P13SystemesElectriques(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>();
      List<Message> actual;
      List<Message.ID> peekMsg;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 4);

      Assert.IsTrue(target.getRess() == Ressource.RID.R05);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 1);
      Assert.IsTrue(peekMsg[0] == Message.ID.P10);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 3);

      Assert.IsTrue(target.getRess() == Ressource.RID.R11);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 2);

      Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1);

      Assert.IsTrue(target.getRess() == Ressource.RID.R04);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0);

      Assert.IsTrue((P13SystemesElectriques.sequenceExec)target.etapeExecution == P13SystemesElectriques.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
