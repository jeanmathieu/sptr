﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P12SuiviDuCheminTest and is intended
    ///to contain all P12SuiviDuCheminTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P12SuiviDuCheminTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P12SuiviDuChemin Constructor
        ///</summary>
        [TestMethod()]
        public void P12SuiviDuCheminConstructorTest()
        {
            P12SuiviDuChemin target = new P12SuiviDuChemin();

      Assert.IsTrue(target.periode != 0);
      Assert.IsTrue(target.periode == target.contrainteFin);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
        }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P12SuiviDuChemin target = new P12SuiviDuChemin(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>(); // TODO: Initialize to an appropriate value
      List<Message> actual;
      List<Message.ID> peekMsg;

      SimulationXML.Instance.Echelle = 2;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 4);
      //#1, Le 1er appel demande R05 et n'a pas de message
      // Et dit que l'auto est brisé pour envoyer un message de ralentir
      Assert.IsTrue(target.getRess() == Ressource.RID.R03);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 1);
      Assert.IsTrue(peekMsg[0] == Message.ID.P02);

      Message msg = new Message(false, Message.ID.P02, Message.ID.P12, 1);
      msg.mMsgPrincipal = 32;
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 3);
      Assert.IsTrue(inMsg.Count == 1); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(actual.Count == 2);
//      Assert.IsTrue(actual[0].mArgDouble1 == 10);
      Assert.IsTrue(actual[0].DelaiTransmission == 1);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P12);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P11);
//      Assert.IsTrue(actual[1].mArgDouble1 == 10);
      Assert.IsTrue(actual[1].DelaiTransmission == 1);
      Assert.IsTrue(actual[1].SourceID == Message.ID.P12);
      Assert.IsTrue(actual[1].DestinationID == Message.ID.P03);

      inMsg = null;
      Assert.IsTrue(target.getRess() == Ressource.RID.R01);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 2);

      Assert.IsTrue(target.getRess() == Ressource.RID.R04);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 1);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].DelaiTransmission == 3);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P12);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P10);

      Assert.IsTrue(target.getRess() == Ressource.RID.R06);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].DelaiTransmission == 1);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P12);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P06);

      Assert.IsTrue((P12SuiviDuChemin.SequenceID)target.etapeExecution == P12SuiviDuChemin.SequenceID.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
