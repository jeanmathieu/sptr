﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P01AutoVerificationTest and is intended
    ///to contain all P01AutoVerificationTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P01AutoVerificationTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P01AutoVerification Constructor
        ///</summary>
        [TestMethod()]
        public void P01AutoVerificationConstructorTest()
    {
      P01AutoVerification target = new P01AutoVerification();

      Assert.IsTrue(target.periode != 0);
      Assert.IsTrue(target.periode == target.contrainteFin);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
        }

    /// <summary>
    ///A test for execute
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P01AutoVerification target = new P01AutoVerification(); // TODO: Initialize to an appropriate value

      List<Message> inMsg = null; // TODO: Initialize to an appropriate value
      List<Message> actual;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 4);

      //#1, Le 1er appel demande R05 et n'a pas de message
      // Et dit que l'auto est brisé pour envoyer un message de ralentir
      R05Environnement.Instance.EtatVehicule = R05Environnement.EtatVehiculeEnum.BRISE;
      Assert.IsTrue(target.getRess() == Ressource.RID.R05);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      //Ne genera pas de message
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 3);

      //#2, Le 2e appel demande E et n'a pas de message
      //Cette valeur ne doit pas etre tenu en compte avant la prochaine passe
      R05Environnement.Instance.EtatVehicule = R05Environnement.EtatVehiculeEnum.FONCTIONNEL;
      Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      //Genere un message jusqu'a P3
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(target.getTempsRestant() == 2);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P01);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P03);
      Assert.IsTrue(actual[0].Blocking == false);
      // Nouvelle vitesse doit-etre 1 u.s./u.t.
      Assert.IsTrue(actual[0].mMsgPrincipal == 1);

      //#3, Le 3e appel demande R08 et n'a pas de message
      Assert.IsTrue(target.getRess() == Ressource.RID.R08);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      //Ne genera pas de message
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1);

      //#3, Le 3e appel demande E et n'a pas de message
      Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      //Ne genera pas de message
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0);


      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 4);
      
      // Refait une passe avec l'auto non-brise, la resource a ete changé plus haut
      actual = target.run(inMsg);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P01);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P03);
      Assert.IsTrue(actual[0].Blocking == false);
      // Nouvelle vitesse doit-etre 1 u.s./u.t.
      Assert.IsTrue(actual[0].mMsgPrincipal == -99); actual = target.run(inMsg);
      actual = target.run(inMsg);

      Assert.IsTrue((P01AutoVerification.sequenceExec)target.etapeExecution == P01AutoVerification.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
