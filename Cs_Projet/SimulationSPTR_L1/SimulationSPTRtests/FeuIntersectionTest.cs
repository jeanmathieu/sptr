﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for FeuIntersectionTest and is intended
    ///to contain all FeuIntersectionTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FeuIntersectionTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


    /// <summary>
    ///A test for init
    ///</summary>
    [TestMethod()]
    public void FeuIntersection_initTest()
    {
      SimulationXML.FeuStructure feu;
      FeuIntersection target = new FeuIntersection(); // TODO: Initialize to an appropriate value

      feu = new SimulationXML.FeuStructure();
      feu.CoordonneeX = 5;
      feu.CoordonneeY = 7;
      feu.Duree = 10;
      feu.Position = "N";
      SimulationXML.Instance.Feu.Add(feu);
      feu = new SimulationXML.FeuStructure();
      feu.CoordonneeX = 15;
      feu.CoordonneeY = 17;
      feu.Duree = 10;
      feu.Position = "N";
      SimulationXML.Instance.Feu.Add(feu);
      feu = new SimulationXML.FeuStructure();
      feu.CoordonneeX = 5;
      feu.CoordonneeY = 7;
      feu.Duree = 20;
      feu.Position = "E";
      SimulationXML.Instance.Feu.Add(feu);

      target.FeuPosition = new Point(5,7);
      target.init();

      Assert.IsTrue(target.directionVertJaune == R07GPS.directionEnum.OUEST);
      Assert.IsTrue(target.currentFeu == 0);
      Assert.IsTrue(target.EtatFeuVertJaune == FeuIntersection.EtatFeu.VERT);
      Assert.IsTrue(target.currentQC == 0);

      Assert.IsTrue(target.listFeu.Count == 2);
      Assert.IsTrue(target.listFeu[0].direction == R07GPS.directionEnum.OUEST);
      Assert.IsTrue(target.listFeu[0].QC == 20);
      Assert.IsTrue(target.listFeu[1].direction == R07GPS.directionEnum.SUD);
      Assert.IsTrue(target.listFeu[1].QC == 10);
    }

    /// <summary>
    ///A test for verifieProchainEtat
    ///</summary>
    [TestMethod()]
    public void verifieProchainEtatTest()
    {
      FeuIntersection target = new FeuIntersection(); // TODO: Initialize to an appropriate value
      SimulationXML.FeuStructure feu;

      SimulationXML.Instance.Feu.Clear();

      feu = new SimulationXML.FeuStructure();
      feu.CoordonneeX = 5;
      feu.CoordonneeY = 7;
      feu.Duree = 10;
      feu.Position = "N";
      SimulationXML.Instance.Feu.Add(feu);
      feu = new SimulationXML.FeuStructure();
      feu.CoordonneeX = 15;
      feu.CoordonneeY = 17;
      feu.Duree = 10;
      feu.Position = "N";
      SimulationXML.Instance.Feu.Add(feu);
      feu = new SimulationXML.FeuStructure();
      feu.CoordonneeX = 5;
      feu.CoordonneeY = 7;
      feu.Duree = 20;
      feu.Position = "E"; 
      SimulationXML.Instance.Feu.Add(feu);
      SimulationXML.Instance.FeuJaune = 5;

      target.FeuPosition.X = 5;
      target.FeuPosition.Y = 7;
      target.init();

      for (int i = 0; i < 15; i++)
      {
        target.verifieProchainEtat();
        Assert.IsTrue(target.directionVertJaune == R07GPS.directionEnum.OUEST);
        Assert.IsTrue(target.EtatFeuVertJaune == FeuIntersection.EtatFeu.VERT);
      }
      for (int i = 0; i < 5; i++)
      {
        target.verifieProchainEtat();
        Assert.IsTrue(target.directionVertJaune == R07GPS.directionEnum.OUEST);
        Assert.IsTrue(target.EtatFeuVertJaune == FeuIntersection.EtatFeu.JAUNE);
      }
      for (int i = 0; i < 5; i++)
      {
        target.verifieProchainEtat();
        Assert.IsTrue(target.directionVertJaune == R07GPS.directionEnum.SUD);
        Assert.IsTrue(target.EtatFeuVertJaune == FeuIntersection.EtatFeu.VERT);
      }
      for (int i = 0; i < 5; i++)
      {
        target.verifieProchainEtat();
        Assert.IsTrue(target.directionVertJaune == R07GPS.directionEnum.SUD);
        Assert.IsTrue(target.EtatFeuVertJaune == FeuIntersection.EtatFeu.JAUNE);
      }
      for (int i = 0; i < 15; i++)
      {
        target.verifieProchainEtat();
        Assert.IsTrue(target.directionVertJaune == R07GPS.directionEnum.OUEST);
        Assert.IsTrue(target.EtatFeuVertJaune == FeuIntersection.EtatFeu.VERT);
      }

    }
  }
}
