﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P05ControleDesEmissionsTest and is intended
    ///to contain all P05ControleDesEmissionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P05ControleDesEmissionsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P05ControleDesEmissions Constructor
        ///</summary>
        [TestMethod()]
        public void P05ControleDesEmissionsConstructorTest()
        {
            P05ControleDesEmissions target = new P05ControleDesEmissions();
      Assert.IsTrue(target.periode == 0);
      Assert.IsTrue(target.contrainteFin == 5);
      //Assert.IsTrue(target.DeclencheurProcessus.Count == 1);
      //Assert.IsTrue(target.DeclencheurProcessus[0] == Message.ID.P03);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
        }

        /// <summary>
        ///A test for getEtat
        ///</summary>
        [TestMethod()]
        public void getEtatTest()
        {
            P05ControleDesEmissions target = new P05ControleDesEmissions(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>(); // TODO: Initialize to an appropriate value
      List<Message> actual;
      target.mXML.Consommation = 15;
      Assert.IsTrue(target.getTempsRestant() == 0);

      // Envoi un message de dépense de ua, regarde si le compteur d'émission fonctionne
      Message msg = new Message(false, Message.ID.P03, Message.ID.P05, 1);
      msg.mMsgPrincipal = 10;
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 1); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.END);

      actual = target.run(inMsg);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);

      // Envoi un message de dépense de ua, regarde si un message est envoyé de surconsommation
      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 1);
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 1); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(target.getTempsRestant() == 0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P05);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P02);

      // Envoi un message de dépense de ua, regarde si aucun message est envoyé de surconsommation
      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 1);
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 1); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(target.getTempsRestant() == 0);

      // Envoi un message de dépense de ua, regarde si un message est envoyé de surconsommation
      target.reinitEtat();
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(inMsg.Count == 1); // Message should not be clear by processus
      inMsg.Clear();
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P05);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P02);

      Assert.IsTrue((P05ControleDesEmissions.sequenceExec)target.etapeExecution == P05ControleDesEmissions.sequenceExec.END);
    }
  }
}
