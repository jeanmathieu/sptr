﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P08GestionDesAppelsTest and is intended
    ///to contain all P08GestionDesAppelsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P08GestionDesAppelsTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P08GestionDesAppels Constructor
        ///</summary>
        [TestMethod()]
        public void P08GestionDesAppelsConstructorTest()
        {
            P08GestionDesAppels target = new P08GestionDesAppels();

      Assert.IsTrue(target.contrainteFin == 100);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 0);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
        }

        /// <summary>
        ///A test for getMsg
        ///</summary>
        [TestMethod()]
    public void executeTest()
    {
      P08GestionDesAppels target = new P08GestionDesAppels(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>();
      List<Message> actual;
      List<Message.ID> peekMsg;
      Assert.IsTrue(target.getTempsRestant() == 0);

      R03CompteurDeVitesse.Instance.vitesse = 4;
      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 2 + 1);

      Assert.IsTrue(target.getRess() == Ressource.RID.R03);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1 + 1);

      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 1);
      Assert.IsTrue(peekMsg[0] == Message.ID.P11);

      Message msg = new Message(false, Message.ID.P11, Message.ID.P08, 1);
      inMsg.Add(msg);

      for(int i=0;i<33;i++)
      {
        Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
        actual = target.run(inMsg);
        inMsg.Clear();
        Assert.IsTrue(actual.Count == 0);
        Assert.IsTrue(target.getTempsRestant() == 1 + (33 -(i+1)));
        peekMsg = target.getMsg();
        Assert.IsTrue(peekMsg.Count == 0);
      }

      Assert.IsTrue(target.getRess() == Ressource.RID.R12);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0);

      Assert.IsTrue((P08GestionDesAppels.sequenceExec)target.etapeExecution == P08GestionDesAppels.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
