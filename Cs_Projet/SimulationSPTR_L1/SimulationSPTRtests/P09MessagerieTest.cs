﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace SimulationSPTRtests
{
    
    
    /// <summary>
    ///This is a test class for P09MessagerieTest and is intended
    ///to contain all P09MessagerieTest Unit Tests
    ///</summary>
    [TestClass()]
    public class P09MessagerieTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for P09Messagerie Constructor
        ///</summary>
        [TestMethod()]
        public void P09MessagerieConstructorTest()
        {
            P09Messagerie target = new P09Messagerie();

      Assert.IsTrue(target.contrainteFin == 30);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 0);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P09Messagerie target = new P09Messagerie(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>();
      List<Message> actual;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 2);

      Assert.IsTrue(target.getRess() == Ressource.RID.R12);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1);

      Assert.IsTrue(target.getRess() == Ressource.RID.R04);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0);

      Assert.IsTrue((P09Messagerie.sequenceExec)target.etapeExecution == P09Messagerie.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
