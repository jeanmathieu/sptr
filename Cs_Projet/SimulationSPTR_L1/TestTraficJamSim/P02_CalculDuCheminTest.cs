﻿//using Simulation.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace TestTraficJamSim
{
    
    
    /// <summary>
    ///This is a test class for P02_CalculDuCheminTest and is intended
    ///to contain all P02_CalculDuCheminTest Unit Tests
    ///</summary>
  [TestClass()]
  public class P02_CalculDuCheminTest
  {


    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[ClassInitialize()]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion


    /// <summary>
    ///A test for P02_CalculDuChemin Constructor
    ///</summary>
    [TestMethod()]
    public void P02_CalculDuCheminConstructorTest()
    {
      P02CalculDuChemin target = new P02CalculDuChemin();

      Assert.IsTrue(target.periode == 30);
      Assert.IsTrue(target.contrainteFin == target.periode);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 0);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P02CalculDuChemin target = new P02CalculDuChemin(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>(); // TODO: Initialize to an appropriate value
      List<Message> actual;
      List<Message.ID> peekMsg;

      SimulationXML.Instance.lireRoute("..\\..\\..\\TraficJamSim\\bin\\sptr-scenario.xml");
      R02CarteRoutiere.Instance.calculChemin.chargerRoutesXML();

      SimulationXML.Instance.Echelle = 2;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 3+4);

      //#1, Le 1er appel demande R05 et n'a pas de message
      // Et dit que l'auto est brisé pour envoyer un message de ralentir
      Assert.IsTrue(target.getRess() == Ressource.RID.R02);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 1);
      Assert.IsTrue(peekMsg[0] == Message.ID.P05);
      Message msg = new Message(false, Message.ID.P05, Message.ID.P02, 1);
      msg.mMsgPrincipal = 1;
      inMsg.Add(msg);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 2+4);
      Assert.IsTrue(inMsg.Count == 1); // Message should not be clear by processus
      inMsg.Clear();

      Assert.IsTrue(target.getRess() == Ressource.RID.R07);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1+4);

      Assert.IsTrue(target.getRess() == Ressource.RID.R05);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0+4);

      for (int i=0; i < 4; i++)
      {
        Assert.IsTrue(actual.Count == 0);
        Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
        Assert.IsTrue(target.getMsg().Count == 0);
        actual = target.run(inMsg);
        Assert.IsTrue(target.getTempsRestant() == 4 - (i + 1));
      }

      Assert.IsTrue(actual.Count == 2);
      Assert.IsTrue(target.getTempsRestant() == 0);
      //      Assert.IsTrue(actual[0].mArgDouble1 == 10);
      Assert.IsTrue(actual[0].DelaiTransmission == 1);
      Assert.IsTrue(actual[0].Blocking == true);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P02);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P07);
      //      Assert.IsTrue(actual[1].mArgDouble1 == 10);
      Assert.IsTrue(actual[1].DelaiTransmission == 1);
      Assert.IsTrue(actual[1].Blocking == false);
      Assert.IsTrue(actual[1].SourceID == Message.ID.P02);
      Assert.IsTrue(actual[1].DestinationID == Message.ID.P12);

      Assert.IsTrue((P02CalculDuChemin.sequenceExec)target.etapeExecution == P02CalculDuChemin.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
