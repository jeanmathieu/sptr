﻿//using Simulation.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestTraficJamSim
{
    
    
    /// <summary>
    ///This is a test class for R05_EnvironnementTest and is intended
    ///to contain all R05_EnvironnementTest Unit Tests
    ///</summary>
  [TestClass()]
  public class R05_EnvironnementTest
  {


    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[ClassInitialize()]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion


    /// <summary>
    ///A test for gestionEtatVehicule
    ///</summary>
    [TestMethod()]
    [DeploymentItem("interfaceGraphique.exe")]
    public void gestionEtatVehiculeTest()
    {
      R05_Environnement_Accessor target = R05_Environnement_Accessor.Instance; // TODO: Initialize to an appropriate value
      SimulationXML.Instance.Bris.Phase = 20;
      SimulationXML.Instance.Bris.Periode = 40;
      SimulationXML.Instance.AutoReparation = 5;

      target.init();

      for (int i = 0; i < SimulationXML.Instance.Bris.Phase; i++)
      {
        target.executeEachUT();
        Assert.IsTrue(target.mEtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.FONCTIONNEL.value__);
      }
      target.executeEachUT();
      Assert.IsTrue(target.EtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.BRISE.value__);

      R03CompteurDeVitesse.Instance.vitesse = 1;

      int idx = 0;
      for (; idx < 5; idx++)
      {
        target.executeEachUT();
        Assert.IsTrue(target.EtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.BRISE.value__);
      }
      target.EtatVehicule = R05_Environnement_Accessor.EtatVehiculeEnum.FONCTIONNEL;
      for (; idx < SimulationXML.Instance.Bris.Periode - 1; idx++)
      {
        target.executeEachUT();
        Assert.IsTrue(target.EtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.FONCTIONNEL.value__);
      }
      target.executeEachUT();
      Assert.IsTrue(target.EtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.BRISE.value__);

      target.EtatVehicule = R05_Environnement_Accessor.EtatVehiculeEnum.FONCTIONNEL;
      for (idx = 0; idx < SimulationXML.Instance.Bris.Periode - 1; idx++)
      {
        target.executeEachUT();
        Assert.IsTrue(target.EtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.FONCTIONNEL.value__);
      }
      target.executeEachUT();
      Assert.IsTrue(target.EtatVehicule.value__ == R05_Environnement_Accessor.EtatVehiculeEnum.BRISE.value__);
    }

    /// <summary>
    ///A test for gestionTemperature
    ///</summary>
    [TestMethod()]
    [DeploymentItem("interfaceGraphique.exe")]
    public void gestionTemperatureTest()
    {
      SimulationXML.Instance.Bris.Periode = 40;
      SimulationXML.Instance.Bris.Phase = 20;
      SimulationXML.Instance.TempsSimulation = 20;
      SimulationXML.Instance.Temperature = 20;
      SimulationXML.Instance.TemperatureMax = 25;
      SimulationXML.Instance.TemperatureMin = 10;

      R05_Environnement_Accessor target = new R05_Environnement_Accessor(); // TODO: Initialize to an appropriate value
      
      target.init();
      target.gestionTemperature();
      target.nbUT++;
      Assert.IsTrue(target.temperatureExterieur == 20);
      Assert.IsTrue(target.TF >= 1 && target.TF <= SimulationXML.Instance.TempsSimulation);

      for (int i = 0; i < target.TF-1; i++)
      {
        target.gestionTemperature();
        target.nbUT++;
        Assert.IsTrue(target.temperatureExterieur == 20);
      }

      target.gestionTemperature();
      target.nbUT++;
      Assert.IsTrue(target.temperatureExterieur == 25);
      Assert.IsTrue(target.TF >= 1 && target.TF <= SimulationXML.Instance.TempsSimulation);
      
      for (int i = 0; i < target.TF-1; i++)
      {
        target.gestionTemperature();
        target.nbUT++;
        Assert.IsTrue(target.temperatureExterieur == 25);
      }

      target.gestionTemperature();
      target.nbUT++;
      Assert.IsTrue(target.TF >= 1 && target.TF <= SimulationXML.Instance.TempsSimulation);
      Assert.IsTrue(target.temperatureExterieur == 20); 

      for (int i = 0; i < target.TF-1; i++)
      {
        target.gestionTemperature();
        target.nbUT++;
        Assert.IsTrue(target.temperatureExterieur == 20);
      }

      target.gestionTemperature();
      target.nbUT++;
      Assert.IsTrue(target.TF >= 1 && target.TF <= SimulationXML.Instance.TempsSimulation);
      Assert.IsTrue(target.temperatureExterieur == 15);

      for (int i = 0; i < target.TF-1; i++)
      {
        target.gestionTemperature();
        target.nbUT++;
        Assert.IsTrue(target.temperatureExterieur == 15);
      }

      target.gestionTemperature();
      target.nbUT++;
      Assert.IsTrue(target.TF >= 1 && target.TF <= SimulationXML.Instance.TempsSimulation);
      Assert.IsTrue(target.temperatureExterieur == 10);

      for (int i = 0; i < target.TF-1; i++)
      {
        target.gestionTemperature();
        target.nbUT++;
        Assert.IsTrue(target.temperatureExterieur == 10);
      }

      target.gestionTemperature();
      target.nbUT++;
      Assert.IsTrue(target.TF >= 1 && target.TF <= SimulationXML.Instance.TempsSimulation);
      Assert.IsTrue(target.temperatureExterieur == 15);

      for (int i = 0; i < target.TF-1; i++)
      {
        target.gestionTemperature();
        target.nbUT++;
        Assert.IsTrue(target.temperatureExterieur == 15);
      }

    }
  }
}
