﻿//using Simulation.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using System.Collections.Generic;

namespace TestTraficJamSim
{


  /// <summary>
  ///This is a test class for SchedulerTest and is intended
  ///to contain all SchedulerTest Unit Tests
  ///</summary>
  [TestClass()]
  public class SchedulerTest
  {


    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[ClassInitialize()]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion


    /// <summary>
    ///A test for Ordonnanceur Constructor
    ///</summary>
    [TestMethod()]
    public void SchedulerConstructorTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      Assert.IsNotNull(target.RessourceLockList);
      Assert.IsNotNull(target.ProcessusByUT);
      Assert.IsNotNull(target.ProcsStatus);
      Assert.IsNull(target.LinkedProcessor);
    }

    /// <summary>
    ///A test for Ordonnanceur Constructor and make it run for a while => message and etc...
    ///</summary>
    [TestMethod()]
    public void SchedulerConstructorTest1()
    {
      Processeur pAssProcessor = new Processeur();
      pAssProcessor.AddProcessus(new P01AutoVerification());
      pAssProcessor.AddProcessus(new P02CalculDuChemin());
      pAssProcessor.AddProcessus(new P03ChangementDeVitesse());
      pAssProcessor.AddProcessus(new P04Climat());
      pAssProcessor.AddProcessus(new P05ControleDesEmissions());
      pAssProcessor.AddProcessus(new P06EnvoiDeBilan());
      pAssProcessor.AddProcessus(new P07EvitementDeCollision());
      pAssProcessor.AddProcessus(new P08GestionDesAppels());
      pAssProcessor.AddProcessus(new P09Messagerie());
      pAssProcessor.AddProcessus(new P10SecuriteA());
      pAssProcessor.AddProcessus(new P11SecuriteB());
      pAssProcessor.AddProcessus(new P12SuiviDuChemin());
      pAssProcessor.AddProcessus(new P13SystemesElectriques());
      //
      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      for (int i = 0; i < 10; ++i)
      {
        target.doOrdonnencement(i);
        target.RunProcessusUT(i);
        target.EndUT(i);
      }
      Ordonnanceur.InitRessourcePartagees();
      Console.WriteLine("TODO: Test more thorougly the scheduler");
    }

    /// <summary>
    ///A test for AddProcessus
    ///</summary>
    [TestMethod()]
    public void AddProcessusTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      Processus pProc = new P01AutoVerification();
      target.AddProcessus(pProc, 0);
      Assert.IsTrue(target.ProcsStatus.Count == 1);
      Ordonnanceur.InitRessourcePartagees();

    }

    /// <summary>
    ///A test for EndUT
    ///</summary>
    [TestMethod()]
    public void EndUTTest()
    {
      //Test Random start of process
      Processeur pAssProcessor = new Processeur();
      List<Processeur> lP = new List<Processeur>();
      lP.Add(pAssProcessor);
      pAssProcessor.AddProcessus(new P13SystemesElectriques());
      pAssProcessor.AddProcessus(new P08GestionDesAppels());
      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      bool RandomStart = false;
      for (int pCurrentUT = 0; pCurrentUT < 1000; ++pCurrentUT)
      {
        target.doOrdonnencement(pCurrentUT);
        MetaOrdonnanceur.GereConflits(lP);
        target.RunProcessusUT(pCurrentUT);
        target.EndUT(pCurrentUT);
        //Test if P13 or P08 were started by Ordonnanceur
        target.ProcsStatus.ForEach(x => RandomStart = RandomStart || x.ProcessusState == EtatProcessus.state.Prêt);
      }
      Assert.IsTrue(RandomStart);

      //Clear up ressources
      Ordonnanceur.InitRessourcePartagees();
      target.RessourceLockList = new List<EtatRessource>();
      foreach (Ressource.RID aID in Enum.GetValues(typeof(Ressource.RID)))
      {
        if (aID != Ressource.RID.INVALID)
          target.RessourceLockList.Add(new EtatRessource(aID));
      }
      //TODO confirmed realeasing of ressource
    }

    /// <summary>
    ///A test for RunProcessusUT
    ///</summary>
    [TestMethod()]
    public void ExecuteProcessTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      target.RunProcessusUT(0);
      //Test a processus who has to send on a different processor a message and see if he's actually executing this
      //TODO verfy if that other process actually received this message
      Processeur pAssProcessor1 = new Processeur();
      Processeur pAssProcessor2 = new Processeur();
      Processeur pAssProcessor3 = new Processeur();
      Processeur pAssProcessor4 = new Processeur();
      List<Processeur> lP = new List<Processeur>();
      lP.Add(pAssProcessor1);
      lP.Add(pAssProcessor2);
      lP.Add(pAssProcessor3);
      lP.Add(pAssProcessor4);
      int mUT = 0;
      pAssProcessor1.AddProcessus(new P01AutoVerification());
      pAssProcessor2.AddProcessus(new P03ChangementDeVitesse());
      pAssProcessor3.AddProcessus(new P02CalculDuChemin());
      pAssProcessor4.AddProcessus(new P07EvitementDeCollision());
      Ordonnanceur target1 = new Ordonnanceur(ref pAssProcessor1, 0);
      Ordonnanceur target2 = new Ordonnanceur(ref pAssProcessor2, 0);
      Ordonnanceur target3 = new Ordonnanceur(ref pAssProcessor3, 0);
      Ordonnanceur target4 = new Ordonnanceur(ref pAssProcessor4, 0);
      Reseau aNet = new Reseau(lP);
      target1.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      target2.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      target3.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      target4.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;

      //TODO verify in debug that emssage all goes into the right place :)
      while(mUT < 10)
      {
        foreach (Processeur aP in lP)
        {
          aP.Ordonnenceur.doOrdonnencement(mUT);
        }
        MetaOrdonnanceur.GereConflits(lP);
        foreach (Processeur aP in lP)
        {
          aP.Ordonnenceur.RunProcessusUT(mUT);
          aNet.EnvoieMessage(aP.Ordonnenceur.CurExecutingOutBuffer, aP.Ordonnenceur.OutBufferMsg, mUT);
          aP.Ordonnenceur.EndUT(mUT);
        }
        ++mUT;
      }
      Ordonnanceur.InitRessourcePartagees();
    }


    /// <summary>
    ///A test for doRoundRobin
    ///</summary>
    [TestMethod()]
    public void OrderRoundRobinTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      target.AddProcessus(new P01AutoVerification(), 0);
      target.AddProcessus(new P02CalculDuChemin(), 0);
      Assert.IsNull(target.ProcessusToExecute);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      target.doOrdonnencement(0);
      Assert.IsNotNull(target.ProcessusToExecute);
      target.RunProcessusUT(0);
      target.EndUT(0);
      Ordonnanceur.InitRessourcePartagees();
    }

    /// <summary>
    ///A test for ReceptionMessage
    ///</summary>
    [TestMethod()]
    public void ReceiveMessageTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      target.AddProcessus(new P02CalculDuChemin(), 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      //NUll lMessage work?
      Message pMsg = null;
      target.ReceptionMessage(pMsg, 0);
      //Try to unblock the processus with a message shouldnt work
      target.doOrdonnencement(0);
      target.RunProcessusUT(0);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.BloquéRessource);
      target.EndUT(0);
      //Send message but do not lock ressource
      pMsg = new Message(false, Message.ID.P05, Message.ID.P02, 0);
      target.ReceptionMessage(pMsg, 1);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.BloquéRessource);
      target.doOrdonnencement(1);
      target.RunProcessusUT(1);
      //verrouiller ressource, now the process should be unblock
      target.RessourceLockList.Single(x => x.getID == Ressource.RID.R02).verrouiller(target.ProcsStatus.First().Processus);
      target.EndUT(1);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.Prêt);
      target.RessourceLockList.Single(x => x.getID == Ressource.RID.R02).deVerrouiller(target.ProcsStatus.First().Processus);

      //Test unblock upon receiving a message
      Processeur P = new Processeur();
      List<Processeur> lP = new List<Processeur>();
      lP.Add(P);
      P.AddProcessus(new P02CalculDuChemin());
      P.AddProcessus(new P07EvitementDeCollision());
      target = new Ordonnanceur(ref P, 0);
      Reseau aNet = new Reseau(lP);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      //Try to unblock the processus with a message shouldnt work
      target.doOrdonnencement(0);
      target.RunProcessusUT(0);
      aNet.EnvoieMessage(target.CurExecutingOutBuffer, target.OutBufferMsg, 0);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.BloquéRessource);
      target.EndUT(0);
      //lock ressource & then Send message
      //verrouiller ressource, now the process should be unblock
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.BloquéRessource);
      target.RessourceLockList.Single(x => x.getID == Ressource.RID.R02).verrouiller(target.ProcsStatus.First().Processus);
      pMsg = new Message(false, Message.ID.P05, Message.ID.P02, 0);
      target.ReceptionMessage(pMsg, 1);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.Prêt);
      target.doOrdonnencement(1);
      target.RunProcessusUT(1);
      aNet.EnvoieMessage(target.CurExecutingOutBuffer, target.OutBufferMsg, 1);
      target.EndUT(1);
      //Test blocking lMessage
      for(int i = 2; i<10; ++i)
      {
        target.doOrdonnencement(i);
        MetaOrdonnanceur.GereConflits(lP);
        target.RunProcessusUT(i);
        target.EndUT(i);
      }
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P02).ProcessusState == EtatProcessus.state.BloquéRessource);
      Message aM = new Message(true, Message.ID.P02, Message.ID.P07, 0);
      List<Message>  aTemp = new List<Message>();
      aTemp.Add(aM);
      aNet.EnvoieMessage(aTemp, aTemp, 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P02).ProcessusState == EtatProcessus.state.Prêt);
      target.doOrdonnencement(10);
      MetaOrdonnanceur.GereConflits(lP);
      aNet.EnvoieMessage(target.CurExecutingOutBuffer, target.OutBufferMsg, 10);
      target.RunProcessusUT(10);
      target.EndUT(10);



      //Test start of processus by message
      target = new Ordonnanceur();
      aM = new Message(false, Message.ID.P07, Message.ID.P03, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      target.AddProcessus(new P03ChangementDeVitesse(), 0);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.Endormi);
      target.ReceptionMessage(aM, 0);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.Prêt);
      //Try to unblock the processus with a message shouldnt work
      target.doOrdonnencement(0);
      target.RunProcessusUT(0);
      Assert.IsTrue(target.ProcsStatus.First().ProcessusState == EtatProcessus.state.BloquéRessource);
      target.EndUT(0);

      Ordonnanceur.InitRessourcePartagees();
    }

    /// <summary>
    ///A test for LinkedProcessor
    ///</summary>
    [TestMethod()]
    public void LinkedProcessorTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      Processeur expected = new Processeur();
      Processeur actual;
      target.LinkedProcessor = expected;
      actual = target.LinkedProcessor;
      Assert.AreEqual(expected, actual);
    }

    /// <summary>
    ///A test for ProcessusByUT
    ///</summary>
    [TestMethod()]
    public void ProcessusByUTTest()
    {
      Processeur pAssProcessor = new Processeur();
      pAssProcessor.AddProcessus(new P01AutoVerification());
      pAssProcessor.AddProcessus(new P02CalculDuChemin());
      pAssProcessor.AddProcessus(new P03ChangementDeVitesse());
      pAssProcessor.AddProcessus(new P04Climat());
      pAssProcessor.AddProcessus(new P05ControleDesEmissions());
      pAssProcessor.AddProcessus(new P06EnvoiDeBilan());
      pAssProcessor.AddProcessus(new P07EvitementDeCollision());
      pAssProcessor.AddProcessus(new P08GestionDesAppels());
      pAssProcessor.AddProcessus(new P09Messagerie());
      pAssProcessor.AddProcessus(new P10SecuriteA());
      pAssProcessor.AddProcessus(new P11SecuriteB());
      pAssProcessor.AddProcessus(new P12SuiviDuChemin());
      pAssProcessor.AddProcessus(new P13SystemesElectriques());

      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
      for (int i = 0; i < 10; ++i)
      {
        target.doOrdonnencement(i);
        target.RunProcessusUT(i);
        target.EndUT(i);
      }
      Assert.IsTrue(target.ProcessusByUT.Count == 10);
      //TODO test etat of some process)

      Ordonnanceur.InitRessourcePartagees();
    }

    /// <summary>
    ///A test for ProcessusToExecute
    ///</summary>
    [TestMethod()]
    public void ProcessusToExecuteTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      EtatProcessus expected = new EtatProcessus();
      EtatProcessus actual;
      target.ProcessusToExecute = expected;
      actual = target.ProcessusToExecute;
      Assert.AreEqual(expected, actual);

    }

    /// <summary>
    ///A test for ProcsStatus
    ///</summary>
    [TestMethod()]
    public void ProcsStatusTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      List<EtatProcessus> expected = new List<EtatProcessus>();
      List<EtatProcessus> actual;
      target.ProcsStatus = expected;
      actual = target.ProcsStatus;
      Assert.AreEqual(expected, actual);
    }

    /// <summary>
    ///A test for RessourceLockList
    ///</summary>
    [TestMethod()]
    public void RessourceLockListTest()
    {
      Ordonnanceur target = new Ordonnanceur();
      List<EtatRessource> actual;
      actual = target.RessourceLockList;
      Assert.IsNotNull(actual);
    }


    /// <summary>
    ///A test for doEarliestDeadline
    ///</summary>
    [TestMethod()]
    [DeploymentItem("interfaceGraphique.exe")]
    public void OrderEDFTest()
    {
      Processeur pAssProcessor = new Processeur();
      //Add all periodic process
      pAssProcessor.AddProcessus(new P01AutoVerification());
      pAssProcessor.AddProcessus(new P02CalculDuChemin());
      pAssProcessor.AddProcessus(new P04Climat());
      pAssProcessor.AddProcessus(new P07EvitementDeCollision());
      pAssProcessor.AddProcessus(new P09Messagerie());
      pAssProcessor.AddProcessus(new P10SecuriteA());
      pAssProcessor.AddProcessus(new P12SuiviDuChemin());

      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.EDF;
      target.doOrdonnencement(0);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P01).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P02).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P04).Priority == 30);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P07).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P09).Priority == 30);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P10).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P12).Priority == 30);
    }

    /// <summary>
    ///A test for doLeastSlack
    ///</summary>
    [TestMethod()]
    [DeploymentItem("interfaceGraphique.exe")]
    public void OrderLSTest()
    {
      Processeur pAssProcessor = new Processeur();
      //Add all periodic process
      pAssProcessor.AddProcessus(new P01AutoVerification());
      pAssProcessor.AddProcessus(new P02CalculDuChemin());
      pAssProcessor.AddProcessus(new P04Climat());
      pAssProcessor.AddProcessus(new P07EvitementDeCollision());
      pAssProcessor.AddProcessus(new P09Messagerie());
      pAssProcessor.AddProcessus(new P10SecuriteA());
      pAssProcessor.AddProcessus(new P12SuiviDuChemin());

      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 1);
      target.OrderType = Ordonnanceur.Ordonnancement.LS;
      target.doOrdonnencement(1);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P01).Priority < target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P04).Priority);
    }

    /// <summary>
    ///A test for doRateMonotonic
    ///</summary>
    [TestMethod()]
    [DeploymentItem("interfaceGraphique.exe")]
    public void OrderRateMonotonicTest()
    {
      Processeur pAssProcessor = new Processeur();
      //Add all periodic process
      pAssProcessor.AddProcessus(new P01AutoVerification());
      pAssProcessor.AddProcessus(new P02CalculDuChemin());
      pAssProcessor.AddProcessus(new P04Climat());
      pAssProcessor.AddProcessus(new P07EvitementDeCollision());
      pAssProcessor.AddProcessus(new P09Messagerie());
      pAssProcessor.AddProcessus(new P10SecuriteA());
      pAssProcessor.AddProcessus(new P12SuiviDuChemin());

      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RateMonotonic;
      target.doOrdonnencement(0);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P01).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P02).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P04).Priority == 30);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P07).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P09).Priority == 30);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P10).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P12).Priority == 30);
    }

    /// <summary>
    ///A test for doRateMonotonicAdj
    ///</summary>
    [TestMethod()]
    [DeploymentItem("interfaceGraphique.exe")]
    public void OrderRateMonotonicHeritTest()
    {
      Processeur pAssProcessor = new Processeur();
      //Add all periodic process
      pAssProcessor.AddProcessus(new P01AutoVerification());
      pAssProcessor.AddProcessus(new P02CalculDuChemin());
      pAssProcessor.AddProcessus(new P04Climat());
      pAssProcessor.AddProcessus(new P07EvitementDeCollision());
      pAssProcessor.AddProcessus(new P09Messagerie());
      pAssProcessor.AddProcessus(new P10SecuriteA());
      pAssProcessor.AddProcessus(new P12SuiviDuChemin());

      Ordonnanceur target = new Ordonnanceur(ref pAssProcessor, 0);
      target.OrderType = Ordonnanceur.Ordonnancement.RateMototonicHerit;
      target.doOrdonnencement(0);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P01).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P02).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P04).Priority == 30);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P07).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P09).Priority == 30);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P10).Priority == 10);
      Assert.IsTrue(target.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P12).Priority == 30);
    }
  }
}
