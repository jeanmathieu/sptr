﻿//using Simulation.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace TestTraficJamSim
{
    
    
    /// <summary>
    ///This is a test class for P10_SecuriteATest and is intended
    ///to contain all P10_SecuriteATest Unit Tests
    ///</summary>
  [TestClass()]
  public class P10_SecuriteATest
  {


    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[ClassInitialize()]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion


    /// <summary>
    ///A test for P10_SecuriteA Constructor
    ///</summary>
    [TestMethod()]
    public void P10_SecuriteAConstructorTest()
    {
      P10SecuriteA target = new P10SecuriteA();

      Assert.IsTrue(target.contrainteFin == target.periode);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 0);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P10SecuriteA target = new P10SecuriteA(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>();
      List<Message> actual;
      List<Message.ID> peekMsg;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 5);

      Assert.IsTrue(target.getRess() == Ressource.RID.R01);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 4);

      Assert.IsTrue(target.getRess() == Ressource.RID.R02);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 3);

      Assert.IsTrue(target.getRess() == Ressource.RID.R01);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 2);

      inMsg.Add(new Message(false, Message.ID.P12, Message.ID.P10, 1));
      Assert.IsTrue(target.getRess() == Ressource.RID.R04);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg[0] == Message.ID.P12);
      actual = target.run(inMsg);
      Assert.IsTrue(peekMsg.Count == 1);
      inMsg.Clear();
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1);

      Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(target.getTempsRestant() == 0);
      Assert.IsTrue(actual.Count == 1);
      Assert.IsTrue(actual[0].DelaiTransmission == 1);
      Assert.IsTrue(actual[0].SourceID == Message.ID.P10);
      Assert.IsTrue(actual[0].DestinationID == Message.ID.P13);

      Assert.IsTrue((P10SecuriteA.sequenceExec)target.etapeExecution == P10SecuriteA.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
