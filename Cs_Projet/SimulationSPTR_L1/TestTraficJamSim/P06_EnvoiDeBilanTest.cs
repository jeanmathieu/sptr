﻿//using Simulation.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace TestTraficJamSim
{
    
    
    /// <summary>
    ///This is a test class for P06_EnvoiDeBilanTest and is intended
    ///to contain all P06_EnvoiDeBilanTest Unit Tests
    ///</summary>
  [TestClass()]
  public class P06_EnvoiDeBilanTest
  {


    private TestContext testContextInstance;

    /// <summary>
    ///Gets or sets the test context which provides
    ///information about and functionality for the current test run.
    ///</summary>
    public TestContext TestContext
    {
      get
      {
        return testContextInstance;
      }
      set
      {
        testContextInstance = value;
      }
    }

    #region Additional test attributes
    // 
    //You can use the following additional attributes as you write your tests:
    //
    //Use ClassInitialize to run code before running the first test in the class
    //[ClassInitialize()]
    //public static void MyClassInitialize(TestContext testContext)
    //{
    //}
    //
    //Use ClassCleanup to run code after all tests in a class have run
    //[ClassCleanup()]
    //public static void MyClassCleanup()
    //{
    //}
    //
    //Use TestInitialize to run code before running each test
    //[TestInitialize()]
    //public void MyTestInitialize()
    //{
    //}
    //
    //Use TestCleanup to run code after each test has run
    //[TestCleanup()]
    //public void MyTestCleanup()
    //{
    //}
    //
    #endregion


    /// <summary>
    ///A test for P06_EnvoiDeBilan Constructor
    ///</summary>
    [TestMethod()]
    public void P06_EnvoiDeBilanConstructorTest()
    {
      P06EnvoiDeBilan target = new P06EnvoiDeBilan();

      Assert.IsTrue(target.periode == 0);
      Assert.IsTrue(target.contrainteFin == 30);
      Assert.IsTrue(target.DeclencheurProcessus.Count == 1);
      Assert.IsTrue(target.DeclencheurProcessus[0] == Message.ID.P12);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }

    /// <summary>
    ///A test for run
    ///</summary>
    [TestMethod()]
    public void executeTest()
    {
      P06EnvoiDeBilan target = new P06EnvoiDeBilan(); // TODO: Initialize to an appropriate value
      List<Message> inMsg = new List<Message>();
      List<Message> actual;
      List<Message.ID> peekMsg;
      Assert.IsTrue(target.getTempsRestant() == 0);

      target.reinitEtat();
      Assert.IsTrue(target.getTempsRestant() == 3);
      Assert.IsTrue(target.getEtat() == Processus.etat.EXEC);

      Assert.IsTrue(target.getRess() == Ressource.RID.R08);
      peekMsg = target.getMsg();
      Assert.IsTrue(peekMsg.Count == 1);
      Assert.IsTrue(peekMsg[0] == Message.ID.P12);

      actual = target.run(inMsg);
      //Ne genera pas de message
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 2);

      // On test le processus quand aucun message n'est disponible
      Assert.IsTrue(target.getRess() == Ressource.RID.R06);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 1);

      Assert.IsTrue(target.getRess() == Ressource.RID.INVALID);
      Assert.IsTrue(target.getMsg().Count == 0);
      actual = target.run(inMsg);
      Assert.IsTrue(actual.Count == 0);
      Assert.IsTrue(target.getTempsRestant() == 0);

      Assert.IsTrue((P06EnvoiDeBilan.sequenceExec)target.etapeExecution == P06EnvoiDeBilan.sequenceExec.END);
      Assert.IsTrue(target.getEtat() == Processus.etat.END);
    }
  }
}
