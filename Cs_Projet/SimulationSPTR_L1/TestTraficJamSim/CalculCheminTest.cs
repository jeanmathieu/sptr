﻿//using Simulation.Code;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Drawing;
using System.Collections.Generic;

namespace TestTraficJamSim
{
    
    
    /// <summary>
    ///This is a test class for CalculCheminTest and is intended
    ///to contain all CalculCheminTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CalculCheminTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for CalculChemin Constructor
        ///</summary>
        [TestMethod()]
        public void CalculCheminConstructorTest()
        {
            CalculChemin target = new CalculChemin();

            Assert.IsTrue(target != null);
//            Assert.Inconclusive("TODO: Implement code to verify target");
        }

        /// <summary>
        ///A test for CalculNouveauChemin
        ///</summary>
        [TestMethod()]
        public void CalculNouveauCheminTest()
        {
          //CalculChemin target = new CalculChemin(); // TODO: Initialize to an appropriate value
          //int nouvellelistpointducheminasuivre = 0; // TODO: Initialize to an appropriate value
          //Point inPositionVoiture = new Point(); // TODO: Initialize to an appropriate value
          //R07GPS.directionEnum direction = new R07GPS.directionEnum(); // TODO: Initialize to an appropriate value
          //bool expected = false; // TODO: Initialize to an appropriate value
          //bool actual;
          //actual = target.CalculNouveauChemin(nouvellelistpointducheminasuivre, inPositionVoiture, direction);
          //Assert.AreEqual(expected, actual);
          //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for chargerRoutesXML
        ///</summary>
        [TestMethod()]
        public void chargerRoutesXMLTest()
        {
            CalculChemin target = new CalculChemin(); // TODO: Initialize to an appropriate value
            SimulationXML.Instance.lireRoute("..\\sptr-scenario.xml");
            target.chargerRoutesXML();
            Assert.Inconclusive("A method that does not return a value cannot be verified.");
        }

        /// <summary>
        ///A test for isPositionNoeud
        ///</summary>
        [TestMethod()]
        public void isPositionNoeudTest()
        {
            CalculChemin target = new CalculChemin(); // TODO: Initialize to an appropriate value
            Point positionVoiture = new Point(); // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.isPositionNoeud(positionVoiture);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for quelDirection
        ///</summary>
        [TestMethod()]
        public void quelDirectionTest()
        {
            //CalculChemin target = new CalculChemin(); // TODO: Initialize to an appropriate value
            //int mCheminASuivre = 0; // TODO: Initialize to an appropriate value
            //Point inPositionVoiture = new Point(); // TODO: Initialize to an appropriate value
            //R07GPS.directionEnum direction = new R07GPS.directionEnum(); // TODO: Initialize to an appropriate value
            //R07GPS.directionEnum expected = new R07GPS.directionEnum(); // TODO: Initialize to an appropriate value
            //R07GPS.directionEnum actual;
            //actual = target.quelDirection(mCheminASuivre, inPositionVoiture, direction);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for quelDistanceProchainNoeud
        ///</summary>
        [TestMethod()]
        public void quelDistanceProchainNoeudTest()
        {
            //CalculChemin target = new CalculChemin(); // TODO: Initialize to an appropriate value
            //Point inPositionVoiture = new Point(); // TODO: Initialize to an appropriate value
            //R07GPS.directionEnum direction = new R07GPS.directionEnum(); // TODO: Initialize to an appropriate value
            //int expected = 0; // TODO: Initialize to an appropriate value
            //int actual;
            //actual = target.quelDistanceProchainNoeud(inPositionVoiture, direction);
            //Assert.AreEqual(expected, actual);
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for chargerRoutesXML
        ///</summary>
        [TestMethod()]
        public void chargerRoutesXMLTest1()
        {
            CalculChemin target = new CalculChemin(); // TODO: Initialize to an appropriate value
            SimulationXML.Instance.lireRoute("..\\sptr-scenario.xml");
            target.chargerRoutesXML();
            Assert.IsTrue(target.mLesRoutes.Count == 17);
            Assert.IsTrue(target.mNoeudRoutes.Count == 16);
        }

        /// <summary>
        ///Test pour calculLongueurChemin
        ///</summary>
        [TestMethod()]
        public void calculLongueurCheminTest()
        {
            SimulationXML.Instance.XArrivee = 30;
            SimulationXML.Instance.YArrivee = 14;
            CalculChemin target = new CalculChemin(); // TODO: initialisez à une valeur appropriée
            List<Point> listpointducheminasuivre = new List<Point>(); // TODO: initialisez à une valeur appropriée
            listpointducheminasuivre.Add(new Point(17,17));
            listpointducheminasuivre.Add(new Point(17, 10));
            listpointducheminasuivre.Add(new Point(20, 10));
            listpointducheminasuivre.Add(new Point(20, 24));
            listpointducheminasuivre.Add(new Point(30, 24));
            listpointducheminasuivre.Add(new Point(30, 17));


            Point positionVoiture = new Point(20,17); // TODO: initialisez à une valeur appropriée
            
            int expected = 27; // TODO: initialisez à une valeur appropriée
            int actual;
            actual = target.calculLongueurChemin(listpointducheminasuivre, positionVoiture);
            Assert.AreEqual(expected, actual);
          //  Assert.Inconclusive("Vérifiez l\'exactitude de cette m");
        }
    }
}
