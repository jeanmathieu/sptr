﻿partial class Form1
{
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <mXML name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</mXML>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.btnSimAll = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.cbOrderType = new System.Windows.Forms.ComboBox();
            this.lblOrderType = new System.Windows.Forms.Label();
            this.processOrderGrid = new System.Windows.Forms.DataGridView();
            this.ut = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.process13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Carte = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.Fctlabel = new System.Windows.Forms.Label();
            this.UTlabel = new System.Windows.Forms.Label();
            this.Intersectionlabel = new System.Windows.Forms.Label();
            this.Publabel = new System.Windows.Forms.Label();
            this.Meteolabel = new System.Windows.Forms.Label();
            this.Dirlabel = new System.Windows.Forms.Label();
            this.Templabel = new System.Windows.Forms.Label();
            this.Odolabel = new System.Windows.Forms.Label();
            this.SpeedLabel = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureCarte = new System.Windows.Forms.PictureBox();
            this.FeuJaunesign = new System.Windows.Forms.PictureBox();
            this.FeuRougesign = new System.Windows.Forms.PictureBox();
            this.FeuVertsign = new System.Windows.Forms.PictureBox();
            this.CheckEngSign = new System.Windows.Forms.PictureBox();
            this.ABSsign = new System.Windows.Forms.PictureBox();
            this.ABSsign_D = new System.Windows.Forms.PictureBox();
            this.CheckEngSign_D = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.processOrderGrid)).BeginInit();
            this.Carte.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureCarte)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuJaunesign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuRougesign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuVertsign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEngSign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ABSsign)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ABSsign_D)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEngSign_D)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSimAll
            // 
            this.btnSimAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSimAll.Location = new System.Drawing.Point(285, 667);
            this.btnSimAll.Name = "btnSimAll";
            this.btnSimAll.Size = new System.Drawing.Size(71, 21);
            this.btnSimAll.TabIndex = 2;
            this.btnSimAll.Text = "Simuler";
            this.btnSimAll.UseVisualStyleBackColor = true;
            this.btnSimAll.Click += new System.EventHandler(this.btnSimAll_Click);
            // 
            // btnPause
            // 
            this.btnPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnPause.Location = new System.Drawing.Point(362, 667);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(69, 21);
            this.btnPause.TabIndex = 35;
            this.btnPause.Text = "Pause";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // cbOrderType
            // 
            this.cbOrderType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cbOrderType.FormattingEnabled = true;
            this.cbOrderType.Location = new System.Drawing.Point(140, 667);
            this.cbOrderType.Name = "cbOrderType";
            this.cbOrderType.Size = new System.Drawing.Size(139, 21);
            this.cbOrderType.TabIndex = 23;
            // 
            // lblOrderType
            // 
            this.lblOrderType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblOrderType.AutoSize = true;
            this.lblOrderType.Location = new System.Drawing.Point(12, 670);
            this.lblOrderType.Name = "lblOrderType";
            this.lblOrderType.Size = new System.Drawing.Size(122, 13);
            this.lblOrderType.TabIndex = 24;
            this.lblOrderType.Text = "Type d\'ordonnancement";
            // 
            // processOrderGrid
            // 
            this.processOrderGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.processOrderGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ut,
            this.process1,
            this.process2,
            this.process3,
            this.process4,
            this.process5,
            this.process6,
            this.process7,
            this.process8,
            this.process9,
            this.process10,
            this.process11,
            this.process12,
            this.process13});
            this.processOrderGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.processOrderGrid.Location = new System.Drawing.Point(0, 0);
            this.processOrderGrid.Name = "processOrderGrid";
            this.processOrderGrid.Size = new System.Drawing.Size(609, 126);
            this.processOrderGrid.TabIndex = 0;
            this.processOrderGrid.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.processOrderGrid_RowsAdded);
            // 
            // ut
            // 
            this.ut.HeaderText = "U.T";
            this.ut.Name = "ut";
            // 
            // process1
            // 
            this.process1.HeaderText = "P1";
            this.process1.Name = "process1";
            // 
            // process2
            // 
            this.process2.HeaderText = "P2";
            this.process2.Name = "process2";
            // 
            // process3
            // 
            this.process3.HeaderText = "P3";
            this.process3.Name = "process3";
            // 
            // process4
            // 
            this.process4.HeaderText = "P4";
            this.process4.Name = "process4";
            // 
            // process5
            // 
            this.process5.HeaderText = "P5";
            this.process5.Name = "process5";
            // 
            // process6
            // 
            this.process6.HeaderText = "P6";
            this.process6.Name = "process6";
            // 
            // process7
            // 
            this.process7.HeaderText = "P7";
            this.process7.Name = "process7";
            // 
            // process8
            // 
            this.process8.HeaderText = "P8";
            this.process8.Name = "process8";
            // 
            // process9
            // 
            this.process9.HeaderText = "P9";
            this.process9.Name = "process9";
            // 
            // process10
            // 
            this.process10.HeaderText = "P10";
            this.process10.Name = "process10";
            // 
            // process11
            // 
            this.process11.HeaderText = "P11";
            this.process11.Name = "process11";
            // 
            // process12
            // 
            this.process12.HeaderText = "P12";
            this.process12.Name = "process12";
            // 
            // process13
            // 
            this.process13.HeaderText = "P13";
            this.process13.Name = "process13";
            // 
            // Carte
            // 
            this.Carte.BackColor = System.Drawing.Color.White;
            this.Carte.Controls.Add(this.pictureCarte);
            this.Carte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Carte.Location = new System.Drawing.Point(0, 0);
            this.Carte.Name = "Carte";
            this.Carte.Size = new System.Drawing.Size(609, 530);
            this.Carte.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(-1, 1);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(1083, 660);
            this.splitContainer1.SplitterDistance = 609;
            this.splitContainer1.SplitterWidth = 6;
            this.splitContainer1.TabIndex = 36;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.splitContainer2.Panel1.Controls.Add(this.Carte);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.processOrderGrid);
            this.splitContainer2.Size = new System.Drawing.Size(609, 660);
            this.splitContainer2.SplitterDistance = 530;
            this.splitContainer2.TabIndex = 0;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.BackgroundImage = global::SimulationSPTR.Properties.Resources.dash_D;
            this.splitContainer3.Panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.splitContainer3.Panel1.Controls.Add(this.CheckEngSign_D);
            this.splitContainer3.Panel1.Controls.Add(this.ABSsign_D);
            this.splitContainer3.Panel1.Controls.Add(this.Fctlabel);
            this.splitContainer3.Panel1.Controls.Add(this.FeuJaunesign);
            this.splitContainer3.Panel1.Controls.Add(this.FeuRougesign);
            this.splitContainer3.Panel1.Controls.Add(this.FeuVertsign);
            this.splitContainer3.Panel1.Controls.Add(this.CheckEngSign);
            this.splitContainer3.Panel1.Controls.Add(this.ABSsign);
            this.splitContainer3.Panel1.Controls.Add(this.UTlabel);
            this.splitContainer3.Panel1.Controls.Add(this.Intersectionlabel);
            this.splitContainer3.Panel1.Controls.Add(this.Publabel);
            this.splitContainer3.Panel1.Controls.Add(this.Meteolabel);
            this.splitContainer3.Panel1.Controls.Add(this.Dirlabel);
            this.splitContainer3.Panel1.Controls.Add(this.Templabel);
            this.splitContainer3.Panel1.Controls.Add(this.Odolabel);
            this.splitContainer3.Panel1.Controls.Add(this.SpeedLabel);
            this.splitContainer3.Panel1.Resize += new System.EventHandler(this.splitContainer3_Panel1_Resize);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer3.Size = new System.Drawing.Size(468, 660);
            this.splitContainer3.SplitterDistance = 373;
            this.splitContainer3.SplitterWidth = 6;
            this.splitContainer3.TabIndex = 0;
            this.splitContainer3.Resize += new System.EventHandler(this.splitContainer3_Resize);
            // 
            // Fctlabel
            // 
            this.Fctlabel.BackColor = System.Drawing.Color.Transparent;
            this.Fctlabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Fctlabel.ForeColor = System.Drawing.Color.Navy;
            this.Fctlabel.Location = new System.Drawing.Point(137, 301);
            this.Fctlabel.Name = "Fctlabel";
            this.Fctlabel.Size = new System.Drawing.Size(196, 24);
            this.Fctlabel.TabIndex = 14;
            this.Fctlabel.Text = "CHARGEMENT EN COURS";
            this.Fctlabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // UTlabel
            // 
            this.UTlabel.BackColor = System.Drawing.Color.Transparent;
            this.UTlabel.ForeColor = System.Drawing.Color.LightGray;
            this.UTlabel.Location = new System.Drawing.Point(362, 306);
            this.UTlabel.Name = "UTlabel";
            this.UTlabel.Size = new System.Drawing.Size(58, 11);
            this.UTlabel.TabIndex = 8;
            this.UTlabel.Text = "0000";
            this.UTlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Intersectionlabel
            // 
            this.Intersectionlabel.BackColor = System.Drawing.Color.Transparent;
            this.Intersectionlabel.Location = new System.Drawing.Point(359, 272);
            this.Intersectionlabel.Name = "Intersectionlabel";
            this.Intersectionlabel.Size = new System.Drawing.Size(61, 24);
            this.Intersectionlabel.TabIndex = 7;
            this.Intersectionlabel.Text = "LOADING";
            this.Intersectionlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Publabel
            // 
            this.Publabel.BackColor = System.Drawing.Color.Transparent;
            this.Publabel.ForeColor = System.Drawing.Color.Navy;
            this.Publabel.Location = new System.Drawing.Point(85, 342);
            this.Publabel.Name = "Publabel";
            this.Publabel.Size = new System.Drawing.Size(305, 18);
            this.Publabel.TabIndex = 6;
            this.Publabel.Text = "CHARGEMENT PUBLICITE";
            this.Publabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Meteolabel
            // 
            this.Meteolabel.BackColor = System.Drawing.Color.Transparent;
            this.Meteolabel.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Meteolabel.ForeColor = System.Drawing.Color.Navy;
            this.Meteolabel.Location = new System.Drawing.Point(137, 261);
            this.Meteolabel.Name = "Meteolabel";
            this.Meteolabel.Size = new System.Drawing.Size(196, 24);
            this.Meteolabel.TabIndex = 4;
            this.Meteolabel.Text = "CHARGEMENT METEO";
            this.Meteolabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Dirlabel
            // 
            this.Dirlabel.BackColor = System.Drawing.Color.Transparent;
            this.Dirlabel.Font = new System.Drawing.Font("Calibri", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dirlabel.ForeColor = System.Drawing.Color.DarkRed;
            this.Dirlabel.Location = new System.Drawing.Point(60, 216);
            this.Dirlabel.Name = "Dirlabel";
            this.Dirlabel.Size = new System.Drawing.Size(59, 29);
            this.Dirlabel.TabIndex = 3;
            this.Dirlabel.Text = "OUEST";
            this.Dirlabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Templabel
            // 
            this.Templabel.BackColor = System.Drawing.Color.Transparent;
            this.Templabel.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Templabel.ForeColor = System.Drawing.Color.DarkRed;
            this.Templabel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Templabel.Location = new System.Drawing.Point(61, 154);
            this.Templabel.Name = "Templabel";
            this.Templabel.Size = new System.Drawing.Size(43, 34);
            this.Templabel.TabIndex = 2;
            this.Templabel.Text = "00";
            this.Templabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Odolabel
            // 
            this.Odolabel.BackColor = System.Drawing.Color.Transparent;
            this.Odolabel.Font = new System.Drawing.Font("Calibri", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Odolabel.ForeColor = System.Drawing.Color.Navy;
            this.Odolabel.Location = new System.Drawing.Point(151, 189);
            this.Odolabel.Name = "Odolabel";
            this.Odolabel.Size = new System.Drawing.Size(123, 45);
            this.Odolabel.TabIndex = 1;
            this.Odolabel.Text = "0";
            this.Odolabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SpeedLabel
            // 
            this.SpeedLabel.BackColor = System.Drawing.Color.Transparent;
            this.SpeedLabel.Font = new System.Drawing.Font("Calibri", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SpeedLabel.ForeColor = System.Drawing.Color.Navy;
            this.SpeedLabel.Location = new System.Drawing.Point(154, 147);
            this.SpeedLabel.Name = "SpeedLabel";
            this.SpeedLabel.Size = new System.Drawing.Size(123, 45);
            this.SpeedLabel.TabIndex = 0;
            this.SpeedLabel.Text = "0";
            this.SpeedLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(468, 281);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(460, 255);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Statistics";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(462, 257);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "parameters";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureCarte
            // 
            this.pictureCarte.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.pictureCarte.BackgroundImage = global::SimulationSPTR.Properties.Resources.grass;
            this.pictureCarte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureCarte.Location = new System.Drawing.Point(0, 0);
            this.pictureCarte.Name = "pictureCarte";
            this.pictureCarte.Size = new System.Drawing.Size(609, 530);
            this.pictureCarte.TabIndex = 0;
            this.pictureCarte.TabStop = false;
            this.pictureCarte.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureCarte_Paint);
            // 
            // FeuJaunesign
            // 
            this.FeuJaunesign.BackColor = System.Drawing.Color.Transparent;
            this.FeuJaunesign.BackgroundImage = global::SimulationSPTR.Properties.Resources.feuJaune;
            this.FeuJaunesign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FeuJaunesign.Location = new System.Drawing.Point(425, 266);
            this.FeuJaunesign.Name = "FeuJaunesign";
            this.FeuJaunesign.Size = new System.Drawing.Size(38, 41);
            this.FeuJaunesign.TabIndex = 13;
            this.FeuJaunesign.TabStop = false;
            this.FeuJaunesign.Visible = false;
            // 
            // FeuRougesign
            // 
            this.FeuRougesign.BackColor = System.Drawing.Color.Transparent;
            this.FeuRougesign.BackgroundImage = global::SimulationSPTR.Properties.Resources.feuRouge;
            this.FeuRougesign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FeuRougesign.Location = new System.Drawing.Point(425, 266);
            this.FeuRougesign.Name = "FeuRougesign";
            this.FeuRougesign.Size = new System.Drawing.Size(38, 41);
            this.FeuRougesign.TabIndex = 12;
            this.FeuRougesign.TabStop = false;
            this.FeuRougesign.Visible = false;
            // 
            // FeuVertsign
            // 
            this.FeuVertsign.BackColor = System.Drawing.Color.Transparent;
            this.FeuVertsign.BackgroundImage = global::SimulationSPTR.Properties.Resources.feuVert;
            this.FeuVertsign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.FeuVertsign.Location = new System.Drawing.Point(425, 266);
            this.FeuVertsign.Name = "FeuVertsign";
            this.FeuVertsign.Size = new System.Drawing.Size(38, 41);
            this.FeuVertsign.TabIndex = 11;
            this.FeuVertsign.TabStop = false;
            this.FeuVertsign.Visible = false;
            // 
            // CheckEngSign
            // 
            this.CheckEngSign.BackColor = System.Drawing.Color.Transparent;
            this.CheckEngSign.BackgroundImage = global::SimulationSPTR.Properties.Resources.CheckEng;
            this.CheckEngSign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CheckEngSign.Location = new System.Drawing.Point(373, 193);
            this.CheckEngSign.Name = "CheckEngSign";
            this.CheckEngSign.Size = new System.Drawing.Size(47, 44);
            this.CheckEngSign.TabIndex = 10;
            this.CheckEngSign.TabStop = false;
            this.CheckEngSign.Visible = false;
            // 
            // ABSsign
            // 
            this.ABSsign.BackColor = System.Drawing.Color.Transparent;
            this.ABSsign.BackgroundImage = global::SimulationSPTR.Properties.Resources.ABS;
            this.ABSsign.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ABSsign.Location = new System.Drawing.Point(372, 147);
            this.ABSsign.Name = "ABSsign";
            this.ABSsign.Size = new System.Drawing.Size(52, 45);
            this.ABSsign.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ABSsign.TabIndex = 9;
            this.ABSsign.TabStop = false;
            this.ABSsign.Visible = false;
            // 
            // ABSsign_D
            // 
            this.ABSsign_D.BackColor = System.Drawing.Color.Transparent;
            this.ABSsign_D.BackgroundImage = global::SimulationSPTR.Properties.Resources.ABS_D;
            this.ABSsign_D.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ABSsign_D.Location = new System.Drawing.Point(372, 147);
            this.ABSsign_D.Name = "ABSsign_D";
            this.ABSsign_D.Size = new System.Drawing.Size(52, 45);
            this.ABSsign_D.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ABSsign_D.TabIndex = 15;
            this.ABSsign_D.TabStop = false;
            this.ABSsign_D.Visible = false;
            // 
            // CheckEngSign_D
            // 
            this.CheckEngSign_D.BackColor = System.Drawing.Color.Transparent;
            this.CheckEngSign_D.BackgroundImage = global::SimulationSPTR.Properties.Resources.CheckEng_D;
            this.CheckEngSign_D.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.CheckEngSign_D.Location = new System.Drawing.Point(373, 193);
            this.CheckEngSign_D.Name = "CheckEngSign_D";
            this.CheckEngSign_D.Size = new System.Drawing.Size(47, 44);
            this.CheckEngSign_D.TabIndex = 16;
            this.CheckEngSign_D.TabStop = false;
            this.CheckEngSign_D.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 692);
            this.Controls.Add(this.lblOrderType);
            this.Controls.Add(this.btnPause);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.cbOrderType);
            this.Controls.Add(this.btnSimAll);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SimulationSPTR";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.processOrderGrid)).EndInit();
            this.Carte.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureCarte)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuJaunesign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuRougesign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FeuVertsign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEngSign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ABSsign)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ABSsign_D)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckEngSign_D)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label lblOrderType;
        private System.Windows.Forms.ComboBox cbOrderType;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.Button btnSimAll;
        private System.Windows.Forms.DataGridView processOrderGrid;
        private System.Windows.Forms.DataGridViewTextBoxColumn ut;
        private System.Windows.Forms.DataGridViewTextBoxColumn process1;
        private System.Windows.Forms.DataGridViewTextBoxColumn process2;
        private System.Windows.Forms.DataGridViewTextBoxColumn process3;
        private System.Windows.Forms.DataGridViewTextBoxColumn process4;
        private System.Windows.Forms.DataGridViewTextBoxColumn process5;
        private System.Windows.Forms.DataGridViewTextBoxColumn process6;
        private System.Windows.Forms.DataGridViewTextBoxColumn process7;
        private System.Windows.Forms.DataGridViewTextBoxColumn process8;
        private System.Windows.Forms.DataGridViewTextBoxColumn process9;
        private System.Windows.Forms.DataGridViewTextBoxColumn process10;
        private System.Windows.Forms.DataGridViewTextBoxColumn process11;
        private System.Windows.Forms.DataGridViewTextBoxColumn process12;
        private System.Windows.Forms.DataGridViewTextBoxColumn process13;
        private System.Windows.Forms.Panel Carte;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.PictureBox pictureCarte;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label UTlabel;
        private System.Windows.Forms.Label Intersectionlabel;
        private System.Windows.Forms.Label Publabel;
        private System.Windows.Forms.Label Meteolabel;
        private System.Windows.Forms.Label Dirlabel;
        private System.Windows.Forms.Label Templabel;
        private System.Windows.Forms.Label Odolabel;
        private System.Windows.Forms.Label SpeedLabel;
        private System.Windows.Forms.PictureBox ABSsign;
        private System.Windows.Forms.PictureBox CheckEngSign;
        private System.Windows.Forms.PictureBox FeuJaunesign;
        private System.Windows.Forms.PictureBox FeuRougesign;
        private System.Windows.Forms.PictureBox FeuVertsign;
        private System.Windows.Forms.Label Fctlabel;
        private System.Windows.Forms.PictureBox CheckEngSign_D;
        private System.Windows.Forms.PictureBox ABSsign_D;
}

