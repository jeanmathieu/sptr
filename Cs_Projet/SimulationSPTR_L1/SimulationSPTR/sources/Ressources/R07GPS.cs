using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class R07GPS : Ressource
  {
    private R07GPS()
    {
      ResourceId = Ressource.RID.R07;
    }
    
    public override void init()
    {
      positionVoiture.X = SimulationXML.Instance.XDepart;
      positionVoiture.Y = SimulationXML.Instance.YDepart;
      currentDirection = directionEnum.UNDEFINED;
    }

    private static R07GPS mInst = null;

    public static R07GPS Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R07GPS();

        return mInst;
      }
    }

    public override void executeEachUT()
    {

    }

    public enum directionEnum{UNDEFINED,NORD,EST,SUD,OUEST};

    
    public void AvanceLaVoiture(int distance)
    {
      switch (currentDirection)
      {
        case directionEnum.NORD:
          positionVoiture.Y -= distance;
          break;
        case directionEnum.EST:
          positionVoiture.X += distance;
          break;
        case directionEnum.SUD:
          positionVoiture.Y += distance;
          break;
        case directionEnum.OUEST:
          positionVoiture.X -= distance;
          break;
        default:
          throw new Exception("La direction est inconnue");
      }
    }

    private directionEnum currentDirection;

    public directionEnum CurrentDirection
    {
      get { return currentDirection; }
      set { currentDirection = value; }
    }
    public Point PositionVoiture
    {
      get { return positionVoiture; }
      set { positionVoiture = value; }
    }

    private Point positionVoiture;
}
