using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class R05Environnement : Ressource
  {
    int nbUT = 0;
    int nbUTBrise = 0;
    
    public double temperatureExterieur;
    int nbUTTemperature = 0;
    int incTemperature = 5;
    int TF = 0;
    public Trafic traficAuto;
    public List<FeuIntersection> listFeuIntersection = new List<FeuIntersection>();

    private R05Environnement()
    {
      ResourceId = Ressource.RID.R05;
    }

    public override void init()
    {
      nbUT = 0;
      nbUTBrise = 0;
      temperatureExterieur = SimulationXML.Instance.TemperatureMin;
      incTemperature = 5;
      TF = SimulationXML.Instance.Temperature;
      initFeuxCirculation();
      traficAuto = new Trafic();
      traficAuto.chargerTraficXML();
    }

    private static R05Environnement mInst = null;

    public static R05Environnement Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R05Environnement();

        return mInst;
      }
    }

    
    public override void executeEachUT()
    {
      gestionEtatVehicule();
      gestionTemperature();
      gestionFeuxCirculation();
      traficAuto.actualiserTrafic();

      nbUT++;
    }

    private void gestionFeuxCirculation()
    {
      List<SimulationXML.FeuStructure> listFeu = SimulationXML.Instance.Feu;
      FeuIntersection intersection;

      for (int i = 0; i < listFeuIntersection.Count; i++)
      {
        intersection = listFeuIntersection[i];

        intersection.verifieProchainEtat();
      }

    }
    
    private void initFeuxCirculation()
    {
      listFeuIntersection.Clear();

      for (int i = 0; i < SimulationXML.Instance.Feu.Count; i++)
      {
        IEnumerable<FeuIntersection> query = listFeuIntersection.Where(x => x.FeuPosition.X == SimulationXML.Instance.Feu[i].CoordonneeX && x.FeuPosition.Y == SimulationXML.Instance.Feu[i].CoordonneeY);

        if (query.Count() == 0)
        {
          FeuIntersection intersection = new FeuIntersection();
          intersection.FeuPosition.X = SimulationXML.Instance.Feu[i].CoordonneeX;
          intersection.FeuPosition.Y = SimulationXML.Instance.Feu[i].CoordonneeY;
          intersection.init();
          listFeuIntersection.Add(intersection);
        }
      }
    }

    private void gestionEtatVehicule()
    {
      
      if (((nbUT - SimulationXML.Instance.Bris.Phase) % SimulationXML.Instance.Bris.Periode) == 0)
      {
        mEtatVehicule = EtatVehiculeEnum.BRISE;
        nbUTBrise = 0;
      }
      
      if (mEtatVehicule == EtatVehiculeEnum.BRISE)
      {
        if (R03CompteurDeVitesse.Instance.vitesse == 1)
        {
          nbUTBrise++;
        }
        if (nbUTBrise > SimulationXML.Instance.AutoReparation)
        {
          mEtatVehicule = EtatVehiculeEnum.FONCTIONNEL;
        }
      }
    }
    
    private void gestionTemperature()
    {
        if (nbUT==0) return;
        if (nbUT % TF == 0) 
        {
            double lTemp = temperatureExterieur;


            if (temperatureExterieur >= SimulationXML.Instance.TemperatureMax) incTemperature = -5;
            if (temperatureExterieur <= SimulationXML.Instance.TemperatureMin) incTemperature = 5;
            temperatureExterieur += incTemperature;
        }


      ////if (nbUTTemperature >= TF)
      ////{
      ////  nbUTTemperature = 1;
      ////  temperatureExterieur += incTemperature;
      ////  Random rnd = new Random();
      ////  TF = rnd.Next(1, SimulationXML.Instance.TempsSimulation + 1);

        
      ////  if (temperatureExterieur > SimulationXML.Instance.TemperatureMax)
      ////  {
      ////    incTemperature = -5;
      ////    temperatureExterieur += 2 * incTemperature;
      ////  }
        
      ////  else if (temperatureExterieur < SimulationXML.Instance.TemperatureMin)
      ////  {
      ////    incTemperature = 5;
      ////    temperatureExterieur += 2 * incTemperature;
      ////  }
      ////}
      ////else
      ////{
      ////  nbUTTemperature++;
      ////}

    }

    public enum EtatVehiculeEnum { FONCTIONNEL, BRISE };

    private EtatVehiculeEnum mEtatVehicule = EtatVehiculeEnum.FONCTIONNEL;
    public EtatVehiculeEnum EtatVehicule
    {
      get { return mEtatVehicule; }
      set { mEtatVehicule = value; }
    }
}
