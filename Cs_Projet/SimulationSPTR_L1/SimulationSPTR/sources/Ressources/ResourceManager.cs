using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


class ResourceManager
{
    public static void init()
    {
      R01CarteGraphique.Instance.init();
      R02CarteRoutiere.Instance.init();
      R03CompteurDeVitesse.Instance.init();
      R04ModuleAffichage.Instance.init();
      R05Environnement.Instance.init();
      R06GES.Instance.init();
      R07GPS.Instance.init();
      R08Odometre.Instance.init();
      R09Radar.Instance.init();
      R10RegulateurVitesse.Instance.init();
      R11StationMeteo.Instance.init();
      R12Telecommunication.Instance.init();
    }

    public void ExecuteResourceCode()
    {
      R01CarteGraphique.Instance.executeEachUT();
      R02CarteRoutiere.Instance.executeEachUT();
      R03CompteurDeVitesse.Instance.executeEachUT();
      R04ModuleAffichage.Instance.executeEachUT();
      R05Environnement.Instance.executeEachUT();
      R06GES.Instance.executeEachUT();
      R07GPS.Instance.executeEachUT();
      R08Odometre.Instance.executeEachUT();
      R09Radar.Instance.executeEachUT();
      R10RegulateurVitesse.Instance.executeEachUT();
      R11StationMeteo.Instance.executeEachUT();
      R12Telecommunication.Instance.executeEachUT();
    }
}
