using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class R11StationMeteo : Ressource
  {
    string mCurrentMeteo = System.String.Empty;
    int nbUT = 0;
    int currMsg = 0;
    private string[] MessageMeteo = new string[] {"Demain: 3C, pluie ","Samedi: 10C, soleil","Dimanche: 4C, nuageux",  "Lundi: Attention vent violent"};

    private R11StationMeteo()
    {
      ResourceId = Ressource.RID.R11;
    }

    public override void init()
    {
      nbUT = 0;
      currMsg = 0;
    }

    private static R11StationMeteo mInst = null;

    public static R11StationMeteo Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R11StationMeteo();

        return mInst;
      }
    }
    
    public override void executeEachUT()
    {
      if (nbUT > 500)
      {
        currMsg++;
        if (currMsg >= MessageMeteo.Count())
        {
          currMsg = 0;
        }
        nbUT = 0;
      }

      mCurrentMeteo = MessageMeteo[currMsg];

      nbUT++;
    }
    public string CurrentMeteo
    {
      get { return mCurrentMeteo; }
    }
}
