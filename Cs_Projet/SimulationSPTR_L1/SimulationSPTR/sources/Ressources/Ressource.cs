using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class Ressource
{
    public enum RID{INVALID = 0,R01 = 1,R02,R03,R04,R05, R06,R07,R08,R09,R10,R11,R12}
    public RID ResourceId = RID.INVALID;
    public abstract void init();
    public abstract void executeEachUT();
}

