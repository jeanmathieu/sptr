using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class R08Odometre : Ressource
  {
    private R08Odometre()
    {
      ResourceId = Ressource.RID.R08;
    }

    public override void init()
    {
      odometre = 0;
    }

    private static R08Odometre mInst = null;

    public static R08Odometre Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R08Odometre();

        return mInst;
      }
    }

    public override void executeEachUT()
    {
      odometre = odometre + R03CompteurDeVitesse.Instance.vitesse;
    }

    int odometre;

    public int Odometre
    {
      get { return odometre; }
      set { odometre = value; }
    }
}
