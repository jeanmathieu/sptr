using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class R06GES : Ressource
  {
    int gesTotalEmis;
    int gesEmisParcours;
    List<int> gesEmisChaque100us = new List<int>();

    private R06GES()
    {
      ResourceId = Ressource.RID.R06;
    }

    public override void init()
    {
      gesTotalEmis = 0;
      gesEmisChaque100us.Clear();
    }

    private static R06GES mInst = null;

    public static R06GES Instance
    {
      get
      {
        if (mInst == null)
          mInst = new R06GES();

        return mInst;
      }
    }

    public override void executeEachUT()
    {
      gesTotalEmis = gesTotalEmis + R03CompteurDeVitesse.Instance.vitesse;
      gesEmisParcours = gesEmisParcours + R03CompteurDeVitesse.Instance.vitesse;
    }

    
    public void addGESEmis(int gesEmis)
    {
      gesTotalEmis+=gesEmis;
      gesEmisParcours+=gesEmis;
    }
    
    public void addEmisParcours()
    {
      gesEmisChaque100us.Add(gesEmisParcours);
      gesEmisParcours = 0;
    }
    
    public int getMinimumGESParcours()
    {
      if (gesEmisChaque100us.Count() > 0)
        return gesEmisChaque100us.Min();
      else
        return 0;
    }
    
    public int GESTotalEmis
    {
      get { return gesTotalEmis; }
    }
}
