using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;


class P11SecuriteB : Processus
  {
    public enum sequenceExec : int { R11 = 0, R09, R03, E, END };
    public int freinABS;
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R11, Ressource.RID.R09, Ressource.RID.R03, Ressource.RID.INVALID, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P12 };
    
    public P11SecuriteB()
    {
      periode = 0;
      contrainteFin = 7;
      etapeExecution = (int)sequenceExec.END;
      ProcessusID = Message.ID.P11;
      DeclencheurProcessus.Add(Message.ID.P12);
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }
    
    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if (etapeExecution == 0)
      {
        message.Add(attMsgSrcId[0]);
      }

      return message;
    }
    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }
    
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R11;
    }
    
    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P11 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R11:
          etapeExecution++;

          if (inMsg.Count == 0 || inMsg[0].SourceID != Message.ID.P12)
            throw new Exception("Le message n'a pas été recu");

          freinABS = inMsg[0].mMsgPrincipal;

          break;
        case sequenceExec.R09:
          etapeExecution++;
          break;
        case sequenceExec.R03:
          etapeExecution++;
          R03CompteurDeVitesse.Instance.frienABSEnAction = freinABS;
          break;
        case sequenceExec.E:
          etapeExecution++;
          lMsgAEnvoyer.Add(new Message(false, Message.ID.P11, Message.ID.P08, 1));
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
