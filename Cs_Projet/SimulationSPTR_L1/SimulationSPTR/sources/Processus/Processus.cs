using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


abstract class Processus
  {
    public enum etat { EXEC, END };
    public int etapeExecution = 0;
    public int contrainteFin = 0; 
    public int periode = 0;
    public Message.ID ProcessusID = Message.ID.INVALID;
    public List<Message.ID> DeclencheurProcessus = new List<Message.ID>();
    public SimulationXML mXML = SimulationXML.Instance;

    public abstract Ressource.RID getRess();
    public abstract List<Message.ID> getMsg();
    public abstract List<Message> run(List<Message> inMsg);
    public abstract void reinitEtat();
    public abstract etat getEtat();
    public abstract int getTempsRestant();
}

