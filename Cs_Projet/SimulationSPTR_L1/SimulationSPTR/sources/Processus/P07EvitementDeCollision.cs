using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Drawing;

class P07EvitementDeCollision : Processus
  {
    public enum sequenceExec : int { R03 = 0, R09, R07, R05, E, WC02, END };
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R03, Ressource.RID.R09, Ressource.RID.R07, Ressource.RID.R05, Ressource.RID.INVALID, Ressource.RID.INVALID, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P02 };

    int autoAvantDistance;
    int autoAvantVitesse;
    int vitesseActuelle;
    Point positionActuelle;
    R07GPS.directionEnum directionActuelle;

    public P07EvitementDeCollision()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 30;
      contrainteFin = periode;
      ProcessusID = Message.ID.P07;
      autoAvantDistance = 0;
      vitesseActuelle = 0;
      directionActuelle = R07GPS.directionEnum.UNDEFINED;
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> message = new List<Message.ID>();

      if ((sequenceExec)etapeExecution == sequenceExec.WC02)
      {
        message.Add(attMsgSrcId[0]);
      }

      return message;
    }
    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }
    
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R03;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P07 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();
      int uid;

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R03:
          vitesseActuelle = R03CompteurDeVitesse.Instance.vitesse;
          etapeExecution++;
          break;
        case sequenceExec.R09:
          R09Radar.Instance.quelDistanceTrafic(out autoAvantDistance, out autoAvantVitesse, out uid);
          etapeExecution++;
          break;
        case sequenceExec.R07:
          positionActuelle = R07GPS.Instance.PositionVoiture;
          directionActuelle = R07GPS.Instance.CurrentDirection;
          etapeExecution++;
          break;
        case sequenceExec.R05:
          etapeExecution++;
          break;
        case sequenceExec.E:
          Message msg = new Message(false, Message.ID.P07, Message.ID.P03, 1);
          msg.mMsgPrincipal = -99;

          if (autoAvantDistance != int.MaxValue)
          {
            int tempsCollisionAvant = autoAvantDistance / autoAvantVitesse;

            if ((autoAvantDistance / autoAvantVitesse) < (2 * periode))
            {
              msg.mMsgPrincipal = autoAvantVitesse;
            }
          }          
          lMsgAEnvoyer.Add(msg);
          etapeExecution++;
          break;
        case sequenceExec.WC02:
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}

