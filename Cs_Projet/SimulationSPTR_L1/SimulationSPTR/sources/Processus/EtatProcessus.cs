using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

class EtatProcessus
  {
    private Processus mProcessus;
    private int mPriority;
    private state mState;
    private int mPeriodStart;
    private bool mDeadlinedRespected;
    public enum state{ Aucun,Exécution,BloquéRessource,BloquéSurLien,Prêt,BloquéCommunication,Endormi};
    
    private void Init()
    {
      mProcessus = null;
      mPriority = 0;
      mPeriodStart = 0;
      mState = state.Aucun;
      mDeadlinedRespected = true;
    }
    
    public EtatProcessus()
    {
      Init();
    }

    public EtatProcessus(Processus inProcessus)
    {
      Init();
      mProcessus = inProcessus;
    }

    public EtatProcessus(Processus inProcessus, state inState, int inPriority)
    {
      Init();
      mProcessus = inProcessus;
      mState = inState;
      mPriority = inPriority;
    }
    
    public EtatProcessus(EtatProcessus inEtatProcessus)
    {
      mPriority = inEtatProcessus.mPriority;
      mProcessus = inEtatProcessus.Processus; 
      mPeriodStart = inEtatProcessus.mPeriodStart;
      mState = inEtatProcessus.mState;
    }
    
    public bool DeadlinedRespected
    {
      get { return mDeadlinedRespected; }
      set { mDeadlinedRespected = value; }
    }
    public Processus Processus
    {
      get { return mProcessus; }
      set { mProcessus = value; }
    }
    public int PeriodStart
    {
      get { return mPeriodStart; }
      set { mPeriodStart = value; }
    }
    public state ProcessusState
    {
      get { return mState; }
      set { mState = value; }
    }
    public int Priority
    {
      get { return mPriority; }
      set { mPriority = value; }
    }
}
