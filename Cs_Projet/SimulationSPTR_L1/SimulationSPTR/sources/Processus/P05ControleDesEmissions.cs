using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

class P05ControleDesEmissions : Processus
  {
    public enum sequenceExec : int { R06 = 0, END };
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R06, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P03 };
    int derniereEmission = 0;
    
    public P05ControleDesEmissions()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 0;
      contrainteFin = 5;
      ProcessusID = Message.ID.P05;
      
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }
    
    public override List<Message.ID> getMsg()
    {
      List<Message.ID> msg = new List<Message.ID>();

      if (etapeExecution == 0 && DeclencheurProcessus.Count > 0)
      {
        msg.Add(attMsgSrcId[0]);
      }

      return msg;
    }
    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }
   
    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R06;
    }
    
    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P05 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R06:
          
          if (inMsg.Count == 0 && DeclencheurProcessus.Count > 0)
            throw new Exception("P05 n'a pas re�u de message");

          for (int i = 0; i < inMsg.Count; i++)
          {
            R06GES.Instance.addGESEmis(inMsg[i].mMsgPrincipal);
          }

          Message msg = new Message(false, Message.ID.P05, Message.ID.P06, 1);
          msg.mMsgPrincipal = 0;

          
          etapeExecution++;
          if (R06GES.Instance.GESTotalEmis - derniereEmission > mXML.Consommation || R06GES.Instance.GESTotalEmis == 0)
          {
            derniereEmission += mXML.Consommation;
            msg.mMsgPrincipal = 1;
          }

          lMsgAEnvoyer.Add(msg);

          if (DeclencheurProcessus.Count == 0)
            DeclencheurProcessus.Add(Message.ID.P03);
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
