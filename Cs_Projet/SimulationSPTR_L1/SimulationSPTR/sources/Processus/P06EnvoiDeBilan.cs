using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

class P06EnvoiDeBilan : Processus
  {
    public enum sequenceExec : int { R08 = 0, R06, E, END };
    Ressource.RID[] sequenceRess = new Ressource.RID[] { Ressource.RID.R08, Ressource.RID.R06, Ressource.RID.INVALID, Ressource.RID.INVALID };
    Message.ID[] attMsgSrcId = new Message.ID[] { Message.ID.P12 };

    int mOdometre;
    int mEnvoiSuivant;
    
    public P06EnvoiDeBilan()
    {
      etapeExecution = (int)sequenceExec.END;
      periode = 0;
      contrainteFin = 30;
      ProcessusID = Message.ID.P06;
      DeclencheurProcessus.Add(Message.ID.P12);
      mEnvoiSuivant = 100;
    }
    
    public override Ressource.RID getRess()
    {
      return sequenceRess[etapeExecution];
    }

    public override List<Message.ID> getMsg()
    {
      List<Message.ID> msg = new List<Message.ID>();

      if (etapeExecution == 0)
        msg.Add(attMsgSrcId[0]);

      return msg;
    }

    public override void reinitEtat()
    {
      etapeExecution = (int)sequenceExec.R08;
    }
    
    public override int getTempsRestant()
    {
      return (int)sequenceExec.END - etapeExecution;
    }

    public override Processus.etat getEtat()
    {
      if ((sequenceExec)etapeExecution == sequenceExec.END)
        return etat.END;
      else
        return etat.EXEC;
    }
    
    public override List<Message> run(List<Message> inMsg)
    {
      Debug.Print("P06 " + etapeExecution);
      List<Message> lMsgAEnvoyer = new List<Message>();

      switch ((sequenceExec)etapeExecution)
      {
        case sequenceExec.R08:
          mOdometre = R08Odometre.Instance.Odometre;
          etapeExecution++;
          break;
        case sequenceExec.R06:
          if (mOdometre > mEnvoiSuivant)
          {
            R06GES.Instance.addEmisParcours();
            mEnvoiSuivant = mOdometre + 100; 
          }
          etapeExecution++;
          break;
        case sequenceExec.E:
          etapeExecution++;
          break;
        case sequenceExec.END:
          break;
      }

      return lMsgAEnvoyer;
    }
}
