using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Reflection;

public class Voiture : ElementGraphique
  {
    
    public Voiture(Point inPos, Color inColor)
    {
      mColor = inColor;
      mSize = new Size(1 * PixelRatio / 2, 1 * PixelRatio / 2);
      mPosition.X = inPos.X - 1;
      mPosition.Y = inPos.Y - 1;

    }

    public void setDirection(R07GPS.directionEnum inDirection)
    {
        mDirection = inDirection;
      switch (inDirection)
      {
        case R07GPS.directionEnum.EST:
          mOffsetY = PixelRatio / 2;
          mOffsetX = PixelRatio / 4;
          break;
        case R07GPS.directionEnum.NORD:
          mOffsetY = PixelRatio / 4;
          mOffsetX = PixelRatio / 2;
          break;
        case R07GPS.directionEnum.OUEST:
          mOffsetY = 0;
          mOffsetX = PixelRatio / 4;
          break;
        case R07GPS.directionEnum.SUD:
          mOffsetY = PixelRatio / 4;
          mOffsetX = 0;
          break;
        case R07GPS.directionEnum.UNDEFINED:
          mOffsetY = PixelRatio / 4;
          mOffsetX = PixelRatio / 4;
          break;
      }
    }

    public void MoveAuto(Point inPoint)
    {
      mPosition.X = inPoint.X;
      mPosition.Y = inPoint.Y;
    }

    public void DrawAuto(Graphics Graph)
    {
      if (mPosition.X > 0 && mPosition.Y > 0)
      {
          setDirection(mDirection);
        int lEchelle;
        if (SimulationXML.Instance.Echelle > 0) lEchelle = SimulationXML.Instance.Echelle;
        else lEchelle = 1;
        mSize = new Size(1 * PixelRatio / 2 + 1, 1 * PixelRatio / 2 + 1);
        //mSize = new Size(PixelRatio, PixelRatio);

        Point lCoordonnee = new Point((mPosition.X * PixelRatio / lEchelle) + ((SidesSpace - 1) * PixelRatio) + mOffsetX, (mPosition.Y * PixelRatio / lEchelle) + ((TopSpace - 1) * PixelRatio) + mOffsetY);

          /*
        //Positions
        int lLeft = lCoordonnee.X;
        int lTop = lCoordonnee.Y;
        int lWidth = mSize.Width;
        int lHeight = mSize.Height;


        //Largeurs
        int lWidth_1of2 = mSize.Width / 2;
        int lWidth_1of3 = mSize.Width / 3;
        int lWidth_1of4 = mSize.Width / 4;
        int lWidth_1of6 = mSize.Width / 6;
        int lWidthEight = mSize.Width / 8;
        //Hauteur
        int lHeight_1of2 = mSize.Height / 2;
        int lHeight_1of4 = mSize.Height / 4;

        //Positions
        int lLeft_1of2 = lLeft + (lWidth / 2);
        int lLeft_1of3 = lLeft + (lWidth / 3);
        int lLeft_1of4 = lLeft + (lWidth / 4);
        int lTop_1of2 = lTop + (lHeight / 2);
        int lTop_1of3 = lTop + (lHeight / 3);
        int lTop_1of4 = lTop + (lHeight / 4);


        if (mDirection == R07GPS.directionEnum.NORD || mDirection == R07GPS.directionEnum.SUD)
        {
            Graph.FillRectangle(new SolidBrush(mColor), new Rectangle(lLeft, lTop, lWidth_1of4, lHeight));
            Graph.FillRectangle(new SolidBrush(mColor), new Rectangle(lLeft + lWidth_1of4, lTop_1of4, lWidth_1of4, lHeight_1of2));
            Graph.FillEllipse(new SolidBrush(Color.Black), new Rectangle(lLeft, lTop, lHeight / 4, lHeight / 4));
            Graph.FillEllipse(new SolidBrush(Color.Black), new Rectangle(lLeft, lTop + lHeight_1of2 + 2, lHeight / 4, lHeight / 4));
            //Graph.FillEllipse(new SolidBrush(mColor), new Rectangle(lCoordonnee, mSize));
        }
        else
        {
            Graph.FillRectangle(new SolidBrush(mColor), new Rectangle(lLeft, lTop, lWidth, lHeight_1of4));
            Graph.FillRectangle(new SolidBrush(mColor), new Rectangle(lLeft_1of4, lTop - lHeight_1of4, lWidth_1of2, lHeight_1of4));
            Graph.FillEllipse(new SolidBrush(Color.Black), new Rectangle(lLeft, lTop, lWidth / 4, lWidth / 4));
            Graph.FillEllipse(new SolidBrush(Color.Black), new Rectangle(lLeft + lWidth_1of2 + 2, lTop, lWidth / 4, lWidth / 4));
       } 
        */
        Graph.FillEllipse(new SolidBrush(mColor), new Rectangle(lCoordonnee, mSize));
      }
        
        

    }

    private Point mPosition;
    private Color mColor;
    private Size mSize;
    int mOffsetY = 0;
    int mOffsetX = 0;
    R07GPS.directionEnum mDirection =R07GPS.directionEnum.EST;

}
