using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Feu : ElementGraphique
{
    public Feu(Point inPos, Color inColor, String inOrientation)
    {
      mOrientation = inOrientation;
      mPositionIntersection = inPos;
      mSize = new Size(1 * PixelRatio / 2, 1 * PixelRatio / 2);
      switch (mOrientation)
      {
        case "N":
          mPosition.X = (inPos.X / SimulationXML.Instance.Echelle) - 2;
          mPosition.Y = (inPos.Y / SimulationXML.Instance.Echelle) - 2;
          
          mOrientationGPS = R07GPS.directionEnum.SUD;
          break;
        case "S":
          mPosition.X = inPos.X / SimulationXML.Instance.Echelle;
          mPosition.Y = (inPos.Y / SimulationXML.Instance.Echelle);
          mOrientationGPS = R07GPS.directionEnum.NORD;
          break;
        case "E":
          mPosition.X = inPos.X / SimulationXML.Instance.Echelle;
          mPosition.Y = (inPos.Y / SimulationXML.Instance.Echelle) - 2;
          mOrientationGPS = R07GPS.directionEnum.OUEST;
          break;
        case "O":
          mPosition.X = (inPos.X / SimulationXML.Instance.Echelle) - 2;
          mPosition.Y = (inPos.Y / SimulationXML.Instance.Echelle);
          mOrientationGPS = R07GPS.directionEnum.EST;
          break;
      }
      mColor = inColor;

      if (mPosition.X <= 0)
      {
        mPosition.X = 0;
      }
      else if (mPosition.X > 32)
      {
        mPosition.X = 32;
      }

      if (mPosition.Y <= 0)
      {
        mPosition.Y = 0;
      }
      else if (mPosition.Y > 32)
      {
        mPosition.Y = 32;
      }
    }
    
    public void ChangeColor(Color inColor)
    {
      mColor = inColor;
    }
    
    public void DrawFeu(Graphics Graph)
    {
      Point lCoordonnee = new Point((mPosition.X + SidesSpace) * PixelRatio + PixelRatio / 4, (mPosition.Y + TopSpace) * PixelRatio + PixelRatio / 4);
      Graph.FillEllipse(new SolidBrush(mColor), new Rectangle(lCoordonnee, mSize));

    }

     public R07GPS.directionEnum OrientationGPS
    {
      get { return mOrientationGPS; }
      set { mOrientationGPS = value; }
    }

    private Color mColor;
    private Point mPosition;
    private String mOrientation;
    private Size mSize;
    public Point mPositionIntersection;
    private R07GPS.directionEnum mOrientationGPS;

}
