using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Road : ElementGraphique
{

    public Road(Point Begin, Point End)
    {
        mBeginEchelle.X = Begin.X * SimulationXML.Instance.Echelle;
        mBeginEchelle.Y = Begin.Y * SimulationXML.Instance.Echelle;
        mEndEchelle.X = End.X * SimulationXML.Instance.Echelle;
        mEndEchelle.Y = End.Y * SimulationXML.Instance.Echelle;

        if (Begin.X <= End.X)
        {
            mBegin.X = Begin.X - 1;
            mEnd.X = End.X;
        }
        else
        {
            mBegin.X = End.X - 1;
            mEnd.X = Begin.X;
        }
        if (Begin.Y <= End.Y)
        {
            mBegin.Y = Begin.Y - 1;
            mEnd.Y = End.Y;
        }
        else
        {
            mBegin.Y = End.Y - 1;
            mEnd.Y = Begin.Y;
        }
        mSize.Width = mEnd.X - mBegin.X;
        mSize.Height = mEnd.Y - mBegin.Y;
    }

    public void DrawRoad(Graphics Graph)
    {
        Point lBegin = new Point((mBegin.X + SidesSpace) * PixelRatio, (mBegin.Y + TopSpace) * PixelRatio);
        Size lSize = new Size(mSize.Width * PixelRatio, mSize.Height * PixelRatio);

        int idxBegin = R04ModuleAffichage.Instance.listpointducheminasuivre.IndexOf(mBeginEchelle);
        int idxEnd = R04ModuleAffichage.Instance.listpointducheminasuivre.IndexOf(mEndEchelle);
        SolidBrush color = new SolidBrush(Color.Gray);
        if (idxBegin != -1 && idxEnd != -1)
            color.Color = Color.Lavender;

        Graph.FillRectangle(color, new Rectangle(lBegin, lSize));
    }

    private Point mBeginEchelle;
    private Point mEndEchelle;

    private Point mBegin;
    private Point mEnd;
    private Size mSize;
}
