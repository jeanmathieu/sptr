using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

public class Grille : ElementGraphique
  {
    public Grille()
    {
    }

    public void DrawGrille(Graphics Graph)
    {
      for (int i = 0; i < 33; i++)
      {
          string number = (i+1).ToString();
          Point StringEmplacement = new Point((i + SidesSpace) * PixelRatio, TopSpace * PixelRatio);
          Point StringSize = new Point((i + SidesSpace) * PixelRatio, (32 + TopSpace) * PixelRatio);
          Size rectanglesize = new Size(StringSize);
          RectangleF StringZone = new RectangleF(StringEmplacement,rectanglesize);
        Graph.DrawLine(new Pen(Color.Black), (i + SidesSpace) * PixelRatio, TopSpace * PixelRatio, (i + SidesSpace) * PixelRatio, (32 + TopSpace) * PixelRatio);
        Graph.DrawLine(new Pen(Color.Black), SidesSpace * PixelRatio, (i + TopSpace) * PixelRatio, (32 + SidesSpace) * PixelRatio, (i + TopSpace) * PixelRatio);
       if(i!= 32) Graph.DrawString(number, font, myBrush, (i + SidesSpace) * PixelRatio, TopSpace * PixelRatio-15);
       if (i != 32) Graph.DrawString(number, font, myBrush, SidesSpace * PixelRatio-17, (i + TopSpace) * PixelRatio);
        
      }
    }
    
    Font font = new Font("Arial", 9.0f);
    Brush myBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
}
