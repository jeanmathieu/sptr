using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class Simulateur
{
    private int mUT;
    private List<Processeur> mProcs;
    private Reseau mNetwork;
    private ResourceManager mResManager;


    private void Initiliaze_1P()
    {
        mProcs = new List<Processeur>();
        mResManager = new ResourceManager();
        mUT = 0;
        Processeur pAssProcessor1 = new Processeur();

        mProcs = new List<Processeur>();
        mProcs.Add(pAssProcessor1);


        pAssProcessor1.AddProcessus(new P02CalculDuChemin());
        pAssProcessor1.AddProcessus(new P03ChangementDeVitesse());
        pAssProcessor1.AddProcessus(new P12SuiviDuChemin());
        pAssProcessor1.AddProcessus(new P01AutoVerification());
        pAssProcessor1.AddProcessus(new P04Climat());
        pAssProcessor1.AddProcessus(new P05ControleDesEmissions());
        pAssProcessor1.AddProcessus(new P06EnvoiDeBilan());
        pAssProcessor1.AddProcessus(new P07EvitementDeCollision());
        pAssProcessor1.AddProcessus(new P08GestionDesAppels());
        pAssProcessor1.AddProcessus(new P09Messagerie());
        pAssProcessor1.AddProcessus(new P10SecuriteA());
        pAssProcessor1.AddProcessus(new P11SecuriteB());
        pAssProcessor1.AddProcessus(new P13SystemesElectriques());

        Ordonnanceur target1 = new Ordonnanceur(ref pAssProcessor1, 0);
        target1.OrderType = SimulationXML.Instance.OrderType;

        pAssProcessor1.Ordonnenceur = target1;

        target1.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P05).ProcessusState = EtatProcessus.state.Pr�t;
        target1.ProcsStatus.Single(x => x.Processus.ProcessusID == Message.ID.P05).Processus.reinitEtat();
        mNetwork = new Reseau(mProcs);
    }

    public Simulateur()
    {
      Initiliaze_1P(); 
    }

    
    public void ExecuteUT()
    {
      mProcs.ForEach(x => x.Ordonnenceur.doOrdonnencement(mUT));
      MetaOrdonnanceur.GereConflits(mProcs);
      mProcs.ForEach(x => x.Ordonnenceur.RunProcessusUT(mUT));
      mProcs.ForEach(x => mNetwork.EnvoieMessage(x.Ordonnenceur.CurExecutingOutBuffer, x.Ordonnenceur.OutBufferMsg, mUT));
      mProcs.ForEach(x => x.Ordonnenceur.EndUT(mUT));

      System.Threading.Thread.Sleep(10);

      mResManager.ExecuteResourceCode();
      ++mUT;

    }

    public Dictionary<int, List<Tuple<EtatProcessus, String>>> GetProcessusByUT()
    {
      return mProcs.First().Ordonnenceur.ProcessusByUT;
    }

    public Object[] getProcessusState(int pCurUT)
    {
      if (GetProcessusByUT().ContainsKey(pCurUT))
      {
        String P1 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P01).Item2;
        String P2 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P02).Item2;
        String P3 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P03).Item2;
        String P4 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P04).Item2;
        String P5 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P05).Item2;
        String P6 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P06).Item2;
        String P7 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P07).Item2;
        String P8 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P08).Item2;
        String P9 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P09).Item2;
        String P10 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P10).Item2;
        String P11 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P11).Item2;
        String P12 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P12).Item2;
        String P13 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P13).Item2;
        return new Object[] { pCurUT, P1, P2, P3, P4, P5, P6, P7, P8, P9, P10, P11, P12, P13 };
      }
      else
        return new Object[] { -1, "", "", "", "", "", "", "", "", "", "", "", "", "" };
    }

    public Color[] getProcessusColor(int pCurUT)
    {
        if (GetProcessusByUT().ContainsKey(pCurUT))
        {
            EtatProcessus.state P1 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P01).Item1.ProcessusState;
            EtatProcessus.state P2 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P02).Item1.ProcessusState;
            EtatProcessus.state P3 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P03).Item1.ProcessusState;
            EtatProcessus.state P4 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P04).Item1.ProcessusState;
            EtatProcessus.state P5 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P05).Item1.ProcessusState;
            EtatProcessus.state P6 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P06).Item1.ProcessusState;
            EtatProcessus.state P7 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P07).Item1.ProcessusState;
            EtatProcessus.state P8 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P08).Item1.ProcessusState;
            EtatProcessus.state P9 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P09).Item1.ProcessusState;
            EtatProcessus.state P10 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P10).Item1.ProcessusState;
            EtatProcessus.state P11 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P11).Item1.ProcessusState;
            EtatProcessus.state P12 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P12).Item1.ProcessusState;
            EtatProcessus.state P13 = GetProcessusByUT()[pCurUT].Single(x => x.Item1.Processus.ProcessusID == Message.ID.P13).Item1.ProcessusState;
            Color lC1 = P1 == EtatProcessus.state.Ex�cution ? Color.Green : P1 == EtatProcessus.state.Pr�t ? Color.Yellow : P1 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC2 = P2 == EtatProcessus.state.Ex�cution ? Color.Green : P2 == EtatProcessus.state.Pr�t ? Color.Yellow : P2 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC3 = P3 == EtatProcessus.state.Ex�cution ? Color.Green : P3 == EtatProcessus.state.Pr�t ? Color.Yellow : P3 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC4 = P4 == EtatProcessus.state.Ex�cution ? Color.Green : P4 == EtatProcessus.state.Pr�t ? Color.Yellow : P4 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC5 = P5 == EtatProcessus.state.Ex�cution ? Color.Green : P5 == EtatProcessus.state.Pr�t ? Color.Yellow : P5 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC6 = P6 == EtatProcessus.state.Ex�cution ? Color.Green : P6 == EtatProcessus.state.Pr�t ? Color.Yellow : P6 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC7 = P7 == EtatProcessus.state.Ex�cution ? Color.Green : P7 == EtatProcessus.state.Pr�t ? Color.Yellow : P7 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC8 = P8 == EtatProcessus.state.Ex�cution ? Color.Green : P8 == EtatProcessus.state.Pr�t ? Color.Yellow : P8 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC9 = P9 == EtatProcessus.state.Ex�cution ? Color.Green : P9 == EtatProcessus.state.Pr�t ? Color.Yellow : P9 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC10 = P10 == EtatProcessus.state.Ex�cution ? Color.Green : P10 == EtatProcessus.state.Pr�t ? Color.Yellow : P10 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC11 = P11 == EtatProcessus.state.Ex�cution ? Color.Green : P11 == EtatProcessus.state.Pr�t ? Color.Yellow : P11 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC12 = P12 == EtatProcessus.state.Ex�cution ? Color.Green : P12 == EtatProcessus.state.Pr�t ? Color.Yellow : P12 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;
            Color lC13 = P13 == EtatProcessus.state.Ex�cution ? Color.Green : P13 == EtatProcessus.state.Pr�t ? Color.Yellow : P13 == EtatProcessus.state.Endormi ? Color.LightBlue : Color.LightSalmon;

            return new Color[] { Color.White, lC1, lC2, lC3, lC4, lC5, lC6, lC7, lC8, lC9, lC10, lC11, lC12, lC13 };
        }
        else
            return new Color[] { Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White, Color.White };
    }

    
    public int CurrentUT
    {
      get { return mUT; }
      set { mUT = value; }
    }
    public List<Processeur> ProcessorsInSim
    {
      get { return mProcs; }
      set { mProcs = value; }
    }
    internal Reseau Network
    {
      get { return mNetwork; }
      set { mNetwork = value; }
    }
}
