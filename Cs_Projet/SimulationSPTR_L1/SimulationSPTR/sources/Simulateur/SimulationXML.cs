using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Windows.Forms;

class SimulationXML
{
    public XmlDocument xmlDoc = new XmlDocument();
    public int TempsSimulation, Communication, AutoReparation, Collision, Echelle;
    public int XDepart, YDepart, XArrivee, YArrivee, Vitesse, Acceleration;
    public int Consommation, FeuJaune, TemperatureMin, TemperatureMax;
    public String Strategie;
    public List<RouteStructure> Route = new List<RouteStructure>();
    public List<FeuStructure> Feu = new List<FeuStructure>();
    public List<TraficStructure> Trafic = new List<TraficStructure>();
    public int Temperature;
    public BrisStructure Bris = new BrisStructure();
    public ConducteurStructure Conducteur = new ConducteurStructure();
    public Ordonnanceur.Ordonnancement OrderType = Ordonnanceur.Ordonnancement.RoundRobin;
    public string mFilename ="";

    string getFileDialog()
    {
        string lFilename = "";
        OpenFileDialog lOpenFileDialog = new OpenFileDialog();
        lOpenFileDialog.Title = "Sélectionner le fichier de paramètres";
        lOpenFileDialog.Filter = "XML files (*.xml)|*.xml|All files (*.*)|*.*";
        lOpenFileDialog.DefaultExt = ".xml";
        DialogResult lResult = lOpenFileDialog.ShowDialog();
        if (lResult != DialogResult.OK) return "";
        lFilename = lOpenFileDialog.FileName;
        if (!System.IO.File.Exists(lFilename)) return "";

        return lFilename;
    }

    public bool setXMLFilename()
    {
        bool lOk = false;
        bool lEscape = false;

        string lFilename = "";
        while (lFilename == "" && !lEscape)
        {
            lFilename = getFileDialog();

            if (lFilename == "")
            {
                DialogResult lResult = MessageBox.Show("Vous devez charger un fichier XML contenant les paramètres de simulation, Désirez-vous quitter?", "Erreur", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1);
                if (lResult == DialogResult.Yes) lEscape = true;
            }
            else
            {
                mFilename = lFilename;
                lOk = true;
            }
        }

        return lOk;
    }

    private SimulationXML()
    {
    }

    private static SimulationXML mInst = null;

    public static SimulationXML Instance
    {
      get
      {
        if (mInst == null)
          mInst = new SimulationXML();

        return mInst;
      }
    }
    
    public struct BrisStructure
    {
      public int Periode, Phase;

      public BrisStructure(int inPeriode, int inPhase)
      {
        Periode = inPeriode;
        Phase = inPhase;
      }
    }
    
    public struct ConducteurStructure
    {
      public int Periode, Phase;

      public ConducteurStructure(int inPeriode, int inPhase)
      {
        Periode = inPeriode;
        Phase = inPhase;
      }
    }
    
    public struct RouteStructure
    {
      public int Numero, XDebut, YDebut, XFin, YFin, Vitesse;

      public RouteStructure(int inNumero, int inXDebut, int inYDebut, int inXFin, int inYFin, int inVitesse)
      {
        Numero = inNumero;
        XDebut = inXDebut;
        YDebut = inYDebut;
        XFin = inXFin;
        YFin = inYFin;
        Vitesse = inVitesse;
      }
    }

    
    public struct FeuStructure
    {
      public int Numero, CoordonneeX, CoordonneeY, Duree;
      public String Position;

      public FeuStructure(int inNumero, int inCoordonneeX, int inCoordonneeY, String inPosition, int inDuree)
      {
        Numero = inNumero;
        CoordonneeX = inCoordonneeX;
        CoordonneeY = inCoordonneeY;
        Position = inPosition;
        Duree = inDuree;
      }
    }
    
    public struct TraficStructure
    {
      public int Numero, XDebut, YDebut, XFin, YFin, Vitesse, Periode, Phase;

      public TraficStructure(int inNumero, int inXDebut, int inYDebut, int inXFin, int inYFin, int inVitesse, int inPeriode, int inPhase)
      {
        Numero = inNumero;
        XDebut = inXDebut;
        YDebut = inYDebut;
        XFin = inXFin;
        YFin = inYFin;
        Vitesse = inVitesse;
        Periode = inPeriode;
        Phase = inPhase;
      }
    }

    
    public void lireFichierXml(String xmlFilePath)
    {
      xmlDoc.Load(xmlFilePath);
    }

    
    public void lireParametres(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Parametres")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            if (j.Name == "Simulation") TempsSimulation = int.Parse(j.InnerText);
            if (j.Name == "Communication") Communication = int.Parse(j.InnerText);
            if (j.Name == "AutoReparation") AutoReparation = int.Parse(j.InnerText);
            if (j.Name == "Collision") Collision = int.Parse(j.InnerText);
            if (j.Name == "Echelle") Echelle = int.Parse(j.InnerText);
            if (j.Name == "XDepart") XDepart = int.Parse(j.InnerText);
            if (j.Name == "YDepart") YDepart = int.Parse(j.InnerText);
            if (j.Name == "XArrivee") XArrivee = int.Parse(j.InnerText);
            if (j.Name == "YArrivee") YArrivee = int.Parse(j.InnerText);
            if (j.Name == "Vitesse") Vitesse = int.Parse(j.InnerText);
            if (j.Name == "Acceleration") Acceleration = int.Parse(j.InnerText);
            if (j.Name == "Consommation") Consommation = int.Parse(j.InnerText);
            if (j.Name == "FeuJaune") FeuJaune = int.Parse(j.InnerText);
            if (j.Name == "TemperatureMin") TemperatureMin = int.Parse(j.InnerText);
            if (j.Name == "TemperatureMax") TemperatureMax = int.Parse(j.InnerText);
            if (j.Name == "Strategie") Strategie = j.InnerText;
          }
        }
      }
      XDepart *= Echelle;
      YDepart *= Echelle;
      XArrivee *= Echelle;
      YArrivee *= Echelle;
    }

    public int lireTemperature(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes)
      {
        if (i.ChildNodes.Count == 1 && i.Name == "Temperature")
        {
          Temperature = int.Parse(i.InnerText);
        }
      }
      return Temperature;
    }
    
    public void lireBris(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Bris")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            if (j.Name == "Periode") Bris.Periode = int.Parse(j.InnerText);
            if (j.Name == "Phase") Bris.Phase = int.Parse(j.InnerText);
          }
        }
      }
    }

    
    public void lireConducteur(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Conducteur")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            if (j.Name == "Periode") Conducteur.Periode = int.Parse(j.InnerText);
            if (j.Name == "Phase") Conducteur.Phase = int.Parse(j.InnerText);
          }
        }
      }
    }

    
    public void lireRoute(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);
      RouteStructure lRoute = new RouteStructure();
      
      Route.Clear();
      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Routes")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            foreach (XmlNode k in j.ChildNodes) 
            {
              lRoute.Numero = int.Parse(j.Attributes["numero"].Value);

              if (k.Name == "XDebut") lRoute.XDebut = int.Parse(k.InnerText);
              if (k.Name == "YDebut") lRoute.YDebut = int.Parse(k.InnerText);
              if (k.Name == "XFin") lRoute.XFin = int.Parse(k.InnerText);
              if (k.Name == "YFin") lRoute.YFin = int.Parse(k.InnerText);
              if (k.Name == "Vitesse") lRoute.Vitesse = int.Parse(k.InnerText);
            }
            Route.Add(lRoute);
          }
        }
      }
    }

    
    public void lireFeu(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);
      FeuStructure lFeu = new FeuStructure();
      
      Feu.Clear();

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Feux")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            foreach (XmlNode k in j.ChildNodes) 
            {
              lFeu.Numero = int.Parse(j.Attributes["numero"].Value);

              if (k.Name == "CoordonneeX") lFeu.CoordonneeX = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "CoordonneeY") lFeu.CoordonneeY = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "Position") lFeu.Position = k.InnerText;
              if (k.Name == "Duree") lFeu.Duree = int.Parse(k.InnerText);
            }
            Feu.Add(lFeu);
          }
        }
      }
    }

    
    public void lireTrafic(String xmlFilePath)
    {
      lireFichierXml(xmlFilePath);
      TraficStructure lTrafic = new TraficStructure();
      
      Trafic.Clear();
      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Trafic")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            foreach (XmlNode k in j.ChildNodes) 
            {
              lTrafic.Numero = int.Parse(j.Attributes["numero"].Value);

              if (k.Name == "XDebut") lTrafic.XDebut = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "YDebut") lTrafic.YDebut = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "XFin") lTrafic.XFin = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "YFin") lTrafic.YFin = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "Vitesse") lTrafic.Vitesse = int.Parse(k.InnerText);
              if (k.Name == "Periode") lTrafic.Periode = int.Parse(k.InnerText);
              if (k.Name == "Phase") lTrafic.Phase = int.Parse(k.InnerText);
            }
            Trafic.Add(lTrafic);
          }
        }
      }
    }

    
    public void lireTousLesParametres()
    //public void lireTousLesParametres(String xmlFilePath)
    {
        lireFichierXml(mFilename);
      //lireFichierXml(xmlFilePath);

      RouteStructure lRoute = new RouteStructure();
      FeuStructure lFeu = new FeuStructure();
      TraficStructure lTrafic = new TraficStructure();

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Parametres")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            if (j.Name == "Simulation") TempsSimulation = int.Parse(j.InnerText);
            if (j.Name == "Communication") Communication = int.Parse(j.InnerText);
            if (j.Name == "AutoReparation") AutoReparation = int.Parse(j.InnerText);
            if (j.Name == "Collision") Collision = int.Parse(j.InnerText);
            if (j.Name == "Echelle") Echelle = int.Parse(j.InnerText);
            if (j.Name == "XDepart") XDepart = int.Parse(j.InnerText);
            if (j.Name == "YDepart") YDepart = int.Parse(j.InnerText);
            if (j.Name == "XArrivee")XArrivee = int.Parse(j.InnerText);
            if (j.Name == "YArrivee")YArrivee = int.Parse(j.InnerText);
            if (j.Name == "Vitesse")Vitesse = int.Parse(j.InnerText);
            if (j.Name == "Acceleration")Acceleration = int.Parse(j.InnerText);
            if (j.Name == "Consommation")Consommation = int.Parse(j.InnerText);
            if (j.Name == "FeuJaune")FeuJaune = int.Parse(j.InnerText);
            if (j.Name == "TemperatureMin")TemperatureMin = int.Parse(j.InnerText);
            if (j.Name == "TemperatureMax")TemperatureMax = int.Parse(j.InnerText);
            if (j.Name == "Strategie")Strategie = j.InnerText;
          }
        }
      }
      XDepart *= Echelle;
      YDepart *= Echelle;
      XArrivee *= Echelle;
      YArrivee *= Echelle;

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes)
      {
        if (i.ChildNodes.Count == 1 && i.Name == "Temperature")
        {
          Temperature = int.Parse(i.InnerText);
        }
      }

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Bris")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            if (j.Name == "Periode") Bris.Periode = int.Parse(j.InnerText);
            if (j.Name == "Phase") Bris.Phase = int.Parse(j.InnerText);
          }
        }
      }

      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Conducteur")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            if (j.Name == "Periode") Conducteur.Periode = int.Parse(j.InnerText);
            if (j.Name == "Phase") Conducteur.Phase = int.Parse(j.InnerText);
          }
        }
      }

      Route.Clear();
      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Routes")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            foreach (XmlNode k in j.ChildNodes) 
            {
              lRoute.Numero = int.Parse(j.Attributes["numero"].Value);

              if (k.Name == "XDebut") lRoute.XDebut = int.Parse(k.InnerText);
              if (k.Name == "YDebut") lRoute.YDebut = int.Parse(k.InnerText);
              if (k.Name == "XFin") lRoute.XFin = int.Parse(k.InnerText);
              if (k.Name == "YFin") lRoute.YFin = int.Parse(k.InnerText);
              if (k.Name == "Vitesse") lRoute.Vitesse = int.Parse(k.InnerText);
            }
            Route.Add(lRoute);
          }
        }
      }

      Feu.Clear();
      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Feux")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            foreach (XmlNode k in j.ChildNodes) 
            {
              lFeu.Numero = int.Parse(j.Attributes["numero"].Value);

              if (k.Name == "CoordonneeX") lFeu.CoordonneeX = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "CoordonneeY") lFeu.CoordonneeY = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "Position")lFeu.Position = k.InnerText;
              if (k.Name == "Duree")lFeu.Duree = int.Parse(k.InnerText);
            }
            Feu.Add(lFeu);
          }
        }
      }

      Trafic.Clear();
      foreach (XmlNode i in xmlDoc.DocumentElement.ChildNodes) 
      {
        if (i.ChildNodes.Count >= 1 && i.Name == "Trafic")
        {
          foreach (XmlNode j in i.ChildNodes) 
          {
            foreach (XmlNode k in j.ChildNodes) 
            {
              lTrafic.Numero = int.Parse(j.Attributes["numero"].Value);

              if (k.Name == "XDebut") lTrafic.XDebut = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "YDebut") lTrafic.YDebut = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "XFin") lTrafic.XFin = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "YFin") lTrafic.YFin = int.Parse(k.InnerText) * Echelle;
              if (k.Name == "Vitesse") lTrafic.Vitesse = int.Parse(k.InnerText);
              if (k.Name == "Periode") lTrafic.Periode = int.Parse(k.InnerText);
              if (k.Name == "Phase") lTrafic.Phase = int.Parse(k.InnerText);
            }
            Trafic.Add(lTrafic);
          }
        }
      }
    }
    

}
