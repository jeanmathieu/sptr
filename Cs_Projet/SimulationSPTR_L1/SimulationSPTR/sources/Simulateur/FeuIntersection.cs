using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

class FeuIntersection  
{
    public Point FeuPosition = new Point();
    public R07GPS.directionEnum directionVertJaune;
    public EtatFeu EtatFeuVertJaune;
    public int currentQC;

    public int currentFeu;
    public List<Feu> listFeu = new List<Feu>();

    public enum EtatFeu { VERT, JAUNE, ROUGE };

    public struct Feu
    {
      public R07GPS.directionEnum direction;
      public int QC;
    }

    public void init()
    {
      IEnumerable<SimulationXML.FeuStructure> query;
      listFeu.Clear();

      query = SimulationXML.Instance.Feu.Where(x => x.CoordonneeX == FeuPosition.X && x.CoordonneeY == FeuPosition.Y && x.Position == "E");
      foreach (SimulationXML.FeuStructure feu in query)
      {
        Feu newFeu = new Feu();
        
        newFeu.direction = R07GPS.directionEnum.OUEST;
        newFeu.QC = feu.Duree;
        listFeu.Add(newFeu);
      }
      query = SimulationXML.Instance.Feu.Where(x => x.CoordonneeX == FeuPosition.X && x.CoordonneeY == FeuPosition.Y && x.Position == "N");
      foreach (SimulationXML.FeuStructure feu in query)
      {
        Feu newFeu = new Feu();
  
        newFeu.direction = R07GPS.directionEnum.SUD;
        newFeu.QC = feu.Duree;
        listFeu.Add(newFeu);
      }
      query = SimulationXML.Instance.Feu.Where(x => x.CoordonneeX == FeuPosition.X && x.CoordonneeY == FeuPosition.Y && x.Position == "O");
      foreach (SimulationXML.FeuStructure feu in query)
      {
        Feu newFeu = new Feu();
        
        newFeu.direction = R07GPS.directionEnum.EST;
        newFeu.QC = feu.Duree;
        listFeu.Add(newFeu);
      }
      query = SimulationXML.Instance.Feu.Where(x => x.CoordonneeX == FeuPosition.X && x.CoordonneeY == FeuPosition.Y && x.Position == "S");
      foreach (SimulationXML.FeuStructure feu in query)
      {
        Feu newFeu = new Feu();
        
        newFeu.direction = R07GPS.directionEnum.NORD;
        newFeu.QC = feu.Duree;
        listFeu.Add(newFeu);
      }

      if (listFeu.Count == 0)
        throw new Exception("L'intersection devrait avoir des feux");

      directionVertJaune = listFeu[0].direction;
      EtatFeuVertJaune = EtatFeu.VERT;
      currentQC = 0;
      currentFeu = 0;
    }
    
    public void verifieProchainEtat()
    {
      if (currentQC >= listFeu[currentFeu].QC - SimulationXML.Instance.FeuJaune)
      {
        EtatFeuVertJaune = EtatFeu.JAUNE;
      }
      if (currentQC >= listFeu[currentFeu].QC)
      {
        currentFeu++;
        if (currentFeu >= listFeu.Count)
          currentFeu = 0;

        currentQC = 0;
        directionVertJaune = listFeu[currentFeu].direction;
        EtatFeuVertJaune = EtatFeu.VERT;
      }

      currentQC++;
    }
}
