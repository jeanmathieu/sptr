using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


  class Message
  {
    private bool mBlocking;
    private Processeur mDestination;
    private Processeur mSource;
    private ID mDestinationID;
    private ID mSourceID;
    private int mDelaiTransmission;

    public int mMsgPrincipal;
    public int mVitesse;
    public int mDistance;
    public int mCouleur;
    public List<Point> mCheminASuivre;

    public enum ID{INVALID = 0,P01 = 1,P02,P03,P04,P05,P06,P07,P08,P09,P10,P11,P12,P13}

    private void Init()
    {
      mBlocking = false;
      mDelaiTransmission = 0;
      mSource = null;
      mDestination = null;
    }
    
    public Message()
    {
      Init();
    }
    
    public Message(bool inBlocking, ID inSourceID, ID inDestinationID, int inDimension)
    {
      mBlocking = inBlocking;
      mDelaiTransmission = inDimension;
      mSourceID = inSourceID;
      mDestinationID = inDestinationID;
    }

    public bool Blocking
    {
      get { return mBlocking; }
      set { mBlocking = value; }
    }
    internal Processeur Destination
    {
      get { return mDestination; }
      set { mDestination = value; }
    }
    public ID DestinationID
    {
      get { return mDestinationID; }
      set { mDestinationID = value; }
    }
    internal Processeur Source
    {
      get { return mSource; }
      set { mSource = value; }
    }
    public ID SourceID
    {
      get { return mSourceID; }
    }
    public int DelaiTransmission
    {
      get { return mDelaiTransmission; }
      set { mDelaiTransmission = value; }
    }
  }
