using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Security.Cryptography;

class Ordonnanceur
  {
    private List<Message> mTamponExecution;
    private List<Message> mTamponEntree;
    private List<Message> mTamponSortie;
    private List<EtatProcessus> mEtatProcessus;
    private Processeur mProcesseur;
    private EtatProcessus mProcessusCourant;
    private Ordonnancement mOrdonnancement;
    private bool mP08demarre;
    
    private static List<EtatRessource> mEtatRessourceParatagees;  
    private static Dictionary<int, List<Tuple<EtatProcessus, String>>> mHistoriqueExecution; 
    private static List<Tuple<Message.ID, Message.ID>> mCommunicationSporadiques;
    private static List<Message.ID> mProcessusSporadiques;

    //Statistiques
    private int mPeriodeRespectee;
    private int mPeriodeDepassee;
    private int mUtilisationDuProcesseur;
    private int mNonUtilisationDuProcesseur;
    private int mTotalProcessusBloques;
    Dictionary<Message.ID, int> mProcessusBloques;
    Dictionary<Message.ID, int> mProcessusBloquesMaximum;

    
    public enum Ordonnancement{Undefined = 0,RoundRobin,RateMonotonic,RateMototonicHerit,EDF,LS};
    
    public Ordonnanceur()
    {
      Init();
    }
    
    public Ordonnanceur(ref Processeur inProcesseur, int inNow)
    {
      Init();
      mProcesseur = inProcesseur;
      inProcesseur.Ordonnenceur = this;
      inProcesseur.Processus.ForEach(x => AddProcessus(x, inNow));
    }
    
    private void Init()
    {
      mP08demarre = false;
      
      if (mEtatRessourceParatagees == null) { InitRessourcePartagees(); }
      InitListeCommunicationSporadiques();
      mEtatProcessus = new List<EtatProcessus>();
      mTamponExecution = new List<Message>();
      mTamponSortie = new List<Message>();
      mTamponEntree = new List<Message>();
      mProcesseur = null;
      mOrdonnancement = Ordonnancement.Undefined;
      
      mPeriodeRespectee = 0;
      mPeriodeDepassee = 0;
      mUtilisationDuProcesseur = 0;
      mNonUtilisationDuProcesseur = 0;
      mTotalProcessusBloques = 0;
      mProcessusBloques = new Dictionary<Message.ID, int>();
      mProcessusBloquesMaximum = new Dictionary<Message.ID, int>();
    }

    private static void InitListeCommunicationSporadiques()
    {
      if (mCommunicationSporadiques == null)
      {
        mCommunicationSporadiques = new List<Tuple<Message.ID, Message.ID>>();
        mCommunicationSporadiques.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P07, Message.ID.P03));
        mCommunicationSporadiques.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P03));
        mCommunicationSporadiques.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P03, Message.ID.P05));
        mCommunicationSporadiques.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P06));
        mCommunicationSporadiques.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P12, Message.ID.P11));
        mCommunicationSporadiques.Add(new Tuple<Message.ID, Message.ID>(Message.ID.P10, Message.ID.P13));
      }
      
      if (mProcessusSporadiques == null)
      {
        mProcessusSporadiques = new List<Message.ID>();
        mProcessusSporadiques.Add(Message.ID.P04);
      }
    }

    public static void InitRessourcePartagees()
    {
      mEtatRessourceParatagees = new List<EtatRessource>();
      foreach (Ressource.RID lRessourcesID in Enum.GetValues(typeof(Ressource.RID)))
      {
        if (lRessourcesID != Ressource.RID.INVALID)
          mEtatRessourceParatagees.Add(new EtatRessource(lRessourcesID));
      }
      
      mHistoriqueExecution = new Dictionary<int, List<Tuple<EtatProcessus, String>>>();
    }

    private void setPrioriteStatique(EtatProcessus inEtatProcessus)
    {
      inEtatProcessus.Priority = inEtatProcessus.Processus.periode;
    }

    private string getEtatProcessus(EtatProcessus inProcessus)
    {
      String lText = inProcessus.ProcessusState.ToString();
      List<Message> lMessageAttendu = ReceivedAllMessageToUnblock(inProcessus);
      List<Message> lMessageAEnvoyer = mTamponSortie.Where(x => x.SourceID == inProcessus.Processus.ProcessusID && x.DelaiTransmission > 0).ToList();
      List<Message> lMessageEnvoye = mTamponSortie.Where(x => x.SourceID == inProcessus.Processus.ProcessusID && x.DelaiTransmission == 0).ToList();
      if (lMessageAEnvoyer.Count == 0)
      {
        
        if (inProcessus.Processus.getEtat() == Processus.etat.EXEC && inProcessus.Processus.getRess() != Ressource.RID.INVALID && ReceivedAllMessageToUnblock(inProcessus).Count == 0)
        {
            lText +=", Ressource: " + inProcessus.Processus.getRess().ToString();
          EtatRessource lEtatRessource = mEtatRessourceParatagees.SingleOrDefault(x => x.getID == inProcessus.Processus.getRess());
          if (lEtatRessource.verrouProcessus != null) { lText += " Verrouillé par: " + lEtatRessource.verrouProcessus.ProcessusID.ToString(); }
        }
        lMessageAttendu.ForEach(x => lText += ", Communication entrante: " + x.SourceID.ToString() + x.DestinationID.ToString());
        lMessageEnvoye.ForEach(x => lText += ", Communication sortante: " + x.SourceID.ToString() + x.DestinationID.ToString());
      }
      else
      {
          lMessageAEnvoyer.ForEach(x => lText += ", Tampon message: " + x.SourceID.ToString() + x.DestinationID.ToString() + "  Delai: " + x.DelaiTransmission.ToString());
        lMessageAttendu.ForEach(x => lText += ", Communication entrante: " + x.SourceID.ToString() + x.DestinationID.ToString());
        lMessageEnvoye.ForEach(x => lText += ", Communication sortante: " + x.SourceID.ToString() + x.DestinationID.ToString());
      }
      return lText;
    }

    private void RunUT(int inNow, List<Message> inMessages)
    {
      RunMessages(inMessages);
      
      if (mProcessusCourant.Processus.getEtat() == Processus.etat.END && inNow <= mProcessusCourant.PeriodStart + mProcessusCourant.Processus.contrainteFin && mTamponSortie.Count(x => x.SourceID == mProcessusCourant.Processus.ProcessusID) == 0)
      { mProcessusCourant.ProcessusState = EtatProcessus.state.Endormi; }
      else if (mTamponSortie.Count(x => x.SourceID == mProcessusCourant.Processus.ProcessusID && x.Blocking) > 0)
      { mProcessusCourant.ProcessusState = EtatProcessus.state.BloquéCommunication; }
      
      else if (mProcessusCourant.Processus.getEtat() == Processus.etat.EXEC && ReceivedAllMessageToUnblock(mProcessusCourant).Count > 0 && mTamponSortie.Count(x => x.SourceID == mProcessusCourant.Processus.ProcessusID) == 0)
      { mProcessusCourant.ProcessusState = EtatProcessus.state.BloquéRessource; }
      else
      { mProcessusCourant.ProcessusState = EtatProcessus.state.Prêt; }
      
      mProcessusCourant = null;
    }

    private void RunMessages(List<Message> inMessages)
    {
      Message lMessageSortant = mTamponSortie.FirstOrDefault(x => x.SourceID == mProcessusCourant.Processus.ProcessusID);
      
      if (lMessageSortant == null)
      { 
        List<Message> lMessagesSortants = mProcessusCourant.Processus.run(inMessages);
        
        if (lMessagesSortants.Count(x => x.DelaiTransmission < 1) != 0) { throw new Exception("Le delai doit etre 1 ou plus; Processus:" + mProcessusCourant.Processus.ProcessusID.ToString()); }
        
        mTamponSortie.AddRange(lMessagesSortants);
        
        if (lMessagesSortants.Count(x => x.DelaiTransmission < 0) != 0) { throw new Exception("Le delai doit etre superieur a 0"); }
        if (lMessagesSortants.Count(x => !mTamponSortie.Contains(x)) > 0) { throw new Exception("On tente de transmettre avec un tampon vide"); }
      }
      int lNbEntree = mTamponEntree.Count;
      int lNbSortie = inMessages.Count;
      
      inMessages.ForEach(y => mTamponEntree.Remove(y));
      if (lNbEntree - lNbSortie != mTamponEntree.Count) { throw new Exception("Certains messages n'ont pu etre enlever du tampon"); }
      
      mTamponExecution = mTamponSortie.Where(x => x.SourceID == mProcessusCourant.Processus.ProcessusID).ToList();
    }

    private void SauverHistoriqueExecution(int inNow)
    {
      if (!mHistoriqueExecution.ContainsKey(inNow)) { mHistoriqueExecution.Add(inNow, new List<Tuple<EtatProcessus, String>>()); }
      mHistoriqueExecution[inNow].Add(new Tuple<EtatProcessus, String>(new EtatProcessus(mProcessusCourant), getEtatProcessus(mProcessusCourant)));
    }

    private void doEarliestDeadline(int pCurUT)
    {
      mEtatProcessus.Where(x => x.ProcessusState == EtatProcessus.state.Prêt && x.ProcessusState != EtatProcessus.state.BloquéCommunication).ToList().ForEach(x => x.Priority = x.PeriodStart + x.Processus.contrainteFin - pCurUT); 
      TrierSelonPriorites();
    }

    private void doLeastSlack(int pCurUT)
    {
      mEtatProcessus.Where(x => x.ProcessusState == EtatProcessus.state.Prêt && x.ProcessusState != EtatProcessus.state.BloquéCommunication).ToList().ForEach(x => x.Priority = (1 + x.PeriodStart + x.Processus.contrainteFin - pCurUT - x.Processus.getTempsRestant()));
      TrierSelonPriorites();
    }

    private void doRoundRobin()
    {
      EtatProcessus lProcessus = mEtatProcessus.Single(x => x.Priority == 1);
      int lCount = mEtatProcessus.Count;
      while (lProcessus.ProcessusState != EtatProcessus.state.Prêt && lProcessus.ProcessusState != EtatProcessus.state.BloquéCommunication && lCount > 0)
      {
        mEtatProcessus.ForEach(x => --x.Priority);
        lProcessus = mEtatProcessus.Single(x => x.Priority == 0);
        lProcessus.Priority = mEtatProcessus.Count;
        --lCount;
      }
      
      if (lProcessus.ProcessusState == EtatProcessus.state.Prêt || lProcessus.ProcessusState == EtatProcessus.state.BloquéCommunication)
      {
        mProcessusCourant = lProcessus;
        
        if (mProcessusCourant.Priority == 1)
        {
          mEtatProcessus.ForEach(x => --x.Priority);
          mProcessusCourant.Priority = mEtatProcessus.Count;
        }
      }
      else { mProcessusCourant = null; }
    }
    
    private void doRateMonotonic()
    {   
      TrierSelonPriorites();
    }

    private void doRateMonotonicAdj()
    {      
      TrierSelonPriorites();
      
      if (mProcessusCourant != null)
      {
        Ressource.RID lRessourceRequise = mProcessusCourant.Processus.getRess();
        List<EtatProcessus> lPrcoessusBloquesSurRessource = mEtatProcessus.Where(x => x.ProcessusState == EtatProcessus.state.BloquéRessource && x.Processus.getRess() == lRessourceRequise).ToList();
        
        if (lPrcoessusBloquesSurRessource.Count > 0) { mProcessusCourant.Priority = lPrcoessusBloquesSurRessource.Max(x => x.Priority); }
      }
    }

    private void TrierSelonPriorites()
    {
      mEtatProcessus = mEtatProcessus.OrderBy(x => x.Priority).ToList();
      
      EtatProcessus lProcessus = getProcessusSuivant();
      ChangerProcessusEnExecution(lProcessus);
    }

    private void ChangerProcessusEnExecution(EtatProcessus inEtatProcessus)
    {
      if (inEtatProcessus == null) { mProcessusCourant = null; }
      else if (mProcessusCourant == null) { mProcessusCourant = inEtatProcessus; }
      else if (mProcessusCourant.ProcessusState != EtatProcessus.state.Prêt) { mProcessusCourant = inEtatProcessus; }
      else if (inEtatProcessus.Priority < mProcessusCourant.Priority) { mProcessusCourant = inEtatProcessus; }
    }
    
    private EtatProcessus getProcessusSuivant()
    {
      EtatProcessus lEtatProcessus = null;
      for (int i = 0; i < mEtatProcessus.Count; ++i)
      {
        if (i < mEtatProcessus.Count - 1 && mEtatProcessus[i + 1].Priority < mEtatProcessus[i].Priority) { throw new Exception("Les processus ne sont pas en ordre de prorite"); }
        if (mEtatProcessus[i].ProcessusState == EtatProcessus.state.Prêt || mEtatProcessus[i].ProcessusState == EtatProcessus.state.BloquéCommunication)
        {
          lEtatProcessus = mEtatProcessus[i];
          break;
        }
      }
      return lEtatProcessus;
    }

    
    private void DemarrageAleatoire(Message.ID inMessageID, int inNow)
    {
      DemarrageAleatoire(mEtatProcessus.SingleOrDefault(x => x.Processus.ProcessusID == inMessageID), inNow);
    }

    
    private void DemarrageAleatoire(EtatProcessus inEtatProcessus, int inNow)
    { 
      if (inEtatProcessus == null) return;
      byte[] lTMP = new Byte[100];
      
      RNGCryptoServiceProvider lGenerateur = new RNGCryptoServiceProvider();
      lGenerateur.GetNonZeroBytes(lTMP); 
      
      if (lTMP[25] > 200)
      { DemarrerProcessus(inEtatProcessus, inNow); }
    }

    
    private void DemarrerProcessus(EtatProcessus inProcessus, int inNow)
    {
      inProcessus.PeriodStart = inNow;
      if (inProcessus.Processus.getEtat() == Processus.etat.END)
      { 
        inProcessus.Processus.reinitEtat();
        inProcessus.ProcessusState = EtatProcessus.state.Prêt;
        if (inProcessus.DeadlinedRespected == true) { ++mPeriodeRespectee; }
        else
        {
          Console.WriteLine("Le processus sporadique n'a pas respecter sa contrainte de fin : " + inProcessus.Processus.ToString());
          ++mPeriodeDepassee;
          inProcessus.DeadlinedRespected = true;
        }

      }
      else { Console.WriteLine("Tentative de démarrer un processus qui l'était déjà: " + inProcessus.ToString()); }
    }

    private void DebloquerProcessus(EtatProcessus inProcessus)
    { 
      if (inProcessus.Processus.getEtat() != Processus.etat.EXEC) return; 
      if (inProcessus.Processus.getRess() == Ressource.RID.INVALID) return;
      
      if (DebloquerRessource(inProcessus) && ReceivedAllMessageToUnblock(inProcessus).Count == 0)
      {
        if (inProcessus.ProcessusState == EtatProcessus.state.BloquéRessource)
        {
          inProcessus.ProcessusState = EtatProcessus.state.Prêt;
        }
        else
        {
          throw new Exception("Le processus n'est pas bloqué, mais tente de se débloquer");
        }
      }
    }

    public void AddProcessus(Processus inProcessus, int inNow)
    {
      EtatProcessus lEtatProcessus = new EtatProcessus(inProcessus, EtatProcessus.state.Endormi, mEtatProcessus.Count + 1);
      mEtatProcessus.Add(lEtatProcessus);
      mProcessusBloques.Add(lEtatProcessus.Processus.ProcessusID, 0);
      mProcessusBloquesMaximum.Add(lEtatProcessus.Processus.ProcessusID, 0);
      if (mProcessusSporadiques.Contains(inProcessus.ProcessusID) == false && lEtatProcessus.Processus.periode != 0)
      {
        DemarrerProcessus(lEtatProcessus, inNow);
      }
    }

    
    public void doOrdonnencement(int pCurUT)
    {
      switch (mOrdonnancement)
      {
        case (Ordonnancement.RoundRobin):
          doRoundRobin();
          break;
        case Ordonnancement.RateMonotonic:
          doRateMonotonic();
          break;
        case Ordonnancement.RateMototonicHerit:
          doRateMonotonicAdj();
          break;
        case Ordonnancement.EDF:
          doEarliestDeadline(pCurUT);
          break;
        case Ordonnancement.LS:
          doLeastSlack(pCurUT);
          break;
        default:
          throw new Exception("Aucun ordonnencent de sélectionné");
      }
    }

    public void RunProcessusUT(int inNow)
    {
      mTamponExecution = new List<Message>();
      
      if (mProcessusCourant == null) return;
      
      bool lAucunBlocage = true;
      List<Message> lMessages = new List<Message>();
      
      if (mTamponSortie.Count(x => x.SourceID == mProcessusCourant.Processus.ProcessusID) == 0)
      {
        Ressource.RID lRessourceID = Ressource.RID.INVALID;
        List<Message.ID> lCommunication = new List<Message.ID>();
        if (mProcessusCourant.Processus.getEtat() == Processus.etat.EXEC)
        {
          lRessourceID = mProcessusCourant.Processus.getRess();
          lCommunication = mProcessusCourant.Processus.getMsg();
        }
        else { lAucunBlocage = false; } 

        if (lRessourceID != Ressource.RID.INVALID && lAucunBlocage) 
        {
          lAucunBlocage = ProcessusDetientRessource(mProcessusCourant);
        }
        if (lCommunication.Count != 0 && lAucunBlocage) 
        {
          lAucunBlocage = ReceivedAllMessageToUnblock(mProcessusCourant).Count == 0 ? true : false;
          if (lAucunBlocage) { lMessages = FetchAllMessageToExec(mProcessusCourant); }
        }
      }
      
      if (lAucunBlocage == true)
      {
        ++mUtilisationDuProcesseur;
        if (mProcessusCourant.ProcessusState == EtatProcessus.state.Prêt){ mProcessusCourant.ProcessusState = EtatProcessus.state.Exécution; }
        else if (mProcessusCourant.ProcessusState != EtatProcessus.state.BloquéCommunication) { throw new Exception("Il n'y a rien a executer"); }
        
        SauverHistoriqueExecution(inNow); 
        RunUT(inNow, lMessages);
      }
      else 
      {
        ++mNonUtilisationDuProcesseur;
        mProcessusCourant.ProcessusState = EtatProcessus.state.BloquéRessource;
        
        SauverHistoriqueExecution(inNow);
      }
    }

    public void EndUT(int inNow)
    {
      List<EtatProcessus> lProcessusBloques = mEtatProcessus.Where(x => x.ProcessusState == EtatProcessus.state.BloquéRessource).ToList();
      lProcessusBloques.ForEach(x => DebloquerProcessus(x));
      
      if (!mHistoriqueExecution.ContainsKey(inNow)) { mHistoriqueExecution.Add(inNow, new List<Tuple<EtatProcessus, string>>()); }
      
      mEtatProcessus.Where(x => mHistoriqueExecution[inNow].SingleOrDefault(y => y.Item1.Processus.ProcessusID == x.Processus.ProcessusID) == null).ToList().ForEach(x => mHistoriqueExecution[inNow].Add(new Tuple<EtatProcessus, String>(new EtatProcessus(x), getEtatProcessus(x))));

      mEtatRessourceParatagees.Where(x => x.verrouillee && x.verrouProcessus.getEtat() == Processus.etat.END).ToList().ForEach(x => x.deVerrouiller(x.verrouProcessus));
      mEtatRessourceParatagees.Where(x => x.verrouillee && x.getID != x.verrouProcessus.getRess()).ToList().ForEach(x => x.deVerrouiller(x.verrouProcessus));

      List<EtatProcessus> lProcessusTermines = mEtatProcessus.Where(x => x.Processus.getEtat() == Processus.etat.END && mTamponSortie.Count(y => y.SourceID == x.Processus.ProcessusID) == 0 && x.ProcessusState != EtatProcessus.state.Endormi).ToList();
      foreach (EtatProcessus lProcessusTermine in lProcessusTermines)
      {
        if (lProcessusTermine.DeadlinedRespected == true) { lProcessusTermine.ProcessusState = EtatProcessus.state.Endormi; } 
        else { DemarrerProcessus(lProcessusTermine, inNow + 1); }
      }
      
      lProcessusTermines = mEtatProcessus.Where(x => (inNow + 1) >= x.PeriodStart + x.Processus.contrainteFin && x.Processus.periode != 0).ToList();
      foreach (EtatProcessus lProcessusTermine in lProcessusTermines)
      {
        if (lProcessusTermine.Processus.getEtat() != Processus.etat.END && (lProcessusTermine.ProcessusState != EtatProcessus.state.Prêt || lProcessusTermine.ProcessusState != EtatProcessus.state.Endormi) && lProcessusTermine.DeadlinedRespected == true)
        {
          lProcessusTermine.DeadlinedRespected = false;
        }
        DemarrerProcessus(lProcessusTermine, inNow + 1);
      }
      
      lProcessusTermines = mEtatProcessus.Where(x => x.Processus.periode == 0).ToList();
      foreach (EtatProcessus lProcessusTermine in lProcessusTermines)
      {
        if (lProcessusTermine.Processus.getEtat() != Processus.etat.END && (lProcessusTermine.ProcessusState != EtatProcessus.state.Prêt || lProcessusTermine.ProcessusState != EtatProcessus.state.Endormi)) { lProcessusTermine.DeadlinedRespected = false; }
        
        List<Message.ID> lCommunicationSporadiques = new List<Message.ID>(); 
        mCommunicationSporadiques.Where(x => x.Item2 == lProcessusTermine.Processus.ProcessusID).ToList().ForEach(x => lCommunicationSporadiques.Add(x.Item1));
        List<Message> lMessageEntrants = mTamponEntree.Where(x => x.DestinationID == lProcessusTermine.Processus.ProcessusID).ToList();
        if (lMessageEntrants.Count(x => lCommunicationSporadiques.Contains(x.SourceID)) > 0 && (lProcessusTermine.ProcessusState == EtatProcessus.state.Prêt || lProcessusTermine.ProcessusState == EtatProcessus.state.BloquéCommunication)) 
        {
          DemarrerProcessus(lProcessusTermine, inNow + 1);
        }
      }
      
      StatistiqueBlocage();
      
      mProcessusSporadiques.ForEach(x => DemarrageAleatoire(x, inNow + 1));
      StartP08(inNow);
    }

    private void StatistiqueBlocage()
    {
      List<EtatProcessus> lProcessusBloques = mEtatProcessus.Where(x => x.ProcessusState == EtatProcessus.state.BloquéRessource).ToList();
      foreach (EtatProcessus lProcessusBloque in lProcessusBloques)
      {
        
        if (!(lProcessusBloque.Processus.periode == 0 && lProcessusBloque.Processus.getEtat() == Processus.etat.END))
        {
          ++mTotalProcessusBloques;
          
          ++mProcessusBloques[lProcessusBloque.Processus.ProcessusID];
          if (mProcessusBloques[lProcessusBloque.Processus.ProcessusID] > mProcessusBloquesMaximum[lProcessusBloque.Processus.ProcessusID])
          { mProcessusBloquesMaximum[lProcessusBloque.Processus.ProcessusID] = mProcessusBloques[lProcessusBloque.Processus.ProcessusID]; }
        }
      }
      
      foreach (Message.ID lKey in mHistoriqueExecution.Keys)
      {
        if (lProcessusBloques.SingleOrDefault(x => x.Processus.ProcessusID == lKey) == null) { mProcessusBloques[lKey] = 0; }
      }
    }

    private void StartP08(int inNow)
    {
      if (SimulationXML.Instance.Conducteur.Phase == inNow && !mP08demarre)
      {
        EtatProcessus lProcessus = mEtatProcessus.SingleOrDefault(x => x.Processus.ProcessusID == Message.ID.P08);
        if (lProcessus != null)
        {
          lProcessus.Processus.periode = SimulationXML.Instance.Conducteur.Periode;
          DemarrerProcessus(lProcessus, inNow);
        }
        mP08demarre = true;
      }
    }

    public List<Message> ReceivedAllMessageToUnblock(EtatProcessus pPS)
    {
      List<Message.ID> aCom = new List<Message.ID>();
      if (pPS.Processus.getEtat() != Processus.etat.END) { aCom = pPS.Processus.getMsg(); }
      List<Message> WaitingOn = new List<Message>();
      foreach (Message.ID aID in aCom)
      {
        Message aT = null;
        aT = mTamponEntree.FirstOrDefault(x => x.SourceID == aID && x.DestinationID == pPS.Processus.ProcessusID);
        if (aT == null)
        {
          WaitingOn.Add(new Message(false, aID, pPS.Processus.ProcessusID, -1));
        }
      }
      return WaitingOn;
    }

    
    private List<Message> FetchAllMessageToExec(EtatProcessus pPS)
    {
      List<Message.ID> aCom = new List<Message.ID>();
      if (pPS.Processus.getEtat() != Processus.etat.END) { aCom = pPS.Processus.getMsg(); }
      List<Message> GotMSG = new List<Message>();
      foreach (Message.ID aID in aCom)
      {
        Message aT = null;
        aT = mTamponEntree.FirstOrDefault(x => x.SourceID == aID && x.DestinationID == pPS.Processus.ProcessusID);
        if (aT != null) { GotMSG.Add(aT); }
      }
      return GotMSG;
    }

    public void ReceptionMessage(Message inMessage, int inNow)
    {
      
      if (inMessage == null) return;
      if (inMessage.DelaiTransmission != 0) { throw new Exception("Le message est arrivé alors qu'il était encore en transit"); }
      Message.ID lPrcoessusID = inMessage.DestinationID;
      
      if (mCommunicationSporadiques.Contains(new Tuple<Message.ID, Message.ID>(inMessage.SourceID, inMessage.DestinationID)))
      { 
        EtatProcessus lProcessus = mEtatProcessus.Single(x => x.Processus.ProcessusID == lPrcoessusID); 
        
        if (lProcessus.ProcessusState == EtatProcessus.state.Endormi)
        { DemarrerProcessus(lProcessus, inNow); }
      }
      
      List<EtatProcessus> lProcessusBloques = mEtatProcessus.Where(x => x.ProcessusState == EtatProcessus.state.BloquéRessource && x.Processus.getEtat() == Processus.etat.EXEC).ToList();
      foreach (EtatProcessus lProcessusBloque in lProcessusBloques)
      {
        
        List<Message> lEnAttenteMessage = ReceivedAllMessageToUnblock(lProcessusBloque);
        if (lProcessusBloque.Processus.getRess() != Ressource.RID.INVALID)
        {
          if (DebloquerRessource(lProcessusBloque) && lEnAttenteMessage.Count == 1)
          {
            if (lEnAttenteMessage[0].SourceID == inMessage.SourceID && lEnAttenteMessage[0].DestinationID == inMessage.DestinationID) 
            { lProcessusBloque.ProcessusState = EtatProcessus.state.Prêt; }
          }
        }
        else
        {
          if (lEnAttenteMessage.Count == 1)
          {
            if (lEnAttenteMessage[0].SourceID == inMessage.SourceID && lEnAttenteMessage[0].DestinationID == inMessage.DestinationID) 
            { lProcessusBloque.ProcessusState = EtatProcessus.state.Prêt; }
          }
        }
      }
      
      mTamponEntree.Add(inMessage);
    }

    
    public bool DebloquerRessource(EtatProcessus inProcessus)
    {
      if (inProcessus.Processus.getRess() == Ressource.RID.INVALID) { Console.WriteLine("Le processus n'attend pas de ressource"); }
      EtatRessource lRessource = mEtatRessourceParatagees.Single(x => x.getID == inProcessus.Processus.getRess());
      return !lRessource.verrouillee;
    }
          
    public bool ProcessusDetientRessource(EtatProcessus inProcessus)
    {
      if (inProcessus.Processus.getRess() == Ressource.RID.INVALID) { throw new Exception("e processus ne peut pas bloquer, car il n'attend pas de ressource"); }
      return mEtatRessourceParatagees.Single(x => x.getID == inProcessus.Processus.getRess()).verrouProcessus == inProcessus.Processus ? true : false;
    }
      
    
    int BlockedProcessus
    {
      get { return mTotalProcessusBloques; }
      set { mTotalProcessusBloques = value; }
    }
    public Dictionary<Message.ID, int> MaxBlockingProcessCount
    {
      get { return mProcessusBloquesMaximum; }
      set { mProcessusBloquesMaximum = value; }
    }
    public int RespectedPeriodCount
    {
      get { return mPeriodeRespectee; }
      set { mPeriodeRespectee = value; }
    }
    public int NonRespectePeriodCount
    {
      get { return mPeriodeDepassee; }
      set { mPeriodeDepassee = value; }
    }
    public int ProcessorUsedCount
    {
      get { return mUtilisationDuProcesseur; }
      set { mUtilisationDuProcesseur = value; }
    }
    public int ProcessorUnusedCount
    {
      get { return mNonUtilisationDuProcesseur; }
      set { mNonUtilisationDuProcesseur = value; }
    }
    
    public Processeur LinkedProcessor
    {
      get { return mProcesseur; }
      set { mProcesseur = value; }
    }
    public Ordonnancement OrderType
    {
      get { return mOrdonnancement; }
      set
      {
        mOrdonnancement = value;
        if (mOrdonnancement == Ordonnancement.RateMonotonic || mOrdonnancement == Ordonnancement.RateMototonicHerit)
        { mEtatProcessus.ForEach(x => setPrioriteStatique(x)); }
      }
    }
    public List<EtatProcessus> ProcsStatus
    {
      get { return mEtatProcessus; }
      set { mEtatProcessus = value; }
    }
    public Dictionary<int, List<Tuple<EtatProcessus, String>>> ProcessusByUT
    {
      get { return Ordonnanceur.mHistoriqueExecution; }
      set { Ordonnanceur.mHistoriqueExecution = value; }
    }
    public List<Message> CurExecutingOutBuffer
    {
      get { return mTamponExecution; }
      set { mTamponExecution = value; }
    }
    public List<Message> OutBufferMsg
    {
      get { return mTamponSortie; }
      set { mTamponSortie = value; }
    }
    public EtatProcessus ProcessusToExecute
    {
      get { return mProcessusCourant; }
      set { mProcessusCourant = value; }
    }
    public List<EtatRessource> RessourceLockList
    {
      set { mEtatRessourceParatagees = value; }
      get { return mEtatRessourceParatagees; }
    }
}