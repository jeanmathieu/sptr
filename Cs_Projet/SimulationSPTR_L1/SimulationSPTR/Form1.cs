﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

 public partial class Form1 : Form
 {

    //Members
    private System.Windows.Forms.Timer timer1;

    Simulateur mSim;
    SimulationXML param = SimulationXML.Instance;
    string fileName;
    private Map mCarte;
    bool mSimThreadCancel;
    string mRandObjLock = "NotNull";
    bool mSimRunning;
    string mSimRunningLock = "NotNull";
    double mDmi;
    Size DashboardPanelOldSize;
    /// <summary>
    /// CTor
    /// </summary>
    public Form1()
    {
      InitializeComponent();

      if (param.setXMLFilename() == false)
      {
          Environment.Exit(1);
      }


      fileName = param.mFilename; //"../sptr-scenario.xml";
      param.lireParametres(fileName);
      param.lireRoute(fileName);
      param.lireFeu(fileName);
      param.lireTrafic(fileName);
      mCarte = new Map();
      mSimThreadCancel = false;

      mSimRunning = false;
      mSim = new Simulateur();

      R02CarteRoutiere.Instance.init();
      mCarte.clearRoad();
      // ajout des routes à la carte  
      foreach (var value in R02CarteRoutiere.Instance.calculChemin.mLesRoutes)
      {
        Road route = new Road(new Point(value.Debut.X / SimulationXML.Instance.Echelle, value.Debut.Y / SimulationXML.Instance.Echelle), new Point(value.Fin.X / SimulationXML.Instance.Echelle, value.Fin.Y / SimulationXML.Instance.Echelle));
        mCarte.addRoad(route);
      }

      //ajout des feux de circulation à la carte
      foreach (SimulationXML.FeuStructure i in param.Feu)
      {
        Feu feux = new Feu(new Point(i.CoordonneeX, i.CoordonneeY), Color.Red, i.Position);
        mCarte.addFeu(feux);
      }

      mCarte.MoveAuto(new Point(param.XDepart, param.YDepart), R07GPS.directionEnum.UNDEFINED);

      components = new System.ComponentModel.Container();
      timer1 = new System.Windows.Forms.Timer(components);
      timer1.Tick += new System.EventHandler(timer1_Tick);
      timer1.Interval = 10;
      timer1.Start();

      //MapWindow.Show();
      chercherXml_Click(new Object(),new EventArgs());

    }

    private void timer1_Tick(object sender, System.EventArgs e)
    {
      UpdateInterface();
    }

    /// <summary>
    /// Mets à jour l'interface avec les données du simulateur
    /// </summary>
    private void UpdateInterface()
    {
      // ********************* Stats in interface update ***************************
      SpeedLabel.Text = R03CompteurDeVitesse.Instance.vitesse.ToString();
      Odolabel.Text = R08Odometre.Instance.Odometre.ToString();
      Templabel.Text = R05Environnement.Instance.temperatureExterieur.ToString();
      Meteolabel.Text = R11StationMeteo.Instance.CurrentMeteo.ToString();
      Fctlabel.Text = R04ModuleAffichage.Instance.DerniereCommande.ToString();
      Publabel.Text = R04ModuleAffichage.Instance.MessagePublicCommerciaux.ToString();
      UTlabel.Text = mSim.CurrentUT.ToString();

      if (R07GPS.Instance.CurrentDirection == R07GPS.directionEnum.UNDEFINED)
          Dirlabel.Text = "LOST";
      else
          Dirlabel.Text = R07GPS.Instance.CurrentDirection.ToString();

      if (R05Environnement.Instance.EtatVehicule == R05Environnement.EtatVehiculeEnum.BRISE)
      {
          CheckEngSign.Visible = true;
          CheckEngSign_D.Visible = false;
      }
      else
      {
          CheckEngSign.Visible = false;
          CheckEngSign_D.Visible = true;
      }

      if (R03CompteurDeVitesse.Instance.frienABSEnAction != 0)
      {
          ABSsign.Visible = true;
          ABSsign_D.Visible = false;
      }
      else
      {
          ABSsign.Visible = false;
          ABSsign_D.Visible = true;
      }

      double Temp = 0, Temp2 = 0;
      mSim.ProcessorsInSim.ForEach(x=> Temp += x.Ordonnenceur.RespectedPeriodCount);
 
        double Temp1 = Temp;
      mSim.ProcessorsInSim.ForEach(x => Temp += x.Ordonnenceur.NonRespectePeriodCount);
   
        double Temp3 = Temp;
      Temp = 0;
      if (mSim.ProcessorsInSim.Count > 0)
      {
        mSim.ProcessorsInSim.Where(x => x.Ordonnenceur.ProcessorUsedCount + x.Ordonnenceur.ProcessorUnusedCount > 0).ToList().ForEach(x => Temp += (double)x.Ordonnenceur.ProcessorUsedCount / (double)(x.Ordonnenceur.ProcessorUsedCount + x.Ordonnenceur.ProcessorUnusedCount));
        Temp /= mSim.ProcessorsInSim.Count;
        //Temp is the mean :=> calculate the deviation with it
        mSim.ProcessorsInSim.Where(x => x.Ordonnenceur.ProcessorUsedCount + x.Ordonnenceur.ProcessorUnusedCount > 0).ToList().ForEach(x => Temp2 += Math.Pow(((double)x.Ordonnenceur.ProcessorUsedCount / (double)(x.Ordonnenceur.ProcessorUsedCount + x.Ordonnenceur.ProcessorUnusedCount) - Temp), 2));
        Temp2 /= mSim.ProcessorsInSim.Count;
        Temp2 = Math.Pow(Temp2, 0.5);
      }
      double MeanCharge = Temp;
      double StdDevCharge = Temp2;
      Temp = 0;
      mSim.ProcessorsInSim.ForEach(x=> x.Ordonnenceur.MaxBlockingProcessCount.ToList().ForEach(y => Temp = Math.Max(y.Value, Temp)));
      double TotalBloc = Temp;
      double MaxTimebloc = Temp;
      Temp = 0;
      mSim.ProcessorsInSim.ForEach(x=> x.Ordonnenceur.MaxBlockingProcessCount.ToList().ForEach(y => Temp += y.Value));
      double TotalTimeBloc = Temp;  

      double Pin = 20, RespCtrTmps = 20, EffParcour = 20, OptimalOper = 20, MiniArchi = 20;
      Pin = 0.2*(100.0 - R04ModuleAffichage.Instance.PointInaptitude);

      RespCtrTmps = 0.2*100*Temp1/Temp3;
   

      if (mSim.CurrentUT > 0 && (mDmi / (double)mSim.CurrentUT <= 1.0))
      { EffParcour = 0.2 * 100.0 * mDmi / (double)mSim.CurrentUT; }
      OptimalOper = 0.2 * 25.0 * (MeanCharge + (1.0 - StdDevCharge) + (TotalTimeBloc - MaxTimebloc) / TotalTimeBloc + (double)R06GES.Instance.getMinimumGESParcours() / (double)R06GES.Instance.GESTotalEmis);
      MiniArchi = 0.2 * 50.0 * ((13.0 - (double)mSim.ProcessorsInSim.Count) / 12.0 + (((double)mSim.ProcessorsInSim.Count * ((double)mSim.ProcessorsInSim.Count - 1.0) / 2.0 - (double)mSim.Network.getNbLien) / ((double)mSim.ProcessorsInSim.Count * ((double)mSim.ProcessorsInSim.Count - 1.0) / 2.0 - ((double)mSim.ProcessorsInSim.Count - 1.0))));
      // ********************* Stats in interface update ***************************
      //conducteurAffiche.Text = R05Environnement.Instance.;

      if (R07GPS.Instance.CurrentDirection != R07GPS.directionEnum.UNDEFINED)
      {
        Point prochainIntersection = R02CarteRoutiere.Instance.calculChemin.quelProchainNoeud(R07GPS.Instance.PositionVoiture, R07GPS.Instance.CurrentDirection);
        FeuIntersection FeuIntersection = R05Environnement.Instance.listFeuIntersection.Find(x => x.FeuPosition == prochainIntersection);
        string text;
        if (FeuIntersection.directionVertJaune == R07GPS.Instance.CurrentDirection)
        {
            text = FeuIntersection.EtatFeuVertJaune.ToString();
            if (text == "VERT")
            {
                FeuVertsign.Visible = true;
                FeuJaunesign.Visible = false;
                FeuRougesign.Visible = false;
                Intersectionlabel.Text = '(' + ((FeuIntersection.FeuPosition.X) / 256).ToString() + ',' + ((FeuIntersection.FeuPosition.Y) / 256).ToString() + ") " + (R07GPS.Instance.CurrentDirection.ToString()).Substring(0, 1);
            }
            else
            {
                FeuVertsign.Visible = false;
                FeuJaunesign.Visible = true;
                FeuRougesign.Visible = false;
                Intersectionlabel.Text = '(' + ((FeuIntersection.FeuPosition.X) / 256).ToString() + ',' + ((FeuIntersection.FeuPosition.Y) / 256).ToString() + ") " + (R07GPS.Instance.CurrentDirection.ToString()).Substring(0, 1);
            }
        }
        else
        {
            Intersectionlabel.Text = "RIEN";
            FeuVertsign.Visible = false;
            FeuJaunesign.Visible = false;
            FeuRougesign.Visible =true;
            Intersectionlabel.Text = '('+((FeuIntersection.FeuPosition.X) / 256).ToString() + ',' + ((FeuIntersection.FeuPosition.Y) / 256).ToString() + ") " + (R07GPS.Instance.CurrentDirection.ToString()).Substring(0, 1);

        }
       }



      // Comment lire la position de la voiture

      mCarte.MoveAuto(R07GPS.Instance.PositionVoiture, R07GPS.Instance.CurrentDirection);

      if (R05Environnement.Instance.traficAuto != null)
        mCarte.MoveTrafic(R05Environnement.Instance.traficAuto.etatTraficRoutiere);

      R07GPS.directionEnum Direction = R07GPS.Instance.CurrentDirection;

      mCarte.updateFeuColor();
      processOrderGrid.ResumeLayout(true);
      processOrderGrid.SuspendLayout();

      Carte.Refresh();
    }

    /// <summary>
    /// Demarre la simulation
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void chercherXml_Click(object sender, EventArgs e)
    {
      //String xmlFilePath = "..\\" + "sptr-scenario.xml";
      SimulationXML.Instance.lireTousLesParametres();
      //SimulationXML.Instance.lireTousLesParametres(xmlFilePath);
      //String xmlFilePath = fileName + "\\" + "sptr-scenario.xml";
      //cheminXml.Text = xmlFilePath;

      //fileContent = System.IO.File.ReadAllText(xmlFilePath); // Read the file as one string.
      //xmlAffiche.Text = fileContent;

      //Update interface parameter
      cbOrderType.SelectedItem = SimulationXML.Instance.OrderType;
    }

    /// <summary>
    /// On load page, do the action for interface
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void Form1_Load(object sender, EventArgs e)
    {
      //Populate ordering type
      foreach (Ordonnanceur.Ordonnancement aOT in Enum.GetValues(typeof(Ordonnanceur.Ordonnancement)))
      {
        if (aOT != Ordonnanceur.Ordonnancement.Undefined) { cbOrderType.Items.Add(aOT); }
      }
      cbOrderType.SelectedItem = cbOrderType.Items[0];
    }

    /// <summary>
    /// Start simulation with defined var step by step
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void simulerButton_Click(object sender, EventArgs e)
    {
      InitSim();
      mSim.ExecuteUT();
      UpdateProcessusList(mSim.CurrentUT - 1);
    }

    /// <summary>
    /// Start simulation with defined var
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void btnSimAll_Click(object sender, EventArgs e)
    {
      InitSim();
      System.Threading.ThreadPool.QueueUserWorkItem((new WaitCallback(SimulationCallWrapper)), null);
    }

    /// <summary>
    /// Thread où roule le simulateur
    /// </summary>
    /// <mXML name="pNull"></mXML>
    private void SimulationCallWrapper(object pNull)
    {
      int DelayBetweenUT = 50;
      bool aKillSignal = false;
      bool aReachedEnd = (SimulationXML.Instance.XArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.X && (SimulationXML.Instance.XArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.X &&
        (SimulationXML.Instance.YArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.Y && (SimulationXML.Instance.YArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.Y;
      lock (mSimRunningLock) { mSimRunning = true; }
      //RunUT until we reach number of planned UT or asked to stop or End of simultation
      while (mSim.CurrentUT < SimulationXML.Instance.TempsSimulation && !aKillSignal && !aReachedEnd)
      {
        mSim.ExecuteUT();
        lock (mRandObjLock) { aKillSignal = mSimThreadCancel; }
        UpdateProcessusList(mSim.CurrentUT - 1);
        aReachedEnd = (SimulationXML.Instance.XArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.X && (SimulationXML.Instance.XArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.X &&
          (SimulationXML.Instance.YArrivee - SimulationXML.Instance.Vitesse) < R07GPS.Instance.PositionVoiture.Y && (SimulationXML.Instance.YArrivee + SimulationXML.Instance.Vitesse) > R07GPS.Instance.PositionVoiture.Y;
        Thread.Sleep(DelayBetweenUT);
      }
      lock (mSimRunningLock) { mSimRunning = false; }
      if (mSim.GetProcessusByUT().Count != SimulationXML.Instance.TempsSimulation) { Console.WriteLine("Simulation did not run as many ut as expected"); }

      //Thread was killed, reset kill signal
      if (aKillSignal) { lock (mRandObjLock) { mSimThreadCancel = false; } }
    }

    /// <summary>
    /// Evenement pour arreter le simulateur à l'aide du bouton reset
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void btnResetSim_Click(object sender, EventArgs e)
    {
      KillSimulation();
      //There reinit the sim
      InitSim(true);
    }

    /// <summary>
    /// Tu la simulation
    /// </summary>
    private void KillSimulation()
    {
      bool aRet = true;
      bool aSimRunning = false;
      lock (mSimRunningLock) { aSimRunning = mSimRunning; }
      if (aSimRunning)
      {
        lock (mRandObjLock) { mSimThreadCancel = true; }
        //Loop while thread is running
        while (aRet)
        { //Read if sim updated kill signal
          lock (mRandObjLock) { aRet = mSimThreadCancel; }
          Thread.Sleep(1000);
        }
      }
    }

    /// <summary>
    /// Update processus list interface
    /// </summary>
    private void UpdateProcessusList(int pCurUt)
    {
      //Thread safe
      if (this.InvokeRequired)
      {
        this.BeginInvoke(new MethodInvoker(() => { UpdateProcessusList(pCurUt); }));
      }
      else
      {
        processOrderGrid.Rows.Add(mSim.getProcessusState(pCurUt));
        Color[] lColor = mSim.getProcessusColor(pCurUt);
        int lCount = lColor.Length;
        int lIndex = processOrderGrid.Rows.Count - 2;

        if (lIndex < 0) return; 
        for (int i = 0; i < lCount; i++)
        {
            processOrderGrid.Rows[lIndex].Cells[i].Style.BackColor = lColor[i];  
        }


      }
    }

    /// <summary>
    /// if first ut, init stuff
    /// </summary>
    private void InitSim(bool Reset = false)
    {
      if (mSim.CurrentUT == 0 || Reset)
      {
        ResourceManager.init();

        Point depart = new Point(SimulationXML.Instance.XDepart, SimulationXML.Instance.YDepart);
        List<Point> listChemin = new List<Point>();
        R02CarteRoutiere.Instance.calculChemin.CalculNouveauChemin(listChemin, depart, R07GPS.directionEnum.UNDEFINED);
        mDmi = R02CarteRoutiere.Instance.calculChemin.calculLongueurCheminEnUT(listChemin, depart);
        SimulationXML.Instance.OrderType = (Ordonnanceur.Ordonnancement)cbOrderType.SelectedItem;
        processOrderGrid.Rows.Clear();
        Ordonnanceur.InitRessourcePartagees();

        mSim = new Simulateur();
      }
    }

    /// <summary>
    /// Appele quand l'application se ferme
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void Form1_FormClosing(object sender, FormClosingEventArgs e)
    {
      KillSimulation();
    }

    /// <summary>
    /// Evenement pour redessiner la carte graphique du simulator
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void pictureCarte_Paint(object sender, PaintEventArgs e)
    {
      mCarte.DrawMap(e.Graphics);
    }

    /// <summary>
    /// Bonton pour mettre en pause le simulateur
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void btnPause_Click(object sender, EventArgs e)
    {
      KillSimulation();
    }

    /// <summary>
    /// Appele quand le picturebox change de grosseur, elle ajuste les parametres de la carte graphique en conséquence
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void pictureCarte_Resize(object sender, EventArgs e)
    {
      if (Size.Width > Size.Height) mCarte.ChangePixelRatio((int)(Size.Height / 42));
      else mCarte.ChangePixelRatio((int)(Size.Width / 42));
      Carte.Refresh();
    }

    /// <summary>
    /// Appele quand la fenetre change de grosseur, change la grosseur du picture box qui contient la carte graphique
    /// </summary>
    /// <mXML name="sender"></mXML>
    /// <mXML name="e"></mXML>
    private void Form1_Resize(object sender, EventArgs e)
    {
      Carte.Size = Size;
      pictureCarte.Size = Size;
    }



    private void processOrderGrid_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
    {
        processOrderGrid.FirstDisplayedScrollingRowIndex = processOrderGrid.RowCount - 1;
    }

    private void splitContainer3_Resize(object sender, EventArgs e)
    {
    }

    private int arrondir(double number)
    {
        double tmp = Math.Ceiling(number) - number;

        if (tmp <= 0.5)
        {
            return (int)Math.Round(number, 0);
        }
        else
        {
            return (int)Math.Ceiling(number);
        }
    }

    private void splitContainer3_Panel1_Resize(object sender, EventArgs e)
    {

        double resizeWidthFactor = 1;
        double resizeHeightFactor = 1;

        if (DashboardPanelOldSize.Width != 0)
        {
            resizeWidthFactor = (double)((Control)sender).Width / (double)DashboardPanelOldSize.Width;
        }
        if (DashboardPanelOldSize.Height != 0)
        {
            resizeHeightFactor = (double)((Control)sender).Height / (double)DashboardPanelOldSize.Height;
        }

        Templabel.Left = arrondir((double)Templabel.Left * resizeWidthFactor);
        Templabel.Top = arrondir((double)Templabel.Top * resizeHeightFactor);

        Dirlabel.Left = arrondir((double)Dirlabel.Left * resizeWidthFactor);
        Dirlabel.Top = arrondir((double)Dirlabel.Top * resizeHeightFactor);

        SpeedLabel.Left = arrondir((double)SpeedLabel.Left * resizeWidthFactor);
        SpeedLabel.Top = arrondir((double)SpeedLabel.Top * resizeHeightFactor);

        Odolabel.Left = arrondir((double)Odolabel.Left * resizeWidthFactor);
        Odolabel.Top = arrondir((double)Odolabel.Top * resizeHeightFactor);

        ABSsign.Left = arrondir((double)ABSsign.Left * resizeWidthFactor);
        ABSsign.Top = arrondir((double)ABSsign.Top * resizeHeightFactor);
        ABSsign.Width = arrondir((double)ABSsign.Width * resizeWidthFactor);
        ABSsign.Height = arrondir((double)ABSsign.Height * resizeHeightFactor);

        ABSsign_D.Left = arrondir((double)ABSsign_D.Left * resizeWidthFactor);
        ABSsign_D.Top = arrondir((double)ABSsign_D.Top * resizeHeightFactor);
        ABSsign_D.Width = arrondir((double)ABSsign_D.Width * resizeWidthFactor);
        ABSsign_D.Height = arrondir((double)ABSsign_D.Height * resizeHeightFactor);


        CheckEngSign.Left = arrondir((double)CheckEngSign.Left * resizeWidthFactor);
        CheckEngSign.Width = arrondir((double)CheckEngSign.Width * resizeWidthFactor);
        CheckEngSign.Top = arrondir((double)CheckEngSign.Top * resizeHeightFactor);
        CheckEngSign.Height = arrondir((double)CheckEngSign.Height * resizeHeightFactor);

        CheckEngSign_D.Left = arrondir((double)CheckEngSign_D.Left * resizeWidthFactor);
        CheckEngSign_D.Width = arrondir((double)CheckEngSign_D.Width * resizeWidthFactor);
        CheckEngSign_D.Top = arrondir((double)CheckEngSign_D.Top * resizeHeightFactor);
        CheckEngSign_D.Height = arrondir((double)CheckEngSign_D.Height * resizeHeightFactor);

        Meteolabel.Left = arrondir((double)Meteolabel.Left * resizeWidthFactor);
        Meteolabel.Top = arrondir((double)Meteolabel.Top * resizeHeightFactor);

        Fctlabel.Left = arrondir((double)Fctlabel.Left * resizeWidthFactor);
        Fctlabel.Top = arrondir((double)Fctlabel.Top * resizeHeightFactor);

        Publabel.Left = arrondir((double)Publabel.Left * resizeWidthFactor);
        Publabel.Top = arrondir((double)Publabel.Top * resizeHeightFactor);

        Intersectionlabel.Left = arrondir((double)Intersectionlabel.Left * resizeWidthFactor);
        Intersectionlabel.Top = arrondir((double)Intersectionlabel.Top * resizeHeightFactor);

        FeuJaunesign.Left = arrondir((double)FeuJaunesign.Left * resizeWidthFactor);
        FeuJaunesign.Top = arrondir((double)FeuJaunesign.Top * resizeHeightFactor);


        FeuVertsign.Left = arrondir((double)FeuVertsign.Left * resizeWidthFactor);
        FeuVertsign.Top = arrondir((double)FeuVertsign.Top * resizeHeightFactor);


        FeuRougesign.Left = arrondir((double)FeuRougesign.Left * resizeWidthFactor);
        FeuRougesign.Top = arrondir((double)FeuRougesign.Top * resizeHeightFactor);


        UTlabel.Left = arrondir((double)UTlabel.Left * resizeWidthFactor);
        UTlabel.Top = arrondir((double)UTlabel.Top * resizeHeightFactor);

        DashboardPanelOldSize = ((Control)sender).Size;      
 
    }

}
